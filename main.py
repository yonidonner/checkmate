#!/usr/bin/env python
import os

#from google.appengine.dist import use_library
#use_library('django', '1.2')

import webapp2
import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext import zipserve
from google.appengine.api import users
from helpers import stimuli
from handlers import util, rpc, tests, user, dataminding, lab, misc, aggregates, download_restore, statistics, reminders, api
from google.appengine.ext.webapp.util import run_wsgi_app
import robots

webapp.template.register_template_library('helpers.templatefilters')

def autoretry_datastore_timeouts(attempts=5.0, interval=0.1, exponent=2.0):
    """
    This function wraps the AppEngine Datastore API to autoretry
    datastore timeouts at the lowest accessible level.

    The benefits of this approach are:

    1. Small Footprint:  Does not monkey with Model internals
                         which may break in future releases.
    2. Max Performance:  Retrying at this lowest level means
                         serialization and key formatting is not
                         needlessly repeated on each retry.
    At initialization time, execute this:

    >>> autoretry_datastore_timeouts()

    Should only be called once, subsequent calls have no effect.

    >>> autoretry_datastore_timeouts() # no effect

    Default (5) attempts: .1, .2, .4, .8, 1.6 seconds

    Parameters can each be specified as floats.

    :param attempts: maximum number of times to retry.
    :param interval: base seconds to sleep between retries.
    :param exponent: rate of exponential back-off.
    """

    import time, logging
    from google.appengine.api import apiproxy_stub_map
    from google.appengine.runtime import apiproxy_errors
    from google.appengine.datastore import datastore_pb

    attempts = float(attempts)
    interval = float(interval)
    exponent = float(exponent)
    wrapped = apiproxy_stub_map.MakeSyncCall
    errors = {datastore_pb.Error.TIMEOUT:'Timeout',
        datastore_pb.Error.CONCURRENT_TRANSACTION:'TransactionFailedError'}

    def wrapper(*args, **kwargs):
        count = 0.0
        while True:
            try:
                return wrapped(*args, **kwargs)
            except apiproxy_errors.ApplicationError, err:
                errno = err.application_error
                if errno not in errors: raise
                sleep = (exponent ** count) * interval
                count += 1.0
                if count > attempts: raise
                msg = "Datastore %s: retry #%d in %s seconds.\n%s"
                vals = ''
                if count == 1.0:
                    vals = '\n'.join([str(a) for a in args])
                logging.warning(msg % (errors[errno], count, sleep, vals))
                time.sleep(sleep)

    setattr(wrapper, '_autoretry_datastore_timeouts', False)
    if getattr(wrapped, '_autoretry_datastore_timeouts', True):
        apiproxy_stub_map.MakeSyncCall = wrapper

autoretry_datastore_timeouts()

ROUTES = [#('/robots.*', robots.RobotsHandler),
          ('/rpc.*', rpc.RPCHandler),
          ('/signup', user.Signup),
          ('/admin', tests.AdminMenu),
          ('/admin/reset/(all)', tests.ResetDefaults),
          ('/admin/reset/(word_lists)', tests.ResetDefaults),
          ('/admin/reset/(test_variants)', tests.ResetDefaults),
          ('/admin/reset/(batteries)', tests.ResetDefaults),
          ('/admin/reset/(experiments)', tests.ResetDefaults),
          ('/admin/clean', tests.RemoveBrokenResults),
          ('/admin/log', tests.AdminLog),
          ('/admin/view_users', tests.AdminViewUsers),
          ('/admin/view_users2/(users)', tests.AdminViewUsers2),
          ('/admin/view_users2/(sessions)', tests.AdminViewUsers2),
          ('/admin/view_users2/(experiments)', tests.AdminViewUsers2),
          ('/admin/view_experiment', tests.AdminViewExperiment),
          ('/admin/test_durations', tests.TestStatistics),
          ('/admin/update_session_summaries', tests.UpdateSessionSummaries),
          ('/admin/add_autoadd_experiments', tests.AddAutoaddExperiments),
          ('/admin/create_experiments_for_old_users', tests.CreateExperimentsForOldUsers),
          ('/admin/switch_email_accounts/([0-9a-zA-Z\.]+)/([0-9a-zA-Z\.]+)', tests.AdminSwitchEmailAccounts),
          ('/admin/aggregate_data', download_restore.AggregateData),
          ('/admin/aggregate_data/([a-z\_]+)', download_restore.AggregateData),
          ('/admin/download_aggregates/(links)', download_restore.DownloadByLink),
          ('/admin/download_aggregates/([0-9]+)/([0-9a-z]+)', download_restore.DownloadByLink),
          ('/admin/download_aggregates/(links)/([0-9a-zA-Z_]+)', download_restore.DownloadByLink),
          ('/admin/download_aggregates/(links)/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)', download_restore.DownloadByLink),
          ('/admin/restore_datastore', download_restore.RestoreDatastore),
          ('/admin/restore_datastore/upload_users', download_restore.UploadUsers),
          ('/admin/restore_datastore/upload_mental_variables_and_experiments', download_restore.UploadMentalVariablesAndExperiments),
          ('/admin/restore_datastore/upload_sessions', download_restore.UploadSessions),
          ('/admin/set_usermap/([0-9a-zA-Z\._]+)', user.SetUsermap),
          ('/admin/clear_usermap', user.SetUsermap),
          ('/tasks/aggregate_datastore', download_restore.DatastoreAggregatorTask),
          ('/tasks/aggregate_sessions', aggregates.TasksAggregateSessions),
          ('/tasks/reminders', reminders.Reminders),
          ('/tasks/daily_updates', reminders.DailyUpdates),
          ('/tasks/aggregate_site_statistics', aggregates.TasksAggregateSiteStatistics),
          ('/authenticate/get_token', user.GetAuthenticationToken),
          ('/authenticate/forward_token', user.ForwardAuthenticationToken),
          ('/api/get_session_data', api.GetSessionData),
          ('/query_anonymous/([0-9]+)/([0-9a-zA-Z\._]+)', api.APIQueryAnonymous),
          ('/(query|explore)', api.APIQuery),
          ('/query/help', api.APIDoc),
          ('/(query|explore)/([0-9]+)', api.APIQuery),
          ('/(query|explore)/([0-9]+)/([0-9]+)', api.APIQuery),
          ('/(query|explore)/([0-9]+)/([0-9]+)/([0-9]+)', api.APIQuery),
          ('/adminapi/lookup_user/([0-9a-zA-Z\._]+)', api.AdminAPILookupUser),
          ('/adminapi/query/(users|sessions|sessions2)', api.AdminAPIQuery),
          ('/adminapi/query/(users|sessions|sessions2)/([0-9a-zA-Z\._\-\=]+)', api.AdminAPIQuery),
          ('/adminapi/query/(tests)/([0-9]+)', api.AdminAPIQuery),
          ('/adminapi/test_query/([0-9a-zA-Z\._]+)', api.AdminAPITestQuery),
          ('/adminapi/test_query/([0-9a-zA-Z\._]+)/([0-9a-zA-Z\._\-\=]+)', api.AdminAPITestQuery),
          ('/adminapi/get_api_key/([0-9a-zA-Z\._]+)', api.AdminAPIGetAPIKey),
          ('/adminapi/get_permissions', api.AdminAPIGetPermissions),
          ('/adminapi/(set|unset)_permissions/([0-9a-zA-Z_]+)/([0-9]+)', api.AdminAPISetPermissions),
          ('/session', lab.Lab),
          #('/session', tests.SessionCentral),
          ('/session/update-information', tests.UpdateSessionInformation),
          ('/session/ignore-last', tests.IgnoreLastResult),
          ('/session/new', tests.NewSession),
          ('/session/end', tests.EndSession),
          ('/session/results/([0-9]+)/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)/([0-9]+)', tests.TestResults),
          ('/session/new/(butter)', tests.NewSessionGenomera),
          ('/session/new/(debug)', tests.NewSessionGenomera),
          ('/session/new/([0-9a-zA-Z_\-]+)', tests.NewSession),
          ('/session/end_and_start', tests.EndAndStart),
          ('/session/end_and_start/([0-9a-zA-Z_\-]+)', tests.EndAndStart),
          ('/tests/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)', tests.TakeTest),
          ('/tests/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)/(practice)', tests.TakeTest),
          ('/anon_tests/([0-9]+)/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)', tests.TakeTestAnonymously),
          ('/anon_tests/([0-9]+)/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)/(practice)', tests.TakeTestAnonymously),
          ('/whats_my_username', statistics.WhatsMyUsername),
          ('/results', statistics.ViewResults),
          ('/(results)/view_session/([0-9]+)', statistics.ViewSession),
          ('/results/download_csv/([0-9]+)', lab.DownloadExperimentCSV),
          ('/statistics', tests.Statistics),
          ('/statistics/([0-9]+)', tests.Statistics),
          ('/admin/create_aggregates/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)', aggregates.CreateAggregateResults),
          ('/admin/create_aggregates2/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)', aggregates.CreateAggregateResults2),
          ('/admin/create_aggregates2/([0-9a-zA-Z_\-]+)', aggregates.CreateAggregateResults2),
          ('/tasks/create_user_aggregate_results', aggregates.CreateUserAggregateResults),
          ('/admin/create_aggregates', aggregates.CreateAggregateResults),
          ('/admin/download_aggregates', aggregates.DownloadAggregateResults),
          ('/admin/download_aggregates/([0-9]+)', aggregates.DownloadAggregateResults),
          ('/admin/update_aggregates/([0-9]+)', aggregates.UpdateAggregateResults),
          ('/admin/download', tests.Download),
          ('/admin/download_results/([0-9a-zA-Z]+)', tests.DownloadResults),
          ('/admin/download_user/([0-9a-zA-Z_\-]+)', tests.DownloadUser),
          ('/admin/download_user_data/([0-9a-zA-Z_\-]+)', tests.DownloadUserData),
          ('/admin/download_user_data2', aggregates.DownloadUserAggregatedResults),
          ('/admin/download_user_data2/([0-9a-zA-Z_\-]+)', aggregates.DownloadUserAggregatedResults),
          ('/admin/upload', tests.Upload),
          ('/admin/upload_user', tests.UploadUser),
          ('/admin/update/results', tests.UpdateResults),
          ('/admin/export/xml', dataminding.FullExportXML),
          ('/admin/detailed_statistics/([0-9a-zA-Z]+)', tests.AdminViewUserStatistics),
          ('/getting_started/(first_time)', misc.GettingStarted),
          ('/getting_started/(skip)', misc.GettingStarted),
          ('/getting_started', misc.GettingStarted),
          ('/settings', tests.Settings),
          ('/explain_result/([0-9a-zA-Z_\-\:]+)', lab.ExplainResult),
          ('/experiment/([0-9a-zA-Z_\-]+)', lab.ExperimentLanding),
          ('/study/experiment_8029', lab.QMIQRavensStudy),
          ('/study/experiment_8029/([0-9a-zA-Z_]+)', lab.QMIQRavensStudy),
          ('/study/doubleblinded/([0-9a-zA-Z_]+)', lab.DoubleBlindedStudy),
          ('/study/(airmov)/([0-9a-zA-Z_]+)', lab.RedirectAnonStudy),
          ('/lab', lab.Lab),
          ('/lab/delete_experiment/([0-9a-zA-Z_\-]+)', lab.DeleteExperiment),
          ('/lab/new_experiment', lab.NewExperiment),
          ('/lab/new_experiment/([0-9a-zA-Z_\-]+)', lab.NewExperiment),
          ('/lab/new_experiment/([0-9a-zA-Z_\-]+)/(customize)', lab.NewExperiment),
          ('/lab/new_experiment/([0-9a-zA-Z_\-]+)/(lp)', lab.NewExperiment),
          ('/lab/create_experiment', lab.CreateExperiment),
          ('/lab/customize_experiment/([0-9a-zA-Z_\-]+)', lab.CustomizeExperiment),
          ('/lab/customize_experiment/([0-9a-zA-Z_\-]+)/(first)', lab.CustomizeExperiment),
          ('/lab/take_tests/([0-9a-zA-Z_\-]+)', lab.TakeTests),
          ('/lab/anon_tests/([0-9a-zA-Z_\-]+)/([0-9a-zA-Z_\-]+)', lab.TakeTestsAnonymously),
          ('/lab/private_experiment', lab.NewExperiment),
          ('/lab/set_subject_id/([0-9a-zA-Z_\-]+)', lab.SetExperimentSubjectID),
          ('/lab/study_session', lab.StudySession),
          ('/lab/study_session/([0-9a-zA-Z_\-]+)', lab.AnonymousSession),
          ('/lab/end_session', lab.EndSession),
          ('/lab/cancel_session', lab.CancelSession),
          ('/lab/cancel_anon_session/([0-9]+)', lab.CancelAnonSession),
          ('/(lab)/view_session/([0-9]+)', statistics.ViewSession),
          ('/lab/update_session_variables', lab.StudySessionUpdateMVs),
          ('/lab/update_session_variables/(box)', lab.StudySessionUpdateMVs),
          ('/lab/confirm_result/([0-9a-zA-Z_\-]+)', lab.ConfirmResult),
          ('/lab/confirm_anon_result/([0-9a-zA-Z_\-]+)', lab.ConfirmAnonResult),
          ('/lab/ignore_last/([0-9a-zA-Z_\-]+)', tests.IgnoreLastResult),
          ('/lab/ignore_anon_last/([0-9a-zA-Z_\-]+)', tests.IgnoreAnonLastResult),
          ('/lab/extend_experiment/([0-9a-zA-Z_\-]+)', lab.ExtendExperiment),
          ('/stimuli/connected/radial/([0-9]+)/([0-9]+)/([0-9]+)/([0-9]+)/([0-9]+)/(true|false)', stimuli.ConnectedRadial),
          ('/unsubscribe/([0-9]+)', user.Unsubscribe),
          ('/battery/([0-9a-zA-Z_\-]+)/test/([0-9a-zA-Z_\-]+)/?', tests.Test),
          ('/my_stats', user.Stats),
          ('/dataminding', dataminding.Dataminding),
          ('/dataminding/([0-9]+)', dataminding.Dataminding),
          ('/dataminding/download_csv/([0-9]+)', dataminding.DownloadCSV),
          ('/dataminding/export/xml', dataminding.ExportXML),
          ('/login', util.Login),
          ('/logout', util.Logout),
          ('/contact', util.Contact),
          ('/clearcache', util.ClearCache),
          ('/cleanup_sessions', util.CleanupSessions),
          ('/tasks/update_aggregate_results', aggregates.UpdateAggregateResults),
          ('/tasks/run_site_backup', aggregates.RunSiteBackup),
          ('/notfound', util.NotFound),
          ('/static/images/bt128/(.*)', zipserve.make_zip_handler(
            'zips_static/bt128.zip')),
          ('/static/images/iaps/(.*)', zipserve.make_zip_handler(
            'zips_static/iaps.zip')),
          ('/static/images/bts/(.*)', zipserve.make_zip_handler(
            'zips_static/bts.zip')),
          ('/static/images/crw/(.*)', zipserve.make_zip_handler(
            'zips_static/crw.zip')),
          ('/static/images/yibaiti/(.*)', zipserve.make_zip_handler(
            'zips_static/yibaiti.zip')),
          ('/([0-9a-zA-Z_\-]+)', util.Static),
          ('/', lab.Lab),
          #('/', util.Static),
          ('.*', util.NotFound)]

if users.is_current_user_admin():
    app = webapp2.WSGIApplication(ROUTES, debug = True)
else:
    app = webapp2.WSGIApplication(ROUTES, debug = False)

# def main():
#   if users.is_current_user_admin():
#     application = webapp.WSGIApplication(ROUTES, debug=True)
#   else:
#     application = webapp.WSGIApplication(ROUTES, debug=False)
#   run_wsgi_app(application)

# if __name__ == '__main__':
#   main()
