from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.db import polymodel
import logging
import string
import datetime
import math
import util
import random
import copy
import pickle
import priors
import entities.experiment
from helpers.templatefilters import time_ms, time_flexible
try:
  from pytz.gae import pytz
except:
  pass

class PickleProperty(db.Property):
  """A property for storing complex objects in the datastore in pickled form.

  Example usage:

  >>> class PickleModel(db.Model):
  ...   data = PickleProperty()

  >>> model = PickleModel()
  >>> model.data = {"foo": "bar"}
  >>> model.data
  {'foo': 'bar'}
  >>> model.put() # doctest: +ELLIPSIS
  datastore_types.Key.from_path(u'PickleModel', ...)

  >>> model2 = PickleModel.all().get()
  >>> model2.data
  {'foo': 'bar'}
  """

  data_type = db.Blob

  def get_value_for_datastore(self, model_instance):
    value = self.__get__(model_instance, model_instance.__class__)
    if value is not None:
      return db.Blob(pickle.dumps(value))

  def make_value_from_datastore(self, value):
    if value is not None:
      return pickle.loads(str(value))

  def default_value(self):
    """If possible, copy the value passed in the default= keyword argument.
    This prevents mutable objects such as dictionaries from being shared across
    instances."""
    return copy.copy(self.default)

def translate_datetime(timezone, dt):
  if dt is None:
    return None
  if timezone:
    try:
      tz = pytz.timezone(timezone)
      return dt.replace(tzinfo = pytz.timezone("UTC")).astimezone(tz)
    except:
      logging.info("Error in datetime translate with timezone %s and dt %s"%(str(timezone), str(dt)))
  UTC = pytz.timezone("UTC")
  return dt.replace(tzinfo = UTC).astimezone(UTC)

class UserMap(db.Model):
  from_username = db.StringProperty()
  to_username = db.StringProperty()

class User(db.Model):
  username = db.StringProperty()
  timezone = db.StringProperty()
  age = db.IntegerProperty()
  gender = db.StringProperty()
  education = db.StringProperty()
  google_account = db.UserProperty(auto_current_user_add=True)
  create_date = db.DateTimeProperty(auto_now_add=True)
  #is_restricted = db.BooleanProperty(default=False)
  #participant_codes = db.ListProperty(str, default =[])
  success_message = db.StringProperty()
  error_message = db.StringProperty()
  first_time = db.BooleanProperty(default=True)
  migration = db.IntegerProperty(default=0)
  receive_emails = db.BooleanProperty(default=True)
  authentication_token = db.StringProperty()
  last_activity = db.DateTimeProperty()

  def __init__(self, *args, **kwargs):
    super(User, self).__init__(*args, **kwargs)

  def has_results(self):
    finished_session = Session2.all().filter('user', self).filter('is_complete', True).get()
    if finished_session:
      return True
    else:
      return False

  def get_authentication_token(self):
    if not self.authentication_token:
      self.authentication_token = self.generate_authentication_token()
      self.put()
    return self.authentication_token

  def generate_authentication_token(self):
    return ''.join([random.choice('0123456789abcdef') for x in xrange(32)])

  def migrate1(self):
    if self.migration is None:
      self.migration = 0
    if self.migration > 0:
      return
    # get last session
    last_session = Session2.all().filter('user', self).order('-start_time').get()
    if last_session:
      battery_name = last_session.battery_name
      battery = Battery.all().filter('name', battery_name).filter('enabled', True).get()
      if battery:
        new_experiment = entities.experiment.Experiment(username = self.username, active = True, deleted = False, name = "My First Experiment", participant_code = None, description = "This experiment was created automatically from your Quantified Mind profile", battery_name = battery_name, battery_focus = battery.focus, sessions_complete = 0, total_sessions = 10, auto_add = False)
        mental_variables = []
        user_settings = UserSettings.all().filter('user', self).get()
        if user_settings and user_settings.mental_variables:
          for (mv_name, mv_joined_states) in zip(user_settings.mental_variables, user_settings.mental_states):
            mv_states = mv_joined_states.split(",")
            new_mv = entities.mental_variables.DiscreteMentalVariable(creator_username = self.username, variable_name = mv_name, variable_type = entities.mental_variables.MV_DISCRETE, add_by_default = True, states = mv_states, default_state = mv_states[0])
            new_mv.put()
            mental_variables.append(new_mv.key())
        new_experiment.mental_variables = mental_variables
        new_experiment.put()
    self.migration = 1
    self.put()

  def update_session_timezone(self):
    sessq = Session2.all().filter('user', self).filter("timezone", None)
    offset = 0
    while True:
      sessions = sessq.fetch(100, offset = offset)
      if len(sessions) == 0:
        break
      offset += len(sessions)
      for session in sessions:
        session.timezone = self.timezone
        session.put()

  def translate_datetime(self, dt):
    return translate_datetime(self.timezone, dt)

  def to_tuple(self):
    settings = UserSettings.all().filter('user', self).get()
    if settings:
      settings = settings.to_tuple()
    return (self.username, self.timezone, self.age, self.gender, self.education, self.google_account, self.create_date, self.first_time, self.migration, self.last_activity, settings)

  @classmethod
  def from_tuple(cls, tup):
    username = tup[0]
    user_by_name = cls.all().filter('username', username).get()
    if user_by_name:
      result = user_by_name
    else:
      result = cls(username = username)
    (result.timezone, result.age, result.gender, result.education, result.google_account, result.create_date, result.first_time, result.migration, result.last_activity, settings_tup) = tup[1:]
    result.put()
    if settings_tup:
      settings = UserSettings.from_tuple(result, settings_tup)
      settings.put()
    return result

  def full_results(self):
    res = []
    res.append(("nickname", "Nickname", "", self.google_account.nickname()))
    res.append(("email", "Email", "", self.google_account.email()))
    res.append(("name", "Name", "", self.username))
    res.append(("age", "Age", "years", self.age))
    res.append(("gender", "Gender", "", self.gender))
    res.append(("education", "Education", "", self.education))
    res.append(("creation_date", "Creation date", "date", self.create_date))
    res.append(("first_time", "First time", "", self.first_time))
    res.append(("migration", "Migration", "", self.migration))
    settings = UserSettings.all().filter('user', self).get()
    if settings:
      res.append(("settings", "Settings", "parent", settings.full_results()))
    return res

  @classmethod
  def is_valid_username(cls, name):
    valid_chars = set(string.letters + string.digits + "_")
    return (all([x in valid_chars for x in name]) and len(name) >= 3 and len(name) <= 20)

  @classmethod
  def get_current(cls):
    # this will change when we add support for non-google accounts, but shouldn't matter to anyone calling User.get_current()
    user = users.get_current_user()
    if not user:
      return None
    actual_user = cls.all().filter("google_account", user).get()
    if actual_user:
      um = UserMap.all().filter('from_username', actual_user.username).get()
      if um:
        actual_user = cls.all().filter("username", um.to_username).get()
    if actual_user:
      actual_user.last_activity = datetime.datetime.utcnow()
    return actual_user

  @classmethod
  def get_or_create_current(cls):
    # this will change when we add support for non-google accounts, but shouldn't matter to anyone calling User.get_current()
    user = cls.all().filter('google_account', users.get_current_user()).get()
    if not user:
      user = cls()
      # One could set defaults here:
      user.config = {}
      user.put()
    return user

  @classmethod
  def get_studies(self):
    studies = []
    for participant_code in self.participant_codes:
      study = Study.all().filter('participant_code', participant_code).get()
      if study:
        studies.append(study)
    return studies

  @classmethod
  def remove_first_time(self):
    first_time = False
    return first_time

class TestVariant(polymodel.PolyModel):
  name = db.StringProperty()
  variant = db.StringProperty()
  group = db.StringProperty()
  practice_variant = db.StringProperty()
  description = db.StringProperty()
  estimated_time = db.StringProperty()

  def __init__(self, *args, **kwargs):
    try:
      super(TestVariant, self).__init__(*args, **kwargs)
    except TypeError, e:
      self = None

  @classmethod
  def variant_to_group(cls, name, variant):
    test_variant = cls.all().filter('name', name).filter('variant', variant).get()
    if not test_variant:
      return None
    return test_variant.group

  @classmethod
  def by_name(cls, name, variant):
    return cls.all().filter("name", name).filter("variant", variant).get()

  @classmethod
  def from_entity(cls, entity):
    """Override this so we can just ignore old classes that don't exist."""
    try:
      return super(TestVariant, cls).from_entity(entity)
    except db.KindError, e:
      # Ignore it!
      #logging.debug("Ignoring bad TestVariant because: %s"%e)
      return TestVariant()

class Battery(db.Model):
  name = db.StringProperty()
  description = db.StringProperty()
  detailed_description = db.StringProperty()
  estimated_duration = db.IntegerProperty()
  max_time = db.IntegerProperty()
  permissions = db.StringProperty()
  flow = PickleProperty()
  fixed_tests = db.BooleanProperty(default=False)
  enabled = db.BooleanProperty(default=True)
  focus = db.StringProperty()

  def init(self, **kwargs):
    super(Battery, self).__init__(**kwargs)

  @classmethod
  def by_name(cls, name):
    return cls.all().filter('name', name).filter('enabled',True).get()

  def sequence_collapse(self, nodes):
    children = [self.process_flow(x) for x in nodes]
    sequence = []
    for child in children:
      if child[0] in ['sequence','and']:
        sequence.extend(self.sequence_collapse(child[1]))
      else:
        sequence.append(child)
    return sequence

  def process_flow(self, node):
    if node[0] in ["and","or","sequence"]:
      return (node[0], [self.process_flow(x) for x in node[1]])
    elif node[0] in ["test","multipart","twopart","testpart"]:
      return node
    elif node[0] == "repeat":
      return (node[0], self.process_flow(node[1]))
    elif node[0] == "sequence_collapse":
      return ("sequence", self.sequence_collapse(node[1]))
    elif node[0] == "shuffle":
      children = list(node[1])
      random.shuffle(children)
      return ("sequence", [self.process_flow(child) for child in children])
    elif node[0] == "random":
      (n_choices, choices) = node[1]
      my_choices = []
      for choice_i in xrange(n_choices):
        while True:
          new_choice = int(random.random() * len(choices))
          if new_choice not in my_choices:
            my_choices.append(new_choice)
            break
      return ("and",[self.process_flow(choices[x]) for x in my_choices])

  def new_session(self, user):
    flow = self.process_flow(self.flow)
    #logging.info("flow: %s"%str(flow))
    now = datetime.datetime.utcnow()
    if self.max_time > 0:
      timeout_time = now + datetime.timedelta(milliseconds = self.max_time)
    else:
      timeout_time = None
    session_information = SessionInformation(free_text = "")
    session_information.put()
    if user is None:
      my_user = None
      anonymous = True
      timezone = 'UTC'
    else:
      my_user = user
      anonymous = False
      timezone = user.timezone
    test_session = Session2(user = my_user, active = True, battery_name = self.name, start_time = now, timeout_time = timeout_time, flow = flow, session_information = session_information, timezone = timezone, is_anonymous = anonymous)
    test_session.put()
    return test_session

  @classmethod
  def create_default(cls):
    while True:
      battery = cls.all().filter('name', 'default').get()
      if not battery:
        break
      battery.delete()
    battery = cls(name = "default", description = "The default battery", estimated_duration = "Eternity", permissions = 'all', max_time = 3600 * 1000)
    battery.put()
    return battery

  def max_time_in_str(self):
    if self.max_time > 0:
      return str(datetime.timedelta(seconds=self.max_time/1000))
    else:
      return "None"

  def is_user_allowed(self, user):
    all_permissions = self.permissions.split(',')
    for permission in all_permissions:
      if ((permission == 'all') or (permission == user.username)):
        return True
    return False

  @classmethod
  def for_user(cls, user):
    batteries = [x for x in cls.all().filter('enabled',True) if x.is_user_allowed(user)]
    return batteries

class SessionInformation(db.Model):
  mental_variables = db.ListProperty(str, default = [])
  mental_states = db.ListProperty(str, default = [])
  free_text = db.StringProperty(default = "")

  def __init__(self, *args, **kwargs):
    super(SessionInformation, self).__init__(*args, **kwargs)

  def full_results(self):
    res = []
    for (mental_variable, mental_state) in zip(self.mental_variables, self.mental_states):
      subres = [("variable", "Variable", "", mental_variable),
                ("state", "State", "", mental_state)]
      res.append(("mental_variable", "Mental variable", "parent", subres))
    res.append(("free_text", "Free text", "", self.free_text))
    return res

  def init(self, **kwargs):
    super(SessionInformation, self).__init__(**kwargs)

  def to_tuple(self):
    return (self.free_text,self.mental_variables,self.mental_states)

  @classmethod
  def from_tuple(cls, tup):
    result = cls(free_text = tup[0])
    if len(tup) > 1:
      result.mental_variables, result.mental_states = tup[1], tup[2]
    return result

class Session(db.Model):
    user = db.ReferenceProperty(User)
    active = db.BooleanProperty(default=True)
    battery = db.ReferenceProperty(Battery)
    start_time = db.DateTimeProperty(auto_now_add = True)
    end_time = db.DateTimeProperty()
    timeout_time = db.DateTimeProperty()
    cumulative_duration = db.IntegerProperty(default=0)
    last_test_start_time = db.DateTimeProperty()
    session_information = db.ReferenceProperty(SessionInformation)
    disabled_tests = db.ListProperty(db.Key)

    @classmethod
    def get_current(cls, user):
      current_session = cls.all().filter('user', user).filter('active', True).get()
      return current_session

    def timed_out(self):
      if not self.timeout_time:
        return False
      now = datetime.datetime.utcnow()
      return (now >= self.timeout_time)

    def finish(self):
      self.active = False
      self.end_time = datetime.datetime.utcnow()
      self.cumulative_duration = sum([(finished_test.end_time - finished_test.start_time).seconds for finished_test in self.get_finished_tests()])
      self.put()

    def duration(self):
      if not self.end_time:
        return
      return (self.end_time - self.start_time).seconds

    def get_finished_tests(self):
      tests = CompletedTest.all().filter('test_session', self).fetch(1000)
      return tests

    def number_of_finished_tests(self):
      return len(self.get_finished_tests())

    def get_available_tests(self):
      all_tests = TestInBattery.all().filter('battery', self.battery).fetch(1000)
      available = [x for x in all_tests if all([db.get(dep).is_satisfied(self) for dep in x.dependencies])]
      return available

    def get_unavailable_tests(self):
      all_tests = TestInBattery.all().filter('battery', self.battery).fetch(1000)
      unavailable = []
      for test in all_tests:
        deps = [db.get(dep) for dep in test.dependencies]
        unsatisfied = [dep.denied_reason() for dep in deps if not dep.is_satisfied(self)]
        if len(unsatisfied) > 0:
          unavailable.append({'test': test, 'reason': ', '.join(unsatisfied)})
      return unavailable

class Session2(db.Model):
    user = db.ReferenceProperty(User)
    active = db.BooleanProperty(default=True)
    battery_name = db.StringProperty()
    start_time = db.DateTimeProperty(auto_now_add = True)
    end_time = db.DateTimeProperty()
    timeout_time = db.DateTimeProperty()
    cumulative_duration = db.IntegerProperty(default=0)
    last_test_start_time = db.DateTimeProperty()
    session_information = db.ReferenceProperty(SessionInformation)
    flow = PickleProperty()
    user_agent = db.StringProperty()
    timezone = db.StringProperty()
    callback_url = db.StringProperty()
    abort_url = db.StringProperty()
    n_completed_tests = db.IntegerProperty()
    total_test_time = db.IntegerProperty()
    is_complete = db.BooleanProperty(default = False)
    experiment = db.ReferenceProperty(entities.experiment.Experiment)
    is_anonymous = db.BooleanProperty(default = False)

    def __init__(self, *args, **kwargs):
      super(Session2, self).__init__(*args, **kwargs)

    def collect_observations(self, collect_all = False):
      observations = {}
      results = CompletedTest.all().filter('test_session', self).run()
      results = [result for result in results if ((result.is_practice is None) or (result.is_practice == False))]
      for result in results:
        if collect_all:
          result.collect_all = True
        result_observations = result.collect_observations()
        CompletedTest.update_observations(observations, result_observations)
      return observations

    def mental_variables(self):
      si = self.session_information
      if si is None:
        return []
      return zip(si.mental_variables, si.mental_states)

    def get_mental_variable(self, mv_name):
      si = self.session_information
      if si is None:
        return None
      for mv_i in xrange(len(si.mental_variables)):
        if si.mental_variables[mv_i] == mv_name:
          return si.mental_states[mv_i]
      return None

    def corrected_start_time(self):
      return translate_datetime(self.timezone, self.start_time)

    def corrected_end_time(self):
      return translate_datetime(self.timezone, self.end_time)

    def corrected_timeout_time(self):
      return translate_datetime(self.timezone, self.timeout_time)

    def get_experiment(self):
      experiment_key = Session2.experiment.get_value_for_datastore(self)
      if experiment_key:
        experiment = db.get(experiment_key)
      else:
        self.experiment = None
        self.put()
        experiment = None
      return experiment

    def update_test_information(self):
      if self.n_completed_tests is None:
        self.n_completed_tests = 0
      self.total_test_time = 0
      testq = CompletedTest.all().filter('test_session', self).filter('is_practice', False)
      offset = 0
      while True:
        tests = testq.fetch(1000, offset = offset)
        offset += len(tests)
        if not tests:
          break
        for test in tests:
          if (test.start_time is not None) and (test.end_time is not None):
            self.n_completed_tests += 1
            self.total_test_time += (test.end_time - test.start_time).seconds
      self.put()

    def battery(self):
      return Battery.all().filter('name', self.battery_name).get()

    def full_results(self):
      res = []
      res.append(("battery", "Battery", "", self.battery_name))
      res.append(("start_time", "Start time", "date", self.start_time))
      res.append(("end_time", "End time", "date", self.end_time))
      res.append(("cumulative_duration", "Time in tests", "seconds", self.cumulative_duration // 1000))
      res.append(("session_information", "Session information", "parent", self.session_information.full_results()))
      res.append(("user_agent", "User agent", "", self.user_agent))
      for finished_test in self.get_finished_tests():
        res.append(("finished_test", "Finished test", "parent", finished_test.full_results()))
      return res

    def to_tuple(self):
      experiment_key_id = None
      if self.experiment:
        experiment_key_id = self.experiment.key().id()
      return (self.user.username, self.active, self.battery_name, self.start_time, self.end_time, self.timeout_time, self.cumulative_duration, self.last_test_start_time, self.session_information.to_tuple(), self.flow, self.user_agent, self.timezone, self.callback_url, self.n_completed_tests, self.total_test_time, self.is_complete, experiment_key_id, [x.to_tuple() for x in self.get_finished_tests()])

    @classmethod
    def from_tuple(cls, tup, exp_dict = None):
      # exp_dict is optional, converting old key ids to Experiment objects
      result = cls()
      (username, result.active, result.battery_name, result.start_time, result.end_time, result.timeout_time, result.cumulative_duration, result.last_test_start_time, session_information_tup, result.flow, result.user_agent, result.timezone, result.callback_url, result.n_completed_tests, result.total_test_time, result.is_complete, experiment_key_id, finished_tests) = tup
      user = User.all().filter('username', username).get()
      if not user:
        return None
      result.user = user
      #battery = Battery.all().filter('name', battery_name).get()
      #if not battery:
      #  return None
      #result.battery = battery
      session_information = SessionInformation.from_tuple(session_information_tup)
      session_information.put()
      result.session_information = session_information
      if exp_dict and exp_dict.has_key(experiment_key_id):
        result.experiment = entities.experiment.Experiment.get_by_id(exp_dict[experiment_key_id])
      result.put()
      for finished_test in finished_tests:
        finished_result = CompletedTest.from_tuple(finished_test)
        if finished_result:
          finished_result.test_session = result
          finished_result.put()
        else:
          logging.info("error restoring test result %s"%str(finished_test))
      result.put()
      return result

    def is_done(self, node):
      if node[0] == "test":
        variant = TestVariant.all().filter('name', node[1][0]).filter('variant', node[1][1]).get()
        test = CompletedTest.all().filter('test_session', self).filter('variant_name', variant.name).filter('variant_variant', variant.variant).get()
        if test:
          return test.end_time
        else:
          return None
      elif (node[0] == "and"):
        ch = [self.is_done(x) for x in node[1]]
        if all([(x is not None) for x in ch]):
          return max(ch)
        else:
          return None
      elif (node[0] == "sequence"):
        last_done = None
        for x in node[1]:
          ch = self.is_done(x)
          if ch is None:
            return None
          last_done = ch
        return last_done
      elif node[0] == "twopart":
        (repeats, min_lag, max_lag, part1, part2) = node[1]
        variant1 = TestVariant.all().filter('name', part1[0]).filter('variant', part1[1]).get()
        variant2 = TestVariant.all().filter('name', part2[0]).filter('variant', part2[1]).get()
        repeat_i = 1
        last_done = None
        while True:
          second_done = CompletedTest.all().filter('test_session', self).filter('variant_name', variant2.name).filter('variant_variant', variant2.variant).filter('pair_id', repeat_i).get()
          if second_done is None:
            break
          else:
            last_done = second_done.end_time
          repeat_i += 1
        if (repeats is not None) and (repeat_i > repeats):
          return last_done
        return None
      elif (node[0] == "multipart"):
        last_done = None
        for (min_lag,max_lag,copy_state,x) in node[1]:
          variant = TestVariant.all().filter('name', x[0]).filter('variant', x[1]).get()
          test = CompletedTest.all().filter('test_session', self).filter('variant_name', variant.name).filter('variant_variant', variant.variant).get()
          if not test:
            return None
          last_done = test.end_time
        return last_done
      elif node[0] == "or":
        for x in node[1]:
          ch = self.is_done(x)
          if ch is not None:
            return ch
        return None
      elif node[0] == "repeat":
        return None

    def get_available_tests(self):
      return self.get_available_tests_flow(self.flow)

    def get_serial_tests(self):
      if self.flow[0] == "sequence" and all([x[0] in ["test","testpart"] for x in self.flow[1]]):
        test_names = [x[1] for x in self.flow[1]]
        tests = []
        last_pair_variant = {}
        for test_i in xrange(len(self.flow[1])):
          test_name = self.flow[1][test_i][1]
          #logging.info("HERE2: %s, %s"%(test_name[0], test_name[1]))
          variant = TestVariant.all().filter('name', test_name[0]).filter('variant', test_name[1]).get()
          test_type = self.flow[1][test_i][0]
          if test_type == "test":
            tests.append(variant)
          elif test_type == "testpart":
            #logging.info("HERE3b")
            part_number, pair_id = self.flow[1][test_i][1][2:]
            # try retrieving result
            ct = CompletedTest.all().filter('test_session', self).filter('variant_name', test_name[0]).filter('variant_variant', test_name[1]).filter('pair_id', pair_id).get()
            if ct is None:
              state_class = variant.get_state_class()
              if self.is_anonymous:
                new_state = state_class.get_or_create_anon(self.key().id(), variant)
                #logging.info("got state %s"%(new_state.key().id()))
              else:
                new_state = state_class.get(self.user, variant)
              new_state.pair_id = pair_id
              if part_number > 1:
                prv_variant = last_pair_variant[pair_id]
                prv_done = CompletedTest.all().filter('test_session', self).filter('variant_name', prv_variant.name).filter('variant_variant', prv_variant.variant).filter('pair_id', pair_id).get()
                #logging.info("foud prv_done: %s"%(str(prv_done)))
                new_state.parent_result = prv_done
                if self.is_anonymous:
                  new_state.parent_state = prv_variant.get_state_class().all().filter('user', None).filter('variant_name', prv_variant.name).filter('variant_variant', prv_variant.variant).filter('session_id', self.key().id()).get()
                else:
                  logging.info("parent test state: %s"%(str(prv_variant.get_state_class().get(self.user, prv_variant).__dict__)))
                  new_state.parent_state = prv_variant.get_state_class().get(self.user, prv_variant)
              else:
                new_state.parent_state = None
              new_state.put()
              #logging.info("parent state: %s"%str(new_state.__dict__))
            last_pair_variant[pair_id] = variant
            tests.append(variant)
        #tests = [TestVariant.all().filter('name', name[0]).filter('variant', name[1]).get() for name in test_names]
        return tests
      return None

    def get_available_tests_flow(self, node, in_repeat = False):
      if (not in_repeat) and (self.is_done(node) is not None):
        return []
      if node[0] == "test":
        variant = TestVariant.all().filter('name', node[1][0]).filter('variant', node[1][1]).get()
        return [variant]
      elif node[0] == "and":
        tests = []
        for ch in node[1]:
          if self.is_done(ch) is None:
            tests.extend(self.get_available_tests_flow(ch))
        return tests
      elif node[0] == "or":
        # since this node is not done, no children are done either, so return all
        return reduce(lambda x,y: x+y, [self.get_available_tests_flow(ch) for ch in node[1]])
      elif node[0] == "sequence":
        for x in node[1]:
          ch = self.is_done(x)
          if ch is None:
            return self.get_available_tests_flow(ch)
        return []
      elif node[0] == "twopart":
        (repeats, min_lag, max_lag, part1, part2) = node[1]
        variant1 = TestVariant.all().filter('name', part1[0]).filter('variant', part1[1]).get()
        variant2 = TestVariant.all().filter('name', part2[0]).filter('variant', part2[1]).get()
        repeat_i = 1
        while True:
          second_done = CompletedTest.all().filter('test_session', self).filter('variant_name', variant2.name).filter('variant_variant', variant2.variant).filter('pair_id', repeat_i).get()
          if second_done is None:
            break
          repeat_i += 1
        if (repeats is not None) and (repeat_i > repeats):
          return []
        # check if the first part was done
        first_done = CompletedTest.all().filter('test_session', self).filter('variant_name', variant1.name).filter('variant_variant', variant1.variant).filter('pair_id', repeat_i).get()
        if first_done is None:
          # first part was not done, so give first part
          new_state = variant1.get_state_class().get(self.user, variant1)
          new_state.pair_id = repeat_i
          new_state.put()
          logging.info("updated state for %s,%s"%(variant1.name, variant1.variant))
          return [variant1]
        else:
          # did first part, give second, but only if the time is right
          time_ok = True
          if min_lag > 0:
            min_time = first_done.end_time + datetime.timedelta(seconds = min_lag)
            if datetime.datetime.utcnow() < min_time:
              time_ok = False
            if max_lag > 0:
              max_time = first_done.end_time + datetime.timedelta(seconds = max_lag)
              if datetime.datetime.utcnow() > max_time:
                time_ok = False
          if time_ok:
            new_state = variant2.get_state_class().get(self.user, variant2)
            new_state.pair_id = repeat_i
            new_state.parent_result = first_done
            new_state.parent_state = variant1.get_state_class().get(self.user, variant1)
            new_state.put()
            return [variant2]
          return []
      elif node[0] == "multipart":
        prv_done = None
        prv_state = None
        for (min_lag,max_lag,copy_state,x) in node[1]:
          variant = TestVariant.all().filter('name', x[0]).filter('variant', x[1]).get()
          test = CompletedTest.all().filter('test_session', self).filter('variant_name', variant.name).filter('variant_variant', variant.variant).get()
          if test:
            prv_result = test
            prv_done = test.end_time
            prv_state = variant.get_state_class().get(self.user, variant)
          else:
            time_ok = True
            if min_lag > 0:
              min_time = prv_done + datetime.timedelta(seconds = min_lag)
              if datetime.datetime.utcnow() < min_time:
                time_ok = False
            if max_lag > 0:
              max_time = prv_done + datetime.timedelta(seconds = max_lag)
              if datetime.datetime.utcnow() > max_time:
                time_ok = False
            if time_ok:
              if copy_state:
                new_state = variant.get_state_class().get(self.user, variant)
                if new_state:
                  new_state.parent_result = prv_result
                  new_state.parent_state = prv_state
                  new_state.put()
              return [variant]
            else:
              return []
      elif node[0] == "repeat":
        return self.get_available_tests_flow(node[1], True)

    @classmethod
    def get_current(cls, user):
      current_session = cls.all().filter('user', user).filter('active', True).get()
      if current_session is not None:
        current_session = cls.get_by_id(current_session.key().id())
      return current_session

    def timed_out(self):
      if not self.timeout_time:
        return False
      now = datetime.datetime.utcnow()
      return (now >= self.timeout_time)

    def finish(self):
      self.active = False
      self.end_time = datetime.datetime.utcnow()
      #self.cumulative_duration = sum([(finished_test.end_time - finished_test.start_time).seconds for finished_test in self.get_finished_tests()])
      self.update_test_information()
      self.put()

    def duration(self):
      if not self.end_time:
        return
      return (self.end_time - self.start_time).seconds

    def get_finished_tests(self):
      tests = CompletedTest.all().filter('test_session', self).fetch(1000)
      tests.sort(key = lambda x: x.start_time)
      return tests

    def number_of_finished_tests(self):
      return len(self.get_finished_tests())

    def get_unavailable_tests(self):
      return []

class CompletedTest(polymodel.PolyModel):
  variant_name = db.StringProperty()
  variant_variant = db.StringProperty()
  variant_group = db.StringProperty()
  test_session = db.ReferenceProperty(Session2)
  start_time = db.DateTimeProperty()
  end_time = db.DateTimeProperty()
  is_tentative = db.BooleanProperty(default = False)
  is_practice = db.BooleanProperty(default = False)

  @classmethod
  def test_name(cls):
    return "Abstract"

  def full_group_name(self):
    variant = self.variant()
    return (variant.name, variant.group)

  def collect_observations(self):
    # observations are a dictionary: the key is a context and the value is the associated sufficient statistics
    # contexts are 4-tuples: (group_name, variable_type, context_key, linear_predictors)
    # variable_type is 0 for boolean and 1 for real number
    # context_key is a unique hashable key that identifies this context
    # linear_predictors is a tuple of context variables that will be added to the predictors, for example, level for level estimation or complexity for visual matching
    # sufficient statistics for boolean variables are (n,k) where n are total observations and k are successes
    # sufficient statistics for real number variables are (n,sumx,sumx2)
    return {}

  @classmethod
  def update_observations(cls, observations, result_observations):
    for k in result_observations.keys():
      if k == 'all':
        if not observations.has_key(k):
          observations[k] = []
        observations[k].extend(result_observations[k])
      else:
        if observations.has_key(k):
          observations[k] = [x+y for (x,y) in zip(observations[k], result_observations[k])]
        else:
          observations[k] = result_observations[k]

  def add_observation(self, observations, observation_type, y, observation_id, context_key = None, linear_predictors = (), group_name = None):
    if observation_type == 'boolean':
      observation_type = 0
    elif observation_type == 'float':
      observation_type = 1
    if group_name is None:
      group_name = (self.variant_name, self.variant_group)#self.test_name()
    k = (group_name, observation_id, observation_type, context_key, linear_predictors)
    if hasattr(self, 'collect_all'):
      if self.collect_all:
        if not observations.has_key('all'):
          observations['all'] = []
        observations['all'].append((k, y))
    if not observations.has_key(k):
      if observation_type == 0:
        observations[k] = [0,0]
      elif observation_type == 1:
        observations[k] = [0, 0.0, 0.0]
    observations[k][0] += 1
    if observation_type == 0:
      if y:
        observations[k][1] += 1
    elif observation_type == 1:
      observations[k][1] += y
      observations[k][2] += y**2

  @classmethod
  def add_boolean_observation(cls, observations, y, observation_id, context_key = None, linear_predictors = (), group_name = None):
    cls.add_observation(observations, 0, y, group_name, observation_id, context_key, linear_predictors)

  @classmethod
  def add_float_observation(cls, observations, y, observation_id, context_key = None, linear_predictors = (), group_name = None):
    cls.add_observation(observations, 1, y, group_name, observation_id, context_key, linear_predictors)

  @classmethod
  def initialize_prior(cls):
    return None

  @classmethod
  def unpack_summary_statistics(cls, ss):
    return []

  @classmethod
  def update_prior(cls, sss, username, variant_info):
    return False

  def get_username(self):
    return self.test_session.user.username

  @classmethod
  def get_prior(cls, username, variant_info):
    if variant_info is None:
      return cls.initialize_prior()
    (variant_name, variant_group) = variant_info
    prior = ResultsPrior.get_or_create(username, variant_name, variant_group)
    #if prior.prior_data is None:
    #  prior.prior_data = cls.initialize_prior()
    #  prior.put()
    return (prior.prior_data, prior.X_post)

  def get_my_prior(self):
    username = self.get_username()
    variant_name = self.variant_name
    variant_variant = self.variant_variant
    variant_group = self.variant().group
    return self.get_prior(username, (variant_name, variant_group))

  def corrected_start_time(self):
    return translate_datetime(self.test_session.timezone, self.start_time)

  def corrected_end_time(self):
    return translate_datetime(self.test_session.timezone, self.end_time)

  def update_variant_group(self):
    variant = self.variant()
    if variant is not None:
      self.variant_group = variant.group
      self.put()
      return True
    return False

  def __init__(self, variant = None, **kwargs):
    if variant is not None:
      variant_name = variant.name
      variant_variant = variant.variant
      variant_group = variant.group
      kwargs.update({'variant_name': variant_name,
                     'variant_variant': variant_variant,
                     'variant_group': variant_group})
    super(CompletedTest, self).__init__(**kwargs)

  def variant(self):
    variant = TestVariant.all().filter('name', self.variant_name).filter('variant', self.variant_variant).get()
    return variant

  def update_result(self):
    new_result = CompletedTest.from_tuple(self.to_tuple())
    new_result.put()
    db.delete(self.key())

  def full_results(self):
    res = []
    res.append(("name", "Name", "", self.variant_name))
    res.append(("variant", "Variant", "", self.variant_variant))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = []
    n = CumulativeStatistic()
    n.data = [len(results)]
    n.titles = ["N"]
    res.append(n)
    return res

  @classmethod
  def cumulative_titles(cls):
    return ["Number of sessions"]

  def to_tuple(self):
    return (self.variant_name, self.variant_variant, self.variant_group, self.start_time, self.end_time, self.is_tentative, self.is_practice)

  @classmethod
  def from_tuple(cls, tup):
    (variant_name, variant_variant, variant_group, start_time, end_time, is_tentative, is_practice) = tup[:7]
    variant = TestVariant.all().filter('name', variant_name).filter('variant', variant_variant).get()
    if not variant:
      if variant_name == 'digit_span':
        try:
          direction,modality,minposneg = variant_variant.split("-")
          variant_variant = "-".join((modality,direction,minposneg))
          variant = TestVariant.all().filter('name', variant_name).filter('variant', variant_variant).get()
          if not variant:
            return None
        except:
          return None
      else:
        return None
    result_class = variant.get_result_class()
    result = result_class.from_tuple(tup[7:])
    if not result:
      return None
    result.variant_name = variant_name
    result.variant_variant = variant_variant
    result.variant_group = variant_group
    result.start_time = start_time
    result.end_time = end_time
    result.is_tentative = is_tentative
    result.is_practice = is_practice
    return result

  @classmethod
  def make_result_anon(cls, session_id, variant, result_data):
    test_session = Session2.get_by_id(session_id)
    if not test_session:
      return
    result = cls(variant = variant, test_session = test_session)
    result.start_time = test_session.last_test_start_time
    result.end_time = datetime.datetime.utcnow()
    result.is_tentative = True
    if result_data.has_key('isPractice'):
      result.is_practice = result_data['isPractice']
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    if (result_data.has_key('anonSession') and result_data['anonSession']):
      try:
        session_id = int(result_data['sessionId'])
      except:
        return
      try:
        test_session = Session2.get_by_id(session_id)
      except:
        return
      if test_session is None:
        return
    else:
      user = User.get_current()
      if not user:
        return
      test_session = Session2.get_current(user)
      if not test_session:
        return
    result = cls(variant = variant, test_session = test_session)
    result.start_time = test_session.last_test_start_time
    result.end_time = datetime.datetime.utcnow()
    result.is_tentative = True
    if result_data.has_key('isPractice'):
      result.is_practice = result_data['isPractice']
    return result

  @classmethod
  def hide_fields(cls):
    return set([])

class TestInBattery(db.Model):
  variant = db.ReferenceProperty(TestVariant)
  battery = db.ReferenceProperty(Battery)
  group_id = db.IntegerProperty(default=-1)
  dependencies = db.ListProperty(db.Key)

  @classmethod
  def create_default(cls):
    battery = Battery.all().filter('name', 'default').get()
    tests = TestVariant.all().filter('variant', 'default').fetch(100)
    for test_variant in tests:
      test_in_battery = cls(variant = test_variant, battery = battery, dependencies = [])
      test_in_battery.put()

class TestDependency(polymodel.PolyModel):
  dep_test = db.ReferenceProperty(TestInBattery)

  def is_satisfied(self, session):
    return True

  def denied_reason(self):
    return ""

class TestFinishedDependency(TestDependency):
  finished_test = db.ReferenceProperty(TestInBattery)
  min_time = db.IntegerProperty()
  max_time = db.IntegerProperty()
  copy_state = db.BooleanProperty(default = False)
  force_one_child = db.BooleanProperty(default = False)

  def is_satisfied(self, session):
    finished_tests = session.get_finished_tests()
    finished_tests2 = [(finished_test.end_time, finished_test) for finished_test in finished_tests if finished_test.variant.key() == self.finished_test.variant.key()]
    if len(finished_tests2) == 0:
      return False
    finished_tests2.sort()
    finished_test = finished_tests2[-1][1]
    if self.force_one_child:
      parent_result = self.dep_test.variant.get_result_class().all().filter('parent_result', finished_test).get()
      if parent_result:
        return False
    if self.min_time:
      min_time = finished_test.end_time + datetime.timedelta(milliseconds = self.min_time)
    else:
      min_time = datetime.datetime.utcnow()
    if self.max_time:
      max_time = finished_test.end_time + datetime.timedelta(milliseconds = self.max_time)
    else:
      max_time = datetime.datetime.utcnow() + datetime.timedelta(minutes = 1)
    if min_time <= datetime.datetime.utcnow() < max_time:
      if self.copy_state:
        finished_state = finished_test.variant.get_state_class().get(session.user, finished_test.variant)
        dep_state = self.dep_test.variant.get_state_class().get(session.user, self.dep_test.variant)
        if dep_state:
          dep_state.parent_result = finished_test
          dep_state.parent_state = finished_state
          dep_state.put()
      return True
    return False

  def denied_reason(self):
    if self.min_time:
      if self.max_time:
        time_dep = "between %d to %d seconds "%(self.min_time / 1000, self.max_time / 1000)
      else:
        time_dep = "at least %d seconds "%(self.min_time / 1000)
    else:
      if self.max_time:
        time_dep = "at most %d seconds "%(self.max_time / 1000)
      else:
        time_dep = ""
    return "can only take %s after %s"%(time_dep, self.finished_test.variant.description)

class MaxTimesDependency(TestDependency):
  num_times = db.IntegerProperty()

  def is_satisfied(self, session):
    finished_tests = session.get_finished_tests()
    c = 0
    for finished_test in finished_tests:
      if finished_test.variant.key() == self.dep_test.variant.key():
        c += 1
    return (c < self.num_times)

  def denied_reason(self):
    if self.num_times == 1:
      s = "once"
    elif self.num_times == 2:
      s = "twice"
    else:
      s = "%d times"%self.num_times
    return "can take at most %s"%s

class CumulativeStatistic(object):
  type = "column"
  data = []
  titles = []
  error_bars = None

  def __init__(self, *args, **kwargs):
    super(CumulativeStatistic, self).__init__(*args, **kwargs)

  @classmethod
  def from_float_sss(cls, float_sss, titles, use_variance = False):
    res = cls()
    if use_variance:
      res.data = [x.variance for x in float_sss]
      res.error_bars = [x.stderr_variance for x in float_sss]
    else:
      res.data = [x.mean for x in float_sss]
      res.error_bars = [x.stderr_mean for x in float_sss]
    res.titles = titles
    return res

  @classmethod
  def from_level_sss(cls, level_sss, titles):
    res = cls()
    res.data = [x.level_estimate for x in level_sss]
    res.error_bars = [x.level_stderr for x in level_sss]
    res.titles = titles
    return res

  @classmethod
  def from_boolean_sss(cls, boolean_sss, titles):
    res = cls()
    res.data = [x.fraction_positive for x in boolean_sss]
    res.error_bars = [math.sqrt(x.fraction_positive * x.fraction_negative / x.n) for x in boolean_sss]
    res.titles = titles
    return res

class ScatterData(object):
  type = "scatter"
  X = []
  Y = []
  x_title = ""
  y_title = ""
  min_x = None
  max_x = None
  min_y = None
  max_y = None

  def __init__(self, *args, **kwargs):
    super(ScatterData, self).__init__(*args, **kwargs)

  def compute_trendline(self):
    lrss = LinearRegressionSummaryStatistic.create(self.X, self.Y)
    self.trendline_x1 = min(self.X)
    self.trendline_x2 = max(self.X)
    self.trendline_y1 = lrss.linear_a * self.trendline_x1 + lrss.linear_b
    self.trendline_y2 = lrss.linear_a * self.trendline_x2 + lrss.linear_b

def counts_to_summary(c):
  ks = c.keys()
  ks.sort()
  res = [len(ks)]
  for k in ks:
    res.extend((k, c[k][0], c[k][1]))
  return res

def summary_to_counts(ss):
  n = ss[0]
  ss = ss[1:]
  c = {}
  for i in xrange(n):
    (k, pos, neg) = ss[:3]
    c[k] = [pos, neg]
    ss = ss[3:]
  return c, ss

def sum_summaries(summaries, n):
  if n == 0:
    c = {}
    new_summaries = [None] * len(summaries)
    for i in xrange(len(summaries)):
      cts, new_summary = summary_to_counts(summaries[i])
      new_summaries[i] = new_summary
      for k in cts.keys():
        if not c.has_key(k):
          c[k] = list(cts[k])
        else:
          for j in xrange(len(cts[k])):
            c[k][j] += cts[k][j]
    return c, new_summaries
  sums = [None for i in xrange(n)]
  for summary in summaries:
    for i in xrange(n):
      if sums[i] is None:
        sums[i] = summary[i]
      else:
        sums[i] = sums[i] + summary[i]
  return sums, [x[n:] for x in summaries]

class FloatSummaryStatistic(db.Model):
  n = db.IntegerProperty()
  mean = db.FloatProperty()
  stderr_mean = db.FloatProperty()
  stderr_variance = db.FloatProperty()
  variance = db.FloatProperty()
  stdev = db.FloatProperty()
  median = db.FloatProperty()
  raw = db.ListProperty(float)

  def to_summary_statistics(self):
    n = 0
    sumX = 0.0
    sumX2 = 0.0
    if self.raw:
      for raw_res in self.raw:
        n += 1
        sumX += raw_res
        sumX2 += raw_res**2
    return (n, sumX, sumX2)

  @classmethod
  def from_summary_statistics(cls, ss, return_n = False, prior = None):
    (n, sumX, sumX2), ss = sum_summaries(ss, 3)
    mean = None
    variance = None
    stdev = None
    stderr_mean = None
    if (prior is not None) and (prior[1] > 0):
      posterior_params = priors.normal_scaled_inverse_gamma_posterior_from_ss(prior, n, sumX, sumX2)
      mle_mean, mle_variance, variance_of_mean = priors.normal_scaled_inverse_gamma_mle(posterior_params)
      mean = mle_mean
      variance = mle_variance
      stdev = math.sqrt(variance)
      stderr_mean = math.sqrt(variance_of_mean)
    else:
      if n > 0:
        mean = sumX * 1.0 / n
      if n > 1:
        variance = (n / (n - 1.0)) * ((sumX2 / float(n)) - mean*mean)
        stdev = math.sqrt(variance)
        stderr_mean = stdev / math.sqrt(n)
    if return_n:
      return (mean, stderr_mean, n), ss
    else:
      return (mean, stderr_mean), ss

  def full_results(self, field, unit):
    res = []
    res.append(('n', 'N', field, self.n))
    res.append(('mean', 'Mean', unit, self.mean))
    res.append(('median', 'Median', unit, self.median))
    res.append(('standard_deviation', 'Standard deviation', unit, self.stdev))
    res.append(('mean_with_stderr', 'Mean', unit, (self.mean, self.stderr_mean)))
    return res

  @classmethod
  def cumulative_results(cls, results):
    raw = reduce(lambda x,y:x+y, [x.raw for x in results], [])
    return cls.create(raw)

  @classmethod
  def ss_to_tuple(cls, ss):
    if ss is None:
      return None
    return ss.to_tuple()

  def to_tuple(self):
    return (self.n, self.mean, self.stderr_mean, self.variance, self.stdev, self.median, self.raw)

  @classmethod
  def from_tuple(cls, tup):
    if tup is None:
      return None
    result = cls()
    (result.n, result.mean, result.stderr_mean, result.variance, result.stdev, result.median, result.raw) = tup
    return result

  @classmethod
  def create(cls, numbers):
    ss = cls(raw = map(float,numbers))
    n = 0
    sumX = 0.0
    sumX2 = 0.0
    for number in numbers:
      n += 1
      sumX += number
      sumX2 += number*number
    ss.n = n
    if n > 0:
      mean = sumX / float(n)
      ss.mean = mean
      if n > 1:
        variance = (n / (n - 1.0)) * ((sumX2 / float(n)) - mean*mean)
        ss.variance = variance
        ss.stdev = math.sqrt(variance)
        ss.stderr_mean = ss.stdev / math.sqrt(n)
        ss.stderr_variance = ss.variance * math.sqrt(2 / float(n - 1))
        srt = sorted(ss.raw)
        if (n % 2) == 1:
          ss.median = srt[n/2]
        else:
          ss.median = 0.5*(srt[n/2-1]+srt[n/2])
    ss.put()
    return ss

  def to_html(self, ms=False):
    if self.mean:
      res = time_ms(self.mean) if ms else "%s"%int(self.mean)
      if self.stdev:
        res += " &plusmn; %s"%(time_ms(self.stdev) if ms else int(self.stdev))
    else:
      res = "n/a"
    return res

class BooleanSummaryStatistic(db.Model):
  n = db.IntegerProperty()
  k = db.IntegerProperty()
  fraction_positive = db.FloatProperty()
  fraction_negative = db.FloatProperty()

  def to_summary_statistics(self):
    return (self.n, self.k)

  @classmethod
  def from_summary_statistics(cls, ss, beta_prior = None):
    (n, k), ss = sum_summaries(ss, 2)
    if beta_prior is not None:
      (alpha, beta) = beta_prior
      n_tag = n + alpha + beta
      k_tag = k + alpha
      p = k_tag * 1.0 / n_tag
      variance = p * (1.0 - p) / n_tag
      p_se = math.sqrt(variance)
      return (p, p_se), ss
    p = None
    p_se = None
    if n > 0:
      p = k * 1.0 / n
    if n > 1:
      variance = p * (1.0 - p) / n
      if variance >= 0.0:
        p_se = math.sqrt(variance)
    return (p, p_se), ss

  def full_results(self):
    res = []
    res.append(('n', 'N', "", self.n))
    res.append(('k', 'K', "", self.k))
    res.append(('fraction_positive', 'Fraction positive', "", self.fraction_positive))
    res.append(('fraction_negative', 'Fraction negative', "", self.fraction_negative))
    return res

  @classmethod
  def cumulative_results(cls, results):
    total_n = sum([x.n for x in results])
    total_k = sum([x.k for x in results])
    return cls.create([True] * total_k + [False] * (total_n - total_k))

  @classmethod
  def ss_to_tuple(cls, ss):
    if ss is None:
      return None
    return ss.to_tuple()

  def to_tuple(self):
    return (self.n, self.k, self.fraction_positive, self.fraction_negative)

  @classmethod
  def from_tuple(cls, tup):
    if tup is None:
      return None
    result = cls()
    (result.n, result.k, result.fraction_positive, result.fraction_negative) = tup
    return result

  @classmethod
  def create(cls, raw):
    ss = cls(n = len(raw))
    ss.k = len([x for x in raw if x])
    if ss.n > 0:
      ss.fraction_positive = ss.k * 1.0 / ss.n
      ss.fraction_negative = 1.0 - ss.fraction_positive
    ss.put()
    return ss

  def to_html(self):
    res = "%d/%d"%(self.k, self.n)
    if self.n > 0:
      res += " (%d&#37;)"%(self.k*100/self.n)
    return res

class LevelSummaryStatistic(db.Model):
  positives = db.ListProperty(float)
  negatives = db.ListProperty(float)
  sigmoid_a = db.FloatProperty()
  sigmoid_b = db.FloatProperty()
  level_estimate = db.FloatProperty()
  level_stderr = db.FloatProperty()

  def __init__(self, *args, **kwargs):
    super(LevelSummaryStatistic, self).__init__(*args, **kwargs)

  def to_tuple(self):
    return (self.positives, self.negatives, self.sigmoid_a, self.sigmoid_b, self.level_estimate)

  @classmethod
  def from_tuple(cls, tup):
    result = cls()
    (result.positives, result.negatives, result.sigmoid_a, result.sigmoid_b, result.level_estimate) = tup
    return result

  @classmethod
  def create(cls, positives, negatives, a_vals, guessing_p):
    if len(positives) > 0 and len(negatives) > 0:
      estimate, se = util.bayesian_estimate_level(positives, negatives, a_vals, guessing_p)
    #if len(positives) > 2 and len(negatives) > 2:
    #  estimate, a, b = util.estimate_level(positives, negatives, target_success_probability)
    else:
      estimate, se = (None,None)
    ss = cls(positives = map(float,positives), negatives = map(float,negatives), level_estimate = estimate, level_stderr = se)
    ss.put()
    return ss

  def to_summary_statistics(self):
    c = {}
    for pos in self.positives:
      cur = c.get(pos, [0, 0])
      cur[0] += 1
      c[pos] = cur
    for neg in self.negatives:
      cur = c.get(neg, [0, 0])
      cur[1] += 1
      c[neg] = cur
    return counts_to_summary(c)

  @classmethod
  def from_summary_statistics(cls, ss, a_vals, guessing_p, prior = None):
    counts, ss = sum_summaries(ss, 0)
    positives = []
    negatives = []
    for k in counts.keys():
      positives.extend([k] * counts[k][0])
      negatives.extend([k] * counts[k][1])
    estimate, se = util.bayesian_estimate_level(positives, negatives, a_vals, guessing_p, b_prior = prior)
    return (estimate, se), ss

class LinearRegressionSummaryStatistic(db.Model):
  linear_a = db.FloatProperty()
  linear_b = db.FloatProperty()
  a_standard_error = db.FloatProperty()
  x_vals = db.ListProperty(float)
  y_vals = db.ListProperty(float)

  @classmethod
  def summary_statistics_from_xy(cls, xs, ys):
    n = 0
    sumyy = 0
    sumxy = 0
    sumxx = 0
    sumx = 0
    sumy = 0
    for i in xrange(len(xs)):
      n += 1
      sumyy += ys[i] * ys[i]
      sumxy += xs[i] * ys[i]
      sumxx += xs[i] * xs[i]
      sumx += xs[i]
      sumy += ys[i]
    return (n, sumx, sumy, sumxx, sumxy, sumyy)

  def to_summary_statistics(self):
    return self.summary_statistics_from_xy(self.x_vals, self.y_vals)

  @classmethod
  def from_summary_statistics(cls, ss):
    (n, sumx, sumy, sumxx, sumxy, sumyy), ss = sum_summaries(ss, 6)
    slope = None
    slope_se = None
    intercept = None
    intercept_se = None
    # sum [y_i-a*x_i-b]^2 = sum [y_i^2 - 2*a*x_i*y_i - 2*b*y_i + a^2*x_i^2 + 2*a*b*x_i + b^2 ] =
    # = sumyy - 2*a*sumxy - 2*b*sumy + a^2*sumxx + 2*a*b*sumx + n*b^2
    # X'X = [n sumx; sumx sumxx]
    # det[X'X] = n*sumxx-sumx*sumx; z = 1.0 / det[X'X]
    # (X'X)^-1 = z * [sumxx -sumx; -sumx n]
    cvx = n*sumxx - sumx**2
    if cvx > 0.0:
      z = 1.0 / cvx
      a = z * (n*sumxy - sumx*sumy)
      b = z * (sumxx*sumy - sumx*sumxy)
      SSE = sumyy - 2*a*sumxy - 2*b*sumy + a*a*sumxx + 2*a*b*sumx + n*b*b
      slope = a
      intercept = b
      if n >= 3:
        S2 = SSE / (n - 2.0)
        slope_se = math.sqrt(S2 * z * n)
        intercept_se = math.sqrt(S2 * z * sumxx)
    return (slope, slope_se, intercept, intercept_se), ss

  def full_results(self):
    res = []
    res.append(("slope", "Slope", "", self.linear_a))
    res.append(("intercept", "Intercept", "", self.linear_b))
    res.append(("slope_standard_error", "Standard error of the slope", "", self.a_standard_error))
    return res

  def to_tuple(self):
    return (self.linear_a, self.linear_b, self.a_standard_error, self.x_vals, self.y_vals)

  @classmethod
  def from_tuple(cls, tup):
    result = cls()
    (result.linear_a, result.linear_b, result.a_standard_error, result.x_vals, result.y_vals) = tup
    return result

  def slope_to_html(self):
    if self.linear_a:
      return "%d ms/item &plusmn; %d ms/item"%(int(self.linear_a), int(self.a_standard_error))
    return "n/a"

  @classmethod
  def create(cls, x_values, y_values):
    xs = map(float,x_values)
    ys = map(float,y_values)
    ss = cls.summary_statistics_from_xy(x_values, y_values)
    (slope, slope_se, intercept, intercept_se), ss = cls.from_summary_statistics([ss])
    ss = cls(linear_a = slope, linear_b = intercept, a_standard_error = slope_se, x_vals = xs, y_vals = ys)
    ss.put()
    return ss

class MultipleRegressionSummaryStatistic(db.Model):
  beta = db.ListProperty(float)
  beta_se = db.ListProperty(float)
  n = db.IntegerProperty()
  p = db.IntegerProperty()
  xtx = db.ListProperty(float)
  xty = db.ListProperty(float)
  yty = db.FloatProperty()

  @classmethod
  def summary_statistics_from_equations(cls, dep, indep):
    return util.summary_statistics_from_equations(dep, indep)

  def to_summary_statistics(self):
    return tuple([self.p, self.n] + self.xtx + self.xty + [self.yty])

  @classmethod
  def from_summary_statistics(cls, ss):
    if len(ss[0]) < 1:
      return None
    p = ss[0][0]
    for i in xrange(1, len(ss)):
      if ss[i][0] != p:
        return None
    ss = [x[1:] for x in ss]
    (n,), ss = sum_summaries(ss, 1)
    xtx, ss = sum_summaries(ss, p*p)
    xty, ss = sum_summaries(ss, p)
    (yty,), ss = sum_summaries(ss, 1)
    beta, beta_se = util.multiple_regression(n, xtx, xty, yty)
    return (beta, beta_se), ss

  def full_results(self):
    res = []
    if self.beta is not None:
      for i in xrange(len(self.beta)):
        v = [("value", "Value", "", self.beta[i])]
        if self.beta_se is not None:
          v.append(("standard_error", "Standard error", "", self.beta_se[i]))
        res.append(("coefficient", "Coefficient", "parent", v))
    return res

  def to_tuple(self):
    return (self.beta, self.beta_se, self.linear_a, self.n, self.p, self.xtx, self.xty, self.yty)

  @classmethod
  def from_tuple(cls, tup):
    result = cls()
    (result.beta, result.beta_se, result.linear_a, result.n, result.p, result.xtx, result.xty, result.yty) = tup
    return result

  @classmethod
  def create(cls, dep, indep):
    ss = cls.summary_statistics_from_equations(dep, indep)
    (n,p,xtx,xty,yty) = ss
    (beta, beta_se), ss = cls.from_summary_statistics([ss])
    ss = cls(beta = beta, beta_se = beta_se, n = n, p = p, xtx = xtx, xty = xty, yty = yty)
    ss.put()
    return ss

class CorrelationSummaryStatistic(db.Model):
  pearson_r = db.FloatProperty()
  fisher_z = db.FloatProperty()
  fisher_z_stderr = db.FloatProperty()
  n = db.IntegerProperty()
  sumxx = db.FloatProperty()
  sumxy = db.FloatProperty()
  sumyy = db.FloatProperty()
  sumx = db.FloatProperty()
  sumy = db.FloatProperty()

  def full_results(self):
    res = []
    res.append(("pearson_r", "Pearson's R", "", self.pearson_r))
    res.append(("f_r", "F(R)", "", self.fisher_z))
    res.append(("f_r_standard_error", "F(R) standard error", "", self.fisher_z_stderr))
    return res

  def to_tuple(self):
    return (self.pearson_r, self.fisher_z, self.fisher_z_stderr, self.n, self.sumxx, self.sumxy, self.sumyy, self.sumx, self.sumy)

  @classmethod
  def from_tuple(cls, tup):
    result = cls()
    (result.pearson_r, result.fisher_z, result.fisher_z_stderr, result.n, result.sumxx, result.sumxy, result.sumyy, result.sumx, result.sumy) = tup
    return result

  def to_summary_statistics(self):
    return (self.n, self.sumx, self.sumy, self.sumxx, self.sumxy, self.sumyy)

  @classmethod
  def from_summary_statistics(cls, ss):
    (n, sumx, sumy, sumxx, sumxy, sumyy), ss = sum_summaries(ss, 6)
    pearson_r = None
    fisher_z = None
    fisher_z_stderr = None
    if n > 0:
      pearson_r = (sumxy - sumx * sumy / n) / math.sqrt( (sumxx - sumx**2 / n) * (sumyy - sumy**2 / n))
      if abs(pearson_r) < 1.0:
        fisher_z = 0.5 * (math.log(1+pearson_r)-math.log(1-pearson_r))
      if n > 3:
        fisher_z_stderr = 1.0 / math.sqrt(n - 3)
    return (pearson_r, fisher_z, fisher_z_stderr), ss

  @classmethod
  def create(cls, x_values, y_values):
    xy_pairs = zip(map(float,x_values),map(float,y_values))
    n = 0
    sumxx = 0.0
    sumxy = 0.0
    sumyy = 0.0
    sumx = 0.0
    sumy = 0.0
    for (x,y) in xy_pairs:
      n += 1
      sumxx += x*x
      sumxy += x*y
      sumyy += y*y
      sumx += x
      sumy += y
    # mean of x is (sumx / n)
    # mean of y is (sumy / n)
    # sum of (x - mean_x)*(y - mean_y) is: sumxy-sumx*sumy/n
    # sum of (x - mean_x)*(x - mean_x) is: sumxx-sumx**2/n
    # sum of (y - mean_y)*(y - mean_y) is: sumyy-sumy**2/n
    # pearson correlation is: (sumxy-sumx*sumy/n)/sqrt((sumxx-sumx**2/n)*(sumyy-sumy**2/n))
    (pearson_r, fisher_z, fisher_z_stderr), ss = cls.from_summary_statistics([(n,sumx,sumy,sumxx,sumxy,sumyy)])
    ss = cls(n = n, sumx = sumx, sumy = sumy, sumxx = sumxx, sumxy = sumxy, sumyy = sumyy,
             pearson_r = pearson_r, fisher_z = fisher_z, fisher_z_stderr = fisher_z_stderr)
    ss.put()
    return ss

class UserTestState(polymodel.PolyModel):
  user = db.ReferenceProperty(User)
  variant_name = db.StringProperty()
  variant_variant = db.StringProperty()
  is_practice = db.BooleanProperty(default = False)
  session_id = db.IntegerProperty(default = None)

  def variant(self):
    return TestVariant.by_name(self.variant_name, self.variant_variant)

  def initialize(self):
    return

  @classmethod
  def get(cls, user, test_variant):
    state = cls.all().filter('user', user).filter('variant_name', test_variant.name).filter('variant_variant', test_variant.variant).get()
    return state

  @classmethod
  def get_or_create(cls, user, test_variant):
    state = cls.get(user, test_variant)
    if not state:
      state = cls(user = user, variant_name = test_variant.name, variant_variant = test_variant.variant)
      state.put()
    return state

  @classmethod
  def get_or_create_anon(cls, session_id, test_variant):
    state = cls.all().filter('user', None).filter('variant_name', test_variant.name).filter('variant_variant', test_variant.variant).filter('session_id', session_id).get()
    if not state:
      state = cls(user = None, variant_name = test_variant.name, variant_variant = test_variant.variant, session_id = session_id)
      state.put()
    return state

  @classmethod
  def update_state(cls, variant, state_data):
    user = User.get_current()
    if not user:
      return
    state = cls.all().filter('user', user).filter('variant_name', variant.name).filter('variant_variant', variant.variant).get()
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant)
    return state

  @classmethod
  def update_state_session(cls, session_id, variant, state_data):
    state = cls.all().filter('user', None).filter('variant_name', variant.name).filter('variant_variant', variant.variant).filter('session_id', session_id).get()
    if not state:
      state = cls(user = None, variant_name = variant.name, variant_variant = variant.variant, session_id = session_id)
    return state

class WordList(db.Model):
  list_title = db.StringProperty()
  word_list = db.ListProperty(str)

  def choose_words(self, n_words):
    words = list(self.word_list)
    used_words = set()
    new_list = []
    for word_i in xrange(n_words):
      while True:
        chosen_word_i = int(random.random() * len(words))
        if chosen_word_i not in used_words:
          break
      used_words.add(chosen_word_i)
      new_list.append(words[chosen_word_i])
    return new_list

class UserSettings(db.Model):
  user = db.ReferenceProperty(User)
  show_statistics = db.BooleanProperty(default = True)
  show_feedback = db.BooleanProperty(default = False)
  mental_variables = db.ListProperty(str)
  mental_states = db.ListProperty(str)

  def to_tuple(self):
    return (self.show_statistics, self.show_feedback, self.mental_variables, self.mental_states)

  @classmethod
  def from_tuple(cls, user, tup):
    result = cls.all().filter('user', user).get()
    if not result:
      result = cls(user = user)
    (result.show_statistics, result.show_feedback, result.mental_variables, result.mental_states) = tup
    return result

  def full_results(self):
    res = []
    res.append(("show_statistics", "Show statistics", "boolean", self.show_statistics))
    res.append(("show_feedback", "Show feedback", "boolean", self.show_feedback))
    for (variable, states) in zip(self.mental_variables, self.mental_states):
      mv = [("variable", "Variable", "", variable)]
      for state in states.split(","):
        mv.append(("state", "State", "", state))
      res.append(("mental_variable", "Mental variable", "parent", mv))
    return res

  @classmethod
  def get_or_create(cls, user):
    settings = cls.all().filter('user', user).get()
    if not settings:
      settings = cls(user = user)
      settings.add_default_state_variables()
      settings.put()
    return settings

  def mental_variables_states(self):
    return [{'id': num,
             'variable': mental_variable,
             'states': [{'id': state_id+1, 'state': state_str} for (state_id, state_str) in zip(range(len(mental_states.split(","))),mental_states.split(','))]}
            for (num, mental_variable, mental_states) in zip(range(len(self.mental_variables)),self.mental_variables, self.mental_states)]

  def add_default_state_variables(self):
    for (name, values) in [('Chocolate', ['No chocolate', 'Dark', 'White']),
                           ('Sleep last night', ['Bad', 'Average', 'Great']),
                           ('Exercise today', ['None', 'Moderate', 'Intense']),
                           ('Food today', ['None', 'Small meal', 'Medium meal', 'Large meal']),
                           ('Caffeine', ['None', 'Some', 'Much']),
                           ('Alcohol', ['None', 'Some', 'Much']),
                           ('Mood', ['Bad', 'Medium', 'Great']),
                           ('Energy', ['Low', 'Medium', 'High']),
                           ('Lunch (for Lunch Study only)', ['N/A', 'No lunch', 'Small lunch', 'Big lunch'])]:
      self.mental_variables.append(name)
      self.mental_states.append(",".join(values))

class DatamindingQuery(db.Model):
  user = db.ReferenceProperty(User)
  query_name = db.StringProperty(default = 'default')
  battery_name = db.StringProperty()
  min_date = db.DateTimeProperty()
  max_date = db.DateTimeProperty()
  circadian_min = db.TimeProperty()
  circadian_max = db.TimeProperty()
  mental_variables = db.ListProperty(str)
  mental_states = db.ListProperty(str)
  variant_description = db.StringProperty()
  compare_across = db.StringProperty()

  def __init__(self, *args, **kwargs):
    super(DatamindingQuery, self).__init__(*args, **kwargs)

  @classmethod
  def get_or_create(cls, user):
    q = cls.all().filter('user', user).filter('query_name', 'default').get()
    if not q:
      q = cls(user = user, query_name = 'default')
      q.put()
    return q

  @classmethod
  def for_user(cls, user):
    already_found = set()
    res = []
    for query in cls.all().filter('user', user):
      if not query.query_name:
        db.delete(query.key())
      if query.query_name in already_found:
        db.delete(query.key())
      else:
        already_found.add(query.query_name)
        res.append(query)
    return res

  def min_date_str(self):
    if self.min_date:
      return self.min_date.strftime("%m/%d/%Y")
    else:
      return ""

  def max_date_str(self):
    if self.max_date:
      return self.max_date.strftime("%m/%d/%Y")
    else:
      return ""

  def circadian_min_str(self):
    if self.circadian_min:
      return self.circadian_min.strftime("%H:%M")
    else:
      return ""

  def circadian_max_str(self):
    if self.circadian_max:
      return self.circadian_max.strftime("%H:%M")
    else:
      return ""

  def query_sessions(self):
    sessions = []
    sessq = Session2.all().filter('user', self.user)
    if self.min_date:
      sessq = sessq.filter('start_time >= ', self.min_date)
    if self.max_date:
      sessq = sessq.filter('start_time < ', self.max_date)
    if self.battery_name:
      #battery = Battery.all().filter('name', self.battery_name).get()
      sessq = sessq.filter('battery_name', self.battery_name)

    all_sessions = sessq.fetch(9001)
    info_keys = [Session2.session_information.get_value_for_datastore(session)
                 for session in all_sessions]
    infos = db.get(info_keys)
    for sess, si in zip(all_sessions, infos):
      start_time = sess.start_time.time()
      if self.circadian_min:
        if self.circadian_max:
          if self.circadian_max > self.circadian_min:
            if not (self.circadian_min <= start_time < self.circadian_max):
              continue
          else:
            if (self.circadian_max <= start_time < self.circadian_min):
              continue
        else:
          if self.circadian_min > start_time:
            continue
      elif self.circadian_max:
        if self.circadian_max <= start_time:
          continue
      mental_dict = dict(zip(si.mental_variables, si.mental_states))
      pass_mv = True
      for (mental_variable, mental_state) in zip(self.mental_variables, self.mental_states):
        negate = False
        if len(mental_state) > 4 and mental_state[:4] == 'NOT ':
          mental_state = mental_state[4:]
          negate = True
        if ((negate and mental_dict.get(mental_variable, None) == mental_state) or
            ((not negate) and mental_dict.get(mental_variable, None) != mental_state)):
          pass_mv = False
          break
      if not pass_mv:
        continue
      sessions.append(sess)
    sessions.sort(key = lambda sess: sess.start_time, reverse = True)
    return sessions

class Study(db.Model):
  participant_code = db.StringProperty()
  battery_name = db.StringProperty()
  description = db.StringProperty()
  enabled = db.BooleanProperty(default = False)
  mental_variables = db.ListProperty(str, default = [])
  mental_states = db.ListProperty(str, default = [])

  def __init__(self, *args, **kwargs):
    super(Study, self).__init__(*args, **kwargs)

class ResultsPrior(db.Model):
  username = db.StringProperty()
  variant_name = db.StringProperty()
  variant_group = db.StringProperty()
  prior_data = PickleProperty()
  X_post = PickleProperty()

  @classmethod
  def get_or_create(cls, username, variant_name, variant_group):
    #variant_group = TestVariant.variant_to_group(variant_name, variant_variant)
    rp = cls.all().filter('username', username).filter('variant_name', variant_name).filter('variant_group', variant_group).get()
    if not rp:
      rp = cls(username = username, variant_name = variant_name, variant_group = variant_group, prior_data = None, X_post = None)
      rp.put()
    return rp
