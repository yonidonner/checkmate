#!/usr/env python

def get_cdn_urls(request):
  host_url = request.host_url
  url_parts = host_url.split('www')
  if len(url_parts) == 1:
    return {'images':[host_url], 'static': host_url, 'scripts': host_url, 'styles':host_url}
  images = []
  i = 1
  while i <= 5:
    images.append(url_parts[0]+'images'+str(i)+url_parts[1])
    i += 1
  return {'images':images, 'static': url_parts[0]+'static'+url_parts[1], 'scripts': url_parts[0]+'scripts'+url_parts[1], 'styles':url_parts[0]+'styles'+url_parts[1]}