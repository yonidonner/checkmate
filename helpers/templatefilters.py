#from google.appengine.dist import use_library
#use_library('django', '1.2')

import logging
import datetime

from google.appengine.ext import webapp

register = webapp.template.create_template_register()

@register.filter
def time_ms(t, units_in_ms=1):
  """ 1315.4526 -> "1315ms" """
  t *= float(units_in_ms)
  return "%dms"%t

@register.filter
def time_seconds(t, units_in_ms=1):
  """
  1315.4527 -> "1.3 seconds"
  13154.527 -> "13 seconds"
  """
  t *= float(units_in_ms)
  seconds = t / 1000.0
  return "%.1f seconds"%seconds if seconds < 10 else "%d seconds"%seconds

@register.filter
def time_minutes(t, units_in_ms=1):
  """
  131545.27 -> "2.2 minutes"
  1315452.7 -> "22 minutes"
  """
  t *= float(units_in_ms)
  minutes = t / 1000.0 / 60.0
  return "%.1f minutes"%minutes if minutes < 10 else "%d minutes"%minutes

@register.filter
def time_hours(t, units_in_ms=1):
  """ 13154527. -> "3.7 hours" """
  t *= float(units_in_ms)
  hours = t / 1000.0 / 60.0 / 60.0
  return "%.1f hours"%hours if hours < 10 else "%d hours"%hours

@register.filter
def time_days(t, units_in_ms=1):
  """ 131545279 -> "1.5 days" """
  t *= float(units_in_ms)
  days = t / 1000.0 / 60.0 / 60.0 / 24.0
  return "%.1f days"%days if days < 10 else "%d days"%days

@register.filter
def time_weeks(t, units_in_ms=1):
  """ 1315452791.7 -> "2.2 weeks" """
  t *= float(units_in_ms)
  weeks = t / 1000.0 / 60.0 / 60.0 / 24.0 / 7.0
  return "%.1f weeks"%weeks if weeks < 10 else "%d weeks"%weeks

@register.filter
def time_months(t, units_in_ms=1):
  """ 13154527913. -> "5.0 months" """
  t *= float(units_in_ms)
  months = t / 1000.0 / 60.0 / 60.0 / 24.0 / 30.4368499
  return "%.1f months"%months if months < 10 else "%d months"%months

@register.filter
def time_years(t, units_in_ms=1):
  """ 131545279137 -> "4.2 years" """
  t *= float(units_in_ms)
  years = t / 1000.0 / 60.0 / 60.0 / 24.0 / 365.242199
  return "%.1f years"%years if years < 10 else "%d years"%years

@register.filter
def add_days(date, days):
  return date + datetime.timedelta(days = days)

@register.filter
def session_mental_variables(session):
  if session is None:
    return ""
  if session.session_information is None:
    return "n/a"
  si = session.session_information
  if len(si.mental_variables) == 0:
    return "None specified"
  return "<br>".join(["<strong>%s: </strong>%s\n"%(mental_variable, mental_state) for (mental_variable, mental_state) in zip(si.mental_variables, si.mental_states) if mental_state])

@register.filter
def score_format(score):
  if score is None:
    return "N/A"
  s = '%.3f'%score[0]
  if score[1] is not None:
    s += ' &plusmn; %.3f'%score[1]
  return s

@register.filter
def result_format(result):
  if result['value'] is None:
    return 'n/a'
  value = result['value']
  stderr = result['stderr']
  if result['unit'] in ['ms']:
    s = time_flexible(value)
    if stderr is not None:
      s += ' &plusmn; ' + time_flexible(stderr)
  elif result['unit'] == 'fraction':
    s = '%.1f&#37;'%(value*100)
    if stderr is not None:
      s += ' &plusmn; %.2f&#37;'%(stderr*100)
  elif result['unit'] == 'z-score':
    s = '%s%.3f'%('+' if value > 0 else '', value)
    if stderr is not None:
      s += ' &plusmn; %.3f'%stderr
  else:
    s = '%.3f'%value
    if stderr is not None:
      s += ' &plusmn; %.3f'%stderr
  return s

@register.filter
def time_flexible(t, units_in_ms=1):
  """
  131.54527137 -> "132ms"
  1315.4527137 -> "1.3 seconds"
  13154.527137 -> "13 seconds"
  131545.27137 -> "2.2 minutes"
  1315452.7137 -> "22 minutes"
  13154527.137 -> "3.7 hours"
  131545279.37 -> "1.5 days"
  1315452791.7 -> "2.2 weeks"
  13154527913. -> "5.0 months"
  131545279137 -> "4.2 years"
  """
  t *= float(units_in_ms)
  for threshold, func in reversed(zip(
    [-900190019001, 1000, 1000 * 60, 1000 * 60 * 60, 1000 * 60 * 60 * 24,
     1000 * 60 * 60 * 24 * 7, 1000 * 60 * 60 * 24 * 30.4368499,
     1000 * 60 * 60 * 24 * 365.242199],
    [time_ms, time_seconds, time_minutes, time_hours, time_days, time_weeks,
     time_months, time_years])):
    #logging.info("%s: %s %s"%(t, threshold, func))
    if t > threshold:
      return func(t)


@register.filter
def getattr(obj, args):
  """ Try to get an attribute from an object.

  Example: {% if block|getattr:"editable,True" %}

  Beware that the default is always a string, if you want this
  to return False, pass an empty second argument:
  {% if block|getattr:"editable," %}
  """
  splitargs = args.split(',')
  try:
    (attribute, default) = splitargs
  except ValueError:
    (attribute, default) = args, ''

  try:
    attr = obj.__getattribute__(attribute)
  except AttributeError:
    attr = obj.__dict__.get(attribute, default)
  except:
    attr = default

  if hasattr(attr, '__call__'):
      return attr.__call__()
  else:
      return attr
        
@register.filter
def killbrackets(obj):
  """
  Subs parens for brackets
  
  Example: {{ value|killbrackets }} for value "I am [Nevin]" will output "I am (Nevin)"
  """
  import re
  return re.sub('\[', '(', re.sub('\]', ')', obj))
  
@register.filter
def fixquotes(obj):
  """
  Subs &quot; for " and so on
  
  Example: {{ value|fixquotes }} for value "I am 'Nevin'" will output "I am &#39;Nevin&#39;"
  """
  import re
  return re.sub('\"', '&quot;', re.sub('\'', '&#39;', obj))

@register.filter
def string_contains(s, substr):
  if s.find(substr) == -1:
    return False
  return True
  
# def fixamps(obj):
#   """
#   Subs & for &amp; and so on
#   
#   Example: {{ value|fixampersands }} for value "I am & you are" will output "I am &amp; you are"
#   """
#   import re
#   return re.sub('&(?!amp;)', '&amp;', obj)

