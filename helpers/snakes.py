import random, traceback, copy

grid_shape = (16, 16)
directions = ((0,-1), (1,0), (0,1), (-1,0)) # NESW, or clockwise from 12:00

def generate_snake(length=16, p_straight=.75, p_branch=.0, p_end=.0, shape=grid_shape):
    occupied = [[0]*shape[0] for i in range(shape[1])]
    pos = start = (random.randint(0,shape[0]-1), random.randint(0, shape[1]-1))
    occupied[pos[1]][pos[0]] = 1
    orient = random.choice(movement_choices(pos, shape))
    snake = [pos, orient, [], shape]
    branch_stack = []
    cur_branch = snake[2]
    while sum(map(sum, occupied)) < length:
        if random.random() < p_branch:
            branch_stack.append([pos, orient, cur_branch])
            cur_branch.append([])
            cur_branch = cur_branch[-1]
            continue
        if random.random() < p_end and branch_stack:
            pos, orient, cur_branch = branch_stack.pop()
            continue
        pos = pos[0] + orient[0], pos[1] + orient[1]
        occupied[pos[1]][pos[0]] = 1
        choices = movement_choices(pos, shape)
        if orient in choices:
            if random.random() < p_straight:
                cur_branch.append(0)
                continue
        if orient in choices: choices.remove(orient)
        back = (orient[0]*-1, orient[1]*-1)
        if back in choices: choices.remove(back)
        cur_dir = directions.index(orient)
        orient = random.choice(choices)
        new_dir = directions.index(orient)
        turn = (new_dir - cur_dir) % 4
        if turn == 3: turn = -1
        cur_branch.append(turn)
    return snake
        
        
def print_grid(grid):
    print stringify_grid(grid)     

def print_snake(snake):
    print_grid(walk_snake(snake))

def stringify_grid(grid):
    res_list = ['X'*(len(grid[0])+2)]
    for l in grid:
        res_list.append('X'+ ''.join([' ' if n==0 else '*' for n in l]) + 'X')
    res_list.append('X'*(len(grid[0])+2))
    return '\n'.join(res_list)

def walk_snake(snake, grid=None):
    shape = snake[3]
    if not grid:
        grid = [[0]*shape[0] for i in range(shape[1])]
        return walk_snake(snake, grid)
    pos = snake[0]
    orient = snake[1]
    if pos[1] < 0 or pos[0] < 0: raise IndexError
    grid[pos[1]][pos[0]] = 1
    if not snake[2]: # no more movements left
        return grid
    elif type(snake[2][0]) == list:
        grid = walk_snake([pos, orient, snake[2][0], shape], grid)
        return walk_snake([pos, orient, snake[2][1:], shape], grid)
    else:
        pos = (pos[0] + orient[0], pos[1] + orient[1])
        cur_dir = directions.index(orient)
        cur_dir = (cur_dir + snake[2][0]) % 4 
        orient = directions[cur_dir]
        #grid[pos[1]][pos[0]] = 1
        return walk_snake([pos, orient, snake[2][1:], shape], grid)

def mutate(snake, p_stepdel=0.5):
    new_snake = copy.deepcopy(snake)
    branches = []
    unexplored_branches = [new_snake[2]]
    while unexplored_branches:
        cur_branch = unexplored_branches.pop()
        branches.append(cur_branch)
        unexplored_branches.extend(filter(lambda l: type(l)==list, cur_branch))
    steps = [sum([1 for step in branch if type(step) == int]) for branch in branches]
    total_steps = sum(steps)
    target = random.randint(0, total_steps-1)
    i = 0
    for n in range(len(branches)):
        if target > i+steps[n]:
            i += steps[n]
            continue
        j = 0
        while i < target:
            if type(branches[n][j]) == int:
                i += 1
            j += 1
        j -= 1 # the last one is the one we actually want
        break
    if random.random() < p_stepdel:
        p_stepdel = 1.
        branches[n].pop(j)
    else:
        p_stepdel = 0.
        branches[n].insert(j, 0)
    try:
        assert stringify_grid(walk_snake(snake)) != stringify_grid(walk_snake(new_snake))
    except:
        return mutate(snake, p_stepdel)
    return new_snake
    
def generate_lures(snake, count=4, mutations=1):
    mutants = []
    string_grids = [stringify_grid(walk_snake(snake))]
    for i in range(count):
        mutant = copy.deepcopy(snake)
        while stringify_grid(walk_snake(mutant)) in string_grids:
            for m in range(mutations):
                mutant = mutate(mutant)
        string_grids.append(stringify_grid(walk_snake(mutant)))
        mutants.append(mutant)
    return mutants
    
def movement_choices(pos, grid):
    choices = []
    if pos[0] > 0:              choices.append((-1,0))
    if pos[0] < grid[0]-1:      choices.append((+1,0))
    if pos[1] > 0:              choices.append((0,-1))
    if pos[1] < grid[1]-1:      choices.append((0,+1))
    return choices

def generate_all_mutations(grid_size, min_complexity, max_complexity, per_complexity):
    res = {}
    for complexity in xrange(min_complexity,max_complexity+1):
        res[complexity] = []
        for repeat_i in xrange(per_complexity):
            snake = generate_snake(length = complexity, shape=(grid_size, grid_size))
            lures = generate_lures(snake, 4, 1)
            grids = [walk_snake(lure) for lure in lures]
            res[complexity].append(grids)
    return res

def all_mutations_javascript(mutations):
    res = "snakes = new Array();\n"
    for complexity in res.keys():
        res += "snakes[%d] = new Array();\n"%complexity
        for (j,mutations) in enumerate(res[complexity]):
            res += "snakes[%d][%d] = new Array();\n"%(complexity, j)
            for (mutation_i, mutation) in enumerate(mutations):
                prefix = "snakes[%d][%d][%d]"%(complexity, j, mutation_i)
                res += "%s = new Array();\n"%prefix
                for (row_i,row) in enumerate(mutation):
                    res += "%s[%d] = new Array();\n"%(prefix, row_i)
                    for (col_i,value) in enumerate(row):
                        res += "%s[%d][%d] = %d;\n"%(prefix, row_i, col_i, value)
    return res

