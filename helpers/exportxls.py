#!/usr/bin/env python

from excel import Workbook
import models
import logging
import datetime

def createListingSpreadsheet(listings):
  """Creates spreadsheet with the listings given."""
  
  listingAttributesCustom = ['Title', 'Live URL', 'Admin URL', 'Categories']
  listingAttributesSimple = ['marketing_contact_name', 'marketing_contact_phone', 'marketing_contact_email', 'premium', 'flagged_out_of_date', 'number_of_ftes', 'revenue', 'founding_year', 'create_date', 'update_date', 'corporate_url', 'corporate_street_address', 'corporate_city', 'corporate_state', 'corporate_zip', 'corporate_phone', 'corporate_princpal_name', 'ria_dept_name', 'ria_contact_name', 'ria_contact_phone', 'ria_contact_email', 'answer1', 'answer2', 'blurb']
  w = Workbook()
  listingsheet = w.add_sheet('listings')
  
  # for a in listing.attributes.dynamic_properties():
  #   if a not in listingAttributes:
  #     listingAttributes.append(a)
     
  # draw header rows
  for index, heading in enumerate(listingAttributesCustom):
    listingsheet.write(0, index, heading)
    
  for index, heading in enumerate(listingAttributesSimple):
    listingsheet.write(0, index+len(listingAttributesCustom), heading)
    
  # draw the listing data
  for index, l in enumerate(listings):
    i = index+1
    listingsheet.write(i, 0, str(l.title))
    listingsheet.write(i, 1, 'http://www.riabiz.com/d/'+str(l.key().id()))
    listingsheet.write(i, 2, 'http://www.riabiz.com/admin/d/listing/'+str(l.key().id()))
    listingsheet.write(i, 3, ", ".join(c.title for c in l.get_categories()))

    for a in listingAttributesSimple:
      val = getattr(l, a)
      if type(val) == datetime.datetime:
        val = val.strftime('%m-%d-%Y')
      elif type(val) == long:
        val = int(val)
      elif val == None:
        val = ''
      else:
        try:
          val = str(val)
        except UnicodeEncodeError:
          val = '-could not encode value-'
      listingsheet.write(i, listingAttributesSimple.index(a)+len(listingAttributesCustom), val)
    
    # for a in l.attributes.dynamic_properties():
    #   listingsheet.write(i, listingAttributes.index(a), getattr(l.attributes, a))   
      
  return w.getStringIO()