#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
from handlers import _base
import logging
import png
import random
import math
from google.appengine.ext import webapp, db

class ConnectedRadial(_base.Handler):
  def get(self, size, radius, complexity, seed, rotation, flipped):
      try:
          seed = int(seed)
          size = int(size)
          radius = int(radius)
          complexity = int(complexity)
          rotation = int(rotation)
          if flipped == 'true':
              flipped = True
          else:
              flipped = False
          stimulus = generate_connected_radius_stimulus(radius, complexity, seed)
          stimulus = scale_up_stimulus(stimulus, size, size)
          if flipped:
              stimulus = flip_stimulus(stimulus)
          stimulus = rotate_stimulus(stimulus, rotation)
      except:
          raise
          self.response.out.write("Error generating stimulus")
          return
      w = png.Writer(len(stimulus[0]), len(stimulus), greyscale=True, bitdepth=1)
      self.response.headers['Content-Type'] = "image/png"
      w.write(self.response.out, stimulus)
      return

def scale_up_stimulus(stimulus, new_x_size, new_y_size):
    x_ratio = len(stimulus) * 1.0 / new_x_size
    y_ratio = len(stimulus[0]) * 1.0 / new_y_size
    x_map = [int(x_ratio * x) for x in xrange(new_x_size)]
    y_map = [int(y_ratio * y) for y in xrange(new_y_size)]
    res = [[stimulus[x_map[x]][y_map[y]] for y in xrange(new_y_size)] for x in xrange(new_x_size)]
    return res

def rotate_stimulus(stimulus, rotation):
    actual_rotation = 2.0*math.pi * (rotation * 1.0 / 360)
    diameter = len(stimulus[0])
    radius = (diameter - 1) / 2
    new_stimulus = [[1] * diameter for i in xrange(diameter)]
    for i in xrange(diameter):
        for j in xrange(diameter):
            x = i-radius
            y = j-radius
            r = math.sqrt(x**2+y**2)
            if x == 0:
                if y >= 0:
                    alpha = math.pi / 2
                else:
                    alpha = -math.pi / 2
            else:
                alpha = math.atan(y*1.0/x)
                if (x < 0):
                    alpha = alpha+math.pi
            new_alpha = alpha + actual_rotation
            new_x = math.cos(new_alpha) * r
            new_y = math.sin(new_alpha) * r
            new_i = int(new_x) + radius
            new_j = int(new_y) + radius
            if ((0 <= new_i < diameter) and (0 <= new_j < diameter)):
                new_stimulus[j][i] = stimulus[new_j][new_i]
    return new_stimulus

def flip_stimulus(stimulus):
    x_size = len(stimulus[0])
    res = [[stimulus[j][x_size-1-i] for i in xrange(x_size)] for j in xrange(len(stimulus))]
    return res

def generate_connected_radius_stimulus(radius, complexity, seed, allowed_symmetric = False):
    diameter = radius*2+1
    state = random.getstate()
    random.seed(seed)
    res = [[0] * diameter for i in xrange(diameter)]
    border = set([(radius, radius)])
    for i in xrange(complexity):
        (x,y) = random.choice(list(border))
        res[y][x] = 1
        border.remove((x,y))
        deltas = ([(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)])
        for (delta_x, delta_y) in deltas:
            new_x = x + delta_x
            new_y = y + delta_y
            new_z = (new_x-radius)**2 + (new_y-radius)**2
            if ((0 <= new_x < diameter) and (0 <= new_y < diameter) and (new_z < radius**2) and (res[new_y][new_x] == 0)):
                border.add((new_x, new_y))
    random.setstate(state)
    res = [[1-px for px in row] for row in res]
    if not allowed_symmetric:
        flip1 = [[res[row_i][len(res[row_i])-1-col_i] for col_i in xrange(len(res[row_i]))] for row_i in xrange(len(res))]
        flip2 = [[res[len(res)-1-row_i][col_i] for col_i in xrange(len(res[row_i]))] for row_i in xrange(len(res))]
        is_symmetric = compare_stimuli(res, flip1) or compare_stimuli(res, flip2)
        if len(res) == len(res[0]):
            flip3 = [[res[len(res)-1-col_i][len(res[row_i])-1-row_i] for col_i in xrange(len(res[row_i]))] for row_i in xrange(len(res))]
            flip4 = [[res[col_i][row_i] for col_i in xrange(len(res[row_i]))] for row_i in xrange(len(res))]
            is_symmetric = is_symmetric or compare_stimuli(res, flip3) or compare_stimuli(res, flip4)
        if is_symmetric:
            return generate_connected_radius_stimulus(radius, complexity, seed + 1, allowed_symmetric)
    return res

def compare_stimuli(stim1, stim2):
    return crop_stimulus(stim1) == crop_stimulus(stim2)

def crop_stimulus(stimulus):
    for i in xrange(len(stimulus)):
        if stimulus[i].count(0) > 0:
            top_i = i
            break
    for i in xrange(len(stimulus),0,-1):
        if stimulus[i - 1].count(0) > 0:
            bottom_i = i
            break
    for i in xrange(len(stimulus[0])):
        if [x[i] for x in stimulus].count(0) > 0:
            left_i = i
            break
    for i in xrange(len(stimulus[0]),0,-1):
        if [x[i - 1] for x in stimulus].count(0) > 0:
            right_i = i
            break
    res = [x[left_i:right_i] for x in stimulus[top_i:bottom_i]]
    return res

