# from google.appengine.dist import use_library
# use_library('django', '1.2')
# webapp_django_version = '1.2'

import os
from helpers.gaesessions import SessionMiddleware

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings' 

from django.conf import settings
_ = settings.TEMPLATE_DIRS

DEBUG = (os.environ['SERVER_SOFTWARE'].startswith('Dev') or
         'staging' in os.environ['CURRENT_VERSION_ID'])

if DEBUG:
    SERVER_HOST = 'http://localhost:8080'

    def webapp_add_wsgi_middleware(app):
      from google.appengine.ext.appstats import recording
      app = recording.appstats_wsgi_middleware(app)
      app = SessionMiddleware(app, cookie_key="aglyaWFiaXpkZXZyGAsSEUFkdmVydGlzaW5nQ2xpZW50GLBeDA")
      return app

    import re
    def appstats_normalize_path(path):
      return re.sub(r'\d+', '#', path)
         
    # Wait for all of those uber-slow requests in order to have stats on them
    appstats_LOCK_TIMEOUT = 10
else:
    SERVER_HOST = 'http://wwww.quantified-mind.com'
    
    def webapp_add_wsgi_middleware(app):
      app = SessionMiddleware(app, cookie_key="aglyaWFiaXpkZXZyGAsSEUFkdmVydGlzaW5nQ2xpZW50GLBeDA")
      return app
