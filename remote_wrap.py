import os, sys

servername = sys.argv.pop(1)
source_file = sys.argv.pop(1)

os.environ['SERVER_SOFTWARE'] = 'Development (remote_api_wrapper)/1.0'

sys.path.append(os.environ['GAE_SDK'])
import dev_appserver
dev_appserver.fix_sys_path()

from google.appengine.dist import use_library
use_library('django', '1.2')

import getpass

from google.appengine.ext.remote_api import remote_api_stub
gae_username = raw_input('Username:')
gae_password = getpass.getpass('Password:')
remote_api_stub.ConfigureRemoteApi(
    app_id = None, path = '/remote_api',
    auth_func = lambda: (gae_username, gae_password),
    servername = servername, save_cookies = True)

execfile(source_file, globals())
