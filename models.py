#!/usr/bin/env python
# encoding: utf-8
from google.appengine.ext import db
from google.appengine.api import users
import datetime
import logging
import pickle
import copy
import os
import util
from helpers import timezone

DEBUG = os.environ['SERVER_SOFTWARE'].startswith('Dev')
SESSION_EXPIRATION_TIME = datetime.timedelta(minutes=30)

class PickleProperty(db.Property):
  """A property for storing complex objects in the datastore in pickled form.

  Example usage:

  >>> class PickleModel(db.Model):
  ...   data = PickleProperty()

  >>> model = PickleModel()
  >>> model.data = {"foo": "bar"}
  >>> model.data
  {'foo': 'bar'}
  >>> model.put() # doctest: +ELLIPSIS
  datastore_types.Key.from_path(u'PickleModel', ...)

  >>> model2 = PickleModel.all().get()
  >>> model2.data
  {'foo': 'bar'}
  """

  data_type = db.Blob

  def get_value_for_datastore(self, model_instance):
    value = self.__get__(model_instance, model_instance.__class__)
    if value is not None:
      return db.Blob(pickle.dumps(value))

  def make_value_from_datastore(self, value):
    if value is not None:
      return pickle.loads(str(value))

  def default_value(self):
    """If possible, copy the value passed in the default= keyword argument.
    This prevents mutable objects such as dictionaries from being shared across
    instances."""
    return copy.copy(self.default)

def prefetch_refprops(entities, props=None, list_props=None):
  """
  entities: a list of entities to prefetch refprops for
  props: a list of properties in the form: [EntityModelName.foo, EntityModelName.bar]
  list_props: a list of list properties in the form: ['foo', 'bar'] -- use this if you have lists of keys of entities that need to be prefetched
  """
  # deal with regular refprops
  if props:
    fields = [(entity, prop) for entity in entities for prop in props]
    ref_keys = [prop.get_value_for_datastore(x) for x, prop in fields]
    ref_entities = dict((x.key(), x) for x in db.get(set(ref_keys)))
    for (entity, prop), ref_key in zip(fields, ref_keys):
      prop.__set__(entity, ref_entities[ref_key])
  # now deal with lists of keys
  if list_props:
    listref_lists = [getattr(entity, prop_name) for entity in entities for prop_name in list_props]
    dupes_listref_keys = []
    for l in listref_lists:
      dupes_listref_keys.extend(l)
    listref_keys = list(set(dupes_listref_keys))
    listref_entities = dict(zip(listref_keys, db.get(listref_keys)))
    for prop_name in list_props:
      for entity in entities:
        try:
          setattr(entity, prop_name+'_entities', [listref_entities[key] for key in getattr(entity, prop_name)])
        except:
          pass
  return entities

class ModelBase(db.Model):
  
  @classmethod
  def create_key(cls):
    return db.Key.from_path(cls.__name__, db.allocate_ids(db.Key.from_path(cls.__name__, 1), 1)[0])

  @classmethod
  def get_all(cls, keys_only=False):
    q = cls.all(keys_only=keys_only)
    holder = q.fetch(1000)
    result = holder
    while len(holder) == 1000:
      holder = q.with_cursor(q.cursor()).fetch(1000)
      result += holder
    return result

  def id(self):
    try:
      return self.key().id()
    except:
      return None

  def create_date_pst(self):
    try:
      return self.create_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def update_date_pst(self):
    try:
      return self.update_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def pub_date_pst(self):
    try:
      return self.pub_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''
      
  def expire_date_pst(self):
    try:
      return self.expire_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def set_timezone(entity):
    """sets timezone and returns entity"""
    for prop in entity.properties():
      try:
        if type(prop) == datetime.datetime:
          try:
            prop.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
          except:
            import logging
            logging.error('___________________'+prop+' date conversion failed')
      except:
        import logging
        logging.error('____________________ problem with timezone method')
    return entity

class ExpandoBase(db.Expando):

  @classmethod
  def get_all(cls):
    q = cls.all()
    holder = q.fetch(1000)
    result = holder
    while len(holder) == 1000:
      holder = q.with_cursor(q.cursor()).fetch(1000)
      result += holder
    return result

  def id(self):
    try:
      return self.key().id()
    except:
      return None

  def create_date_pst(self):
    try:
      return self.create_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def update_date_pst(self):
    try:
      return self.update_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def pub_date_pst(self):
    try:
      return self.pub_date.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
    except:
      return ''

  def set_timezone(entity):
    """sets timezone and returns entity"""
    for prop in entity.properties():
      try:
        if type(prop) == datetime.datetime:
          try:
            prop.replace(tzinfo=timezone.tzutc()).astimezone(timezone.Pacific_tzinfo())
          except:
            import logging
            logging.error('___________________'+prop+' date conversion failed')
      except:
        import logging
        logging.error('____________________ problem with timezone method')
    return entity
    
class User(ModelBase):
  username = db.StringProperty()
  name = db.StringProperty()
  google_account = db.UserProperty(auto_current_user_add=True)
  create_date = db.DateTimeProperty(auto_now_add=True)
  config = PickleProperty()
  
  @classmethod
  def get_current(cls):
    # this will change when we add suport for non-google accounts, but shouldn't matter to anyone calling User.get_current()
    return cls.all().filter('google_account', users.get_current_user()).get()
    
  @classmethod
  def get_or_create_current(cls):
    # this will change when we add suport for non-google accounts, but shouldn't matter to anyone calling User.get_current()
    user = cls.all().filter('google_account', users.get_current_user()).get()
    if not user:
      user = cls()
      # One could set defaults here:
      user.config = {}
      user.put()
    return user

class Battery(ModelBase):
  """
  Example:
  >> battery.tests = [{'title':'Simple Reaction Time','name':'simple_reaction','config':{},'blurb':'This is a special blurb!'}, ect..]
  """
  tests = PickleProperty()
  title = db.TextProperty()
  # still need to support ordered/not
  
  @classmethod
  def create_or_update_main_battery(cls): # for convenience for dev
    battery = cls.get_by_key_name('all')
    if not battery:
      battery = cls(key_name='all')
    battery.tests = []
    TESTS = [('simple_reaction', 'Simple Reaction Time'),
             ('choice_reaction', 'Choice Reaction Time'),
             ('inspection_time', 'Inspection Time'),
             ('stroop', 'Stroop'),
             ('wcs', 'Wisconsin Card Sort'),
             ('digit_symbol', 'Digit Symbol'),
             ('finger_tapping', 'Finger Tapping Speed'),
             ('digit_span', 'Digit Span'), 
             ('spatial_span', 'Spatial Span'),
             ('design_copy', 'Design Copy'),
             ('design_recognition', 'Design Recognition'),
             ('mental_rotation', 'Mental Rotation'),
             ('tova', 'TOVA'),
             ('verbal_learning', 'Verbal Learning'),
             ('verbal_paired_associates', 'Verbal Paired Associates'),
             ('simple_n_back', 'Simple N-Back'),
             ('dual_n_back', 'Dual N-Back'),
             ('jaeggi_dual_n_back', 'Jaeggi Dual N-Back'),
             ('feature_match', 'Feature Match'),
             ('visuospatial_paired_associates', 'Visuospatial Paired Associates')]

    TEST_INDEX = dict(TESTS)
    for i in TEST_INDEX:
      battery.tests.append({'title':TEST_INDEX[i],'name':i,'config':{},'blurb':'This is a special blurb about this battery\'s version of the %s test!' % TEST_INDEX[i]})
    battery.put()
    return battery

class Session(ModelBase):
  create_date = db.DateTimeProperty(auto_now_add=True)
  update_date = db.DateTimeProperty(auto_now=True)
  active = db.BooleanProperty(default=True)
  user = db.ReferenceProperty(User)
  battery = db.ReferenceProperty(Battery)
  results = PickleProperty()
  
  @classmethod
  def create(cls, battery, user, result_key=None, test_name=None):
    session = cls(user=user, battery=battery)
    if result_key and test_name:
      session.results=[{'test_name':test_name, 'key':result_key}]
    return session
  
  @classmethod
  def create_or_update(cls, battery, test_name, result_key, user):
    active_sessions = cls.all().filter('user', user).filter('active', True).fetch(1000)
    for s in active_sessions:
      if datetime.datetime.now() - s.update_date < SESSION_EXPIRATION_TIME and s.battery == battery:
        s.results.append({'test_name':test_name, 'key':result_key})
        return s.put()
      else:
        s.active = False
        s.put()
    return cls.create(battery, user, result_key, test_name).put()
  
class Result(ModelBase):
  # We don't assign the user property here because we want the 
  # collection names ([whatever]_set) to correspond to the actual result classes 
  # that we are using. See: http://code.google.com/appengine/articles/modeling.html
  create_date = db.DateTimeProperty(auto_now_add=True)
  data = PickleProperty()
  session = db.ReferenceProperty(Session)
  test_duration = db.IntegerProperty()
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = cls()
    result.user = user
    result.session = Session.create_or_update(battery, test_name, result.create_key(), user)
    result.data = result_dict
    for key in kwargs:
      setattr(result, key, kwargs[key])
    result.test_duration = result.data['endTime'] - result.data['startTime']
    return result
  
class MultiTrialResult(Result):
  n_trials = db.IntegerProperty()
  # other multi-trial-specific properties here
  mean_trial_duration = db.FloatProperty()
  stdev_trial_duration = db.FloatProperty()
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(MultiTrialResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    result.n_trials = len(result.data['trials'])
    durations = [x['endTime'] - x['startTime'] for x in result.data['trials']]
    if len(durations) > 1:
        duration_stats = util.float_statistics(durations)
        result.mean_trial_duration = duration_stats['mean']
        result.stdev_trial_duration = duration_stats['stdev']
    # assign other multi-trial-specific properties here
    return result
  
class TimedStimulusResponseResult(MultiTrialResult):
  mean_reaction_time = db.FloatProperty()
  stdev_reaction_time = db.FloatProperty()
  ready_trials = db.IntegerProperty()
  correct_trials = db.IntegerProperty()
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(TimedStimulusResponseResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign TSR-specific properties here
    reaction_times = [x['reactionTime'] for x in result.data['trials'] if x['ready'] and x['reactionTime'] is not None]
    if len(reaction_times) > 1:
        rt_stats = util.float_statistics(reaction_times)
        result.mean_reaction_time = rt_stats['mean']
        result.stdev_reaction_time = rt_stats['stdev']
    readies = [x['ready'] for x in result.data['trials']]
    result.ready_trials = readies.count(True)
    corrects = [x['responseCorrect'] for x in result.data['trials'] if x['ready']]
    result.correct_trials = corrects.count(True)
    return result
    
class ReactionTimeResult(MultiTrialResult):
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(ReactionTimeResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign TSR-specific properties here
    return result
    
class TOVAResult(MultiTrialResult):
  user = db.ReferenceProperty(User)
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(TOVAResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result
  
class ChoiceReactionResult(ReactionTimeResult):
  user = db.ReferenceProperty(User)
  # some_other_stat = db.FloatProperty()
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(ChoiceReactionResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign CRT-specific properties here
    return result

  
class SimpleReactionResult(ReactionTimeResult):
  user = db.ReferenceProperty(User)
  # some_other_other_stat = db.FloatProperty()
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(SimpleReactionResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign SRT-specific properties here
    return result


class StroopResult(TimedStimulusResponseResult):
  user = db.ReferenceProperty(User)
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(StroopResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign stroop-specific properties here
    return result


class FingerTappingResult(Result):
  user = db.ReferenceProperty(User)
  number_of_taps = db.IntegerProperty() # or something

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(FingerTappingResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # result.number_of_taps = result.data.number_of_taps or something
    return result

    
class LevelEstimationResult(TimedStimulusResponseResult):
  level = db.FloatProperty()
  success_probability = 0.5
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(LevelEstimationResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign LevelEstimation-specific properties here
    positives = [x['level'] for x in result.data['trials'] if x['ready'] and x['responseCorrect']]
    negatives = [x['level'] for x in result.data['trials'] if x['ready'] and not x['responseCorrect']]
    if len(positives) > 2 and len(negatives) > 2:
        result.level = util.estimate_level(positives, negatives, result.success_probability)
    return result

class InspectionTimeResult(LevelEstimationResult):
  user = db.ReferenceProperty(User)

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(InspectionTimeResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result

    
class DesignCopyResult(LevelEstimationResult):
  user = db.ReferenceProperty(User)

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(DesignCopyResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result

    
class DesignRecognitionResult(LevelEstimationResult):

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(DesignRecognitionResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result
    
class MentalRotationResult(LevelEstimationResult):
  user = db.ReferenceProperty(User)

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(MentalRotationResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result

    
class SpanResult(LevelEstimationResult):

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(SpanResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result
    
class DigitSpanResult(SpanResult):
  user = db.ReferenceProperty(User)

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(DigitSpanResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result

    
class SpatialSpanResult(SpanResult):
  user = db.ReferenceProperty(User)

  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(SpatialSpanResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    return result

    
class WCSResult(TimedStimulusResponseResult):
  user = db.ReferenceProperty(User)
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(WCSResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign WCS-specific properties here
    return result

    
class DigitSymbolResult(TimedStimulusResponseResult):
  user = db.ReferenceProperty(User)
  
  @classmethod
  def create(cls, battery, test_name, user, result_dict, **kwargs):
    result = super(DigitSymbolResult, cls).create(battery, test_name, user, result_dict, **kwargs)
    # assign DS-specific properties here
    return result
