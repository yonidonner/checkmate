// This should handle turning raw result data into graphs, whether Highcharts, Google Charts, or dygraphs

google.load("visualization", "1", {packages:["corechart"]});

function make_graph(test_name, title, series, continuous, add_line) {
  if(!series.length) return null;
  if (continuous && (title !== "Date") && (title !== "Time")) {
    add_line = true;
  }
  var chart_class = get_chart_class(continuous);
  var container = $('<div class="results_graph">');
  var width = 940, height = 400;
  var data = new google.visualization.DataTable();
  if(continuous) {
    var data_type = "number";
    if(title == "Date")
      data_type = 'date';
    else if(title == "Time")
      data_type = 'datetime';
    data.addColumn(data_type, title);
    for(var i in series)
      data.addColumn('number', series[i].name);
    if (add_line) {
      for(var i in series) {
        data.addColumn('number', series[i].name + " regression line");
      }
    }
    var state_num = 0;
    for(i in series) {
      var s = series[i];
      for(var j in series[i].points) {
        var row = [series[i].points[j].x];
        for(var k = 0; k < series.length; ++k)
          row.push(state_num == k ? series[i].points[j].y : null);
        if (add_line) {
          for(var k = 0; k < series.length; ++k) {
            row.push(null);
          }
        }
        data.addRow(row);
      }
      if (add_line) {
        var line_points = compute_regression_line(series[i].points);
        for (var point_i = 0; point_i < 2; ++point_i) {
          var row = [line_points[point_i*2]];
          for (var k = 0; k < series.length; ++k) {
            row.push(null);
          }
          for (var k = 0; k < series.length; ++k) {
            row.push(state_num == k ? line_points[point_i*2+1] : null);
          }
          data.addRow(row);
        }
      }
      ++state_num;
    }
    if(data_type == 'date')
      (new google.visualization.DateFormat({formatType: 'long'})).format(data, 0);
    else if(data_type == 'datetime')
      (new google.visualization.DateFormat({pattern: "hh:mm"})).format(data, 0);
  }
  else {
    data.addColumn('string', 'State');
    data.addColumn('number', 'Score');
    data.addColumn({type:'number', role:'interval'});
    data.addColumn({type:'number', role:'interval'});
    for(var i in series) {
      var bar = series[i];
      var row = [bar.name, bar.value, bar.value - bar.stderr || 0, bar.value + bar.stderr || 0];
      data.addRow(row);
    }
  }

  var options = {width: width, height: height, title: test_name + ": " + title, pointSize: 5/*,
                 min: 300, max: 400*/};
  if (add_line) {
    var colors = ["#8a56e2", "#cf56e2", "#e256ae", "#e25668", "#e28956", "#e2cf56", "#aee256", "#68e256", "#56e289", "#56e2cf", "#56aee2", "#5668e2"];
    var series_options = [];
    for (var k = 0; k < series.length; ++k) {
      series_options.push({color: colors[k % colors.length]});
    }
    for (var k = 0; k < series.length; ++k) {
      series_options.push({lineWidth: 5, pointSize: 0, visibleInLegend: false, color: colors[k % colors.length]});
    }
    options["series"] = series_options;
  }

  var chart = new chart_class(container[0]);
  chart.draw(data, options);
  return container;
}

function get_chart_class(continuous) {
  if(continuous)
    return google.visualization.ScatterChart;
  return google.visualization.ColumnChart;
}

function compute_regression_line(points) {
  var sxx = 0.0;
  var sxy = 0.0;
  var syy = 0.0;
  var sx = 0.0;
  var sy = 0.0;
  var n = 0;
  var minx = null;
  var maxx = null;
  for(var j in points) {
    var x = points[j].x;
    var y = points[j].y;
    sx += x;
    sy += y;
    sxx += x*x;
    syy += y*y;
    sxy += x*y;
    n += 1;
    if ((maxx === null) || (x > maxx)) {
      maxx = x;
    }
    if ((minx === null) || (x < minx)) {
      minx = x;
    }
  }
  var beta = (sxy - sx*sy/n) / (sxx - sx*sx/n);
  var alpha = (sy - beta * sx) / n;
  var new_x1 = minx;
  var new_x2 = maxx;
  var new_y1 = alpha + beta * new_x1;
  var new_y2 = alpha + beta * new_x2;
  return [new_x1, new_y1, new_x2, new_y2];
}
