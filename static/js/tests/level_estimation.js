// Level Estimation tests are stimulus-response tests where the level of the stimulus is dynamically adjusted in order to estimate a latent "level" of the user.
var LevelEstimation = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    $('span#level_statistic_name').text("Level");
    $('div#ready_statistic').hide();
    $('div#correct_statistic').hide();
    $('div#reaction_time_statistic').hide();
    this.levelStatistic = new LevelStatistic($('span#level_statistic'));
    //this.targetSuccessProbability = window.targetSuccessProbability;
    this.guessingProbability = 0.0;
    this.initialLevel = window.initialLevel;
    this.minPosNeg = window.minPosNeg;
    this.testTimeLimit = window.testTimeLimit;
    this.a_vals = [-0.125, -0.25, -0.5, -1.0, -2.0, -4.0];
    return this;
  },

  /*
  initConfig: function() {
    test._super();
    if (test.config.level != null) {
      test.initialLevel = test.config.level;
    } else {
      test.initialLevel = test.defaultInitialLevel || 1;
    }
  },
  */

  startTest: function() {
    test.targetSuccessProbability = 0.5 + 0.5 * test.guessingProbability;
    this.minPos = this.minPosNeg * this.targetSuccessProbability;
    this.minNeg = this.minPosNeg * (1 - this.targetSuccessProbability);
    if (test.levelStatistic) {
      test.levelStatistic.reset(test.guessingProbability, test.a_vals);
    }
    test.currentLevel = test.initialLevel;
    test.nCorrect = 0;
    test.nIncorrect = 0;
    test._super();
  },

  generateTrial: function() {
    test._super();
    test.currentTrial.level = test.currentLevel;
  },

  updateLevel: function() {
    // Back to deterministic updates!
    if (test.currentTrial.responseCorrect) {
      test.increaseLevel();
    }
    else {
      test.decreaseLevel();
    }
  },

  updateLevelProbabilistically: function() {
    // by default we have a discrete level that goes up and down
    var u = Math.random();
    if ((test.currentTrial.responseCorrect) && (u < (1 - test.targetSuccessProbability))) {
      test.increaseLevel();
    }
    else if ((!test.currentTrial.responseCorrect) && (u < test.targetSuccessProbability) && (test.currentLevel > 1)) {
      test.decreaseLevel();
    }
  },

  increaseLevel: function() {
    test.currentLevel = test.currentLevel + 1;
  },

  decreaseLevel: function() {
    if (test.currentLevel > 1) {
      test.currentLevel = test.currentLevel - 1;
    }
  },

  endTrial: function() {
    if (test.currentTrial.responseCorrect) {
      ++test.nCorrect;
    }
    else {
      ++test.nIncorrect;
    }
    test.updateLevel();
    test._super();
  },

  isTestFinished: function() {
    var currentTime = new Date().getTime();
    if ((test.testTimeLimit) && ((currentTime - test.results.startTime) > test.testTimeLimit)) {
      return true;
    }
    //console.log("nCorrect: " + test.nCorrect + " nIncorrect: " + test.nIncorrect);
    if ((test.nCorrect > test.minPos) && (test.nIncorrect > test.minNeg)) {
      return true;
    }
    else {
      return false;
    }
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.level = test.currentLevel;
    if (test.levelStatistic) {
      test.levelStatistic.setCurrent(test.currentTrial.level);
    }
  },

  updateTrialStatistics: function() {
    test._super();
    if (test.levelStatistic) {
      test.levelStatistic.add(test.currentTrial.responseCorrect);
    }
  },

  endTest: function() {
    console.log("Updating state.level to ", test.currentLevel);
    test.user_test_state.level = test.currentLevel;
    test._super();
  }

});

