// Visuospatial Paired Associates: the subject needs to associate images with spatial locations.
var VisuospatialPairedAssociates = PairedAssociates.extend({
  init: function() {
    this._super();
    this.gridRows = window.gridRows;
    this.gridCols = window.gridCols;
    this.grid = new Grid(this.gridRows, this.gridCols, 60);
    this.initImages();
    return this;
  },

  initImages: function() {
    test.allImages = new Array();
    var i;
    for (i = 0; i < 679; ++i) {
      test.allImages[i] = "/static/images/yibaiti/" + ("00000" + i).slice(-5)+".png";
    }
    /*
    for (i = 0; i < 1000; ++i) {
      test.allImages[i] = "/static/images/bts/bt_" + ("00" + i).slice(-3)+".png";
    }
    */
  },
  
  setupStimulusDisplay: function() {
    test._super();
    cueHtml = "";
    var i;
    for (i in test.currentTrial.pairs) {
      var image = test.currentTrial.pairs[i][0];
      cueHtml += "<div id=\"cue_" + i + "\"><img src=\"" + image + "\" height=\"50\" width=\"50\"></div>";
    }
    $('div#cue').html(cueHtml);
    test.hideCues();
    gridCreate(test.grid, $('div#stimulus'));
  },

  hideCues: function() {
    var i;
    for (i in test.currentTrial.pairs) {
      $('div#cue_' + i).hide();
    }
  },

  showCue: function(index) {
    test.hideCues();
    $('div#cue_' + index).show();
  },

  showPair: function(cue, associate) {
    console.log("associate: " + associate);
    var grid_row = Math.floor(associate / test.gridCols);
    var grid_col = associate % test.gridCols;
    var cell = gridGetCell(test.grid, grid_row, grid_col);
    cell.addClass("stimulusOn");
    cell.html('<img src="' + cue + '" height="30" width="30">');
    cell.css("padding", "");
  },
  
  unshowPair: function(cue, associate) {
    var grid_row = Math.floor(associate / test.gridCols);
    var grid_col = associate % test.gridCols;
    var cell = gridGetCell(test.grid, grid_row, grid_col);
    cell.removeClass("stimulusOn");
    cell.html("");
    cell.css("padding", test.grid.cellSize / 2 + "px");
  },

  generatePairs: function(level) {
    var images = randomSubset(test.allImages, level);
    var pairs = new Array();
    for (i in images) {
      pairs[i] = new Array();
      pairs[i][0] = images[i];
      pairs[i][1] = Math.floor(Math.random() * test.gridRows * test.gridCols);
    }
    return pairs;
  },

  setupInput: function() {
    test.currentInput = -1;
    test.setupNextInput();
  },

  setupNextInput: function() {
    if (++test.currentInput === test.currentTrial.level) {
      removeGrid(test.grid);
      console.log("user choice: " + test.currentTrial.userChoice);
      test.handleResponse();
    }
    else {
      test.showCue(test.currentInput);
      gridSetupRead(test.grid, $('div#stimulus'), test.gridClicked, "sequence_element");
    }
  },

  gridClicked: function(grid, row, col) {
    test.currentTrial.userChoice[test.currentInput] = row * test.gridCols + col;
    if (test.currentTrial.userChoice[test.currentInput] !== test.currentTrial.pairs[test.currentInput][1]) {
      test.currentInput = test.currentTrial.level - 1;
    }
    test.setupNextInput();
  },

  endTrial: function() {
    $('div#cue').html("");
    test._super();
  },

});

var test = new VisuospatialPairedAssociates();