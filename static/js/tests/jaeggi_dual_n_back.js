// An imitation of Jaeggi's Dual N-Back.
var JaeggiDualNBack = KNBack.extend({
  init: function() {
    this._super();
    this.gridSize = window.gridSize;
    this.letters = window.letters; //["T", "Q", "G", "P", "D", "V", "C", "K"];
    this.auditoryTargets = window.auditoryTargets;
    this.visualTargets = window.visualTargets;
    this.audiovisualTargets = window.audiovisualTargets;
    this.setupInstructionsAndStatistics();
    this.preloadAudioFiles(this.letters);
    return this;
  },

  setupKNBackKeyHandlers: function() {
    test.keys[0] = [65];  // 'a'
    test.keys[1] = [76];  // 'l'
    test._super();
  },

  startTest: function() {
    test.setupGrid();
    test._super();
  },

  setupGrid: function() {
    var gridHtml = "<table style=\"width: " + (50 * test.gridSize) +  "px;\">";
    test.gridNames = [];

    for (var i = 0; i < test.gridSize; ++i) {
      test.gridNames[i] = [];
      gridHtml += "<tr>";
      for (var j = 0; j < test.gridSize; ++j) {
        test.gridNames[i][j] = "grid_"+i+"_"+j;
        gridHtml += "<td id=\""+test.gridNames[i][j]+"\"></td>";
      }
      gridHtml += "</tr>";
    }
    gridHtml += "</table>";
    $("div#stimulus").html(gridHtml);
  },

  generateBuffer: function() {
    // Let's just randomly generate until we find one that works
    // Don't call super
    var generation = 0;
    do { test.generateAllTrials(); ++generation; }
    while (!test.validateTrials())
    console.log("Generated valid test after", generation, "tries.");
    test.buffer = test.allTrials.slice(0, 2).reverse();
  },

  generateAllTrials: function() {
    test.allTrials = [];
    for(var i = 0; i < test.maxTrials + test.n; ++i) {
      var trial = [];
      trial[0] = Math.floor(Math.random() * test.gridSize * test.gridSize);
      trial[1] = Math.floor(Math.random() * test.letters.length);
      test.allTrials[i] = trial;
    }
  },

  validateTrials: function() {
    var a = 0, v = 0, av = 0;
    for(var i = test.n; i < test.allTrials.length; ++i) {
      var t0 = test.allTrials[i - test.n];
      var t = test.allTrials[i];
      if(t0[0] == t[0] && t0[1] == t[1])
        ++av;
      else if(t0[0] == t[0])
        ++v;
      else if(t0[1] == t[1])
        ++a;
    }
    //console.log("Generated tests with", a, v, av);
    return (a == test.auditoryTargets &&
            v == test.visualTargets &&
            av == test.audiovisualTargets);
  },

  generateStimulus: function() {
    test.currentStimulus = test.allTrials[test.currentTrial.n];
  },

  showStimulus: function(stimulus) {
    var stimulus = test.currentTrial.stimulus;  // Why not use the argument?
    var i = Math.floor(stimulus[0] / test.gridSize);
    var j = Math.floor(stimulus[0] % test.gridSize);
    $("td#" + test.gridNames[i][j]).addClass('highlighted_square');
    test.playAudio(test.letters[stimulus[1]]);
  },

  clearStimulus: function() {
    var stimulus = test.currentTrial.stimulus;
    var i = Math.floor(stimulus[0] / test.gridSize);
    var j = Math.floor(stimulus[0] % test.gridSize);
    $("td#" + test.gridNames[i][j]).removeClass('highlighted_square');
  }

});

var test = new JaeggiDualNBack();
