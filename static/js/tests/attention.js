// An attention test.
var Attention = ReactionTime.extend({
  init: function() {
    this._super();
    this.missedStatistic = new BinaryStatistic($('span#missed_statistic'));
    this.hideStimulus();
    this.maxResponseTime = window.maxResponseTime;
    return this;
  },

  startTest: function() {
    test.readyStatistic.reset();
    test._super();
  },

  updateTrialStatistics: function() {
    test._super();
    if (test.currentTrial.ready) {
      test.missedStatistic.add(test.currentTrial.missed);
    }
  },

  startTrial: function() {
    test._super();
    test.stimulusOff();
    test.currentTrial.state = "beforeStimulusDisplay";
  },
  
  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.stimulus = (Math.random() < 0.5) ? 0 : 1;
  },

  displayStimulus: function() {
    $('div#inspection_time_stimulus_both').hide();
    $('div#inspection_time_stimulus_1').hide();
    $('div#inspection_time_stimulus_2').hide();
    $('div#inspection_time_stimulus_' + (test.currentTrial.stimulus+1)).show();
    test.missedTimeout = setTimeout(test.missedStimulus, test.maxResponseTime);
    test.stopStimulusDisplay();
  },

  missedStimulus: function() {
    test.currentTrial.missed = true;
    test.handleResponse();
  },
  
  stimulusOff: function() {
    $('div#inspection_time_stimulus_1').hide();
    $('div#inspection_time_stimulus_2').hide();
    $('div#inspection_time_stimulus_both').show();
  },
  
  hideStimulus: function() {
    $('div#inspection_time_stimulus_1').hide();
    $('div#inspection_time_stimulus_2').hide();
    $('div#inspection_time_stimulus_both').hide();
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[37, 39], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (keyState === "down")) {
        clearTimeout(test.missedTimeout);
        test.currentTrial.missed = false;
	test.currentTrial.userResponseTime = new Date().getTime();
        test.currentTrial.userResponse = (keyCode == 37) ? 0 : 1;
	test.handleResponse();
      }
    }]);
  },

  readyForResponse: function() {
  },

  isResponseCorrect: function() {
    if (test.currentTrial.missed) {
      return false;
    }
    return (test.currentTrial.stimulus === test.currentTrial.userResponse);
  },

});

var test = new Attention();

