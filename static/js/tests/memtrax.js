// The Memtrax test.
// A sequence of n_trials images is shown. Each image is shown for at most maxResponseTime or until the user hits the space bar.

var Memtrax = ReactionTime.extend({
    init: function() {
	this._super();
	this.missedStatistic = new BinaryStatistic($('span#missed_statistic'));
	this.hideStimulus();
	this.minResponseTime = window.minResponseTime;
	this.maxResponseTime = window.maxResponseTime;
	this.generateImageURLs();
	this.generateImageSequence();
	this.generateCorrectSequence();
	this.preload();
	//this.maxTrials = 5;
	return this;
    },

    startTest: function() {
	test.readyStatistic.reset();
	test.showStimulus();
	test._super();
    },

    updateTrialStatistics: function() {
	test._super();
	if (test.currentTrial.ready) {
	    test.missedStatistic.add(test.currentTrial.missed);
	}
    },

    generateImageURLs: function() {
	test.imageURLs = new Array();
	var i;
	for (i = 0; i < test.maxTrials; ++i) {
	    test.imageURLs[i] = 'http://www.memtrax.org/NS/' + (i+1) + '.jpg';
	}
    },

    generateImageSequence: function() {
	test.imageSequence = [0, 1, 2, 3, 0, 1, 4, 5, 6, 2, 3, 7, 8, 4, 9, 5, 6, 7, 10, 11, 12, 8, 13, 14, 9, 0, 1, 2, 15, 3, 4, 10, 16, 17, 11, 12, 18, 19, 20, 13, 14, 21, 22, 23, 15, 16, 24, 17, 18, 19]
    },

    generateCorrectSequence: function() {
	var i,j;
	test.correctSequence = new Array();
	test.sequenceCounts = new Array();
	test.prvAppearanceCounts = new Array();
	for (i = 0; i < test.imageSequence.length; ++i) {
	    test.sequenceCounts[test.imageSequence[i]] = 0;
	}
	for (i = 0; i < test.imageSequence.length; ++i) {
	    j = test.imageSequence[i];
	    if (test.sequenceCounts[j] === 0) {
		test.correctSequence[i] = false;
	    }
	    else {
		test.correctSequence[i] = true;
	    }
	    test.prvAppearanceCounts[i] = test.sequenceCounts[j];
	    ++test.sequenceCounts[j];
	}
    },

    preload: function() {
	var elem = $('#preload');
	elem.hide();
	elem.empty();
	var i;
	for (i = 0; i < test.imageURLs.length; ++i) {
	    var url = test.imageURLs[i];
	    elem.append($('<img/>')
			.attr('src', url));
	}
  },

    startTrial: function() {
	test._super();
	test.currentTrial.userResponse = false;
	clearTimeout(test.minTimeout);
	clearTimeout(test.missedTimeout);
    },

    showCurrentImage: function() {
	var elem = $('#stimulus');
	elem.empty();
	var ind = test.imageSequence[test.currentTrial.n];
	var url = test.imageURLs[ind];
	test.currentTrial.imageIndex = ind;
	test.currentTrial.prvAppearances = test.prvAppearanceCounts[test.currentTrial.n];
	test.currentTrial.displayStartTime = new Date().getTime();
	elem.append($('<img/>')
		    .attr('src', url));
	elem.show();
    },

    generateTrialStimulus: function() {
	test._super();
	test.currentTrial.stimulus = test.correctSequence[test.currentTrial.n];
    },

    generateTrialDelay: function() {
	test._super();
	test.currentTrial.delay = 0;
	return test.currentTrial;
    },

    setFeedback: function(feedback) {
	return;
    },

    startDisplayStimulus: function() {
	test.displayStimulus();
    },

    updateTrialStatistics: function() {
	test.currentTrial.ready = false
	test._super();
	test.correctStatistic.add(test.currentTrial.responseCorrect);
	if ((test.currentTrial.stimulus === true) && (test.currentTrial.responseCorrect)) {
	    test.RTStatistic.add(test.currentTrial.reactionTime);
	}
    },


    displayStimulus: function() {
	test.currentTrial.state = "duringStimulusDisplay";
	test.showCurrentImage();
	test.missedTimeout = setTimeout(test.missedStimulus, test.maxResponseTime);
	test.minTimeout = setTimeout(test.minTimePassed, test.minResponseTime);
    },

    minTimePassed: function() {
	test.stopStimulusDisplay();
    },

    missedStimulus: function() {
	test.currentTrial.userResponse = false;
	test.handleResponse();
    },

    handleResponse: function() {
	test.currentTrial.reactionTime = test.currentTrial.userResponseTime - test.currentTrial.displayStartTime;
	if (test.isResponseCorrect()) {
	    test.responseCorrect();
	}
	else {
	    test.responseIncorrect();
	}
	test.endTrial();
    },

    stimulusOff: function() {
	test.hideStimulus();
    },
  
    hideStimulus: function() {
	$('div#stimulus').hide();
    },

    showStimulus: function() {
	$('div#stimulus').show();
    },

    addKeyHandlers: function() {
	test._super();
	test.keyHandlers.unshift([[32], function(keyCode, keyState) {
	    if ((test.state === "inTrial") && (keyState === "down") && (test.currentTrial.state === "afterStimulusDisplay")) {
		clearTimeout(test.missedTimeout);
		test.currentTrial.missed = false;
		test.currentTrial.userResponseTime = new Date().getTime();
		test.currentTrial.userResponse = true;
		test.handleResponse();
	    }
	}]);
    },

    readyForResponse: function() {
    },

    isResponseCorrect: function() {
	return (test.currentTrial.stimulus === test.currentTrial.userResponse);
    },

});

var test = new Memtrax();
