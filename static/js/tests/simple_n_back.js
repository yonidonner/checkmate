// k modalities n-back.
var SimpleNBack = KNBack.extend({
  init: function() {
    this._super();
    this.nStimuli = window.nStimuli; // number of possible different stimuli
    this.pIdentical = window.pIdentical; // probability of same stimulus as before
    this.setupInstructionsAndStatistics();
    this.preload();
    return this;
  },

  generateStimulus: function() {
    test.currentStimulus = new Array();
    if (test.buffer.length < test.n) {
      test.currentStimulus[0] = Math.floor(Math.random() * test.nStimuli);
    }
    else {
      if (Math.random() < test.pIdentical) {
        test.currentStimulus[0] = test.buffer[test.n-1][0];
      }
      else {
        test.currentStimulus[0] = Math.floor(Math.random() * (test.nStimuli - 1));
        if (test.currentStimulus[0] === test.buffer[test.n-1][0]) {
          ++test.currentStimulus[0];
        }
      }
    }
  },

  showStimulus: function(stimulus) {
    $('div#stimulus').html('<img class="creature" src="' + test.getImageURL(stimulus) + '"/>');
  },

  clearStimulus: function() {
    $('div#stimulus').empty();
  },

  getImageURL: function(number) {
    var stimuli = ['dino', 'narwhal', 'jellyfish', 'snail'];
    return "/static/images/tests/n_back/" + stimuli[number] + "_green.png";
  },

  preload: function() {
    // Preload stimuli. Let's hope there aren't more than four!
    for(var i = 0; i < test.nStimuli; ++i)
      $('#preload_area').append($('<img class="creature"/>').attr('src', test.getImageURL(i)));
  }

});

var test = new SimpleNBack();
