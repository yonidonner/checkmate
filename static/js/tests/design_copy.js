// Design Copy: the user should memorize and reconstruct a binary design.
var DesignCopy = LevelEstimation.extend({
  init: function() {
    this._super();
    this.nRows = window.nRows;
    this.nCols = window.nCols;
    this.grid = new Grid(this.nRows, this.nCols, 30);
    $('span#level_statistic_name').text("Complexity");
    this.stimulusDisplayTime = window.stimulusDisplayTime;
    this.maskRepeats = window.maskRepeats;
    this.maskRepeatTime = window.maskRepeatTime;
    return this;
  },

  startTest: function() {
    test.currentLevel = test.initialLevel;
    test._super();
  },

  createDesign: function() {
    test.currentTrial.design = randomizeDesign(test.nRows, test.nCols, test.currentLevel);
  },

  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    test.createDesign();
  },

  startDisplayStimulus: function() {
    test._super();
    gridCreate(test.grid, $('div#stimulus'));
    gridDisplayDesign(test.grid, test.currentTrial.design);
    setTimeout(test.maskStimulus, test.stimulusDisplayTime);
  },

  maskStimulus: function() {
    gridMask(test.grid, test.maskRepeats, test.maskRepeatTime, test.afterMaskStimulus);
  },

  afterMaskStimulus: function() {
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    gridStopMasking(test.grid);
    removeGrid(test.grid);
    test._super();
  },

  readyForResponse: function() {
    test._super();
    gridSetupRead(test.grid, $('div#stimulus'), test.userDesignSubmit, "binary_design");
  },

  userDesignSubmit: function(grid, userDesign) {
    test.currentTrial.userDesign = userDesign;
    test.currentTrial.userResponseTime = new Date().getTime();
    test.handleResponse();
  },
  
  isResponseCorrect: function() {
    console.log(test.currentTrial);
    for (var i = 0; i < test.nRows; ++i) {
      for (var j = 0; j < test.nCols; ++j) {
        if (test.currentTrial.design[i][j] !== test.currentTrial.userDesign[i][j]) {
          return false;
        }
      }
    }
    return true;
  },

});

var test = new DesignCopy();