// Spatial Span: present the user with a sequence of squares that light up on different locations on a grid, which they need to repeat forwards or backwards.
var SpatialSpan = Span.extend({
  init: function() {
    this._super();
    this.nRows = window.nRows;
    this.nCols = window.nCols;
    this.grid = new Grid(this.nRows, this.nCols, 40);
    return this;
  },
  
  generateStimulusSequence: function() {
    test._super();
    for (var i = 0; i < test.currentTrial.level; ++i) {
      var gridElement = [Math.floor(Math.random()*test.nRows),
                     Math.floor(Math.random()*test.nCols)];
      test.currentTrial.sequence[i] = gridElement;
    }
  },

  startTrial: function() {
    gridCreate(test.grid, $('div#stimulus'));
    test._super();
  },
  
  startDisplayStimulus: function() {
    test._super();
  },
  
  showSequenceElement: function(element) {
    var i = element[0];
    var j = element[1];
    cellSetStyle(gridGetCell(test.grid, i, j), 'cellOn');
  },

  unshowSequenceElement: function(element) {
    var i = element[0];
    var j = element[1];
    cellSetStyle(gridGetCell(test.grid, i, j), 'cellOff');
  },

  stopStimulusDisplay: function() {
    removeGrid(test.grid);
    test._super();
  },

  readyForResponse: function() {
    test._super();
    gridSetupRead(test.grid, $('div#stimulus'), test.userSequenceSubmit, "sequence");
  },
  
  userSequenceSubmit: function(grid, userSequence) {
    if (userSequence.length === test.currentTrial.sequence.length) {
      for (var i = 0; i < userSequence.length; ++i) {
	test.currentTrial.userSequence[i] = userSequence[i];
      }
    }
    test.handleResponse();
  },

});

var test = new SpatialSpan();