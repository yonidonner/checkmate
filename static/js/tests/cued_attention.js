// Cued attention task.
var CuedAttention = ReactionTime.extend({
  init: function() {
    this._super();
    this.testAxis = window.testAxis;
    this.pCongruentCue = window.pCongruentCue;
    this.cueTypes = window.cueTypes;
    this.cueStyle = window.cueStyle;
    this.stimulusStyle = window.stimulusStyle;
    if ((this.stimulusStyle === "dot") || (this.stimulusStyle === "square")) {
      this.stimulusType = "simple";
    }
    else {
      this.stimulusType = "choice";
    }
    this.cueDuration = window.cueDuration;
    this.cueStimulusDelay = window.cueStimulusDelay;
    this.cueStimulusDelayLambda = window.cueStimulusDelayLambda;
    this.maxResponseTime = window.maxResponseTime;
    this.missedStatistic = new BinaryStatistic($('span#missed_statistic'));
    if (this.testAxis === "horizontal") {
      this.spatialtd1 = $('div#cued_attention_cell4');
      this.spatialtd2 = $('div#cued_attention_cell6');
    }
    if (this.testAxis === "vertical") {
      this.spatialtd1 = $('div#cued_attention_cell2');
      this.spatialtd2 = $('div#cued_attention_cell8');
    }
    return this;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[32, 37, 39], function(keyCode, keyState) {
      if (((keyCode == 32) && (test.stimulusType === "choice")) ||
          (((keyCode == 37) || (keyCode == 39)) && (test.stimulusType === "simple"))) {
        return;
      }
      if ((test.state === "inTrial") && (keyState === "down")) {
        clearTimeout(test.missedTimeout);
        test.currentTrial.missed = false;
	test.currentTrial.userResponseTime = new Date().getTime();
        if (keyCode == 32) {
          test.currentTrial.userResponse = 0;
        }
        else {
          test.currentTrial.userResponse = (keyCode == 37) ? 0 : 1;
        }
	test.handleResponse();
      }
    }]);
  },

  startTest: function() {
    test.readyStatistic.reset();
    test._super();
  },

  updateTrialStatistics: function() {
    test._super();
    if (test.currentTrial.ready) {
      test.missedStatistic.add(test.currentTrial.missed);
    }
  },

  startTrial: function() {
    if (test.missedTimeout) {
      clearTimeout(test.missedTimeout);
    }
    test._super();
    test.stimulusOff();
    test.currentTrial.state = "beforeStimulusDisplay";
  },
  
  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.stimulusLocation = (Math.random() < 0.5) ? 0 : 1;
    if (Math.random() < test.pCongruentCue) {
      test.currentTrial.cueLocation = test.currentTrial.stimulusLocation;
    }
    else {
      test.currentTrial.cueLocation = 1 - test.currentTrial.stimulusLocation;
    }
    if (test.stimulusType === "simple") {
      test.currentTrial.stimulus = 0;
    }
    else {
      test.currentTrial.stimulus = (Math.random() < 0.5) ? 0 : 1;
    }
  },

  startDisplayStimulus: function() {
    test.displayStimulus();
  },

  displayStimulus: function() {
    // this is actually cue display
    test.displayCue();
    if (test.missedTimeout) {
      clearTimeout(test.missedTimeout);
    }
    test.missedTimeout = setTimeout(test.undisplayCue, test.cueDuration);
  },

  undisplayCue: function() {
    test.cueOff();
    test.currentTrial.cueStimulusDelay = test.cueStimulusDelay - Math.log(Math.random())*test.cueStimulusDelayLambda;
    test.missedTimeout = setTimeout(test.actualDisplayStimulus, test.currentTrial.cueStimulusDelay);
  },

  displayCue: function() {
    test.cueOn(test.currentTrial.cueLocation);
  },

  elementDisplayStimulus: function(element) {
    if (test.stimulusStyle === "dot") {
      element.html(".");
    }
    if (test.stimulusStyle === "farrow") {
    }
    if (test.stimulusStyle === "arrow") {
      var content;
      if (test.currentTrial.stimulus === 0) {
        content = "←";
      }
      if (test.currentTrial.stimulus === 1) {
        content = "→";
      }
      element.html(content);
    }
  },

  actualDisplayStimulus: function() {
    if (test.currentTrial.stimulusLocation === 0) {
      test.elementDisplayStimulus(test.spatialtd1);
    }
    if (test.currentTrial.stimulusLocation === 1) {
      test.elementDisplayStimulus(test.spatialtd2);
    }
    test.missedTimeout = setTimeout(test.missedStimulus, test.maxResponseTime);
    test.stopStimulusDisplay();
  },

  missedStimulus: function() {
    test.currentTrial.missed = true;
    test.handleResponse();
  },

  showFixation: function() {
    $('div#cued_attention_cell5').html('+');
  },
  
  hideFixation: function() {
    $('div#cued_attention_cell5').html('');
  },

  setRectangle: function(element) {
    element.removeClass("rectangle_bold");
    element.addClass("rectangle");
  },

  cueOn: function(cueLocation) {
    if (test.cueStyle === "arrow") {
      if (cueLocation === 0) {
        content = "←";
      }
      if (cueLocation === 1) {
        content = "→";
      }
      $('div#cued_attention_cell5').html(content);
      return;
    }
    var element;
    if (cueLocation === 0) {
      element = test.spatialtd1;
    }
    else {
      element = test.spatialtd2;
    }
    test.elementCueOn(element);
  },

  elementCueOn: function(element) {
    if (test.cueStyle === "bold") {
      element.addClass("rectangle_bold");
    }
    if (test.cueStyle === "asterisk") {
      element.html("*");
    }
  },

  cueOff: function() {
    if (test.cueStyle === "arrow") {
      test.showFixation();
      return;
    }
    test.elementCueOff(test.spatialtd1);
    test.elementCueOff(test.spatialtd2);
  },

  elementCueOff: function(element) {
    if (test.cueStyle === "bold") {
      element.removeClass("rectangle_bold");
    }
    if (test.cueStyle === "asterisk") {
      element.html("");
    }
  },

  stimulusOff: function() {
    $('div#cued_attention_cell1').html("");
    $('div#cued_attention_cell2').html("");
    $('div#cued_attention_cell3').html("");
    $('div#cued_attention_cell4').html("");
    $('div#cued_attention_cell5').html("");
    $('div#cued_attention_cell6').html("");
    $('div#cued_attention_cell7').html("");
    $('div#cued_attention_cell8').html("");
    $('div#cued_attention_cell9').html("");
    test.showFixation();
    test.setRectangle(test.spatialtd1);
    test.setRectangle(test.spatialtd2);
  },
  
  hideStimulus: function() {
  },

  readyForResponse: function() {
  },

  isResponseCorrect: function() {
    if (test.currentTrial.missed) {
      return false;
    }
    return (test.currentTrial.stimulus === test.currentTrial.userResponse);
  },

});

var test = new CuedAttention();