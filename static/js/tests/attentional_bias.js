// Our version of ColorWord.
var AttentionalBias = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.nBaseTrials = window.nBaseTrials;
    this.maxTrials = this.nBaseTrials * 16;
    this.list1 = window.wordList1;
    this.list2 = window.wordList2;
    this.neutralList = window.wordListNeutral;
    this.fixationDuration = window.fixationDuration;
    this.displayDuration = window.displayDuration;
    this.addAttentionalBiasKeyHandlers();    
    return this;
  },

  generateAllTrials: function() {
    var perm = randomPermutation(test.nBaseTrials * 16);
    var perm2 = randomPermutation(test.nBaseTrials * 16);
    var perm3 = randomPermutation(test.nBaseTrials * 16);
    var i, j;
    test.allTrials = new Array();
    for (j = 0; j < 16; ++j) {
      for (i = 0; i < test.nBaseTrials; ++i) {
        test.allTrials[perm[j * test.nBaseTrials + i]] = [j % 2,((j / 2) % 2) == 0,((j / 4) % 2) == 0,j<4,perm3[j * test.nBaseTrials + i] % (test.nBaseTrials * 8),perm2[j * test.nBaseTrials + i]];
      }
    }
  },
  
  addAttentionalBiasKeyHandlers: function() {
    test.keyHandlers.unshift([[37,39], function(keyCode, keyState) {
      if (keyState === "up") {
	return;
      }
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay")) {
        if (keyCode === 37) {
          test.currentTrial.userResponse = false;
        }
        else {
          if (keyCode === 39) {
            test.currentTrial.userResponse = true;
          }
        }
	test.currentTrial.userResponseTime = new Date().getTime();
        test.handleResponse();
      }
    }]);
  },
  
  isResponseCorrect: function() {
    return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.correctResponse = test.allTrials[test.currentTrial.n][1];
    test.currentTrial.stimulusPosition = test.allTrials[test.currentTrial.n][2];
    test.currentTrial.biasPosition = test.allTrials[test.currentTrial.n][3];
    test.currentTrial.listNumber = test.allTrials[test.currentTrial.n][0];
    var theList = (test.currentTrial.listNumber === 0) ? test.list1 : test.list2;
    test.currentTrial.biasWord = theList[test.allTrials[test.currentTrial.n][4]];
    test.currentTrial.neutralWord = test.neutralList[test.allTrials[test.currentTrial.n][5]];
  },

  startTest: function() {
    test.generateAllTrials();
    test._super();
  },

  startTrial: function() {
    $('#left_word').text('');
    $('#right_word').text('');
    test._super();
  },

  endTrial: function() {
    test._super();
  },

  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.fixationDuration;
  },

  startDisplayStimulus: function() {
    test._super();
    console.log('current trial: correct='+test.currentTrial.correctResponse+' bias position=' + test.currentTrial.biasPosition + ' list number: ' + test.currentTrial.listNumber + ' bias word: ' + test.currentTrial.biasWord + ' neutral word:' + test.currentTrial.neutralWord)
    if (test.currentTrial.biasPosition) {
      $('#left_word').text(test.currentTrial.neutralWord);
      $('#right_word').text(test.currentTrial.biasWord);
    }
    else {
      $('#left_word').text(test.currentTrial.biasWord);
      $('#right_word').text(test.currentTrial.neutralWord);
    }
    setTimeout(test.displayStimulus2, test.displayDuration);
  },

  displayStimulus2: function() {
    $('#left_word').text('');
    $('#right_word').text('');
    var stimulusText = test.currentTrial.correctResponse ? '→' : '←';
    if (test.currentTrial.stimulusPosition) {
      $('#right_word').text(stimulusText);
    }
    else {
      $('#left_word').text(stimulusText);
    }
    test.stopStimulusDisplay();
  },

    hideColorWordWord: function() {
	$('#color_word_word').text('');
    },

    stopStimulusDisplay: function() {
  	test._super();
    },

    readyForResponse: function() {
  	test._super();
    },

  preload: function() {
    test.preloadList(test.list1);
    test.preloadList(test.list2);
    test.preloadList(test.neutralList);
  },
  
  getImageURL: function(imageName) {
    return "/static/images/iaps/" + imageName + ".png";
  },


  preloadList: function(l) {
    for (var i in l) {
      $('#preload_area').append($('<img class="affective_images"/>').attr('src', l[i]));
    }
  },

});

var test = new AttentionalBias();