// Inspection Time: tries to identify the minimal time you require to distinguish between two stimuli
var InspectionTime = LevelEstimation.extend({
  init: function() {
    this._super();
    // TODO: read initialLevel somehow
    $('span#level_statistic_name').text("Inspection time");
    this.decLambda = window.decLambda;
    this.incDelta = window.incDelta;
    $('div#inspection_time_stimulus_1').hide();
    $('div#inspection_time_stimulus_2').hide();
    $('div#inspection_time_stimulus_mask').hide();
    this.guessingProbability = 0.5;
    this.a_vals = [0.25, 0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0];
    return this;
  },

  increaseLevel: function() {
    var u = Math.random();
    test.currentLevel *= (1.0 - test.incDelta*Math.log(u));
    //test.currentLevel = (test.multiplier1 + u * (1.0 - test.multiplier1)) * test.currentLevel;
  },

  decreaseLevel: function() {
    var u = Math.random();
    test.currentLevel /= (1.0 - test.decLambda*Math.log(u));
    //test.currentLevel = (1.0 + u * (test.multiplier2 - 1.0)) * test.currentLevel;
  },

  updateLevel: function() {
    if (test.currentTrial.responseCorrect)
      test.decreaseLevel();
    else if (!test.currentTrial.responseCorrect)
      test.increaseLevel();
  },

  updateLevelOld: function() {
    // Override the parent discrete level updating with our continuous updating
    // where increaseLevel actually decreases the level.
    if (test.levelStatistic.estimate === false) {
      if (test.currentTrial.responseCorrect)
        test.decreaseLevel();
      else if (!test.currentTrial.responseCorrect)
        test.increaseLevel();
      console.log('changed level on', test.currentTrial.responseCorrect,
                  'to', test.currentLevel);
    }
    else {
      // We actually finish the test as soon as we have an estimate, for now.
      // Is that what's supposed to happen? This doesn't get called, then.
      test.currentLevel = test.levelStatistic.estimate;
    }
  },

  startTest: function() {
    test._super();
  },

  startTrial: function() {
    test.stimulusOff();
    test._super();
  },
  
  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.stimulus = (Math.random() < 0.5) ? 0 : 1;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[37, 39], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (keyState === "down") && (test.currentTrial.state == "afterStimulusDisplay")) {
        test.currentTrial.userResponse = (keyCode == 37) ? 0 : 1;
	test.handleResponse();
      }
    }]);
  },

  startDisplayStimulus: function() {
    test._super();
    test.stimulusOn();
    setTimeout(test.stopStimulusDisplay, test.currentTrial.level);
  },

  stopStimulusDisplay: function() {
    test.maskStimulus();
    test._super();
  },

  maskStimulus: function() {
    test.stimulusOff();
    $('div#inspection_time_stimulus_mask').show();
  },

  stimulusOn: function() {
    test.stimulusOff();
    $('div#inspection_time_stimulus_' + (test.currentTrial.stimulus+1)).show();
  },

  stimulusOff: function() {
    $('div#inspection_time_stimulus_1').hide();
    $('div#inspection_time_stimulus_2').hide();
    $('div#inspection_time_stimulus_mask').hide();
  },

  readyForResponse: function() {
    test.setFeedback('Which was longer?');
  },

  isResponseCorrect: function() {
    return (test.currentTrial.stimulus === test.currentTrial.userResponse);
  },

  responseCorrect: function() {
    test._super();
    test.setFeedback('Right! (' + parseInt(test.currentTrial.level) + ' ms)');
  },
  
  responseIncorrect: function() {
    test._super();
    test.setFeedback('Wrong (' + parseInt(test.currentTrial.level) + ' ms)');
    console.log('Wrong (' + test.currentTrial.level + ' ms)');
  }

});

var test = new InspectionTime();