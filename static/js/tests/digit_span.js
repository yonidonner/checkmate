// Digit Span: present the user with a sequence of digits which they need to repeat forwards or backwards.
var DigitSpan = Span.extend({
  init: function() {
    this._super();
    this.digits = window.digits;
    this.isVisual = window.isVisual;
    this.isAuditory = window.isAuditory;
    return this;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[13], function(keyCode, keyState) {
      if(keyState === 'down') {
        test.userSequenceSubmit();
      }
    }]);
  },

  startTest: function() {
    test._super();
  },

  startTrial: function() {
    $('div#digit_span_input_form').hide();
    test._super();
  },

  generateStimulusSequence: function() {
    test._super();
    for (var i = 0; i < test.currentTrial.level; ++i) {
      test.currentTrial.sequence[i] = test.digits[Math.floor(Math.random()*test.digits.length)];
    }
  },

  showSequenceElement: function(element) {
    if(test.isVisual)
      $('div#stimulus').show().text(element);
    if(test.isAuditory)
      test.playAudio(element);
  },

  unshowSequenceElement: function(element) {
    $('div#stimulus').hide().text('');
  },

  readyForResponse: function() {
    test._super();
    $('div#digit_span_input_form').show();
  },

  userSequenceSubmit: function() {
    if (test.state === "inTrial") {
      var userSequence = $("#user_sequence_input").val();
      if(!userSequence || !userSequence.length)
        return;  // Don't submit an empty sequence
      //if (userSequence.length === test.currentTrial.sequence.length) {
      if (true) {
	for (var i = 0; i < userSequence.length; ++i) {
	  test.currentTrial.userSequence[i] = userSequence[i];
	}
      }
      $("#user_sequence_input").val('');
      $('div#digit_span_input_form').hide();
      test.handleResponse();
    }
  },

});

var test = new DigitSpan();