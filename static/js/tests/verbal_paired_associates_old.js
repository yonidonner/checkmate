// Verbal Paired Associates: the subject needs to learn and then recall a list of words.
var VerbalPairedAssociates = PairedAssociates.extend({
  init: function() {
    this._super();
    $('span#level_statistic_name').text("Word pairs");
    return this;
  },

  showPair: function(cue, associate) {
    $('div#stimulus1').text(cue);
    $('div#stimulus2').text(associate);
  },
  
  unshowPair: function(cue, associate) {
    $('div#stimulus1').text("");
    $('div#stimulus2').text("");
  },

  generatePairs: function(level) {
    var pairs = window.wordPairs;
    return pairs;
  },

  setupInput: function() {
    var html = "<form name=\"word_list_form\"><table>";
    for (var i in test.currentTrial.pairs) {
      html += "<tr><td>" + test.currentTrial.pairs[i][0] + "</td><td><input type=\"text\" name=\"associate_" + i + "\"></td></tr>";
    }
    html += "</table><input type='button' onclick=\"test.userInputSubmit()\" value='Done' /></form>";
    $("div#associate_input_form").html(html);
  },

  userInputSubmit: function() {
    for (var i in test.currentTrial.pairs) {
      test.currentTrial.userChoice[i] = document.forms["word_list_form"]["associate_" + i].value;
    }
    test.hideWordInput();
    test.handleResponse();
  },

  hideWordInput: function() {
    $("div#associate_input_form").html("");
  },

  isTestFinished: function() {
    return true;
  },

});

var test = new VerbalPairedAssociates();
