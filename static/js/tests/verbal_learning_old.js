// Verbal Learning: the subject needs to learn and then recall a list of words.
var VerbalLearning = LevelEstimation.extend({
  init: function() {
    this._super();
    $('span#level_statistic_name').text("Number of words");
    this.maxLearningTrials = window.maxLearningTrials;
    this.wordOnTime = window.wordOnTime;
    this.wordOffTime = window.wordOffTime;
    this.testPart = window.testPart;
    if (this.testPart === 1) {
      this.initialLevel = window.wordList.length;
      this.levelStatistic = false;
    }
    return this;
  },
  
  startTest: function() {
    if (test.testPart === 0) {
      test.sequenceTimer = SequenceTimer(test.wordOnTime, test.wordOffTime, test.displayCallback, test.hideCallback, test.doneCallback);
    }
    test._super();
  },

  displayCallback: function(index) {
    $('div#stimulus').text(test.currentTrial.wordList[index]);
  },
  
  hideCallback: function(index) {
    $('div#stimulus').text("");
  },

  doneCallback: function() {
    test.resetWordInput();
    test.showWordInput();
    test.setFeedback('What were the ' + test.currentTrial.level + ' words?');
  },
  
  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.wordList = window.wordList;
    test.currentTrial.level = test.currentTrial.wordList.length;
    test.currentTrial.sortedWords = test.currentTrial.wordList.slice(0).sort();
    /*
    rpc('get_word_list', ['concrete_nouns', test.currentTrial.level, test.config.word_lists_used['concrete_nouns'][test.currentTrial.level]], function(response) {
      if (response.success) {
        test.config.word_lists_used['concrete_nouns'][test.currentTrial.level] = response.already_seen;
        test.currentTrial.wordList = response.word_list;
        test.currentTrial.sortedWords = test.currentTrial.wordList.slice(0).sort();
      }
    });
    */
    test.currentTrial.userWords = new Array();
  },

  startDisplayStimulus: function() {
    test._super();
    if (test.testPart === 0) {
      test.subTrial = 0;
      test.startSubtrial();
    }
    else {
      test.doneCallback();
    }
  },

  startSubtrial: function() {
    startSequence(test.sequenceTimer, test.currentTrial.level);
  },

  stopStimulusDisplay: function() {
    test._super();
  },

  setupTextArea: function() {
    $("div#word_list_input_form").html("<form name=\"word_list_form\"><textarea rows=\"" + test.currentLevel + "\" name=\"user_words\"></textarea><input type='button' onclick=\"test.userWordsSubmit()\" value='Done' /></form>");
  },

  userWordsSubmit: function() {
    userWordsText = document.forms["word_list_form"]["user_words"].value;
    test.hideWordInput();
    userWords = userWordsText.split(/\W+/);
    userWords.sort();
    while (userWords[0] === "") {
      userWords.splice(0, 1);
    }
    test.currentTrial.userWords[test.subTrial] = userWords;
    console.log("User words: " + test.currentTrial.userWords[test.subTrial]);
    test.allCorrect = false;
    test.doneSubtrial();
  },

  checkWords: function() {
    // todo: check commissions and omissions
    var list1 = test.currentTrial.sortedWords;
    var list2 = test.currentTrial.userWords[test.subTrial];
    if (list1.length !== list2.length) {
      return false;
    }
    for (i in list1) {
      if (list1[i] !== list2[i]) {
        return false;
      }
    }
    return true;
  },

  readyForResponse: function() {
    test.handleResponse();
  },
  
  doneSubtrial: function() {
    test.allCorrect = test.checkWords();
    if (test.allCorrect) {
      test.stopStimulusDisplay();
    }
    else if (++test.subTrial === test.maxLearningTrials) {
      test.responseIncorrect();
      test.stopStimulusDisplay();
    }
    else {
      test.responseIncorrect();
      if (test.part === 0) {
        test.startSubtrial();
      }
      else {
        test.stopStimulusDisplay();
      }
    }
  },

  resetWordInput: function() {
    test.setupTextArea();
  },

  showWordInput: function() {
    test.setupTextArea();
    //$('div#word_list_input_form').show();
  },

  hideWordInput: function() {
    $('div#word_list_input_form').html("");
  },

  isResponseCorrect: function() {
    return test.allCorrect;
  },

  responseCorrect: function() {
    test._super();
    test.setFeedback('Right! (' + parseInt(test.currentTrial.level) + ' words)');
  },
  
  responseIncorrect: function() {
    test._super();
    test.setFeedback('Wrong (' + parseInt(test.currentTrial.level) + ' words)');
  },

  isTestFinished: function() {
    return true;
  },
  
  endTest: function() {
    test._super();
    /*
    rpc('store_config', ['word_lists_used', test.config.word_lists_used], function(response) {
      if (response.success) {
        console.log('Stored word_lists_used config');
      }
    });
    */
  }

});

var test = new VerbalLearning();
