// Our version of ColorWord.
var ColorWord = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.nColors = window.nColors;
    this.nBaseTrials = window.nBaseTrials;
    this.color_wordVariant = window.color_wordVariant;
    this.allColors = ['red','green','blue'];
    this.addColorWordKeyHandlers();
    this.pTaskSwitch = window.pTaskSwitch;
    return this;
  },

    addColorWordKeyHandlers: function() {
	test.keyCodeToNumber = new Array();
	test.allKeys = [];
	for (var i = 0; i < test.allColors.length; ++i) {
	    var colorKeyCode = test.allColors[i].toUpperCase().charCodeAt(0);
	    test.keyCodeToNumber[colorKeyCode] = i;
	    //test.keyCodeToNumber[colorKeyCode - 32] = i;
	    test.allKeys.push(colorKeyCode);
	    //test.allKeys.push(colorKeyCode - 32);
	}
	test.keyHandlers.unshift([test.allKeys, function(keyCode, keyState) {
	    if (keyState === "up") {
		return;
	    }
	    //alert("Pressed "+test.keyCodeToNumber[keyCode]);
	    if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay")) {
		test.currentTrial.userResponseTime = new Date().getTime();
		test.currentTrial.userResponse = test.keyCodeToNumber[keyCode];
		test.color_wordUserResponse();
	    }
	}]);
    },

    isResponseCorrect: function() {
	return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
    },

    color_wordUserResponse: function() {
	test.handleResponse();
    },

    color_wordChooseWordAndColor: function() {
	test.currentTrial.word = Math.floor(Math.random()*test.nColors);
	if (test.currentTrial.isConcordant) {
            test.currentTrial.color = test.currentTrial.word;
	}
	else {
	    test.currentTrial.color = Math.floor(Math.random() * (test.nColors - 1));
            if (test.currentTrial.color === test.currentTrial.word) {
		test.currentTrial.color++;
	    }
	}
    },

  generateTrialStimulus: function() {
    test._super();
    // $('#color_word_test_part').text(test.color_wordCurrentType);
    // $('#color_word_remaining_trials').text(test.color_wordCurrentType === "inhibition" ? test.nRemainingSubPartTrials : test.nRemainingPartTrials);
    // $('#color_word_trial_number').text(test.currentTrial.n);
    if (test.color_wordCurrentType === "context switching") {
      test.currentTrial.isConcordant = false;
      test.color_wordChooseWordAndColor();
      test.currentTrial.isTaskWord = (Math.random() < 0.5);
    }
    else if (test.color_wordCurrentType === "both") {
      test.currentTrial.word = Math.floor(Math.random()*test.nColors);
      test.currentTrial.color = Math.floor(Math.random()*test.nColors);
      test.currentTrial.isConcordant = (test.currentTrial.word === test.currentTrial.color);
      test.currentTrial.isTaskWord = (Math.random() < 0.5);
    }
    else if (test.color_wordCurrentType === "mixed") {
      test.currentTrial.word = Math.floor(Math.random()*test.nColors);
      test.currentTrial.color = Math.floor(Math.random()*test.nColors);
      test.currentTrial.isConcordant = (test.currentTrial.word === test.currentTrial.color);
      if (test.previousTask === -1) {
        test.currentTrial.isTaskWord = (Math.random() < 0.5);
        test.previousTask = test.currentTrial.isTaskWord;
      }
      else {
        if (Math.random() < test.pTaskSwitch) {
          test.currentTrial.isTaskWord = !test.previousTask;
        }
        else {
          test.currentTrial.isTaskWord = test.previousTask;
        }
        test.previousTask = test.currentTrial.isTaskWord;
      }
    }
    else if (test.color_wordCurrentType === "inhibition") {
      test.currentTrial.isConcordant = test.inhibitionSubParts[test.inhibitionSubPart][0];
      test.currentTrial.isTaskWord = test.inhibitionSubParts[test.inhibitionSubPart][1];
      test.color_wordChooseWordAndColor();
    }
    test.currentTrial.correctResponse = test.currentTrial.isTaskWord ? test.currentTrial.word : test.currentTrial.color;
    $('#color_word_concordant').text(test.currentTrial.isConcordant ? "Concordant" : "Discordant");
    $('#color_word_task').text(test.currentTrial.isTaskWord ? "Word" : "Color");
  },

    startTest: function() {
	test.color_wordFinished = false;
	test.generateColorWordTestParts();
	test.color_wordTestPart = -1;
	test.nextColorWordTestPart();
	test._super();
    },

    startTrial: function() {
	test.hideColorWordWord();
	test._super();
    },
    
  generateColorWordTestParts: function() {
    test.order = new Array();
    if (test.color_wordVariant === "all") {
      var allColorWordParts;
      allColorWordParts = ["inhibition","context switching", "both"];
      var i,j;
      for (i = 0; i < 3; ++i) {
	j = Math.floor(Math.random()*allColorWordParts.length);
	test.order[i] = allColorWordParts[j];
	allColorWordParts.splice(j, 1);
      }
    }
    else {
      test.order[0] = test.color_wordVariant;
      test.previousTask = -1;
    }
  },

    nextColorWordTestPart: function() {
	test.color_wordTestPart++;
	if (test.color_wordTestPart === test.order.length) {
	    test.color_wordFinished = true;
	    return;
	}
	test.color_wordCurrentType = test.order[test.color_wordTestPart];
	test.nRemainingPartTrials = 6 * test.nBaseTrials;
	if (test.color_wordCurrentType === "inhibition") {
            test.inhibitionSubPart = 0;
            test.inhibitionSubParts = [[true, true, test.nBaseTrials],
				       [false, true, 2 * test.nBaseTrials],
				       [true, false, test.nBaseTrials],
				       [false, false, 2 * test.nBaseTrials]];
            test.nRemainingSubPartTrials = test.inhibitionSubParts[test.inhibitionSubPart][2];
	}
    },

    isTestFinished: function() {
    if (test.fixedDuration > 0){
        return test._super();
    }
	return test.color_wordFinished;
    },


    endTrial: function() {
	test.hideColorWordWord();
	if (test.color_wordCurrentType === "inhibition") {
            if (--test.nRemainingSubPartTrials == 0) {
		if (++test.inhibitionSubPart == test.inhibitionSubParts.length) {
		    test.nextColorWordTestPart();
		}
		else {
		    test.nRemainingSubPartTrials = test.inhibitionSubParts[test.inhibitionSubPart][2];
		}
	    }
	}
	else {
	    if (--test.nRemainingPartTrials == 0) {
		test.nextColorWordTestPart();
	    }
	}
	test._super();
    },
    
    generateTrialDelay: function() {
	test._super();
	test.currentTrial.delay = test.prestimulusDelay;
    },

    startDisplayStimulus: function() {
  	test._super();
	$('#color_word_word').text(test.allColors[test.currentTrial.word]);
	for (i = 0; i < test.allColors.length; ++i) {
	    $('#color_word_word').removeClass(test.allColors[i]);
	}
	$('#color_word_word').addClass(test.allColors[test.currentTrial.color]);
	test.stopStimulusDisplay();
    },

    hideColorWordWord: function() {
	$('#color_word_word').text('');
    },

    stopStimulusDisplay: function() {
  	test._super();
    },

    readyForResponse: function() {
  	test._super();
    },

});

var test = new ColorWord();