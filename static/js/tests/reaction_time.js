// A TimedStimulusResponse test which just measures how quickly one can press a button to indicate which stimulus was displayed. This one is still abstract.
var ReactionTime = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.delayLambda = window.delayLambda;
    this.delayAlpha = window.delayAlpha;
    this.maxTrials = window.nTrials;
    return this;
  },

  endTrial: function() {
    test.hideStimulus();
    test._super();
  },
  
  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = -Math.log(Math.random())*test.delayLambda + test.delayAlpha;
  },

  startDisplayStimulus: function() {
    test._super();
    test.displayStimulus();
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    test._super();
  },

  readyForResponse: function() {
    test._super();
  }

});











