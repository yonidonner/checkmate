// Verbal Paired Associates: the subject needs to learn and then recall a list of words.
var VerbalPairedAssociates = Base.extend({
  init: function() {
    this._super();
    this.testPart = window.testPart;
    if (this.testPart === 0) {
      this.pairOnTime = window.pairOnTime;
      this.pairOffTime = window.pairOffTime;
    }
    if (window.parentID) {
      this.parentID = window.parentID;
    }
    if (window.pair_id) {
      test.pair_id = window.pair_id;
    }
    this.wordPairs = window.wordPairs;
    return this;
  },

  startTest: function() {
    test._super();
    test.userWords = new Array();
    if (test.testPart === 0) {
      test.randomOrder = randomPermutation(test.wordPairs.length);
      test.sequenceTimer = SequenceTimer(test.pairOnTime, test.pairOffTime, test.displayCallback, test.hideCallback, test.doneCallback);
      startSequence(test.sequenceTimer, test.wordPairs.length);
    }
    else {
      test.setupInput();
      test.setFeedback('What were the ' + test.wordPairs.length + ' associates you learned last time?');
    }
  },

  displayCallback: function(index) {
    var index2 = test.randomOrder[index];
    var cue = test.wordPairs[index2][0];
    var associate = test.wordPairs[index2][1];
    $('div#stimulus1').text(cue);
    $('div#stimulus2').text(associate);
  },
  
  hideCallback: function(index) {
    $('div#stimulus1').text("");
    $('div#stimulus2').text("");
  },

  doneCallback: function() {
    test.setupInput();
    test.setFeedback('What were the ' + test.wordPairs.length + ' associates?');
  },

  setupInput: function() {
    var html = "<form name=\"word_list_form\"><table>";
    for (var i in test.wordPairs) {
      html += "<tr><td>" + test.wordPairs[i][0] + "</td><td><input type=\"text\" name=\"associate_" + i + "\"></td></tr>";
    }
    html += "</table><input type='button' onclick=\"test.userInputSubmit()\" value='Done' /></form>";
    $("div#associate_input_form").html(html);
  },

  userInputSubmit: function() {
    var i;
    for (i in test.wordPairs) {
      test.userWords[i] = document.forms["word_list_form"]["associate_" + i].value;
    }
    test.hideWordInput();
    test.complete = true;
    test.stopTest();
  },

  hideWordInput: function() {
    $("div#associate_input_form").html("");
  },

  generateResults: function() {
    test._super();
    console.log("saving wordpairs:"+test.wordPairs);
    test.results.wordPairs = test.wordPairs;
    test.results.userWords = test.userWords;
    if (test.parentID) {
      test.results.parentID = test.parentID;
    }
    if (test.pair_id) {
      test.results.pair_id = test.pair_id;
    }
  },

});

var test = new VerbalPairedAssociates();
