// Our version of Sorting.
var Sorting = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.ruleChangeProbability = window.ruleChangeProbability;
    this.maxTrials = window.maxTrials;
    this.ruleLanguage = window.ruleLanguage;
    this.addSortingKeyHandlers();
    this.rules = ["species", "color", "number"];
    this.hebrewRules = ["מין",
		       "צבע",
		       "מספר"];
    this.colors = ["red","green","blue","yellow"];
    this.shapes = ["snail", "jellyfish", "narwhal", "dino"];
    this.numbers = [1,2,3,4];
    this.preload();
    return this;
  },

  addSortingKeyHandlers: function() {
    test.keyHandlers.unshift([[49,50,51,52,65,83,68,70], function(keyCode, keyState) {
      if (keyState === "up") {
        return;
      }
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay")) {
	test.currentTrial.userResponseTime = new Date().getTime();
        if ((keyCode >= 49) && (keyCode <= 52)) {
          test.currentTrial.userResponse = keyCode - 49;
        }
        else {
          var asdfKeys = [65,83,68,70];
          var i;
          for (i = 0; i < 4; ++i) {
            if (keyCode === asdfKeys[i]) {
              test.currentTrial.userResponse = i;
              break;
            }
          }
        }
        test.handleResponse();
      }
    }]);
  },

  isResponseCorrect: function() {
    return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
  },

  chooseStimulus: function() {
    test.currentTrial.stimulusShape = Math.floor(Math.random() * 4);
    test.currentTrial.stimulusNumber = Math.floor(Math.random() * 4);
    test.currentTrial.stimulusColor = Math.floor(Math.random() * 4);
  },

  chooseNewRule: function() {
    while (true) {
      var newRule = Math.floor(Math.random() * 3);
      if (newRule !== test.currentRule) {
        test.currentRule = newRule;
        return;
      }
    }
  },

  generateTrialStimulus: function() {
    test._super();
    test.chooseStimulus();
    test.currentTrial.stimulusProperties = [test.currentTrial.stimulusShape, test.currentTrial.stimulusColor, test.currentTrial.stimulusNumber];
    if (Math.random() < test.ruleChangeProbability) {
      test.chooseNewRule();
    }
    test.currentTrial.rule = test.currentRule;
    test.currentTrial.correctResponse = test.currentTrial.stimulusProperties[test.currentTrial.rule];
    test.fillWithImages($('#stimulus'), test.colors[test.currentTrial.stimulusColor], test.shapes[test.currentTrial.stimulusShape], test.numbers[test.currentTrial.stimulusNumber]);
  },

  fillWithImages: function(elem, color, shape, number) {
    // Arrange the right images in given elem
    elem.empty();
    var url = test.getImageURL(color, shape);
    var ew = elem.width(), eh = elem.height();
    var cw = 75, ch = 75;  // creature img size
    var offsets = [];
    if(number == 1)  // middle
      offsets = [[(ew - cw) / 2, (eh - ch) / 2]];
    else if(number == 2)  // top/bottom
      offsets = [[(ew - cw) / 2, eh / 4 - ch / 2],
                 [(ew - cw) / 2, 3 * eh / 4 - ch / 2]];
    else if(number == 3)  // triangle
      offsets = [[ew / 4 - cw / 2, eh / 4 - ch / 2],
                 [3 * ew / 4 - cw / 2, eh / 4 - ch / 2],
                 [(ew - cw) / 2, 3 * eh / 4 - ch / 2]];
    else if(number == 4)  // square
      offsets = [[ew / 4 - cw / 2, eh / 4 - ch / 2],
                 [3 * ew / 4 - cw / 2, eh / 4 - ch / 2],
                 [ew / 4 - cw / 2, 3 * eh / 4 - ch / 2],
                 [3 * ew / 4 - cw / 2, 3 * eh / 4 - ch / 2]];
    else
      console.log("How many is", number, "anyway?");

    for(var i = 0; i < offsets.length; ++i)
      elem.append($('<img class="creature"/>')
                  .attr('src', url)
                  .css({left: offsets[i][0] + 'px',
                        top: offsets[i][1] + 'px'}));
  },

  getImageURL: function(color, shape) {
    return "/static/images/tests/sorting/" + shape + "_" + color + "_small.png";
  },

  startTest: function() {
    test.currentRule = -1;
    test.chooseNewRule();
    test._super();
  },
  
  startTrial: function() {
    test._super();
  },
    
  hideStimulus: function() {
    $('div#stimulus').removeClass('stimulus_on');
  },

  endTrial: function() {
    $('span#sorting_rule').text('...');
    test.hideStimulus();
    test._super();
  },
    
  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.prestimulusDelay;
  },

  startDisplayStimulus: function() {
    test._super();
    var ruleNames = test.rules;
    if (test.ruleLanguage === 'Hebrew') {
      ruleNames = test.hebrewRules;
    }
    var ruleText = ruleNames[test.currentRule];
    $('span#sorting_rule').text(ruleText);
    $('div#stimulus').addClass('stimulus_on');
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    test._super();
  },

  readyForResponse: function() {
    test._super();
  },

  preload: function() {
    // Preload all 16 species/colors, either into empty preload box,
    // or to the stimulus examples
    for(var i = 0; i < 4; ++i)
      for(var j = 0; j < 4; ++j)
        test.fillWithImages($(i == j ? ('#pattern' + (i + 1)) : '#preload'), test.colors[i], test.shapes[j], test.numbers[i]);
  }

});

var test = new Sorting();
