// Super basic multi-trial test to see how many times they can tap a key in a short time.
var FingerTapping = MultiTrial.extend({
  init: function() {
    this._super();
    this.trialDuration = window.trialDuration;
    this.pauseDuration = window.pauseDuration;
    this.maxTrials = window.maxTrials;
    this.keyState = "up";
    this.switchHands();
  },

  switchHands: function() {
    if(!test.hand)
      test.hand = Math.random() < 0.5 ? 'left' : 'right';
    else
      test.hand = test.hand == 'left' ? 'right' : 'left';
    $('.hand_indicator strong').text(test.hand);
    return test.hand;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[32], function(keyCode, keyState) {
      if(this.keyState == keyState) return;  // no repeat, like holding it down
      this.keyState = keyState;
      if(this.keyState == "up") {
        $('.tap_indicator').toggleClass('tapped', false);
        return;  // mostly just interested in keydown
      }
      
      if(test.state == "notStarted")
        return;
      else if(test.state == "before") {
        test.reallyStartTrial();
      }
      else if(test.state == "inTrial") {
        ++test.currentTrial.tapCount;
        test.currentTrial.taps.push((new Date()).getTime() - test.currentTrial.startTime);
        test.updateStatistics(this);
        $('.tap_indicator').toggleClass('tapped');
      }
      else if(test.state == "after") {
        // do nothing, they're either waiting for new trial, or done
      }
    }]);
  },

  updateStatistics: function() {
    if(test.state != "inTrial") return;
    $(".tap_count span").text(test.currentTrial.tapCount);
    $(".time_left span").text(parseInt((test.trialDuration - (new Date()).getTime() + test.currentTrial.startTime) / 1000));
  },

  startTest: function() {
    test._super();
    test.prepareToStartTrial();
    test.updateStatisticsInterval = setInterval(test.updateStatistics, 100);
  },

  endTest: function() {
    test._super();
    clearInterval(test.updateStatisticsInterval);
  },

  prepareToStartTrial: function() {
    if(test.isTestFinished()) return;
    test.state = "before";
    $('.tap_count').add('.time_left').add('.between_trials').hide();
    test.setFeedback("");
    $('.prepare_yourself').show();
  },
  
  generateTrial: function() {
    test._super();
    test.currentTrial.tapCount = 0;
    test.currentTrial.taps = [];
    test.currentTrial.hand = test.hand;
    return test.currentTrial;
  },

  startTrial: function() {
    if(test.state == 'after') return;  // we'll do this in a second
    //test._super();
    test.previousStartTrial = test._super;
    test.prepareToStartTrial();
  },

  reallyStartTrial: function() {
    test.previousStartTrial();
    setTimeout(test.endTrial, test.trialDuration);
    $('.tap_count').add('.time_left').show();
    $('.prepare_yourself').hide();
    test.updateStatistics();
  },

  endTrial: function() {
    test.state = "after";
    test._super();
    if (!test.isTestFinished()) {
      $('.between_trials').show();
      test.setFeedback("Great!");
      setTimeout(test.prepareToStartTrial, test.pauseDuration);
      test.switchHands();
    }
  }

});

var test = new FingerTapping();