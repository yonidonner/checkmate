// Decision Making
var DecisionMaking = LevelEstimation.extend({

  init: function() {
    this._super();
    this.testSubType = window.testSubType;
    $('span#level_statistic').hide();
    $('span#level_statistic_name').hide();
    /*if (this.testSubType === 'patience') {
	$('span#level_statistic_name').text("Discount factor");
    }*/
    this.maxTrials = 30;
    this.decLambda = window.decLambda;
    this.incDelta = window.incDelta;
    $('div#decision_making_choice_1').hide();
    $('div#decision_making_choice_2').hide();
    $('div#gamble').hide();
    this.a_vals = [0.25, 0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0];
    return this;
  },

  increaseLevel: function() {
    var u = Math.random();
    test.currentLevel *= (1.0 - test.incDelta*Math.log(u));
    //test.currentLevel = (test.multiplier1 + u * (1.0 - test.multiplier1)) * test.currentLevel;
  },

  decreaseLevel: function() {
    var u = Math.random();
    test.currentLevel /= (1.0 - test.decLambda*Math.log(u));
    /*if (test.currentLevel < 1.0) {
	test.currentLevel = 1.0;
    }*/
    //test.currentLevel = (1.0 + u * (test.multiplier2 - 1.0)) * test.currentLevel;
  },

  updateLevel: function() {
    if (!test.currentTrial.catchType) {
        if (test.currentTrial.tookBig) {
          test.increaseLevel();
        }
        else {
          test.decreaseLevel();
        }
    }
  },

  isTestFinished: function() {
    var currentTime = new Date().getTime();
    //console.log("nCorrect: " + test.nCorrect + " nIncorrect: " + test.nIncorrect);
    if ((test.nSmaller > test.minPos) && (test.nLarger > test.minNeg)) {
      return true;
    }
    if (test.nSmaller + test.nLarger >= test.maxTrials) {
      // This sum differs from test.nTrials in that it excludes
      // catch trials.
      return true;
    }
    else {
      return false;
    }
  },

  startTest: function() {
    test.nSmaller = 0;
    test.nLarger = 0;
    test._super();
  },

  startTrial: function() {
    test.stimulusOff();
    test._super();
  },

  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    var transformedLevel = test.currentLevel / (1.0 + test.currentLevel);
    test.currentTrial.catchType =
        Math.random() < .1
      ? Math.random() < .5
        ? 'small'
        : 'big'
      : null;
    var factor =
        test.currentTrial.catchType == 'small'
        // On small catch trials, make the small option dominate,
        // so all subjects should choose the small option. (In
        // this case, smallAmount will actually be bigger than
        // largeAmount.)
      ? 1.13
      : test.currentTrial.catchType == 'big'
        // On big catch trials, make the difference between the
        // options lopsided so that only very extreme preferences
        // would be consistent with choosing the small option.
      ? .03
      : transformedLevel;
    test.currentTrial.bigAmount = Math.floor((Math.random() * 9500) + 500);
    test.currentTrial.smallAmount = Math.floor(test.currentTrial.bigAmount * factor);
    var smallAmountT = "$" + (test.currentTrial.smallAmount / 100).toFixed(2);
    var bigAmountT = "$" + (test.currentTrial.bigAmount / 100).toFixed(2);
    var smallLabel;
    var bigLabel;
    if ((test.testSubType === "patience")) {
	$("div#stimulus").text("Which would you prefer?");
        smallLabel = smallAmountT + " today";
        bigLabel = bigAmountT + " in one month";
    }
    else if (test.testSubType === "shifted_patience") {
        $("div#stimulus").text("Which would you prefer?");
        smallLabel = smallAmountT + " in one month";
        bigLabel = bigAmountT + " in two months";
    }
    else if (test.testSubType === "loss_aversion") {
	$("td#gamble_option_1").html("&#36;" + test.currentTrial.bigAmount.toFixed(2));
	$("td#gamble_option_2").html("&#36;" + test.currentTrial.smallAmount.toFixed(2));
        smallLabel = "Reject"
        bigLabel = "Accept"
    }
    else if (test.testSubType === "risk_aversion") {
        $("div#stimulus").text("Which would you prefer?");
        smallLabel = "Gaining " + smallAmountT
        bigLabel = "95% chance of gaining " + bigAmountT +
            " and 5% chance of nothing"
    }
    test.currentTrial.bigOnLeft = Math.random() < 0.5;
    $("div#decision_making_choice_1").text(test.currentTrial.bigOnLeft
      ? bigLabel : smallLabel);
    $("div#decision_making_choice_2").text(test.currentTrial.bigOnLeft
      ? smallLabel : bigLabel);
    //test.currentTrial.stimulus = (Math.random() < 0.5) ? 0 : 1;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[37, 39], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (keyState === "down") && (test.currentTrial.state == "afterStimulusDisplay")) {
        test.currentTrial.choseLeft = (keyCode == 37);
	test.handleResponse();
      }
    }]);
  },

  endTrial: function() {
    if (!test.currentTrial.catchType) {
        if (test.currentTrial.tookBig) {
          ++test.nLarger;
        }
        else {
          ++test.nSmaller;
        }
    }
    test.updateLevel();
    test.currentTrial.endTime = new Date().getTime();
    test.trials.push(test.currentTrial);
    ++test.nTrials;
    test.updateTrialStatistics();
    //test.updateStatistics;
    if (test.isTestFinished()) {
      test.complete = true;
      test.stopTest();
      test.state = "finished";
    }
    else {
      test.startTrial();
    }
  },

  handleResponse: function() {
    test.currentTrial.ready = true;
    test.currentTrial.correct = true;
    ++test.totalReady;
    test.currentTrial.reactionTime = test.currentTrial.userResponseTime - test.currentTrial.displayEndTime;
    test.currentTrial.tookBig = (test.currentTrial.bigOnLeft == test.currentTrial.choseLeft);
    test.endTrial();
  },

  startDisplayStimulus: function() {
    test._super();
    test.stimulusOn();
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    test._super();
  },

  stimulusOn: function() {
    $('div#stimulus').show();
    if (test.testSubType === "loss_aversion") {
	$('div#gamble').show();
    }
    $('div#decision_making_choice_1').show();
    $('div#decision_making_choice_2').show();
  },

  stimulusOff: function() {
    $('div#stimulus').hide();
    if (test.testSubType === "loss_aversion") {
	$('div#gamble').hide();
    }
    $('div#decision_making_choice_1').hide();
    $('div#decision_making_choice_2').hide();
  },

  readyForResponse: function() {
    test.setFeedback('Make a decision');
  },

});

var test = new DecisionMaking();
