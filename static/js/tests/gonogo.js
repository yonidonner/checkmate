// A brief GoNoGo.
var GoNoGo = MultiTrial.extend({
  init: function() {
    this._super();
    this.stimulusOnTime = window.stimulusOnTime;
    this.interstimulusDelay = window.interstimulusDelay;
    this.firstHalfVariant = window.firstHalfVariant;
    this.secondHalfVariant = window.secondHalfVariant;
    if (this.firstHalfVariant === "random") {
      this.firstHalfVariant = Math.random() < 0.5 ? "boring" : "exciting";
    }
    if (this.secondHalfVariant === "random") {
      this.secondHalfVariant = Math.random() < 0.5 ? "boring" : "exciting";
    }
    console.log(this.firstHalfVariant + " then " + this.secondHalfVariant);
    this.nTotalTrials = window.nTotalTrials; // Should be multiple of 18
    $('div#target').hide();
    $('div#nontarget').hide();
    this.correctStatistic = new BinaryStatistic($('span#correct_statistic'));
    this.RTStatistic = new MeanStdevStatistic($('span#reaction_time_statistic'));
    return this;
  },

  startTest: function() {
    test.generateAllTrials();
    $('div#target').hide();
    $('div#nontarget').hide();
    test.correctStatistic.reset();
    test.RTStatistic.reset();
    test._super();
  },

  generateAllTrials: function() {
    test.GoNoGoTrials = [];
    for (var i = 0; i < test.nTotalTrials; ++i)
      test.GoNoGoTrials[i] = false;

    var permFirstHalf = randomPermutation(test.nTotalTrials / 2);
    var nTotalTargetsFirstHalf = test.nTotalTrials / 18.0 * (this.firstHalfVariant == "boring" ? 2 : 7);
    for (i = 0; i < nTotalTargetsFirstHalf; ++i)
      test.GoNoGoTrials[permFirstHalf[i]] = true;
    var permSecondHalf = randomPermutation(test.nTotalTrials / 2);
    var nTotalTargetsSecondHalf = test.nTotalTrials / 18.0 * (this.secondHalfVariant == "boring" ? 2 : 7);
    for (i = 0; i < nTotalTargetsSecondHalf; ++i)
      test.GoNoGoTrials[permSecondHalf[i] + test.nTotalTrials / 2] = true;

    console.log('trials: ' + test.GoNoGoTrials);
  },

  startTrial: function() {
    test._super();
    $('span#trial_number').text(test.nTrials+1+"/"+test.nTotalTrials);
    test.displayGoNoGoStimulus();
  },

  displayGoNoGoStimulus: function() {
    if (test.currentTrial.isTarget) {
      $('div#target').show();
    }
    else {
      $('div#nontarget').show();
    }
    setTimeout(test.hideGoNoGoStimulus, test.stimulusOnTime);
  },

  hideGoNoGoStimulus: function() {
    $('div#target').hide();
    $('div#nontarget').hide();
    setTimeout(test.endTrial, test.interstimulusDelay - test.stimulusOnTime);
  },

  generateTrial: function() {
    test._super();
    test.currentTrial.isTarget = test.GoNoGoTrials[test.currentTrial.n];
    test.currentTrial.userResponse = false;
    test.currentTrial.state = "running";
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[32,49], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (test.currentTrial.state === "running") && (keyState === "down")) {
        test.currentTrial.state = "done";
        test.currentTrial.userResponse = true;
        test.currentTrial.userResponseTime = new Date().getTime();
        test.setFeedback(test.currentTrial.isTarget ? "Right!" : "Wrong :(");
        test.setFeedback(""); // hide it, just trigger the flash
      }
    }]);
  },

  isTestFinished: function() {
    return test.nTrials === test.nTotalTrials;
  },

  endTrial: function() {
    test.currentTrial.responseCorrect = (test.currentTrial.isTarget ? test.currentTrial.userResponse : !test.currentTrial.userResponse);
    if(!test.currentTrial.userResponse && !test.currentTrial.responseCorrect) {
      test.setFeedback("Wrong :(");
      test.setFeedback(""); // hide it, just trigger the flash
    }

    test.correctStatistic.add(test.currentTrial.responseCorrect);
    if (test.currentTrial.userResponse) {
      test.currentTrial.reactionTime = test.currentTrial.userResponseTime - test.currentTrial.startTime;
      test.RTStatistic.add(test.currentTrial.reactionTime);
    }
    test._super();
  },
    
  generateResults: function() {
    test._super();
    test.results.firstHalfVariant = test.firstHalfVariant;
    test.results.secondHalfVariant = test.secondHalfVariant;
  }
});

var test = new GoNoGo();