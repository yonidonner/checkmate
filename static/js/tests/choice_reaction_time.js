// A ReactionTime test which has nChoices possible stimuli.
var ChoiceReaction = ReactionTime.extend({
  init: function() {
    this._super();
    this.nChoices = window.numChoices;
    return this;
  },

  startTest: function() {
    test.resetStimuli();
    test._super();
  },

  resetStimuli: function() {
    for (var i = 0; i < test.nChoices; ++i) {
      $('div#choice_reaction_stimulus_'+(i+1)).removeClass('stimulus_on');
    }
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.type = Math.floor(Math.random()*test.nChoices)+1;
  },

  displayStimulus: function() {
    $('div#choice_reaction_stimulus_'+test.currentTrial.type).addClass('stimulus_on');
  },
  
  hideStimulus: function() {
    $('div#choice_reaction_stimulus_'+test.currentTrial.type).removeClass('stimulus_on');
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[49, 50, 51, 97, 98, 99, 65, 83, 68], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (keyState === "down")) {
	test.currentTrial.userResponseTime = new Date().getTime();
        if ((keyCode >= 49) && (keyCode <= 51)) {
	  test.currentTrial.userResponse = (keyCode - 48);
        }
        else {
          if ((keyCode >= 97) && (keyCode <= 99)) {
	    test.currentTrial.userResponse = (keyCode - 96);
          }
          else {
            if (keyCode == 65) {
              test.currentTrial.userResponse = 1;
            }
            else {
              if (keyCode == 83) {
                test.currentTrial.userResponse = 2;
              }
              else {
                if (keyCode == 68) {
                  test.currentTrial.userResponse = 3;
                }
              }
            }
          }
        }
	test.handleResponse();
      }
    }]);
  },

  isResponseCorrect: function() {
    return (test.currentTrial.type === test.currentTrial.userResponse);
  }
});

var test = new ChoiceReaction();

