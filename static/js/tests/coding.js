// Coding.
var Coding = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    if (window.imageSet) {
      this.imageSet = window.imageSet;
    }
    else {
      this.imageSet = "default";
    }
    if (this.imageSet === "default") {
      this.nImages = 1000;
    }
    else if (this.imageSet === "digit_symbol") {
      this.nImages = 9;
    }
    else if (this.imageSet === "blots") {
      this.nImages = 9;
    }
    this.allowSequences = window.allowSequences;
    $('div#ready_statistic').hide();
    $('div#coding_key').hide();
    $('div#current_symbol').hide();
    return this;
  },

  imageURL: function(imageNum) {
    if (test.imageSet === "default") {
      return "/static/images/bts/bt_" + String("00" + imageNum).slice(-3)+".png";
    }
    else if (test.imageSet === "digit_symbol") {
      return "/static/images/tests/coding/DigitSymbol" + (1+imageNum) + ".png";
    }
    else if (test.imageSet === "blots") {
      return "/static/images/tests/coding/blot" + (1+imageNum) + ".png";
    }
  },
  
  loadImages: function() {
    var imgs = "";
    for (var i = 0; i < test.permutation.length; ++i) {
      imgs = imgs + '<td><img src="' + test.imageURL(test.permutation[i]) + '" width="32" height="32"></td>';
    }
    //console.log("Imgs = " + imgs);
    $('tr#coding_key').html(imgs);
  },

  addKeyHandlers: function() {
    test._super();
    var varkeys = [];
    var asdfKeys = [65, 83, 68, 70, 71, 72, 74, 75, 76];
    var i;
    for (i = 1; i < 10; ++i) {
      varkeys.push(48 + i);
      varkeys.push(asdfKeys[i-1]);
    }
    test.keyHandlers.unshift([varkeys, function(keyCode, keyState) {
      if (keyState === "up") {
        return;
      }
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay")) {
        if (test.isRunning) {
	  test.currentTrial.userResponseTime = new Date().getTime();
          if ((keyCode >= 49) && (keyCode <= 57)) {
            test.currentTrial.userResponse = keyCode - 49;
          }
          else {
            for (i = 0; i < 9; ++i) {
              if (keyCode === asdfKeys[i]) {
                test.currentTrial.userResponse = i;
                break;
              }
            }
          }
          test.handleResponse();
        }
      }
    }]);
  },

  isResponseCorrect: function() {
    return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
  },

  generateTrialStimulus: function() {
    test._super();
    if ((test.allowSequences) || (test.previousStimulus === -1)) {
      test.currentTrial.correctResponse = Math.floor(Math.random() * 9);
    }
    else {
      test.currentTrial.correctResponse = Math.floor(Math.random() * 8);
      if (test.currentTrial.correctResponse >= test.previousStimulus) {
        ++test.currentTrial.correctResponse;
      }
    }
    test.previousStimulus = test.currentTrial.correctResponse;
    //var url = $('#current_symbol img').attr('src');
    //url = url.replace(/\d\.png/, test(1+test.permutation[test.currentTrial.correctResponse]) + '.png');
    url = test.imageURL(test.permutation[test.currentTrial.correctResponse]);
    $('#current_symbol img').attr('src', url);
  },

  startTest: function() {
    test.permutation = randomPermutation(test.nImages).slice(0,9);
    test.loadImages();
    test.previousStimulus = -1;
    $('div#coding_key').show();
    test._super();
  },
  
  startTrial: function() {
    test._super();
  },
    
  hideStimulus: function() {
    $('div#current_symbol').hide();
  },

  endTrial: function() {
    test.hideStimulus();
    test._super();
  },
    
  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.prestimulusDelay;
  },

  startDisplayStimulus: function() {
    test._super();
    $('div#current_symbol').show();
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    test._super();
  },

  readyForResponse: function() {
    //test._super();  // don't display the 'Go!' message
  }

});

var test = new Coding();