// Span tests are Level Estimation tests where the estimated level is the user's span, the length of a sequence they can repeat forwards or backwards.
var Span = LevelEstimation.extend({
  init: function() {
    this._super();
    // TODO: read initialLevel somehow
    $('span#level_statistic_name').text("Span");
    this.isBackward = window.isBackward;
    //this.targetSuccessProbability = 0.5;
    //this.prestimulusDelay = 500;
    this.elementOnTime = window.elementOnTime; // 1000;
    this.elementOffTime = window.elementOffTime; // 500;
    return this;
  },

    startTest: function() {
	test.currentLevel = test.initialLevel;
	test._super();
    },

    generateTrial: function() {
	test._super();
    },

    generateTrialDelay: function() {
	test.currentTrial.delay = test.prestimulusDelay;
    },

    generateTrialStimulus: function() {
	test._super();
	test.generateStimulusSequence();
    },

    generateStimulusSequence: function() {
	test.currentTrial.sequence = new Array();
    },

    startDisplayStimulus: function() {
  	test._super();
	test.setupDisplaySequence();
    },

    setupDisplaySequence: function() {
	test.currentDisplayedElement = -1;
	test.displayNextSequenceElement();
    },

    displayNextSequenceElement: function() {
	if (++test.currentDisplayedElement == test.currentTrial.level) {
	    test.stopStimulusDisplay();
	}
	else {
	    test.displayedElement = test.currentTrial.sequence[test.currentDisplayedElement];
	    test.displaySequenceElement();
	}
    },

    displaySequenceElement: function() {
	test.showSequenceElement(test.displayedElement);
	setTimeout(test.afterDisplaySequenceElement, test.elementOnTime);
    },

    afterDisplaySequenceElement: function() {
	test.unshowSequenceElement(test.displayedElement);
	setTimeout(test.displayNextSequenceElement, test.elementOffTime);
    },

    readyForResponse: function() {
	test._super();
	test.currentTrial.userSequence = new Array();
    },

    isResponseCorrect: function() {
      if (test.isBackward) {
	    test.currentTrial.origUserSequence = test.currentTrial.userSequence.slice(0);
	    test.currentTrial.userSequence.reverse();
	}
	//console.log("Real sequence: " + test.currentTrial.sequence.toString() + "; user sequence: " + test.currentTrial.userSequence.toString());
	return (test.currentTrial.sequence.toString() === test.currentTrial.userSequence.toString());
    },

    responseCorrect: function() {
	test._super();
	test.setFeedback('Correct!');
    }

});
