// A ReactionTime test which has just one simple stimulus.
var SimpleReactionTime = ReactionTime.extend({
  init: function() {
    this._super();
    $('div#correct_statistic').hide();
    return this;
  },

  startTest: function() {
    test.hideStimulus();
    test._super();
  },

  displayStimulus: function() {
    $('div#simple_reaction_time_stimulus').addClass('stimulus_on');
  },
  
  hideStimulus: function() {
    $('div#simple_reaction_time_stimulus').removeClass('stimulus_on');
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[32,49], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (keyState === "down")) {
	test.currentTrial.userResponseTime = new Date().getTime();
	test.handleResponse();
      }
    }]);
  },

  isResponseCorrect: function() {
    return true;
  }
});

var test = new SimpleReactionTime();