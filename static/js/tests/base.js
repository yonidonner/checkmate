// Base test class that all tests will inherit from. Needs class.js for this type of OO to work.
var Base = Class.extend({
  init: function() {
    window.test = this;
    if (!window.show_statistics) {
      $('div#all_statistics').hide();
    }
    /*  Turning this off only if it's right/wrong message in setFeedback instead
    if (!window.show_feedback) {
      $('div#all_feedback').hide();
    }
    */
    this.test = this; // Now we can use "test" from either context. Not so hard.
    this.anon_session = window.anon_session;
    if (this.anon_session) {
      this.session_id = window.session_id;
    }
    this.name = name;
    this.startButtonDelay = window.start_button_delay;
    this.isPractice = window.is_practice;
    $('span#test_content_header').text("");
    // In order to make this work, I also modified class.js to always use
    // window.test as a context if we were in the window context.
    this.addKeyHandlers();
    this.bindKeyHandlers();
    this.bindButtons();
    this.initResults();
    return this;
  },
  
  addKeyHandlers: function() {
    // Set site-wide keyhandlers. The keyHandlers property is a list of lists, where the first element in the inner lists is itself a list of the keycodes that correspond to the function, which is the second element. Each extending method can add on a new list with keycodes and a handler function, and should append it to the *front* of the list using test.keyHandlers.unshift(... so that it takes priority over any more generic handlers.
    if (typeof(test.keyHandlers) == "undefined")
      test.keyHandlers = [];
    /* example
    test.keyHandlers.unshift([[32], function(keyCode, keyState) {
      if(keyState === 'down') {
        
        alert('Space bar was pressed.');
      }
    }]);
    */
    return this;
  },

  bindKeyHandlers: function() {
    // Function to actually handle keydown
    var test = this;
    var onKeyDown = function(event) {
      if(event.metaKey || event.ctrlKey || event.altKey) return;
      var keyCode = event.keyCode; // event.which
      for (i in test.keyHandlers) {
        if ($.inArray(keyCode, test.keyHandlers[i][0]) != -1) {
	  //console.log("keydown: " + event.keyCode);
          test.keyHandlers[i][1](keyCode, 'down');
          if(event.preventDefault) {
            event.preventDefault();
          }
          else {
            event.returnValue = false;
          }
	  //event.preventDefault();
          break;
        }
      }
    };
    var onKeyUp = function(event) {
      if(event.metaKey || event.ctrlKey || event.altKey) return;
      for (i in test.keyHandlers) {
        if ($.inArray(event.which, test.keyHandlers[i][0]) != -1) {
          test.keyHandlers[i][1](event.which, 'up');
          if(event.preventDefault) {
            event.preventDefault();
          }
          else {
            event.returnValue = false;
          }
          break;
        }
      }
    };
    try {
      if (window.addEventListener) {
        window.addEventListener("keydown", onKeyDown, true);
        window.addEventListener("keyup", onKeyUp, true);
      } else if (document.attachEvent) { // IE 
        document.attachEvent("onkeydown", onKeyDown);
        document.attachEvent("onkeyup", onKeyUp);
      } else {
        document.addEventListener("keydown", onKeyDown, true);
        document.addEventListener("keyup", onKeyUp, true);
      }
    } catch (e) {
      alert(e);
    }
    //$(window).keydown(onKeyDown);
    //$(window).keyup(onKeyUp);
  },

  bindButtons: function() {
    // Bind the start button to test.startTest
    var test = this;
    $('#start_button').click(function() {
      $(this).hide();
      test.startTest0();
    });
    return this;
  },

  startTest0: function() {
    $('.test_content').parent().addClass('test_started');
    test.countDown = 3;
    $('span#test_content_header').text(test.countDown);
    setTimeout(test.startTest1, test.startButtonDelay);
  },

  startTest1: function() {
    if (--test.countDown === 0) {
      $('span#test_content_header').text("");
      test.startTest();
    }
    else {
      $('span#test_content_header').text(test.countDown);
      setTimeout(test.startTest1, test.startButtonDelay);
    }
  },

  startTest: function() {    
    test.results.startTime = new Date().getTime();
    test.complete = false;
    test.updateDurationStatistic();
  },

  updateDurationStatistic: function() {
    if(test.complete) return;
    var duration = new Date().getTime() - test.results.startTime;
    var total_seconds = Math.floor(duration / 1000);
    var minutes = Math.floor(total_seconds / 60);
    var seconds = total_seconds % 60;
    $('span#test_duration_statistic').text(minutes.toString() + ':' + ((seconds < 10) ? '0' : '') + seconds.toString());
    setTimeout(test.updateDurationStatistic, 1000);
  },	

  stopTest: function() {
    $('.test_content').parent().removeClass('test_started');
    if (test.complete) {
      test.endTest();
    }
  },
  
  updateConfig: function() {
  },

  endTest: function() {
    test.keyHandlers = [];
    test.generateResults();
    test.uploadResults();
  },

  uploadResults: function() {
    $('.test_content').parent().addClass('test_finished');
    $('.test_content').html('<p>Uploading test results...</p>');
    test.uploadRetriesLeft = 5;
    test.uploadResults1();
  },

  retryDelay1: function() {
    if (--test.uploadRetryCountdown >= 0) {
      $('.test_content').html('<p>Error uploading. Retrying in ' + (test.uploadRetryCountdown) + ' seconds.<p><p>' + (test.uploadRetriesLeft + 0) + ' attempts left.</p>');
      setTimeout(test.retryDelay1, 1000);
    }
    else {
      $('.test_content').html('<p>Uploading test results...</p>');
      setTimeout(test.uploadResults1, 1000);
    }
  },

  uploadResults1: function() {
    if (--test.uploadRetriesLeft >= 0) {
      rpc("test_finished", [window.test_name, window.test_variant_name, test.results, test.user_test_state], function(response) {
        $('.test_content').html(response);
      },
	 function(response) {
           test.uploadRetryCountdown = 12;
           test.retryDelay1();
         });
    }
    else {
      $('.test_content').html('<p>Failed uploading results.</p><p>Please make sure you are connected to the internet.</p><p>If you think this is a bug, please <a href="/contact">let us know</a>.</p><p><button class="btn-large btn btn-primary" id="upload_retry_button">Retry</button></p>');
      $('button#upload_retry_button').click(function() { test.uploadResults(); });
    }
    /*
    test.updateConfig();
    console.log("Going to submit RPC", test.name, "with results:", test.results);
    rpc("store_result", [test.battery.name, test.name, test.results, test.config], function(response) {
      if (response.success) {
        $('.statistics').append('<div>Saved to server and got response: "' + response.message + '"</div>');
      } else {
        console.log('An error occurred: ' + response.message);
      };
    });
    */
  },

  initResults: function() {
    test.results = {};
    test.user_test_state = {};
  },

  generateResults: function() {
    test.results.endTime = new Date().getTime();
    test.results.isPractice = test.isPractice;
    if (test.anon_session) {
      test.results.anonSession = true;
      test.results.sessionId = test.session_id;
    }
  },

  // Send immediate feedback on the last trial to the user. Ex.: "Oops, too early!", "312 ms", etc.
  setFeedback: function(feedback) {
    var incorrect = feedback.search(/incorrect|wrong|too early/i) != -1;
    var correct = !incorrect && feedback.search(/correct|right|great/i) != -1;
    if(correct || incorrect) {
      var color = $.Color(correct ? "rgba(50, 255, 100, 0.25)"
                                  : "rgba(255, 50, 100, 0.25)");
      $('.test_left > div:not(.test_content)').css("backgroundColor", color).animate({backgroundColor: $.Color("white")}, 150);
    }

    if(window.show_feedback || !(correct || incorrect))
      $('.feedback_message').html('<h3>' + feedback + '</h3>');
    else
      $('.feedback_message').html('');
  },

  preloadAudioFiles: function(filenames) {
    for(var i = 0; i < filenames.length; ++i) {
      var filename = filenames[i];
      /* doesn't work--requests pile up and get canceled
      var audioElement = document.createElement('audio');
      audioElement.setAttribute('src', '/static/sounds/' + filename + '.mp3');
      audioElement.setAttribute('preload', 'auto');
      $('body').append(audioElement);
      console.log("preloading audio", audioElement.getAttribute('src'));
      */
      /* doesn't work--interrupts currently playing sound
      var func = function(f) {
        return function() { console.log('preloading', f); $("#jplayer").jPlayer("setMedia", {mp3: f}); };
      }('/static/sounds/' + filename + '.mp3');
      setTimeout(func, 1000 * i);
      */
      // TODO: just use an audio sprite for this, so we can preload just one file (and of course have better audio)
    }
  },

  playAudio: function(filename) {
    $("#jplayer").jPlayer("setMedia", {mp3: '/static/sounds/' + filename + '.mp3'}).jPlayer("play");
    console.log("playing audio", filename);
    // TODO: fall back to ogg versions if we are in, say, Firefox or Opera
    // http://html5doctor.com/native-audio-in-the-browser/
  }
});
