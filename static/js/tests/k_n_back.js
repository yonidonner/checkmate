// k modalities n-back.
var KNBack = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.k = window.config_k; // default is just a single modality
    this.n = window.config_n; // default is 1-back
    this.initDefaultKNBackKeys();
    this.setupKNBackKeyHandlers();
    this.pace = window.pace; // set to 0 for self-paced
    this.bufferOnTime = window.bufferOnTime;
    this.bufferOffTime = window.bufferOffTime;
    this.maxTrials = window.maxTrials;
    this.wantUserInput = false;
    $('div#ready_statistic').hide();
    return this;
  },

  setupInstructionsAndStatistics: function() {
    $('span#test_n').text(this.n + " trial" + ((this.n > 1) ? "s" : ""));
    $('span#instructions1').hide();
    $('span#instructions2').hide();
    $('span#instructions3').hide();
    if ((test.k === 1) && (test.pace === 0)) {
      $('span#instructions1').show();
    }
    else if (test.k === 1) {
      $('span#instructions2').show();
    }
    else {
      $('span#instructions3').show();
    }
    if (this.pace > 0) {
      $('div#reaction_time_statistic').hide();
    }
  },

  initDefaultKNBackKeys: function() {
    test.keys = [];
    var i;
    for (i = 0; i < 9; ++i) {
      test.keys[i] = [];
      test.keys[i].push(65+i);
      test.keys[i].push(49+i);
    }
    test.keys[0].push(37);
    test.keys[1].push(39);
  },

  setupKNBackKeyHandlers: function() {
    test.keyCodeToNumber = [];
    test.allKeyCodes = [];
    var i,j;
    for (i in test.keys) {
      for (j in test.keys[i]) {
        test.keyCodeToNumber[test.keys[i][j]] = i;
        test.allKeyCodes.push(test.keys[i][j]);
      }
    }
    test.keyHandlers.unshift([test.allKeyCodes, function(keyCode, keyState) {
      if ((test.wantUserInput) && (keyState === "down")) {
        test.userKey(test.keyCodeToNumber[keyCode]);
      }
    }]);
  },

  userKey: function(number) {
    console.log('got user key', number);
    if (test.pace > 0) {
      // user flips the corresponding modality
      if (number < test.k) {
        test.currentTrial.userResponse[number] = !test.currentTrial.userResponse[number];
      }
    }
    else {
      // self paced
      if (number < 2*test.k) {
        var whichModality = Math.floor(number / 2);
        var whichDirection = number % 2;
        test.currentTrial.userResponse[whichModality] = ((whichDirection == 0) ? false : true);
        if (!test.currentTrial.responseGiven[i]) {
          test.currentTrial.responseGiven[i] = true;
          if (--test.currentTrial.responsesMissing === 0) {
            test.finishTrial();
          }
        }
      }
    }
  },

  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrial: function() {
    test._super();
    test.wantUserInput = false;
    test.currentTrial.userResponse = [];
    if (test.pace === 0) {
      test.currentTrial.responseGiven = [];
      test.currentTrial.responsesMissing = test.k;
    }
    var i;
    for (i = 0; i < test.k; ++i) {
      test.currentTrial.userResponse[i] = false;
      if (test.pace === 0) {
        test.currentTrial.responseGiven[i] = false;
      }
    }
  },

  generateTrialStimulus: function() {
    test.generateStimulus();
    test.buffer.unshift(test.currentStimulus);
    if (test.buffer.length > (1+test.n)) {
      test.buffer.splice(test.n+1, 1);
    }
    test.currentTrial.stimulus = test.currentStimulus.slice(0);
    test.currentTrial.stimulus_history = test.buffer.slice(1);
    test.currentTrial.correctAnswer = [];
    var i;
    for (i = 0; i < test.k; ++i) {
      test.currentTrial.correctAnswer[i] = (test.currentTrial.stimulus[i] == test.buffer[test.n][i]);
    }
  },

  generateStimulus: function() {
    test.currentStimulus = [];
    test.currentStimulus[0] = 0;
  },

  isResponseCorrect: function() {
    var i;
    for (i = 0; i < test.k; ++i) {
      if (test.currentTrial.userResponse[i] !== test.currentTrial.correctAnswer[i]) {
        return false;
      }
    }
    return true;
  },

  readyForResponse: function() {
    test._super();
    test.wantUserInput = true;
    if (test.pace > 0) {
      setTimeout(test.trialTimedOut, test.pace);
    }
  },

  trialTimedOut: function() {
    test.finishTrial();
  },

  finishTrial: function() {
    test.currentTrial.userResponseTime = new Date().getTime();
    test.wantUserInput = false;
    test.clearStimulus();
    test.handleResponse();
  },

  startDisplayStimulus: function() {
    test._super();
    if (test.currentTrial.n === 0) {
      test.showInitialBuffer();
    }
    else {
      test.showCurrentStimulus();
    }
  },

  startTest: function() {
    test.generateBuffer();
    test._super();
  },

  generateBuffer: function() {
    test.buffer = [];
    var i;
    for (i = 0; i < test.n; ++i) {
      test.generateStimulus();
      test.buffer.unshift(test.currentStimulus.slice(0));
    }
  },

  showInitialBuffer: function() {
    $('div#k_n_back_information').text("Remember these first stimuli");
    test.sequenceTimer = SequenceTimer(test.bufferOnTime, test.bufferOffTime, test.displayCallback, test.hideCallback, test.doneCallback);
    startSequence(test.sequenceTimer, test.n);
  },

  displayCallback: function(index) {
    test.showStimulus(test.buffer[test.n - index]);
  },

  hideCallback: function(index) {
    test.clearStimulus();
  },

  doneCallback: function() {
    test.showCurrentStimulus();
  },

  showCurrentStimulus: function() {
    $('div#k_n_back_information').text("");
    test.showStimulus(test.buffer[0]);
    test.stopStimulusDisplay();
  }

});

