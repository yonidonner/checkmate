// A test that has multiple trials.
var MultiTrial = Base.extend({
  init: function() {
    this._super();
    this.state = "notStarted";
    return this;
  },

  generateTrial: function() {
    //console.log("generate trial", test.nTrials);
    test.currentTrial = {
      n: test.nTrials,
      startTime: new Date().getTime()
    };
    return test.currentTrial;
  },

  startTrial: function() {
    //console.log("start trial", test.nTrials);
    $('span#trial_number').text(test.nTrials+1);
    test.state = "inTrial";
    test.generateTrial();
    //    if(typeof(test.currentTrial) == "undefined")
    //      test.generateTrial();
  },

  updateTrialStatistics: function() {
  },

  endTrial: function() {
    //console.log("end trial", test.nTrials);
    test.currentTrial.endTime = new Date().getTime();
    test.trials.push(test.currentTrial);
    ++test.nTrials;
    test.updateTrialStatistics();
    //test.updateStatistics;
    if (test.isTestFinished()) {
      test.complete = true;
      test.stopTest();
      test.state = "finished";
    }
    else {
      test.startTrial();
    }
  },

  isTestFinished: function() {
    if(test.maxTrials)
      return test.nTrials >= test.maxTrials;
    return false;
  },

  startTest: function() {
    test.nTrials = 0;
    test.trials = [];
    test._super();
    test.startTrial();
  },

  stopTest: function() {
    test._super();
  },

  generateResults: function() {
    test._super();
    test.results.trials = test.trials;
  }
});
