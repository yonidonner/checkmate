// Design Recognition: the user should recognize (multiple-choice) a binary design.
var DesignRecognition = LevelEstimation.extend({
  init: function() {
    this.nChoices = window.nChoices;
    this.nRows = window.nRows;
    this.nCols = window.nCols;
    if (!this.gridCellSize1) {
      this.gridCellSize1 = 14;
      this.gridCellSize2 = 8;
    }
    this._super();
    this.guessingProbability = 0.25;
    this.grid = new Grid(this.nRows, this.nCols, this.gridCellSize1);
    this.answerGrids = new Array();
    for (var i = 0; i < this.nChoices; ++i) {
      this.answerGrids[i] = new Grid(this.nRows, this.nCols, this.gridCellSize2);
    }
    $('span#level_statistic_name').text("Complexity");
    $('div#choices_area').hide();
    this.stimulusDisplayTime = window.stimulusDisplayTime;
    this.maskRepeats = window.maskRepeats;
    this.maskRepeatTime = window.maskRepeatTime;
    this.addClickHandlers();
    return this;
  },

  initConfig: function() {
    test._super();
    if (test.battery.config.nRows != null && test.battery.config.nCols != null) {
      this.nRows = test.battery.config.nRows;
      this.nCols = test.battery.config.nCols;
    } else {
      this.nRows = 8;
      this.nCols = 8;
    }
  },

  addKeyHandlers: function() {
    test._super();
    var keys = [];
    for (var i = 0; i < test.nChoices; ++i) {
      keys.push(49 + i);
    }
    test.keyHandlers.unshift([keys, function(keyCode, keyState) {
      if (keyState === "up") {
        return;
      }
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay")) {
	test.currentTrial.userResponseTime = new Date().getTime();
        test.currentTrial.userResponse = keyCode - 49;
        test.handleResponse();
      }
    }]);
  },

  addClickHandlers: function() {
    for (var i = 0; i < test.nChoices; ++i) {
      $('#choice' + (i + 1)).click(function() {
        if(test.state === "inTrial" && test.currentTrial.state === "afterStimulusDisplay") {
          var clicked = parseInt($(this).attr('id').replace(/[^\d]/g, '')) - 1;
	  test.currentTrial.userResponseTime = new Date().getTime();
          test.currentTrial.userResponse = clicked;
          test.handleResponse();
        }
      });
    }
  },

  startTrial: function() {
    $('div#choices_area').hide();
    test._super();
  },

  decreaseLevel: function() {
    if (test.currentLevel > 2) {
      test.currentLevel = test.currentLevel - 1;
    }
  },


  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    //var originalDesign = randomizeDesign(test.nRows, test.nCols, test.currentLevel);
    //var mutations = mutateDesign(originalDesign, test.nChoices);
    var mutations = greedyGrid(test.nRows, test.nCols, test.currentLevel, test.nChoices);
    test.currentTrial.choices = mutations;
    test.currentTrial.correctResponse = Math.floor(Math.random()*test.nChoices);
    test.currentTrial.design = test.currentTrial.choices[test.currentTrial.correctResponse];
  },

  startDisplayStimulus: function() {
    test._super();
    gridCreate(test.grid, $('div#stimulus'));
    gridDisplayDesign(test.grid, test.currentTrial.design);
    setTimeout(test.maskStimulus, test.stimulusDisplayTime);
  },

  maskStimulus: function() {
    gridMask(test.grid, test.maskRepeats, test.maskRepeatTime, test.afterMaskStimulus);
  },

  afterMaskStimulus: function() {
    test.stopStimulusDisplay();
  },

  stopStimulusDisplay: function() {
    gridStopMasking(test.grid);
    removeGrid(test.grid);
    test._super();
  },

  readyForResponse: function() {
    test._super();
    for (var i = 0; i < test.nChoices; ++i) {
      gridCreate(test.answerGrids[i], $('span#choice'+(i+1)));
      gridDisplayDesign(test.answerGrids[i], test.currentTrial.choices[i]);
      $('div#choices_area').show();
    }
  },

  isResponseCorrect: function() {
    return (test.currentTrial.userResponse === test.currentTrial.correctResponse);
  },

  endTrial: function() {
    removeGrid(test.grid);
    for (var i = 0; i < test.nChoices; ++i) {
      removeGrid(test.answerGrids[i]);
    }
    test._super();
  }

});

if (window.test_name === "design_recognition") {
  var test = new DesignRecognition();
}
