// Verbal Learning: the subject needs to learn and then recall a list of words.
var VerbalLearning = Base.extend({
  init: function() {
    this._super();
    this.maxLearningTrials = window.maxLearningTrials;
    this.wordOnTime = window.wordOnTime;
    this.wordOffTime = window.wordOffTime;
    this.testPart = window.testPart;
    this.wordList = window.wordList;
    this.fixedListSize = window.fixedListSize;
    if (window.wordsShown) {
      this.wordsShown = window.wordsShown;
    }
    if (window.parentID) {
      test.parentID = window.parentID;
    }
    if (window.pair_id) {
      test.pair_id = window.pair_id;
    }
    return this;
  },
  
  startTest: function() {
    test._super();
    if (test.testPart === 0) {
      test.sequenceTimer = SequenceTimer(test.wordOnTime, test.wordOffTime, test.displayCallback, test.hideCallback, test.doneCallback);
      test.userWords = new Array();
      test.shownWords = new Array();
      test.subTrial = 0;
      if (test.fixedListSize > 0) {
        test.firstNewWord = test.fixedListSize;
        test.gotThem = new Array();
        test.userWordsCorrect = new Array();
        test.currentWords = new Array();
        var i;
        for (i = 0; i < test.fixedListSize; ++i) {
          test.currentWords[i] = i;
        }
      }
      test.startSubtrial();
    }
    else {
      test.showWordInput();
      test.setFeedback('What were the words you learned last time?');
    }
  },

  startSubtrial: function() {
    var sequenceLength = (test.fixedListSize === 0) ? test.wordList.length : test.fixedListSize;
    startSequence(test.sequenceTimer, sequenceLength);
  },

  displayCallback: function(index) {
    var currentWord;
    if (test.fixedListSize === 0) {
      currentWord = test.wordList[index];
    }
    else {
      currentWord = test.wordList[test.currentWords[index]];
    }
    if (index === 0) {
      test.shownWords[test.subTrial] = new Array();
    }
    test.shownWords[test.subTrial][index] = currentWord;
    $('div#stimulus').text(currentWord);
  },
  
  hideCallback: function(index) {
    $('div#stimulus').text("");
  },

  doneCallback: function() {
    test.showWordInput();
    var numberOfWords = (test.fixedListSize === 0) ? test.wordList.length : test.fixedListSize;
    test.setFeedback('What were the ' + numberOfWords + ' words?');
  },

  setupTextArea: function() {
    var numberOfWords;
    if (test.testPart === 0) {
      numberOfWords = (test.fixedListSize === 0) ? test.wordList.length : test.fixedListSize;
    }
    else {
      numberOfWords = (test.wordsShown) ? test.wordsShown : test.wordList.length;
    }
    $("div#word_list_input_form").html("<form name=\"word_list_form\"><textarea rows=\"" + numberOfWords + "\" name=\"user_words\"></textarea><input type='button' onclick=\"test.userWordsSubmit()\" value='Done' /></form>");
  },

  userWordsSubmit: function() {
    userWordsText = document.forms["word_list_form"]["user_words"].value;
    test.hideWordInput();
    userWords = userWordsText.split(/\W+/);
    while (userWords[0] === "") {
      userWords.splice(0, 1);
    }
    if (test.testPart === 0) {
      test.userWords[test.subTrial] = userWords;
      test.doneSubtrial();
    }
    else {
      test.userWords = userWords;
      test.complete = true;
      test.stopTest();
    }
  },

  checkWords: function(list1, list2) {
    var slist1 = list1.slice(0).sort();
    var slist2 = list2.slice(0).sort();
    if (list1.length !== list2.length) {
      return false;
    }
    for (i in slist1) {
      if (slist1[i] !== slist2[i]) {
        return false;
      }
    }
    return true;
  },

  doneSubtrial: function() {
    if (test.fixedListSize === 0) {
      allCorrect = test.checkWords(test.wordList, test.userWords[test.subTrial]);
      if (allCorrect) {
        test.complete = true;
        test.stopTest();
      }
      else if (++test.subTrial === test.maxLearningTrials) {
        test.complete = true;
        test.stopTest();
      }
      else {
        test.startSubtrial();
      }
    }
    else {
      var i1,i2;
      test.gotThem[test.subTrial] = new Array();
      test.userWordsCorrect[test.subTrial] = new Array();
      for (i1 = 0; i1 < test.userWords[test.subTrial].length; ++i1) {
        test.userWordsCorrect[test.subTrial][i1] = false;
      }
      for (i1 = 0; i1 < test.fixedListSize; ++i1) {
        test.gotThem[test.subTrial][i1] = false;
        for (i2 = 0; i2 < test.userWords[test.subTrial].length; ++i2) {
          if (test.userWords[test.subTrial][i2] === test.wordList[test.currentWords[i1]]) {
            test.gotThem[test.subTrial][i1] = true;
            test.userWordsCorrect[test.subTrial][i2] = true;
            break;
          }
        }
        if (test.gotThem[test.subTrial][i1]) {
          // replace the word
          test.currentWords[i1] = test.firstNewWord++;
        }
      }
      if (++test.subTrial === test.maxLearningTrials) {
        test.complete = true;
        test.stopTest();
      }
      else {
        test.startSubtrial();
      }
    }
  },

  showWordInput: function() {
    test.setupTextArea();
  },

  hideWordInput: function() {
    $('div#word_list_input_form').html("");
  },

  generateResults: function() {
    test._super();
    test.results.wordList = test.wordList;
    test.results.userWords = test.userWords;
    if (test.fixedListSize > 0) {
      test.results.shownWords = test.shownWords;
      test.results.totalWordsShown = test.firstNewWord;
    }
    if (test.parentID) {
      test.results.parentID = test.parentID;
    }
    if (test.pair_id) {
      test.results.pair_id = test.pair_id;
    }
  }

});

var test = new VerbalLearning();
