// A test where there are many trials and each trial presents a stimulus and measures the response time.
var TimedStimulusResponse = MultiTrial.extend({
  init: function() {
    this._super();
    this.readyStatistic = new BinaryStatistic($('span#ready_statistic'));
    this.correctStatistic = new BinaryStatistic($('span#correct_statistic'));
    this.RTStatistic = new MeanStdevStatistic($('span#reaction_time_statistic'));
    this.fixedDuration = window.fixedDuration;
    this.prestimulusDelay = window.prestimulusDelay;
    return this;
  },

  delay: function() {
    return Math.floor(Math.random()*1000);  // TODO: replace this with exponential
  },
  
  generateTrial: function() {
    test._super();
    test.generateTrialDelay();
    test.generateTrialStimulus();
    return test.currentTrial;
  },

  generateTrialDelay: function() {
    test.currentTrial.delay = 0;
  },

  generateTrialStimulus: function() {
  },

  startTest: function() {
    test.readyStatistic.reset();
    test.correctStatistic.reset();
    test.RTStatistic.reset();
    test.sumRT = 0;
    test.sumRT2 = 0;
    if (test.fixedDuration > 0) {
      setTimeout(test.updateFixedDurationTime, 100);
    }
    test.isRunning = true;
    test._super();
  },

  updateFixedDurationTime: function() {
    if (test.complete) {
      return;
    }
    if(test.isTestFinished()) {
      if (test.currentTrial.state === "duringStimulusDisplay") {
        stopStimulusDisplay();
      }
      test.complete = true;
      test.stopTest();
      return;
    }
    var elapsed = new Date().getTime() - test.results.startTime;
    var remaining = test.fixedDuration - elapsed;
    var time_text = test.formatTimer(remaining);
    $('#test_countdown_timer').text(time_text);
    setTimeout(test.updateFixedDurationTime, Math.floor(Math.random() * 100) + 50);
  },

  formatTimer: function(remaining) {
    var hundredths = Math.floor(remaining / 10) % 100;
    var seconds = Math.floor(remaining / 1000) % 60;
    var minutes = Math.floor(remaining / 60000);
    //var time_text = minutes.toString() + ":" + ((seconds < 10) ? "0" : "") + seconds.toString() + "." + ((hundredths < 10) ? "0" : "") + hundredths.toString();
    var time_text = minutes.toString() + ":" + ((seconds < 10) ? "0" : "") + seconds.toString();  // no hundredths
    return time_text;
  },
  
  startTrial: function() {
    test._super();
    test.currentTrial.state = "beforeStimulusDisplay";
    test.trialTimer = setTimeout(test.startDisplayStimulus, test.currentTrial.delay);
  },

  startDisplayStimulus: function() {
    test.currentTrial.state = "duringStimulusDisplay";
  },

  stopStimulusDisplay: function() {
    test.currentTrial.state = "afterStimulusDisplay";
    test.currentTrial.displayEndTime = new Date().getTime();
    test.readyForResponse();
  },

  interruptStimulusDisplay: function() {
  },

  readyForResponse: function() {
    test.setFeedback('Go!');
  },

  endTrial: function() {
    test._super();
  },

  isTestFinished: function() {
    if(test.fixedDuration > 0) {
      var elapsed = new Date().getTime() - test.results.startTime;
      var remaining = test.fixedDuration - elapsed;
      if (remaining < 0) {
        test.isRunning = false;
        return true;
      }
    }
    return test._super();
  },

  updateTrialStatistics: function() {
    test._super();
    test.readyStatistic.add(test.currentTrial.ready);
    if (test.currentTrial.ready) {
      test.correctStatistic.add(test.currentTrial.responseCorrect);
      if (test.currentTrial.responseCorrect) {
	test.RTStatistic.add(test.currentTrial.reactionTime);
      }
    }
  },

  generateResults: function() {
    test._super();
  },

  handleResponse: function() {
    if (test.currentTrial.state === "afterStimulusDisplay") {
      test.currentTrial.ready = true;
      ++test.totalReady;
      test.currentTrial.reactionTime = test.currentTrial.userResponseTime - test.currentTrial.displayEndTime;
      if (test.isResponseCorrect()) {
	test.responseCorrect();
      }
      else {
	test.responseIncorrect();
      }
    }
    else {
      if (test.currentTrial.state === "duringStimulusDisplay") {
	test.interruptStimulusDisplay();
      }
      clearTimeout(test.trialTimer);
      test.responseTooEarly();
    }
    test.endTrial();
  },

  responseCorrect: function() {
    // Was it ever a good idea to show reaction times here?
    // test.setFeedback('Right! (' + test.currentTrial.reactionTime + ' ms)');
    test.setFeedback('Right!');
    test.currentTrial.responseCorrect = true;
    test.totalCorrect++;
  },
  
  responseIncorrect: function() {
    test.setFeedback('Wrong :(');
    test.currentTrial.responseCorrect = false;
    test.totalIncorrect++;
  },
  
  responseTooEarly: function() {
    test.setFeedback('Too early!');
    test.currentTrial.ready = false;
    test.totalNotReady++;
  }
});
