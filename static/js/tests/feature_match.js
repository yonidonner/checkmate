// Feature match: compare two grids with items.
var FeatureMatch = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    this.gridRows = window.gridRows;
    this.gridCols = window.gridCols;
    this.grid1 = new Grid(this.gridRows, this.gridCols, 20);
    this.grid2 = new Grid(this.gridRows, this.gridCols, 20);
    this.minTrialsPerLevel = window.minTrialsPerLevel;  // even number; half will match, half won't
    this.minLevel = window.minLevel;
    this.maxLevel = window.maxLevel;
    $('div#ready_statistic').hide();
    return this;
  },

  startTest: function() {
    test.generateAllTrials();
    test._super();
  },

  generateAllTrials: function() {
    test.allTrials = [];
    for (var i = test.minLevel; i <= test.maxLevel; ++i) {
      for (var j = 0; j < test.minTrialsPerLevel / 2; ++j) {
        test.allTrials.push({level: i, correct: false});
        test.allTrials.push({level: i, correct: true});
      }
    }
    test.allTrials.sort(function() { return 0.5 - Math.random(); });
    test.maxTrials = test.allTrials.length;
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[37,39], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay") && (keyState === "down")) {
        if (keyCode === 37) {
          test.currentTrial.userResponse = false;
        }
        else {
          test.currentTrial.userResponse = true;
        }
        test.currentTrial.userResponseTime = new Date().getTime();
        test.handleResponse();
      }
    }]);
  },

  isResponseCorrect: function() {
    return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
  },

  generateTrialStimulus: function() {
    test._super();

    var trial = test.allTrials[test.currentTrial.n];
    test.currentTrial.level = trial.level;
    test.currentTrial.correctResponse = trial.correct;

    var originalDesign = randomizeDesign(test.gridRows, test.gridCols, test.currentTrial.level);
    var mutations = mutateDesign(originalDesign, 2);
    if (test.currentTrial.correctResponse) {
      test.currentTrial.design1 = mutations[0];
      test.currentTrial.design2 = mutations[0];
    }
    else {
      test.currentTrial.design1 = mutations[0];
      test.currentTrial.design2 = mutations[1];
    }
  },

  endTrial: function() {
    removeGrid(test.grid1);
    removeGrid(test.grid2);
    $('span#stimulus_left').html('');
    $('span#stimulus_right').html('');
    test._super();
  },

  startDisplayStimulus: function() {
    test._super();
    $('span#feature_match_items').text(test.currentTrial.level);
    gridCreate(test.grid1, $('span#stimulus_left'));
    gridCreate(test.grid2, $('span#stimulus_right'));
    gridDisplayDesign(test.grid1, test.currentTrial.design1);
    gridDisplayDesign(test.grid2, test.currentTrial.design2);
    test.stopStimulusDisplay();
  },
  
  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.prestimulusDelay;
  },

});

var test = new FeatureMatch();