// Paired Associates: the subject needs to learn and then recall a set of assiciates that are paired to cues.
var PairedAssociates = LevelEstimation.extend({
  init: function() {
    this._super();
    // TODO: read initialLevel somehow
    $('span#level_statistic_name').text("Pairs");
    this.onTime = window.onTime;
    this.offTime = window.offTime;
    return this;
  },

  startTest: function() {
    test.sequenceTimer = SequenceTimer(test.onTime, test.offTime, test.displayCallback, test.hideCallback, test.doneCallback);
    test._super();
  },

  displayCallback: function(index) {
    var index2 = test.currentTrial.randomOrder[index];
    test.showPair(test.currentTrial.pairs[index2][0], test.currentTrial.pairs[index2][1]);
  },
  
  hideCallback: function(index) {
    var index2 = test.currentTrial.randomOrder[index];
    test.unshowPair(test.currentTrial.pairs[index2][0], test.currentTrial.pairs[index2][1]);
  },

  doneCallback: function() {
    test.stopStimulusDisplay();
  },
  
  generateTrialDelay: function() {
    test.currentTrial.delay = test.prestimulusDelay;
  },

  generateTrialStimulus: function() {
    test._super();
    test.currentTrial.pairs = test.generatePairs(test.currentTrial.level);
    test.currentTrial.randomOrder = randomPermutation(test.currentTrial.level);
    test.currentTrial.userChoice = new Array();
  },

  startDisplayStimulus: function() {
    test._super();
    test.setupStimulusDisplay();
    startSequence(test.sequenceTimer, test.currentTrial.level);
  },

  setupStimulusDisplay: function() {
  },

  readyForResponse: function() {
    test._super();
    test.setupInput();
  },
  
  isResponseCorrect: function() {
    var i;
    for (i in test.currentTrial.pairs) {
      if (test.currentTrial.userChoice[i] !== test.currentTrial.pairs[i][1]) {
        return false;
      }
    }
    return true;
  },

  responseCorrect: function() {
    test._super();
    test.setFeedback('Right! (' + parseInt(test.currentTrial.level) + ' pairs)');
  },
  
  responseIncorrect: function() {
    test._super();
    test.setFeedback('Wrong (' + parseInt(test.currentTrial.level) + ' pairs)');
  }

});
