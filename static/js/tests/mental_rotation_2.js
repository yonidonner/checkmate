// Mental Rotation 2: compare two stimuli and identify if they're the same (rotated) or flipped (and rotated).
var MentalRotation2 = TimedStimulusResponse.extend({
  init: function() {
    this._super();
    test.gridSize = window.gridSize;
    test.nRotations = window.nRotations;
    test.nTrialsPerRotation = window.nTrialsPerRotation;
    test.minComplexity = window.minComplexity;
    test.complexityJump = window.complexityJump;
    test.nComplexities = window.nComplexities;
    $('div#ready_statistic').hide();
    test.generateTrials();
    test.preload();
    return this;
  },

  generateImageURL: function(size, radius, complexity, seed, rotation, flipped) {
    var rotation2 = (rotation > 0) ? rotation : (360 - rotation);
    return "/stimuli/connected/radial/" + size + "/" + radius + "/" + complexity + "/" + seed + "/" + rotation2 + "/" + flipped;
  },

  generateTrials: function() {
    var i,j,k;
    test.allStimuli = new Array();
    test.allTrials = [];
    for (i = 0; i < test.nComplexities; ++i) {
      test.allStimuli[i] = new Array();
      var complexity = test.minComplexity + test.complexityJump * i;
      for (j = 0; j < test.nRotations; ++j) {
        test.allStimuli[i][j] = new Array();
        var rotation = (1.0 + j) * 180 / test.nRotations;
        if (Math.random() < 0.5) {
          rotation = -rotation;
        }
        for (k = 0; k < 2*test.nTrialsPerRotation; ++k) {
          var flipped = false;
          if (k < test.nTrialsPerRotation) {
            flipped = true;
          }
          test.allStimuli[i][j][k] = Math.floor(Math.random()*65536);
          var rotation0 = Math.floor(Math.random()*360);
          var rotation1 = rotation0 + rotation;
          test.allTrials.push({complexity: complexity,
                               rotation0: rotation0,
                               rotation1: rotation1,
                               flipped: flipped,
                               seed: test.allStimuli[i][j][k]});
        }
      }
    }

    test.allTrials.sort(function() { return 0.5 - Math.random(); });
    test.maxTrials = test.allTrials.length;
  },

  preload: function() {
    // Preload all the images that will be used in this test
    var elem1 = $('#stimulus_left');
    var elem2 = $('#stimulus_right');
    elem1.empty();
    elem2.empty();
    var radius = Math.floor(test.gridSize / 2);
    for(var i = 0; i < test.allTrials.length; ++i) {
      var trial = test.allTrials[i];
      url1 = test.generateImageURL(256, radius, trial.complexity, trial.seed, trial.rotation0, false);
      url2 = test.generateImageURL(256, radius, trial.complexity, trial.seed, trial.rotation1, trial.flipped);
      elem1.append($('<img>').attr('src', url1).attr('id', 'stimulus_left_' + i));
      elem2.append($('<img>').attr('src', url2).attr('id', 'stimulus_right_' + i));
      $('#stimulus_left_'+i).hide();
      $('#stimulus_right_'+i).hide();
    }
  },

  startTest: function() {
    test._super();
  },

  addKeyHandlers: function() {
    test._super();
    test.keyHandlers.unshift([[37,39], function(keyCode, keyState) {
      if ((test.state === "inTrial") && (test.currentTrial.state === "afterStimulusDisplay") && (keyState === "down")) {
        if (keyCode === 37) {
          test.currentTrial.userResponse = false;
        }
        else {
          test.currentTrial.userResponse = true;
        }
        test.currentTrial.userResponseTime = new Date().getTime();
        test.handleResponse();
      }
    }]);
  },

  isResponseCorrect: function() {
    return (test.currentTrial.correctResponse === test.currentTrial.userResponse);
  },

  generateTrialStimulus: function() {
    test._super();

    var trial = test.allTrials[test.currentTrial.n];
    test.currentTrial.complexity = trial.complexity;
    test.currentTrial.rotation0 = trial.rotation0;
    test.currentTrial.rotation = trial.rotation1 - trial.rotation0;
    test.currentTrial.flipped = trial.flipped;
    test.currentTrial.seed = trial.seed;
    test.currentTrial.correctResponse = !trial.flipped;
    // $('#mental_rotation_2_statistic').text(test.currentTrial.rotation);
  },

  endTrial: function() {
    $('#stimulus_left_' + test.currentTrial.n).hide();
    $('#stimulus_right_' + test.currentTrial.n).hide();
    test._super();
  },

  startDisplayStimulus: function() {
    test._super();
    $('#stimulus_left_' + test.currentTrial.n).show();
    $('#stimulus_right_' + test.currentTrial.n).show();
    test.stopStimulusDisplay();
  },
  
  generateTrialDelay: function() {
    test._super();
    test.currentTrial.delay = test.prestimulusDelay;
  },

});

var test = new MentalRotation2();
