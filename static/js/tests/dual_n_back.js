// 2 modalities n-back.
var DualNBack = KNBack.extend({
  init: function() {
    this._super();
    this.nStimuli = window.nStimuli; // number of possible different stimuli
    this.maxTrials = window.maxTrials;
    this.setupInstructionsAndStatistics();
    return this;
  },

  generateStimulus: function() {
    test.currentStimulus = new Array();
    for (var i = 0; i < test.k; ++i) {
      test.currentStimulus[i] = Math.floor(Math.random() * test.nStimuli);
    }
  },

  showStimulus: function(stimulus) {
    var html = '<table class="table"><tr>';
    for (var i = 0; i < test.k; ++i) {
      html = html + "<td>" + stimulus[i] + "</td>";
    }
    test.playAudio(stimulus[0]);
    html = html + "</tr></table>";
    $('div#stimulus').html(html);
  },

  clearStimulus: function() {
    $('div#stimulus').html('');
  },

});

var test = new DualNBack();
