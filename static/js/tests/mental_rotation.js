// Mental Rotation: the user should recognize (mutliple-choice) a binary design, rotated.
var MentalRotation = DesignRecognition.extend({
  init: function() {
    this.gridCellSize1 = 8;
    this.gridCellSize2 = 6;
    this._super();
    this.timeLimit = window.MentalRotationTimeLimit;
    if (window.isConnected) {
      this.isConnected = window.isConnected;
    }
    else {
      this.isConnected = false;
    }
    $('div#stimulus').hide();
    $('div#correct_statistic').show();
    $('div#reaction_time_statistic').show();
    return this;
  },

  startTrial: function() {
    $('div#stimulus').hide();
    test._super();
  },

  generateTrialStimulus: function() {
    test.currentTrial.level = test.currentLevel;
    test.levelStatistic.setCurrent(test.currentTrial.level);
    //var originalDesign;
    //if (test.isConnected) {
    //  originalDesign = randomizeConnectedDesign(test.nRows, test.nCols, test.currentLelel);
    //}
    //else {
    //  originalDesign = randomizeDesign(test.nRows, test.nCols, test.currentLevel);
    //}
    //var mutations = mutateDesign(originalDesign, test.nChoices);
    var mutations = greedyGrid(test.nRows, test.nCols, test.currentLevel, test.nChoices);
    test.currentTrial.correctResponse = Math.floor(Math.random()*test.nChoices);
    test.currentTrial.design = mutations[test.currentTrial.correctResponse];
    var i,row,col,orow,ocol;
    for (i = 0; i < test.nChoices; ++i) {
      var rotation;
      rotation = Math.floor(Math.random()*3);
      test.currentTrial.rotation = rotation;
      var new_mutation = [];
      for (row = 0; row < test.nRows; ++row) {
        new_mutation[row] = [];
        for (col = 0; col < test.nRows; ++col) {
          if (rotation === 0) {
            orow = test.nRows - 1 - row;
            ocol = test.nCols - 1 - col;
          }
          else if (rotation === 1) {
            orow = test.nCols - 1 - col;
            ocol = row;
          }
          else if (rotation == 2) {
            orow = col;
            ocol = test.nRows - 1 - row;
          }
          new_mutation[row][col] = mutations[i][orow][ocol];
        }
      }
      mutations[i] = new_mutation;
    }
    test.currentTrial.choices = mutations;
  },

  startDisplayStimulus: function() {
    test.currentTrial.state = "duringStimulusDisplay";
    gridCreate(test.grid, $('div#stimulus'));
    gridDisplayDesign(test.grid, test.currentTrial.design);
    $('div#stimulus').show();
    for (var i = 0; i < test.nChoices; ++i) {
      gridCreate(test.answerGrids[i], $('span#choice'+(i+1)));
      gridDisplayDesign(test.answerGrids[i], test.currentTrial.choices[i]);
      $('div#choices_area').show();
    }
    test.stopStimulusDisplay();
    test.currentTrial.stimulusDisplayTime = new Date().getTime();
    $('div#countdown_timer').show();
    test.updateCountdownTimer();
  },

  updateCountdownTimer: function() {
    if (test.complete) {
      return;
    }
    if (test.currentTrial.state !== "afterStimulusDisplay") {
      return;
    }
    var elapsed = new Date().getTime() - test.currentTrial.stimulusDisplayTime;
    var remaining = test.timeLimit - elapsed;
    if (remaining < 0) {
      test.currentTrial.userResponse = false;
      test.handleResponse();
    }
    else {
      var time_text = test.formatTimer(remaining);
      $('div#countdown_timer').text(time_text);
      setTimeout(test.updateCountdownTimer, Math.floor(Math.random() * 100) + 50);
    }
  },

  stopStimulusDisplay: function() {
    test.currentTrial.state = "afterStimulusDisplay";
    test.currentTrial.displayEndTime = new Date().getTime();
    test.readyForResponse();
  },

  readyForResponse: function() {
    test._super();
    
  },

  endTrial: function() {
    $('div#countdown_timer').hide();
    test._super();
  },

});

if (window.test_name === "mental_rotation") {
  var test = new MentalRotation();
}
