function RegressionResult(A, b) {
  // A is a Matrix, b is a ColumnVector
  this.n = A.rows();
  this.p = A.columns()
  this.beta = b.regression_coefficients(A);
  if (this.beta === "Singular") {
    return;
  }
  this.y_hat = A.mult(this.beta);
  var i;
  var residual;
  this.SSR = 0.0;
  for (i = 0; i < b.rows(); ++i) {
    residual = b.mtx[i][0] - this.y_hat.mtx[i][0];
    this.SSR += residual * residual;
  }
}

Matrix.prototype.clone = function() {
  var new_data = new Array();
  var i,j;
  for (i = 0; i < this.height; ++i) {
    new_data[i] = new Array();
    for (j = 0; j < this.width; ++j) {
      new_data[i][j] = this.mtx[i][j];
    }
  }
  return new Matrix(new_data);
}

Matrix.prototype.add_column = function(v) {
  var new_matrix = this.clone();
  new_matrix.width++;
  var i;
  for (i = 0; i < this.height; ++i) {
    new_matrix.mtx[i].push(v[i]);
  }
  return new_matrix;
}

function regression_new_variable_f(A, b, new_column, first_result, return_regression) {
  if (first_result === undefined) {
    first_result = new RegressionResult(A, b);
  }
  if (return_regression === undefined) {
    return_regression = false;
  }
  var A2 = A.add_column(new_column);
  var second_result = new RegressionResult(A2, b);
  if (second_result.beta === "Singular") {
    return 1.0;
  }
  var num = first_result.SSR - second_result.SSR;
  var df2 = (second_result.n - second_result.p);
  var denom = second_result.SSR / df2;
  var F = num / denom;
  var df1 = 1;
  //alert("numerator: " + num + " denominator: " + denom);
  var p_value = 1.0 - Fcdf(F, df1, df2);
  if (return_regression) {
    return [p_value, second_result, A2];
  }
  else {
    return p_value;
  }
}

function regression_new_variables_f(A, b, new_columns, first_result, return_regression) {
  if (first_result === undefined) {
    first_result = new RegressionResult(A, b);
  }
  if (return_regression === undefined) {
    return_regression = false;
  }
  var A2 = A;
  var i;
  for (i = 0; i < new_columns.length; ++i) {
    A2 = A2.add_column(new_columns[i]);
  }
  var second_result = new RegressionResult(A2, b);
  if (second_result.beta === "Singular") {
    return 1.0;
  }
  var df1 = second_result.p - first_result.p;
  var num = (first_result.SSR - second_result.SSR) / df1;
  var df2 = (second_result.n - second_result.p);
  var denom = second_result.SSR / df2;
  var F = num / denom;
  //alert("numerator: " + num + " denominator: " + denom);
  var p_value = 1.0 - Fcdf(F, df1, df2);
  if (return_regression) {
    return [p_value, second_result, A2];
  }
  else {
    return p_value;
  }
}

CONTINUOUS_VARIABLE = 0;
DISCRETE_VARIABLE = 1;

function Variable(vals) {
  this.vals = vals;
}

function DiscreteVariable(states, vals) {
  this.type = DISCRETE_VARIABLE;
  this.states = states;
  this.vals = vals;
}
DiscreteVariable.prototype = Variable.prototype;

function ContinuousVariable(vals) {
  this.type = CONTINUOUS_VARIABLE;
  this.vals = vals;
}
ContinuousVariable.prototype = Variable.prototype;

Variable.prototype.interaction = function(other) {
  
}

function RegressionTerm(vals, component) {
  this.components = new Array();
  if (!(component === undefined)) {
    this.components.push(component);
  }
  this.vals = vals;
}

function Intercept(n) {
  var component = "1";
  this.components = new Array();
  this.components.push(component);
  this.vals = new Array();
  var i;
  for (i = 0; i < n; ++i) {
    this.vals[i] = 1.0;
  }
}
Intercept.prototype = RegressionTerm.prototype;

RegressionTerm.prototype.multiply = function(other) {
  var new_vals = new Array();
  var i;
  for (i = 0; i < this.vals.length; ++i) {
    new_vals[i] = this.vals[i] * other.vals[i];
  }
  var new_term = new RegressionTerm(new_vals);
  for (i = 0; i < this.components.length; ++i) {
    new_term.components[i] = this.components[i];
  }
  for (i = 0; i < other.components.length; ++i) {
    new_term.components.push(other.components[i]);
  }
  return new_term;
}

function build_regression_equation(terms, outcome, p_value_threshold) {
  // A variable has a continuous value and a set of discrete filters
  // Algorithm: start with only a single term, the intercept ("1").
  // Loop over all variables and consider their interaction with the current terms.
  // The interaction is just the product of the previous variable and the new variable.
  if (p_value_threshold === undefined) {
    p_value_threshold = 0.001;
  }
  var equation_terms = new Array();
  var n = outcome.length;
  var intercept_term = new Intercept(n);
  equation_terms.push(intercept_term);
  var A = new ColumnVector(intercept_term.vals);
  var b = new ColumnVector(outcome);
  var current_regression = new RegressionResult(A, b);
  var i,j;
  for (i = 0; i < terms.length; ++i) {
    var equation_terms_length = equation_terms.length;
    for (j = 0; j < equation_terms_length; ++j) {
      var new_term = equation_terms[j].multiply(terms[i]);
      var new_result = regression_new_variable_f(A, b, new_term.vals, current_regression, true);
      var p_val = new_result[0];
      var new_regression = new_result[1];
      alert("New term: " + new_term.components + " p-value: " + p_val);
      if (p_val <= p_value_threshold) {
        A = new_result[2];
        equation_terms.push(new_term);
        current_regression = new_regression;
        alert("current regression beta: " + current_regression.beta);
      }
      else {
        alert("current regression beta: " + current_regression.beta);
      }
    }
  }
  return [equation_terms, current_regression];
}

function find_significant_anova(variables, terms, outcome, p_value_threshold) {
  if (p_value_threshold === undefined) {
    p_value_threshold = 0.001;
  }
  var n = outcome.length;
  var intercept_term = new Intercept(n);
  var A = new ColumnVector(intercept_term.vals);
  var b = new ColumnVector(outcome);
  var current_regression = new RegressionResult(A, b);
  var significant_terms = [];
  // Now go over all variables and find main effects
  var i,j;
  for (i = 0; i < variables.length; ++i) {
    var var_terms = [];
    var columns = [];
    for (j = 0; j < terms.length; ++j) {
      if ((terms[j].components.length === 1) && (terms[j].components[0] !== "1") && (terms[j].components[0][0] == i)) {
        var_terms.push(j);
        columns.push(terms[j].vals);
        if (terms[j].components[0].length === 1) {
          // continuous variable, do poly regression
          var prv_column = terms[j].vals;
          for (var k = 0; k < 2; ++k) {
            var new_column = new Array();
            for (var column_i = 0; column_i < prv_column.length; ++column_i) {
              new_column[column_i] = prv_column[column_i] * terms[j].vals[column_i];
            }
            columns.push(new_column);
            prv_column = new_column;
          }
        }
      }
    }
    var p_val = regression_new_variables_f(A, b, columns, current_regression, false);
    //console.log("p value for variable " + i + ": " + p_val);
    if (p_val < p_value_threshold) {
      for (j = 0; j < var_terms.length; ++j) {
        significant_terms.push(var_terms[j]);
      }
    }
  }
  return significant_terms;
}

function build_regression_equation2(terms, outcome, p_value_threshold, main_effects) {
  if (p_value_threshold === undefined) {
    p_value_threshold = 0.001;
  }
  if (main_effects === undefined) {
    main_effects = [];
  }
  var equation_terms = new Array();
  var new_terms = new Array();
  var next_term = new Array();
  var n = outcome.length;
  var intercept_term = new Intercept(n);
  new_terms.push(intercept_term);
  if (main_effects.length > 0) {
    next_term.push(terms.length);
  }
  else {
    next_term.push(0);
  }
  var i,j;
  // Now add all main effects
  for (i = 0; i < main_effects.length; ++i) {
    new_terms.push(terms[main_effects[i]]);
    next_term.push(main_effects[i]+1);
  }
  var A = new ColumnVector(intercept_term.vals);
  var b = new ColumnVector(outcome);
  var current_regression = new RegressionResult(A, b);
  do {
    var new_new_terms = new Array();
    var new_next_term = new Array();
    for (j = 0; j < new_terms.length; ++j) {
      equation_terms.push(new_terms[j]);
      for (i = next_term[j]; i < terms.length; ++i) {
        if (current_regression.SSR < 1.0) {
          break;
        }
        var new_term = new_terms[j].multiply(terms[i]);
        if (array_variance(new_term.vals) < 0.01) {
          continue;
        }
        var new_result = regression_new_variable_f(A, b, new_term.vals, current_regression, true);
        var p_val = new_result[0];
        var new_regression = new_result[1];
        //alert("New term: " + new_term.components + " p-value: " + p_val);
        if (p_val <= p_value_threshold) {
          A = new_result[2];
          new_new_terms.push(new_term);
          new_next_term.push(i + 1);
          current_regression = new_regression;
          //alert("current regression beta: " + current_regression.beta);
        }
      }
    }
    new_terms = new_new_terms;
    next_term = new_next_term;
  } while (new_terms.length > 0);
  return [equation_terms, current_regression];
}

function copy_array(a) {
  var new_array = new Array();
  var i;
  for (i = 0; i < a.length; ++i) {
    new_array[i] = a[i];
  }
  return new_array;
}

function array_mean(a) {
  var s = 0.0;
  var i;
  for (i = 0; i < a.length; ++i) {
    s += a[i];
  }
  return (s / a.length);
}

function array_variance(a) {
  if (length.a < 2) {
    return null;
  }
  var m = array_mean(a);
  var s2 = 0.0;
  var i;
  for (i = 0; i < a.length; ++i) {
    var x = a[i] - m;
    s2 += x*x;
  }
  return (s2 / (a.length - 1));
}

function build_terms(variables) {
  var terms = new Array();
  var i,j,k;
  for (i = 0; i < variables.length; ++i) {
    if (variables[i].type === CONTINUOUS_VARIABLE) {
      var component = new Array();
      component[0] = i;
      var new_vals = copy_array(variables[i].vals);
      var av = array_variance(new_vals);
      if ((av !== null) && (av > 0.01)) {
        var new_term = new RegressionTerm(new_vals, component);
        terms.push(new_term);
      }
    }
    else if (variables[i].type === DISCRETE_VARIABLE) {
      for (j = 1; j < variables[i].states; ++j) {
        var component = new Array();
        component[0] = i;
        component[1] = j;
        var new_vals = new Array();
        var appeared = false;
        for (k = 0; k < variables[i].vals.length; ++k) {
          if (variables[i].vals[k] == j) {
            new_vals[k] = 1.0;
            appeared = true;
          }
          else {
            new_vals[k] = 0.0;
          }
        }
        if (appeared) {
          var new_term = new RegressionTerm(new_vals, component);
          terms.push(new_term);
        }
      }
    }
  }
  return terms;
}

function group_by_plot(variables, equation_terms) {
  var by_plot = {};
  var i,j;
  for (i = 0; i < equation_terms.length; ++i) {
    var contis = [];
    var discretes = [];
    for (j = 0; j < equation_terms[i].components.length; ++j) {
      var component = equation_terms[i].components[j];
      if (component !== "1") {
        if (variables[component[0]].type === CONTINUOUS_VARIABLE) {
          contis.push(component[0]);
        }
        else if (variables[component[0]].type === DISCRETE_VARIABLE) {
          discretes.push(component);
        }
      }
    }
    if (by_plot[contis] === undefined) {
      by_plot[contis] = [];
    }
    by_plot[contis].push(discretes);
  }
  return by_plot;
}

function pearsons_r(x, y) {
  var n = x.length;
  if (y.length !== n) {
    console.log("x and y must be same length");
    return;
  }
  var x_sum = 0.0;
  var y_sum = 0.0;
  var xx_sum = 0.0;
  var yy_sum = 0.0;
  var xy_sum = 0.0;
  var i;
  for (i = 0; i < n; ++i) {
    x_sum += x[i];
    y_sum += y[i];
    xy_sum += x[i] * y[i];
    xx_sum += x[i] * x[i];
    yy_sum += y[i] * y[i];
  }

  var num = xy_sum - x_sum*y_sum/n;
  var denom = Math.sqrt((xx_sum - x_sum*x_sum/n) * (yy_sum - y_sum*y_sum/n));
  return num / denom;
}

function make_powerlaw(x, d) {
  var pl = [];
  var i;
  for (i = 0; i < x.length; ++i) {
    pl[i] = Math.log(x[i] + d);
  }
  return pl;
}

function fit_powerlaw(x, y, max_d) {
  var n = x.length;
  if (y.length !== n) {
    console.log("x and y must be same length");
    return null;
  }
  if (n < 3) {
    return null;
  }
  if (max_d === undefined) {
    max_d = 100;
  }
  var d = 0;
  var best_d;
  var best_r2 = null;
  var best_pl;
  while (d <= max_d) {
    var pl = make_powerlaw(x, d);
    var r = pearsons_r(pl, y);
    if ((best_r2 === null) || (r*r >= best_r2)) {
      best_r2 = r*r;
      best_d = d;
      best_pl = pl;
    }
    if (d < 10) {
      ++d;
    }
    else {
      d *= 1.1;
    }
  }
  return [best_d, best_r2, best_pl];
}

function test_regression_equation() {
  // y = 7 + 3*x1 - 2*x2 + 4*x3 - x4 + 0.05*x1*x2 - 0.01*x2*x3*x4
  var AA = new Array();
  var Ab = new Array();
  var n = 500;
  var p = 4;
  var i,j;
  var term_vals = new Array();
  for (j = 0; j < p; ++j) {
    term_vals[j] = new Array();
  }
  for (i = 0; i < n; ++i) {
    AA[i] = new Array();
    for (j = 0; j < p; ++j) {
      AA[i][j] = Math.nrand();
      term_vals[j][i] = AA[i][j];
    }
    Ab[i] = 7.0 + 3*AA[i][0] - 2*AA[i][1] + 4*AA[i][2] - AA[i][3] + 0.05*AA[i][0]*AA[i][1] - 0.01*AA[i][1]*AA[i][2]*AA[i][3];
  }
  var terms = new Array();
  for (j = 0; j < p; ++j) {
    terms[j] = new RegressionTerm(term_vals[j], "x"+(j+1).toString());
  }
  return build_regression_equation2(terms, Ab, 0.01);
}

function test_regression_equation2() {
  var n = 1000;
  var n_continuous = 4;
  var n_discrete = 4;
  var i,j,k;
  var continuous_vars = new Array();
  for (i = 0; i < n_continuous; ++i) {
    var vals = new Array();
    for (j = 0; j < n; ++j) {
      vals[j] = Math.nrand();
    }
    var v = new ContinuousVariable(vals);
    continuous_vars.push(v);
  }
  var discrete_vars = new Array();
  for (i = 0; i < n_discrete; ++i) {
    var vals = new Array();
    for (j = 0; j < n; ++j) {
      var u = Math.random();
      if (u < 0.3) {
        vals[j] = 0;
      }
      else if (u < 0.7) {
        vals[j] = 1;
      }
      else {
        vals[j] = 2;
      }
    }
    var v = new DiscreteVariable(3, vals);
    discrete_vars.push(v);
  }
  var continuous_weights = [0,5,-3,0];
  var discrete_weights = new Array();
  for (i = 0; i < n_discrete; ++i) {
    discrete_weights[i] = new Array();
    for (j = 0; j < 3; ++j) {
      discrete_weights[i][j] = 0.0;
    }
  }
  discrete_weights[1][1] = 1.0;
  discrete_weights[3][1] = -1.0;
  discrete_weights[3][2] = 2.0;
  var outcome = new Array();
  for (i = 0; i < n; ++i) {
    outcome[i] = Math.nrand() * 0.01;
    for (j = 0; j < n_continuous; ++j) {
      outcome[i] += continuous_weights[j] * continuous_vars[j].vals[i];
    }
    for (j = 0; j < n_discrete; ++j) {
      for (k = 0; k < discrete_vars[j].states; ++k) {
        if (discrete_vars[j].vals[i] === k) {
          outcome[i] += discrete_weights[j][k];
        }
      }
    }    
  }
  var variables = continuous_vars.concat(discrete_vars);
  var terms = build_terms(variables);
  return build_regression_equation2(terms, outcome, 0.01);
}