// MENUS & QUERIES ////////////////////

function make_menus(initialized) {
  $("#download_button").click(function() {
    $('#dataminding_mode').val('download');
    window.alert('Downloading not implemented yet.');
  });
  make_experiments(initialized);
  make_mental_variables(initialized);
}

function make_experiments(initialized) {
  $('#experiment_list').empty();
  var some_incomplete = false;
  var some_busted = false;
  $.each(all_experiments, function(i, experiment) {
    var row = $('<div>');
    if(!experiment.complete) {
      row.append('* ');
      some_incomplete = true;
    }
    var id = 'experiments_' + experiment.name;
    if($('#' + id).is('*')) return;
    var label = $('<label>' + experiment.name + '</span>');
    label.attr('for', id);
    var checkbox = $('<input type="checkbox" class="pull-right" name="experiments" value="' +
                     experiment.name + '"/>');
    if((!initial_experiment_filters && i == 0 && !initialized) ||
       (initial_experiment_filters && $.inArray(experiment.name, initial_experiment_filters) != -1))
      checkbox.attr('checked', true);
    checkbox.attr('id', id);
    checkbox.change(make_mental_variables);
    row.append(label);
    if(experiment.tests.length)
      row.append(checkbox);
    else {
      label.css('text-decoration', 'line-through');
      some_busted = true;
      if(!admin) return;  // don't let them see busted ones
    }
    $('#experiment_list').append(row);
  });
  if(some_incomplete)
    $('#experiment_list').append('<em class="pull-right">* experiment in progress</em>').append('<div class="clearfix"></div>');
  if(some_busted && admin)
    $('#experiment_list').append('<em class="pull-right" style="text-decoration: line-through;">experiment has no tests</em>').append('<div class="clearfix"></div>');
}

var mv_names_to_keys = {};
function make_mental_variables(initialized) {
  var old_variables = $('form[name="dataminding"]').serializeObject()["variable_keys"];
  if(!old_variables && initial_variable_keys)
    old_variables = initial_variable_keys;
  $('#mental_variable_list').empty();
  var mental_variables = {};
  var selected_count = 0;
  $.each(all_experiments, function(i, experiment) {
    if(!is_experiment_selected(experiment)) return;
    $.each(experiment.mental_variables, function(j, variable) {
      var key = variable.name;
      if(mental_variables[key]) return;  // no duplicates
      mv_names_to_keys[variable.name] = variable.key_id;
      // Make sure that every selected experiment also has this mental variable
      for(var exp_index in all_experiments) {
        if(!is_experiment_selected(all_experiments[exp_index])) continue;
        var matched_mv = false;
        for(var mv_index in all_experiments[exp_index].mental_variables)
          if(all_experiments[exp_index].mental_variables[mv_index].name == key)
            matched_mv = true;
        if(!matched_mv)  // This selected experiment didn't have it
          return;
      }
      
      mental_variables[key] = variable;
      var row = $('<div class="variable_row">');
      var across_box = $('<input class="pull-left" type="checkbox" name="variable_keys" value="' + variable.key_id + '" id="variable_' + variable.key_id + '" />');
      across_box.change(function() { $('#mind_button').attr('disabled', !is_mindable()); });
      var label = $('<label for="variable_' + variable.key_id + '"> ' + variable.name + '</label/>');
      var filter_box = $('<input disabled="disabled" class="pull-right" type="checkbox" "variable_' + variable.key_id + '" />');
      if((!old_variables && !selected_count) ||
         (old_variables && (old_variables == variable.key_id || $.inArray(variable.key_id + "", old_variables) != -1))) {
        across_box.add(filter_box).attr('checked', true);
        ++selected_count;
      }
      row.append(across_box).append(label).append(filter_box);
      $('#mental_variable_list').append(row);
    });
  });
  $('#mind_button').attr('disabled', !is_mindable());
}

function is_experiment_selected(experiment) {
  return $('#experiment_list input[value="' + experiment.name + '"]').is(':checked');
};

function is_mv_name_selected(mv_name) {
  //console.log("is", mv_name, "selected?", $('#mental_variable_list input[value="' + mv_names_to_keys[mv_name] + '"]').is(':checked'));
  //console.log("  key is", mv_names_to_keys[mv_name]);
  return $('#mental_variable_list input[value="' + mv_names_to_keys[mv_name] + '"]').is(':checked');
};

function is_mindable() {
  return $('#mental_variable_list input[type="checkbox"].pull-left:checked').length > 0;
}

function compare_experiment_dates(a, b) {
  if(a.tests.length && !b.tests.length)
    return -1;
  else if(b.tests.length && !a.tests.length)
    return 1;
  if(a.complete && !b.complete)
    return -1;
  else if(b.complete && !a.complete)
    return 1;
  if(a.end && b.end)
    return new Date(a.end) - new Date(b.end);
  return 0;
}

// RESULTS GRAPHS ////////////////////

// {Date: {numeric: true, continuous: true, mean: 45.5, count: 31, min: 0.0, max: 83.0},
//  Time: {numeric: true, continuous: true, mean: 11.25, count: 31, min: 7.38, max: 23.5},
//  Mood: {numeric: true, continuous: false, mean: 5.0, count: 3, min: 4.0, max: 6.0},
//  Rage: {numeric: false, continuous: false, count: 3, states: ["None", "Some", "Double"]},
//  Work: {numeric: false, continuous: false, count: 3, states: ["Unknown", "No", "Yes"]}}
function make_variable_map(results, experiments) {
  var variable_map = {};
  $(["Date", "Time", "Session"]).each(function(i, fake_mv_name) {
    if(is_mv_name_selected(fake_mv_name))
      variable_map[fake_mv_name] = [];
  });
  for(var session_index = 0; session_index < results.length; ++session_index) {
    if ($.inArray(results[session_index].experiment, experiments) == -1) {
      continue;
    }
    for(var mv_index in results[session_index].variables) {
      var mv = results[session_index].variables[mv_index];
      if(!is_mv_name_selected(mv.name))
        continue;
      if(!variable_map[mv.name])
        variable_map[mv.name] = [];
      variable_map[mv.name].push(mv.value);
    }
    if(variable_map["Date"])
      variable_map["Date"].push(parseDate(results[session_index].date));
    if(variable_map["Time"])
      variable_map["Time"].push(results[session_index].time);
    if(variable_map["Session"])
      variable_map["Session"].push(1.0 + session_index);
  }
  var mv_index = 0;
  for(var mv_name in variable_map) {
    var numeric = true;
    var mean = 0;
    var mv_min = null;
    var mv_max = null;
    var values = variable_map[mv_name];
    for(var i = 0; i < values.length; ++i) {
      if(isNumber(values[i])) {
        var current_val = parseFloat(values[i]);
        mean += current_val / values.length;
        if ((mv_min === null) || (current_val < mv_min)) {
          mv_min = current_val;
        }
        if ((mv_max === null) || (current_val > mv_max)) {
          mv_max = current_val;
        }
      }
      else {
        numeric = false;
      }
    }
    if(numeric) {
      variable_map[mv_name] = {index: mv_index++, numeric: true, continuous: values.length > 4, mean: mean, count: values.length, min: mv_min, max: mv_max};
    }
    else {
      var unique_values = {};  // Seen it once?
      var doubled_values = []; // Seen it twice? (Ignore any singleton states.)  // TODO: do a shifting threshold here to try to keep us at 4 or 5 useful states
      for(i = 0; i < values.length; ++i) {
        var v = values[i];
        if(v == "")
          v = "Unknown";
        if(unique_values[v] && $.inArray(v, doubled_values) == -1)
          doubled_values.push(v);
        else
          unique_values[v] = true;
      }
      if(values.length < results.length && $.inArray("Unknown", doubled_values) == -1)  // some are unknown for this variable
        doubled_values.push("Unknown");
      doubled_values.sort(state_sort_function);
      variable_map[mv_name] = {index: mv_index++, numeric: false, continuous: false, count: doubled_values.length, states: doubled_values};
      if(doubled_values.length == 1 && doubled_values[0] == "Unknown")
        delete variable_map[mv_name];
    }
  }
  return variable_map;
}

// tests: test names -> arrays of mvs
//   mvs: mental variable combination names -> {continuous: true/false, states: array of extant states, sessions: arrays of sessions}
//     sessions: {x: mv value (or null), y: test score value, stderr: stderr}
//     sessions must match the experiment and variable filters
function make_all_test_mvs(results, variable_map, experiments) {
  var tests = {};
  var test_counts = {};
  play_results = results;
  for (var session_index=0; session_index < results.length; ++session_index) {
    var session = results[session_index];
    var i;
    if ($.inArray(session.experiment, experiments) == -1) {
      for(i = 0; i < session.scores.length; ++i) {
        var test = session.scores[i].name;
        if(!tests[test]) {
          test_counts[test] = 0;
        }
        ++test_counts[test];
      }
      continue;
    }
    for(i = 0; i < session.scores.length; ++i) {
      var test = session.scores[i].name;
      if(!tests[test]) {
        tests[test] = {};
        test_counts[test] = 0;
        for(var mv_name in variable_map) {
          var mv = variable_map[mv_name];
          tests[test][mv.index] = {type: mv.numeric ? "continuous" : "discrete", sessions: [], count: mv.count};
        }
      }
      ++test_counts[test];

      if(variable_map["Date"]) {
        // Add fake Date mental variable
        var date_point = {x: parseDate(session.date) - variable_map["Date"].mean, y: session.scores[i].value, stderr: session.scores[i].stderr};
        tests[test][variable_map["Date"].index].sessions.push(date_point);
      }

      if(variable_map["Time"]) {
        // Add fake Time mental variable
        var time_point = {x: session.time - variable_map["Time"].mean, y: session.scores[i].value, stderr: session.scores[i].stderr};
        tests[test][variable_map["Time"].index].sessions.push(time_point);
      }

      if(variable_map["Session"]) {
        // Add fake Session mental variable
        var session_number_point = {x: test_counts[test], y: session.scores[i].value, stderr: session.scores[i].stderr};
        tests[test][variable_map["Session"].index].sessions.push(session_number_point);
      }

      // Add all normal mental variables
      var seen_variables = {Date: true, Time: true, "Session": true};
      for(var j = 0; j < session.variables.length; ++j) {
        var mv_name = session.variables[j].name;
        var mv = variable_map[mv_name];
        if(!mv)  // ignoring this variable
          continue;
        var point = {x: session.variables[j].value, y: session.scores[i].value, stderr: session.scores[i].stderr};
        if(mv.numeric)
          point.x -= mv.mean;
        else {
          if ($.inArray(point.x, mv.states) === -1) {
            point.x = "Unknown";
          }
          point.x = $.inArray(point.x, mv.states);
          if (point.x === -1) {
            console.log("-1 found");
          }
         }
        tests[test][mv.index].sessions.push(point);
        seen_variables[mv_name] = true;
      }
      // Put 0 for missing numeric variables, and 0 (index of "Unknown") for missing non-numeric variables
      for(mv_name in variable_map)
        if(!seen_variables[mv_name])
          tests[test][variable_map[mv_name].index].sessions.push({x: 0, y: session.scores[i].value, stderr: session.scores[i].stderr});
    }
  }
  return tests;
}

function mvs_to_variables(mvs) {
  // Convert mvs as used here to the variables representation used in linear_models.js.
  var variables = [];
  var outcomes = [];
  for (mv_name in mvs) {
    var mv = mvs[mv_name];
    var sessions = mv.sessions;
    var vals = [];
    for (var i = 0; i < sessions.length; ++i) {
      vals[i] = sessions[i].x;
      if (i >= outcomes.length) {
        outcomes[i] = sessions[i].y;
      }
    }
    if (mv.type === "discrete")
      variables.push(new DiscreteVariable(mv.count, vals));
    else if (mv.type === "continuous")
      variables.push(new ContinuousVariable(vals));
  }
  return [variables, outcomes];
}

function fit_practice_mv(variable_map, all_tests) {
  if(!variable_map["Session"]) return;
  for (var test in all_tests) {
    var mvs = all_tests[test];
    var session_mv = mvs[variable_map["Session"].index];
    var x = [];
    var y = [];
    var i;
    for (i in session_mv.sessions) {
      x.push(session_mv.sessions[i].x);
      y.push(session_mv.sessions[i].y);
    }
    var pl_fit = fit_powerlaw(x, y);
    if (pl_fit !== null) {
      var d = pl_fit[0];
      var pl = pl_fit[2];
      var pl_mean = array_mean(pl);
      for (i in session_mv.sessions) {
        session_mv.sessions[i].x = pl[i] - pl_mean;
      }
      session_mv.new_name = "log(session+"+d+")-"+pl_mean;
    }
  }
}

var play_variables = [];
var play_terms = [];
var play_regression;
var play_series = [];
var play_groups = [];
var play_mvs = [];
var play_results;
function make_graphs(results, experiments) {
  $('#graphs_area').add('#graphs_nav').empty();
  if(!is_mindable()) return;
  if(!experiments || !experiments.count) {
    experiments = [];
    $.each(all_experiments, function(i, experiment) {
      if(is_experiment_selected(experiment))
        experiments.push(experiment.name);
    });
  }
  var variable_map = make_variable_map(results, experiments);
  var all_tests = make_all_test_mvs(results, variable_map, experiments);
  fit_practice_mv(variable_map, all_tests);
  var test_tab_number = 0;
  for(var test in all_tests) {
    var test_tab = $('<li class="graphs_tab"><a href="#test_tab'+test_tab_number+'" data-toggle="tab">'+test+'</a></li>');
    var test_container = $('<div class="tab-pane" id="test_tab'+test_tab_number+'"></div>');
    if(!test_tab_number) {
      test_tab.addClass('active');
      test_container.addClass('active');
    }
    $('#graphs_nav').append(test_tab);
    $('#graphs_area').append(test_container);
    var mvs = all_tests[test];
    //console.log("MVs:", mvs);
    //console.log("variable map:", variable_map);
    var variables_and_outcomes = mvs_to_variables(mvs);
    var variables = variables_and_outcomes[0];
    var terms = build_terms(variables);
    var outcomes = variables_and_outcomes[1];
    var p_value_threshold = 0.05;
    var main_effects = find_significant_anova(variables, terms, outcomes, p_value_threshold);
    //console.log("Main effects for test " + test + ": " + main_effects);
    var terms_and_regression = build_regression_equation2(terms, outcomes, p_value_threshold, main_effects);
    var equation_terms = terms_and_regression[0];
    var regression = terms_and_regression[1];
    var plot_groups = group_by_plot(variables, equation_terms);
    //console.log("Terms:", equation_terms);
    //console.log("Regression:", regression);
    //if(equation_terms.length > play_terms.length) {
    if (test === "Attentional Focus") {
      play_variables = variables;
      play_terms = equation_terms;
      play_regression = regression;
      play_groups = plot_groups;
      play_mvs = mvs;
    }
    // Components: for decoding the variables. Each element is one of the things in an interaction. "1" is just the intercept. [0] would be the first variable. If it were discrete, it would be like, [1, 3]: second variable, fourth state. Real interactions would be like: ["1", [0], [1, 3]].
    // Regression: y_hat is the predictions from the regression formula, showing how good the fit is. Each entry in mtx is a row -- it's a one-column, two-dimensional matrix. What we really care about is beta: also a column matrix, more columns, with each row being a coefficient, matching up with the regression terms. SSR = sum of squared residuals: the remaining error after the regression. Don't need it.
    // To get back to the formula: add the coefficient times the mean. If we add coefficient * means to the intercept for all the continuous ones, we get the correct formula for the original data.

    var had_graph = false;
    for(var cvs in plot_groups) {
      var series = prepare_series(cvs, plot_groups[cvs], mvs, variable_map);
      play_series.push(series);
      var graph = null;
      if(!cvs.length)
        graph = make_graph(test, "", series, false);
      else {
        var title = mv_names_from_numbers(cvs, variable_map);
        if ((cvs.length === 1) && (title === "Session"))
          title = mvs[cvs[0]].new_name;
        graph = make_graph(test, title, series, true);
      }
      if(graph) {
        had_graph = true;
        test_container.append(graph);
      }        
    }
    if(!had_graph) {
      test_tab.addClass('disabled');  // doesn't work; maybe our bootstrap version is old?
      test_container.append($('<h2>No significant results for ' + test + ' with these mental variables.</h3>'));
    }
    test_container.append($('<div class="clearfix"></div>'));
    ++test_tab_number;
  }
  $("#graphs_nav").tab();
}

function prepare_series(cvs, mv_combos, all_mvs, variable_map) {
  // If no continuous variables; make one bar chart.
  // Otherwise, make one scatterplot where the x is the product of the x's for each cv in cvs.
  // Return an array: one series for each significant combination of discrete variables.
  var series = [];
  var bar = !cvs.length;
  for(var mv_combo_index in mv_combos) {
    // For each combo, we need to grab every data point that matches that combo.
    // If mv_combo is [], then we call it "Baseline" and get every point where the default state is chosen for each mental variable.
    // For each series of points, we calculate the new mean and stderr, and make a data series in 'series' with its name and an array of points as dicts with mean and stderr.
    var mv_combo = mv_combos[mv_combo_index];
    var mv_combo_mv_names = [];
    for(var mv_combo_mv_index in mv_combo) {
      var mv_and_state = mv_combo[mv_combo_mv_index];
      var mv_name = mv_name_from_number(mv_and_state[0], variable_map);
      var mv_state = mv_state_from_number(mv_and_state[0], mv_and_state[1], variable_map);
      mv_combo_mv_names.push(mv_name + ": " + mv_state);
    }
    var series_name = mv_combo.length ? mv_combo_mv_names.join(", ") : "Baseline";
    var points = [];
    for(var point_index = 0; point_index < all_mvs[0].sessions.length; ++point_index) {
      var point;
      var matches = true;
      var x = 1;  // used for scatter, not bar
      for(var mv_index in all_mvs) {
        play_variables = variable_map;
        point = all_mvs[mv_index].sessions[point_index];
        if(mv_combo.length) {
          for(var mv_combo_pair_index in mv_combo)
            if(mv_combo[mv_combo_pair_index][0] == mv_index &&
               point.x != mv_combo[mv_combo_pair_index][1]) {
              matches = false;
              break;
            }
        }
        //else if(bar && point.x != 0 && variable_map[mv_index].type == 'discrete') {
        else if(bar && point.x != 0 && mv_type_from_number(mv_index, variable_map) === "discrete") {
          // Match only first state from every discrete variable for the baseline. This still doesn't make sense to Nick.
          matches = false;
          break;
        }
        for(var cv_index in cvs) {
          var cv = cvs[cv_index];
          if(cv == mv_index) {
            var multiplier = restore_original_units(point.x, mv_index, variable_map);
            if(x == 1)
              x = multiplier;
            else
              x *= multiplier;
          }
        }
      }
      if(matches)
        points.push({x: x, y: point.y, stderr: point.stderr});
    }
    if(bar) {
      var mean_and_stderr = pool_means_and_stderrs(points);
      var mean = restore_original_units(mean_and_stderr[0]);
      var stderr = restore_original_units(mean_and_stderr[1]);
      series.push({name: series_name, value: mean, stderr: stderr});
    }
    else
      series.push({name: series_name, points: points});
  }
  return series;
}


function parseDate(s) {
  // Return number of days since 2011-01-01 for easy day calculations
  return Math.round(((new Date(s.split('-')[0], s.split('-')[1] - 1, s.split('-')[2])).getTime() - (new Date(2011, 0, 1)).getTime()) / 86400 / 1000);
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function cartesianProduct() {
  return Array.prototype.reduce.call(arguments, function(a, b) {
    var ret = [];
    $.each(a, function(i, v1) {
      $.each(b, (function(j, v2) {
        ret.push(v1.concat([v2]));
      }));
    });
    return ret;
  }, [[]]);
}

function state_sort_function(a, b) {
  if(isNumber(a) && isNumber(b))
    return parseFloat(a) - parseFloat(b);
  var defaults = ["unknown", "none", "no", "zero", "false"];
  if(!isNumber(b) && $.inArray(b.toLowerCase(), defaults) != -1)
    return 1;
  if(!isNumber(a) && $.inArray(a.toLowerCase(), defaults) != -1)
    return -1;
  return a == b ? 0 : a < b ? -1 : 1;
}

function mv_mean_from_number(mv_index, variable_map) {
  for(var mv_name in variable_map) {
    if(variable_map[mv_name].index == mv_index) {
      if (mv_name === "Session") {
        return 0;
      }
      return variable_map[mv_name].mean;
    }
  }
}

function mv_type_from_number(mv_index, variable_map) {
  for(var mv_name in variable_map)
    if(variable_map[mv_name].index == mv_index)
      return variable_map[mv_name].continuous ? "continuous" : "discrete";
}

function mv_name_from_number(mv_index, variable_map) {
  for(var mv_name in variable_map)
    if(variable_map[mv_name].index == mv_index)
      return mv_name;
}

function mv_state_from_number(mv_index, state_index, variable_map) {
  for(var mv_name in variable_map)
    if(variable_map[mv_name].index == mv_index)
      return variable_map[mv_name].states[state_index];
}

function mv_names_from_numbers(mv_indices, variable_map) {
  var names = [];
  for(mv_index in mv_indices)
    names.push(mv_name_from_number(mv_indices[mv_index], variable_map));
  return names.join(" x ");
}

function pool_means_and_stderrs(points) {
  var w = 0;
  var xw = 0;
  for (var i in points) {
    var wi = 1 / (points[i].stderr * points[i].stderr);
    // hack
    // wi = 1 / points.length;
    w += wi;
    xw += wi * points[i].y;
  }
  var pooled_mean = xw / w;
  var pooled_stderr = Math.sqrt(1 / w);
  return [pooled_mean, pooled_stderr];
}

function restore_original_units(x, mv_index, variable_map) {
  if(!variable_map) return x;
  var new_x = x + mv_mean_from_number(mv_index, variable_map);
  if(mv_name_from_number(mv_index, variable_map) == "Date")
    new_x = new Date((new Date(2011, 0, 1)).getTime() + new_x * 86400 * 1000);
  else if(mv_name_from_number(mv_index, variable_map) == "Time")
    new_x = new Date((new Date(2011, 0, 1)).getTime() + x * 3600 * 1000);
  return new_x;
}
