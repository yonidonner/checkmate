// A grid widget allowing generating, drawing, and reading various kinds of input from a grid.
function Grid(nRows, nCols, cellSize) {
  this.nRows = nRows;
  this.nCols = nCols;
  this.random_id = Math.floor(Math.random() * 1000000);
  this.cellSize = cellSize;
  this.width = (cellSize+2) * nCols;
  this.height = (cellSize+2) * nRows;
//  eval("window." + gridGlobalName(this) + " = this;");
  return this;
}

//function gridGlobalName(grid) {
//  return "grid_" + grid.random_id;
//}

function gridCreate(grid, el) {
  grid.el = el;
  grid.el.html(gridCreateHtml(grid));
}

function gridCellName(grid, cellX, cellY) {
    return 'grid_cell_' + grid.random_id + '_' + cellX + '_' + cellY;
}

function gridCreateHtml(grid, with_done_button, with_clear_button) {
  var html = '<div id="grid_' + grid.random_id + '">';
  html += '<table class="grid_table" style="width: ' + grid.width + 'px; height: ' + grid.height + 'px;">';
  for (var i = 0; i < grid.nRows; ++i) {
    html += '<tr height="' + grid.cellSize + '">';
    for (var j = 0; j < grid.nCols; ++j) {
      html += '<td class="grid_cell" id="' + gridCellName(grid, i, j) + '" style="padding: ' + grid.cellSize / 2 + 'px"></td>';
    }
    html += '</tr>';
  }
  html += '</table>';
  html += '</div>';
  if (with_done_button === true) {
    html += '<div class="grid_prompt_area">';
    html += '<div>What was the sequence?</div>';
    html += '<div>';
    if(with_clear_button)
      html += '<button id="grid_clear_button_' + grid.random_id + '" class="btn-large">Clear</button> &nbsp; &nbsp; ';
    html += '<button id="grid_done_button_' + grid.random_id + '" class="btn-large btn-primary">Done</button>';
    html += '</div>';
    html += '</div>';
  }
  return html;
}

function gridGetDoneButton(grid) {
  return $('button#grid_done_button_' + grid.random_id);
}

function gridGetClearButton(grid) {
  return $('button#grid_clear_button_' + grid.random_id);
}

function gridGetCell(grid, i, j) {
  return $('td#' + gridCellName(grid, i,j));
}

function forAllCells(grid, callback) {
  for (var i = 0; i < grid.nRows; ++i) {
    for (var j = 0; j < grid.nCols; ++j) {
      callback(gridGetCell(grid, i, j), i, j);
    }
  }
}

function gridCellClear(cell, clearSequence) {
  if(clearSequence)
    cell.removeClass('cellSequenced cellSequenced2 cellSequenced3');
  return cell.removeClass('cellOn cellOff cellOnHover cellOffHover');
}
  
function cellSetStyle(cell, style, clearSequence) {
  if(style == 'cellSequenced' && cell.hasClass('cellSequenced'))
    style = 'cellSequenced2';
  if(style == 'cellSequenced2' && cell.hasClass('cellSequenced2'))
    style = 'cellSequenced3';
  return gridCellClear(cell, clearSequence).addClass(style);
}

function clearGrid(grid, clearSequence) {
  forAllCells(grid, function(cell, i, j) {
    cellSetStyle(cell, 'cellOff', clearSequence);
  });
}

function getGridElement(grid) {
  return $('div#grid_' + grid.random_id);
}

function gridStartDisplaySequence(grid, el, sequence, element_on_time, element_off_time, callback) {
  grid.sequence_position = -1;
  grid.sequence = sequence;
  grid.sequence_in_progress = true;
  grid.sequence_element_off_time = element_off_time;
  grid.sequence_element_on_time = element_on_time;
  grid.sequence_done_callback = callback;
  grid.el = el;
  grid.el.html(gridCreateHtml(grid));
  grid.el.show();
  gridDisplayNextInSequence(grid);
}

function gridSequenceElementOff(grid) {
  var i = grid.sequence[grid.sequence_position][0];
  var j = grid.sequence[grid.sequence_position][1];
  cellSetStyle(gridGetCell(grid, i, j), 'cellOff');
  setTimeout(gridDisplayNextInSequence, grid.sequence_element_off_time, grid);
}

function gridDisplayNextInSequence(grid) {
  if (++grid.sequence_position == grid.sequence.length) {
    removeGrid(grid);
    grid.sequence_done_callback();
    return;
  }
  var i = grid.sequence[grid.sequence_position][0];
  var j = grid.sequence[grid.sequence_position][1];
  cellSetStyle(gridGetCell(grid, i, j), 'cellOn');
  setTimeout(gridSequenceElementOff, grid.sequence_element_on_time, grid);
}

function gridSetupRead(grid, el, callback, type) {
  grid.el = el;
  grid.reading_design = false;
  if (typeof type === "undefined") {
    type = "single";
  }
  if (type === "sequence") {
    grid.el.html(gridCreateHtml(grid, true, true));
    gridGetDoneButton(grid).bind('click', {grid: grid, callback: callback}, gridSequenceDoneCallback);
    gridGetClearButton(grid).bind('click', {grid: grid}, gridSequenceClear);
    grid.reading_sequence = true;
    grid.user_sequence = [];
    gridSetupRead(grid, el, gridSequenceCallback, "sequence_element");
    return;
  }
  var keep_after = false;
  if (type === "sequence_element") {
    keep_after = true;
  }
  else {
    if (type === "binary_design") {
      grid.el.html(gridCreateHtml(grid, true, false));
      gridGetDoneButton(grid).bind('click', {grid: grid, callback: callback}, gridDesignDoneCallback);
      grid.reading_design = true;
      grid.user_design = [];
      for (var i = 0; i < grid.nRows; ++i) {
        grid.user_design[i] = [];
        for (var j = 0; j < grid.nCols; ++j) {
          grid.user_design[i][j] = false;
        }
      }
    }
    else {
      grid.el.html(gridCreateHtml(grid, false));
    }
  }
  grid.mouse_i = false;
  grid.mouse_j = false;
  grid.click_state = "no click";
  forAllCells(grid, function(cell, i, j) {
    cell.bind('mouseenter', {grid: grid, i: i, j: j}, gridMouseEnter);
    cell.bind('mouseleave', {grid: grid, i: i, j: j}, gridMouseLeave);
    cell.bind('mousedown', {grid: grid, i: i, j: j}, gridMouseDown);
    cell.bind('mouseup', {grid: grid, i: i, j: j, callback: callback, keep_after: keep_after}, gridMouseUp);
  });
  clearGrid(grid);
  grid.el.show();
}

function gridMouseEnter(event) {
  var grid = event.data.grid;
  var i = event.data.i;
  var j = event.data.j;
  var new_style = 'cellOffHover';
  if ((grid.reading_design) && (grid.user_design[i][j])) {
    new_style = 'cellOnHover';
  }
  cellSetStyle(gridGetCell(grid, i, j), new_style);
}

function gridMouseLeave(event) {
  var grid = event.data.grid;
  var i = event.data.i;
  var j = event.data.j;
  grid.click_state = "no click";
  var new_style = 'cellOff';
  if ((grid.reading_design) && (grid.user_design[i][j])) {
    new_style = 'cellOn';
  }
  cellSetStyle(gridGetCell(grid, i, j), new_style);
}

function gridMouseDown(event) {
  var grid = event.data.grid;
  var i = event.data.i;
  var j = event.data.j;
  grid.click_state = "click";
  var new_style = 'cellOn';
  if ((grid.reading_design) && (grid.user_design[i][j])) {
    new_style = 'cellOff';
  }
  cellSetStyle(gridGetCell(grid, i, j), new_style);
}

function gridMouseUp(event) {
  var grid = event.data.grid;
  var i = event.data.i;
  var j = event.data.j;
  var callback = event.data.callback;
  var keep_after = event.data.keep_after;
  if (grid.click_state === "click") {
    grid.click_state = "no click";
    if (grid.reading_design === true) {
      grid.user_design[i][j] = !grid.user_design[i][j];
      return;
    }
    if(!grid.reading_sequence)
      cellSetStyle(gridGetCell(grid, grid.mouse_i, grid.mouse_j), 'cellOff');
    else
      cellSetStyle(gridGetCell(grid, i, j), 'cellSequenced');
    if (keep_after !== true) {
      removeGrid(grid);
    }
    callback(grid, i, j);
  }
}

function removeGrid(grid) {
  grid.el.html("");
}

function gridSequenceCallback(grid, i, j) {
  grid.user_sequence.push([i, j]);
  gridSetupRead(grid, grid.el, gridSequenceCallback, "sequence_element");
}

function gridSequenceDoneCallback(event) {
  var grid = event.data.grid;
  var callback = event.data.callback;
  removeGrid(grid);
  callback(grid, grid.user_sequence);
}

function gridSequenceClear(event) {
  event.data.grid.user_sequence = [];
  clearGrid(event.data.grid, true);
}

function gridDesignDoneCallback(event) {
  var grid = event.data.grid;
  var callback = event.data.callback;
  removeGrid(grid);
  callback(grid, grid.user_design);
}

function gridMask(grid, maskRepeats, maskRepeatTime, afterMaskCallback) {
  grid.masking = true;
  grid.maskRepeats = maskRepeats;
  grid.maskRepeatTime = maskRepeatTime;
  grid.afterMaskCallback = afterMaskCallback;
  gridMaskNext(grid);
}

function gridStopMasking(grid) {
  grid.masking = false;
}

function gridMaskNext(grid) {
  if (grid.masking !== true) {
    return;
  }
  if (grid.maskRepeats === 0) {
    grid.afterMaskCallback();
  }
  else {
    --grid.maskRepeats;
    clearGrid(grid);
    for (var i = 0; i < grid.nRows; ++i) {
      for (var j = 0; j < grid.nCols; ++j) {
        if (Math.random() < 0.5) {
          cellSetStyle(gridGetCell(grid, i, j), 'cellOn');
        }
      }
    }
    setTimeout(gridMaskNext, grid.maskRepeatTime, grid);
  }
}

function randomizeDesign(nRows, nCols, complexity) {
  return greedyGrid(nRows, nCols, complexity);
  var perm = randomPermutation(nRows * nCols);
  var design = [];
  for (var i = 0; i < nRows; ++i) {
    design[i] = [];
    for (var j = 0; j < nCols; ++j) {
      design[i][j] = false;
    }
  }
  for (var perm_i = 0; perm_i < complexity; ++perm_i) {
    var row = Math.floor(perm[perm_i] / nCols);
    var col = perm[perm_i] % nCols;
    design[row][col] = true;
  }
  return design;
}

function isConnectedDesign(design) {
  var i1,i2,j1,j2;
  var nRows = design.length;
  var nCols = design[0].length;
  for (i1 = 0; i1 < nRows; ++i1) {
    for (i2 = 0; i2 < nCols; ++i2) {
      if (design[i1][i2]) {
        var anyNeighbor = false;
        for (j1 = -1; j1 <= 1; ++j1) {
          for (j2 = -1; j2 <= 1; ++j2) {
            if ((j1 != 0) || (j2 != 0)) {
              if (((i1 + j1) >= 0) && ((i1 + j1) < nRows) && ((i2 + j2) >= 0) && ((i2 + j2) < nCols)) {
                if (design[i1+j1][i2+j2]) {
                  anyNeighbor = true;
                }
              }
            }
          }
        }
        if (!anyNeighbor) {
          return false;
        }
      }
    }
  }
  return true;
}

function randomizeConnectedDesign(nRows, nCols, complexity) {
  while (true) {
    var design = randomizeDesign(nRows, nCols, complexity);
    if (isConnectedDesign(design)) {
      return design;
    }
  }
}

function mutateDesign(design, nMutations) {
  var i, row, col, nRows, nCols, result;
  nRows = design.length;
  nCols = design[0].length;
  var allMutations = [];
  var neighborhood = [[-1,0],[1,0],[0,-1],[0,1]];
  result = [];
  while (true) {
    for (row = 0; row < nRows; ++row) {
      for (col = 0; col < nCols; ++col) {
        if (design[row][col]) {
          for (i in neighborhood) {
            var neighbor = neighborhood[i];
            var neighbor_row = row + neighbor[0];
            var neighbor_col = col + neighbor[1];
            if ((neighbor_row >= 0) && (neighbor_col >= 0) && (neighbor_row < nRows) && (neighbor_col < nCols)) {
              if (!design[neighbor_row][neighbor_col]) {
                allMutations.push([[row,col],[neighbor_row,neighbor_col]]);
              }
            }
          }
        }
      }
    }
    if (allMutations.length >= nMutations) {
      break;
    }
  }
  var perm = randomPermutation(allMutations.length);
  for (i = 0; i < nMutations; ++i) {
    var change = allMutations[perm[i]];
    var prv_row = change[0][0];
    var prv_col = change[0][1];
    var new_row = change[1][0];
    var new_col = change[1][1];
    var mutation = [];
    for (row = 0; row < nRows; ++row) {
      mutation[row] = [];
      for (col = 0; col < nCols; ++col) {
        mutation[row][col] = design[row][col];
      }
    }
    mutation[prv_row][prv_col] = false;
    mutation[new_row][new_col] = true;
    result[i] = mutation;
  }
  return result;
}

function gridDisplayDesign(grid, design) {
  var i,j;
  for (i = 0; i < grid.nRows; ++i) {
    for (j = 0; j < grid.nCols; ++j) {
      if (design[i][j]) {
        cellSetStyle(gridGetCell(grid, i, j), 'cellOn');
      }
    }
  }
}
