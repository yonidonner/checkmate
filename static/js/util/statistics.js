// Statistics classes for accumulating, computing and displaying statistics.
var MeanStdevStatistic = Class.extend({
  init: function(el) {
    this.el = el;
    this.reset();
    return this;
  },

  reset: function() {
    this.n = 0;
    this.sum1 = 0;
    this.sum2 = 0;
    this.el.html("");
  },

  add: function(result) {
    this.lastResult = result;
    ++this.n;
    this.sum1 += result;
    this.sum2 += (result * result);
    this.recompute();
    this.el.html(this.format());
  },

  recompute: function() {
    this.mean = this.sum1 / this.n;
    this.variance = (this.sum2 / this.n) - (this.mean * this.mean);
    this.stdev = Math.sqrt(this.variance);
  },

  format: function() {
    return (this.lastResult.toString()) + ((this.n > 1) ? (" (" + Math.floor(this.mean).toString() + "&plusmn;" + Math.floor(this.stdev).toString() + ")") : "");
  }
});

var BinaryStatistic = Class.extend({
  init: function(el) {
    this.el = el;
    this.reset();
    return this;
  },

  reset: function() {
    this.total = 0;
    this.totalSuccesses = 0;
    this.el.html("");
  },

  add: function(result) {
    ++this.total;
    this.lastResult = result;
    if (result) {
      ++this.totalSuccesses;
    }
    this.el.html(this.format());
  },

  format: function() {
    return formatBinaryStatistic(this.lastResult, this.totalSuccesses, this.total);
  }
});

function formatBinaryStatistic(currentValue, totalTrue, total) {
  var s1 = (currentValue ? "&#x2713;" : "&#x2717;");
  var s2 = totalTrue.toString() + "/" + total.toString() + ((total > 0) ? (" (" + Math.floor(totalTrue * 100 / total).toString() + "&#37;)") : "");
  return s1 + " " + s2;
};

var LevelStatistic = Class.extend({
  init: function(el) {
    this.el = el;
    this.reset(0.5);
    return this;
  },

  reset: function(guessingProbability, a_vals) {
    this.guessingProbability = guessingProbability;
    this.target = 0.5 * (1.0 + guessingProbability);
    this.positives = [];
    this.negatives = [];
    this.a = 0.0;
    this.b = 0.0;
    this.estimate = false;
    this.el.html("");
    this.a_vals = a_vals;
  },

  setCurrent: function(currentLevel) {
    this.level = currentLevel;
    this.el.html(this.format());
  },

  add: function(result) {
    if (result) {
      this.positives.push(this.level);
    }
    else {
      this.negatives.push(this.level);
    }
    this.recompute();
    this.el.html(this.format());
  },

  recompute: function() {
    if ((this.positives.length > 0) && (this.negatives.length > 0)) {
      var bsres = bayesian_sigmoid(this.positives, this.negatives, this.a_vals, this.guessingProbability, this.target);
      this.estimate = bsres[0];
      this.stderr = bsres[1];
    }
  },

  recomputeMean: function() {
    if ((this.positives.length > 0) && (this.negatives.length > 0)) {
      var s=0.0;
      var n=0;
      var i;
      for (i in this.positives) {
        s += this.positives[i];
        n++;
      }
      for (i in this.negatives) {
        s += this.negatives[i];
        n++;
      }
      this.estimate = s / n;
    }
  },

  recomputeSigmoid: function() {
    if ((this.positives.length > 2) && (this.negatives.length > 2)) {
      var ab = sigmoid_fit(this.positives, this.negatives, this.a, this.b);
      this.a = ab[0];
      this.b = ab[1];
      this.estimate = (this.target_x - this.b) / this.a;
      if (this.estimate < 0) {
        this.estimate = false;
      }
    }

  },

  format: function() {
    return (toFixed2(this.level)) + ((this.estimate === false) ? "" : (" (" + this.estimate.toFixed(2) + "&plusmn;" + Math.floor(this.stderr).toString() + ")"));
  }
});

var toFixed2 = function(num) {
  if (!num) {
    return num;
  }
  if (num == Math.floor(num)) {
    return num.toString();
  }
  else {
    return num.toFixed(2);
  }
};

var randomPermutation = function(permSize) {
  var available = new Array();
  for (var i = 0; i < permSize; ++i) {
    available[i] = i;
  }
  var permutation = new Array();
  for (i = 0; i < permSize; ++i) {
    var j = Math.floor(Math.random()*(permSize-i));
    permutation[i] = available[j];
    available.splice(j, 1);
  }
  return permutation;
};

var randomSubset = function(array, nElem) {
  var available = new Array();
  for (var i = 0; i < array.length; ++i) {
    available[i] = i;
  }
  var result = new Array();
  for (i = 0; i < nElem; ++i) {
    var j = Math.floor(Math.random()*available.length);
    result[i] = array[available[j]];
    available.splice(j, 1);
  }
  return result;  
};