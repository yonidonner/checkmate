var sigmoid_fit = function(positives, negatives, initial_a, initial_b) {
  a = (typeof initial_a == 'undefined') ? 0.0 : initial_a;
  b = (typeof initial_b == 'undefined') ? 0.0 : initial_b;
  while (true) {
    der_a = 0.0;
    der_b = 0.0;
    H11 = 0.0;
    H12 = 0.0;
    H21 = 0.0;
    H22 = 0.0;
    f0 = 0.0;
    for (bi = 0; bi < 2; ++bi) {
      if (bi == 0) {
        Xs = negatives;
        y = 0;
      }
      else {
        Xs = positives;
        y = 1;
      }
      for (i in Xs) {
        x = Xs[i];
        q0 = Math.exp(a * x + b);
        q1 = q0 / (1.0 + q0);
        //console.log("a = " + a + " b = " + b + " x = " + x + " y = " + y + " q1 = " + q1);
        der_a += x * (q1 - y);
        der_b += (q1 - y);
        w = q1 * (1.0 - q1);
        xw = x * w;
        H11 += x * xw;
        H12 += xw;
        H22 += w;
        f0 += Math.log(1.0 + q0) - y * (a * x + b);
      }
    }
    det = (H11 * H22) - (H12 * H12);
    z = 1.0 / det;
    iH11 = z * H22;
    iH12 = -z * H12;
    iH22 = z * H11;
    dxnt_a = iH11 * (-der_a) + iH12 * (-der_b);
    dxnt_b = iH12 * (-der_a) + iH22 * (-der_b);
    lambdasq = -(der_a * dxnt_a + der_b * dxnt_b);
    if (lambdasq < 1e-8) {
      return [a, b];
    }
    t = 1.0
    //console.log("score: " + f0);
    while (true) {
      for (bi = 0; bi < 2; ++bi) {
        if (bi == 0) {
          Xs = negatives;
          y = 0;
        }
        else {
          Xs = positives;
          y = 1;
        }
        ca = a + dxnt_a * t;
        cb = b + dxnt_b * t;
        f1 = 0.0;
        for (i in Xs) {
          x = Xs[i];
          d = ca * x + cb;
          q2 = Math.exp(d);
          f1 += Math.log(1.0 + q2) - y * d;
        }
      }
      if (f1 < f0) {
        break;
      }
      t *= 0.5;
      if (t < 1e-8) {
        return [a,b];
      }
    }
    a = a + dxnt_a * t;
    b = b + dxnt_b * t;
  }
};

//pos = [1,2];
//neg = [3,4];
//console.log(sigmoid_fit(pos,neg));

var sigmoid_likelihood = function(positives, negatives, a, b, guessing_p) {
  var ll = 0.0;
  var i,l,p,x,y,j;
  prnd = 0.1;
  for (i = 0; i < 2; ++i) {
    if (i == 0) {
      l = positives;
      y = 1;
    }
    else {
      l = negatives;
      y = 0;
    }
    for (j in l) {
      x = l[j];
      p = prnd*0.5+(1.0-prnd)*(1.0-1.0/(1.0+Math.exp(a*(x-b))));
      p = p * 1.0 + (1 - p) * guessing_p;
      ll += (y * Math.log(p) + (1-y) * Math.log(1-p));
    }
  }
  return ll;
};

var bayesian_sigmoid = function(positives, negatives, a_vals, guessing_p, target_p) {
  var num_b = 100;
  // The logistic function here is: p(y=0)=1/(1+exp(a(x-b))).
  var b_min = positives[0];
  var b_max = positives[0];
  var i,j;
  var l;
  for (j = 0; j < 2; ++j) {
    if (j == 0) {
      l = positives;
    }
    else {
      l = negatives;
    }
    for (i in l) {
      if (l[i] > b_max) {
        b_max = l[i];
      }
      if (l[i] < b_min) {
        b_min = l[i];
      }
    }
  }
  var b_vals = new Array();
  for (i = 0; i < num_b; ++i) {
    b_vals[i] = b_min + (b_max - b_min) * i / (num_b - 1.0);
  }
  var res = new Array();
  var max_ll = null;
  for (i in a_vals) {
    var a = a_vals[i];
    for (j in b_vals) {
      var b = b_vals[j];
      var ll = sigmoid_likelihood(positives, negatives, a, b, guessing_p);
      var cur_res = [a,b,ll];
      res.push(cur_res);
      if ((!max_ll) || (ll > max_ll)) {
        max_ll = ll;
      }
    }
  }
  var s = 0.0;
  for (i in res) {
    s += Math.exp(res[i][2] - max_ll);
  }

  // We have p(y=1) = p0 + (1-p0)*p_g where p0=exp(a*(x-b))/(1+exp(a*(x-b))).
  // So if we want to meet the target t, we have:
  // t = p0 + (1-p0)*p_g
  // (t-p_g)/(1-p_g) = p0
  // exp(a*(x-b))=p0+p0*exp(a*(x-b))
  // exp(a*(x-b))*(1-p0)=p0
  // a*(x-b) = log(p0/(1-p0))
  // x-b = log(p0/(1-p0))/a
  // x = b + log(p0/(1-p))/a
  var est = 0.0;
  var target_p1 = (target_p - guessing_p) / (1.0 - guessing_p);
  var target_x = Math.log(target_p1 / (1.0 - target_p1));
  var var_est = 0.0;
  for (i in res) {
    var current_estimate = res[i][1] + target_x/res[i][0];
    var w = (Math.exp(res[i][2] - max_ll) / s);
    est += w * current_estimate;
    var_est += w * current_estimate * current_estimate;
  }
  var stderr = Math.sqrt((var_est - est*est));
  return [est,stderr];
};