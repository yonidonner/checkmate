// Greedily grow a grid

function chooseFirstPixel(nRows, nCols) {
  // Fill first pixel
  var firstRow = Math.floor(Math.random() * nRows);
  var firstCol = Math.floor(Math.random() * nCols);
  return [firstRow, firstCol];
}

function initializeEmptyGrid(nRows, nCols) {
  var grid = new Array();
  var i,j;
  // Initialize empty grid
  for (i = 0; i < nRows; ++i) {
    grid[i] = new Array();
    for (j = 0; j < nCols; ++j) {
      grid[i][j] = false;
    }
  }
  return grid;
}

function addPixelToGrid(grid, row, col, bordering) {
  var nRows = grid.length;
  var nCols = grid[0].length;
  grid[row][col] = true;
  bordering[row][col] = false;
  var dr,dc,nr,nc;
  for (dr = -1; dr <= 1; ++dr) {
    for (dc = -1; dc <= 1; ++dc) {
      nr = row + dr;
      nc = col + dc;
      if ((nr >= 0) && (nr < nRows) && (nc >= 0) && (nc < nCols)) {
        if (!grid[nr][nc]) {
          bordering[nr][nc] = true;
        }
      }
    }
  }
}

function choosePixelToAdd(grid, bordering, nLures) {
  var nRows = grid.length;
  var nCols = grid[0].length;
  var i,j,k;
  var pixels = new Array();
  k = 0;
  for (i = 0; i < nRows; ++i) {
    for (j = 0; j < nCols; ++j) {
      if ((!grid[i][j]) && (bordering[i][j])) {
        pixels[k] = new Array();
        pixels[k][0] = i;
        pixels[k][1] = j;
        ++k;
      }
    }
  }
  if (!nLures) {
    i = Math.floor(Math.random() * k);
    return pixels[i];
  }
  var perm = randomPermutation(k);
  var result = new Array();
  for (i = 0; i < nLures; ++i) {
    result[i] = pixels[perm[i]];
  }
  return result;
}

function copyGrid(grid) {
  var newGrid = new Array();
  var i,j;
  for (i = 0; i < grid.length; ++i) {
    newGrid[i] = new Array();
    for (j = 0; j < grid[i].length; ++j) {
      newGrid[i][j] = grid[i][j];
    }
  }
  return newGrid;
}

function greedyGrid(nRows, nCols, nFilled, nLures) {
  var grid = initializeEmptyGrid(nRows, nCols);
  var bordering = initializeEmptyGrid(nRows, nCols);
  var firstPixel = chooseFirstPixel(nRows, nCols);
  addPixelToGrid(grid, firstPixel[0], firstPixel[1], bordering);
  var i;
  for (i = 1; i < nFilled - ((nLures && (nLures > 1)) ? 1 : 0); ++i) {
    var newPixel = choosePixelToAdd(grid, bordering);
    addPixelToGrid(grid, newPixel[0], newPixel[1], bordering);
  }
  if ((nLures) && (nLures > 1)) {
    var newPixels = choosePixelToAdd(grid, bordering, nLures);
    var result = new Array();
    for (i = 0; i < nLures; ++i) {
      result[i] = copyGrid(grid);
      result[i][newPixels[i][0]][newPixels[i][1]] = true;
    }
    return result;
  }
  else {
    return grid;
  }
}
