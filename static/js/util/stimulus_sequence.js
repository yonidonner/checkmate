// Display a stimulus which is a sequence of elements with an on / off time.

function SequenceTimer(onTime, offTime, displayCallback, hideCallback, doneCallback) {
  this.onTime = onTime;
  this.offTime = offTime;
  this.displayCallback = displayCallback;
  this.hideCallback = hideCallback;
  this.doneCallback = doneCallback;
  return this;
}

function startSequence(sequenceTimer, sequenceLength) {
  sequenceTimer.sequenceIndex = -1;
  sequenceTimer.sequenceLength = sequenceLength;
  displayNextInSequence(sequenceTimer);
}

function stopSequence(sequenceTimer) {
  clearTimeout(sequenceTimer.timeout);
}

function displayNextInSequence(sequenceTimer) {
  if (++sequenceTimer.sequenceIndex == sequenceTimer.sequenceLength) {
    sequenceTimer.doneCallback();
  }
  else {
    sequenceTimer.displayCallback(sequenceTimer.sequenceIndex);
    sequenceTimer.timeout = setTimeout(function() {hideSequenceElement(sequenceTimer);}, sequenceTimer.onTime);
//    sequenceTimer.timeout = setTimeout(hideSequenceElement, sequenceTimer.onTime, [sequenceTimer]);
  }
}

function hideSequenceElement(sequenceTimer) {
  sequenceTimer.hideCallback(sequenceTimer.sequenceIndex);
  sequenceTimer.timeout = setTimeout(function() {displayNextInSequence(sequenceTimer);}, sequenceTimer.offTime);
  //sequenceTimer.timeout = setTimeout(displayNextInSequence, sequenceTimer.offTime, [sequenceTimer]);
}
