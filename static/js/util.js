// Our RPC wrapper fn -- just a wrapper for jQuery's ajax function so that we can make rpc calls in one line like this:
// rpc("name_of_method", [arg1,arg2], function(response){
//  do_something(response.whatever);
// });

function rpc(function_name, args, success, error, context) {
  if (!args) {args = new Array();};
  if (!success) { success = function(response) {return response;};};
  if (!error) { error = function(response) {success(response);}; }
  var params = new Array(function_name);
  for (var i = 0; i < args.length; i++) {params.push(args[i]);};
  var body = JSON.stringify(params);
  $.ajax({
    url: "/rpc",
    type: "POST",
    data: body,
    dataType: "html",
    success: function(response, textStatus) {success(response, textStatus);},
    error: function(jqXHR, textStatus, errorThrown) { error(jqXHR, textStatus, errorThrown);},
    context: context
  });
};


// http://stackoverflow.com/a/5199982
$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name] !== undefined) {
      if (!o[this.name].push)
        o[this.name] = [o[this.name]];
      o[this.name].push(this.value || '');
    }
    else
      o[this.name] = this.value || '';
  });
  return o;
};
