#### Simple Reaction Time

def SimpleReactionTime():
    ReactionTime()
    initSimpleReactionTimeKeyBinding()

def initSimpleReactionTimeKeyBinding():
    addKeyBinding(['space'], SimpleReactionTimeHandleKey)

def SimpleReactionTimeHandleKey(key):
    if (state == AFTER_STIMULUS_DISPLAY):
        responseCorrect = true
    ReactionTimeHandleResponse()

def stimulusOn():
    SimpleReactionTimeStimulusOn()

def SimpleReactionTimeStimulusOn():
    # turn on the circle

def stimulusOff():
    SimpleReactionTimeStimulusOff()
    # turn off the circle or remove the number

def start():
    SimpleReactionTimeStart()

def SimpleReactionTimeStart():
    ReactionTimeStart()
