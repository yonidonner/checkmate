#### Base

def Base():
    test = {}
    initBaseTestScreen()
    initStatistics()
    initKeyAndMouseBindings()
    initBaseKeyBinding()
    showIntroScreen()

def showIntroScreen():
    instructions = getInstructions()
    # show instructions
    # make start button and bind to start()

def start():
    baseStart()

def baseStart():
    startTime = currentTime()
    startTimer(testTimer)
    bindEvent(testTimer, "callback to testTimerEvent every 1 second")

def testTimerEvent():
    updateStatistic(duration, currentTime() - startTime())

def initKeyAndMouseBindings():
    # set up variables to enable easy key binding, mouse clicking etc..

def addKeyBinding(keys, function):
    # bind key to function

def initBaseKeyBinding():
    addKeyBinding(['q','Q','escape'], quit)

def initBaseStatistics():
    addStatistic("Test duration", durationStatistic)

def quit():
    baseQuit()

def baseQuit():
    isComplete = False
    exitTest()

def testFinished():
    baseTestFinished()

def baseTestFinished():
    isComplete = True
    generateResults()
    exitTest()

def generateResults():
    baseGenerateResults()

def baseGenerateResults():
    results = {}

def exitTest():
    BaseExitTest()

def BaseExitTest():
    endingScreen() # what should this do exactly?
    backToTestSelectionScreen() # how does this work?

def initBaseTestScreen():
    initClassroom()

def initClassroom():
    initTestArea()
    initStatisticsArea()
    initStartButton()

def initTestArea():
    # allocate some area for the test

def initStatisticsArea():
    # allocate some area for statistics
    test.statisticsRows = []

