#### Span

SpanDirections = [FORWARD, BACKWARD]

def Span(direction, initialSpan, delay0, elementOnTime, elementOffTime):
    LevelEstimation(initialSpan)

def SpanStart():
    LevelEstimationStart()

def createTrial():
    SpanCreateTrial()

def SpanCreateTrial():
    generateSequence(currentLevel)

def startStimulusDisplay():
    SpanStartStimulusDisplay()

def SpanStartStimulusDisplay():
    LevelEstimationStartStimulusDisplay()
    setupDisplaySequence()

def setupDisplaySequence():
    SpanSetupDisplaySequence()

def SpanSetupDisplaySequence():
    currentElement = -1
    setTimer(delay0, displayNextSequenceElement)

def displayNextSequenceElement():
    SpanDisplayNextSequenceElement()

def SpanDisplayNextSequenceElement():
    currentElement += 1
    if (currentElement == currentLevel):
        afterStimulusDisplay()
    else:
        displaySequenceElement(sequence[currentElement])

def displaySequenceElement(element):
    SpanDisplaySequenceElement(element)

def SpanDisplaySequenceElement(element):
    showElement(element)
    setTimer(elementOnTime, afterDisplaySequenceElement, element) # 2nd argument - callback function, 3rd argument - arguments to callback function

def afterDisplaySequenceElement(element):
    SpanAfterDisplaySequenceElement(element)

def SpanAfterDisplaySequenceElement(element):
    unshowElement(element)
    setTimer(elementOffTime, displayNextSequenceElement)

def afterStimulusDisplay():
    SpanAfterStimulusDisplay()

def SpanAfterStimulusDisplay():
    LevelEstimationAfterStimulusDisplay()
    setupReadSequence()

def showElement(element):
    SpanShowElement(element)

def SpanShowElement(element):
    pass

def unshowElement(element):
    SpanUnshowElement(element)

def SpanUnshowElement(element):
    pass

def setupReadSequence():
    SpanSetupReadSequence()

def SpanSetupReadSequence():
    pass # this is totally test-specific

def handleUserInputSequence(userSequence):
    self.userSequence = userSequence
    if state == AFTER_STIMULUS_DISPLAY:
        if direction == FORWARD:
            responseCorrect = sequence == userSequence
        elif direction == BACKWARD:
            responseCorrect = sequence == reverse(userSequence)
    handleUserResponse() # this will call in turn EndTrial so we don't need to worry about that

def buildTrialResult():
    SpanBuildTrialResult()

def SpanBuildTrialResult():
    LevelEstimationBuildTrialResult()
    trialResult['realSequence'] = sequence
    trialResult['userSequence'] = userSequence

def generateResults():
    SpanGenerateResults()

def SpanGenerateResults():
    LevelEstimationGenerateResults()
    results['direction'] = direction
    results['delay0'] = delay0
    results['elementOnTime'] = elementOnTime
    results['elementOffTime'] = elementOffTime
