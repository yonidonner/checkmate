#### Spatial Span

def SpatialSpan(grid_x_size, grid_y_size, direction, initialSpan):
    Span(direction, initialSpan)
    grid = Grid(grid_x_size, grid_y_size)

def start():
    SpatialSpanStart()

def SpatialSpanStart():
    SpanStart()

def generateSequence(span):
    SpatialSpanGenerateSequence(span)

def SpatialSpanGenerateSequence(span):
    sequence = [(random.randint(0,grid_x_size), random.randint(0,grid_y_size)) for i in range(span)]

def showElement(element):
    gridElementOn(grid, element)

def unshowElement(element):
    gridElementOff(grid, element)

def buildTrialResult():
    SpatialSpanBuildTrialResult()

def SpatialSpanBuildTrialResult():
    SpanBuildTrialResult()

def generateResults():
    SpatialSpanGenerateResults()

def SpatialSpanGenerateResults():
    SpanGenerateResults()
    results['grid_x_size'] = grid_x_size
    results['grid_y_size'] = grid_y_size

def setupReadSequence():
    SpatialSpanSetupReadSequence()

def SpatialSpanSetupReadSequence():
    setupGridSequenceBindings(grid, SpatialSpanInputHandler)

def SpatialSpanInputHandler(sequence):
    # the input is always valid coming from the grid, so no input checking
    handleUserInputSequence(sequence)

