#### Digit Span

DigitSpanVariant = [AUDITORY, VISUAL, AUDIOVISUAL]

def DigitSpan(direction = FORWARD, variant = VISUAL, initialSpan):
    Span(direction, initialSpan)
    digits = '12345689'

def start():
    DigitSpanStart()

def DigitSpanStart():
    SpanStart()

def generateSequence(span):
    DigitSpanGenerateSequence(span)

def DigitSpanGenerateSequence(span):
    sequence = [random.choice(digits) for i in range(span)]

def showElement(element):
    if variant in [VISUAL, AUDIOVISUAL]:
        displayDigit(element)
    if variant in [AUDITORY, AUDIOVISUAL]:
        playDigitSound(element)

def unshowElement(element):
    if variant in [VISUAL, AUDIOVISUAL]:
        hideDigit()

def buildTrialResult():
    DigitSpanBuildTrialResult()

def DigitSpanBuildTrialResult():
    SpanBuildTrialResult()

def generateResults():
    DigitSpanGenerateResults()

def DigitSpanGenerateResults():
    SpanGenerateResults()
    results['variant'] = variant

def setupReadSequence():
    DigitSpanSetupReadSequence()

def DigitSpanSetupReadSequence():
    # show dialog box or something more pretty where the user can put input
    # show a "done" button and bind this and the enter key to call DigitSpanInputHandler

def DigitSpanInputHandler():
    userInput = readUserInput() # readUserInput returns whatever the user put in
    inputValid = True
    for i in range(len(userInput)):
        if userInput[i] not in digits:
            inputValid = False
    if inputValid:
        userSequence = userInput
    else:
        userSequence = []
    handleUserInputSequence(userSequence)
