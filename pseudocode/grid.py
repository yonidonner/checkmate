#### Grid: this is a helper class for presenting images or just fixed colors in a grid and reading user input.
#### It's not part of the test hierarchy but many tests use this.

grid_click_states = [NO_CLICK, DURING_CLICK]

def Grid(x_size, y_size):
    # generate and return a grid object of that dimensions
    click_state = NO_CLICK
    mouse_x = None
    mouse_y = None

def gridElementOn(grid, element):
    x,y = element
    # turn on (change background to white) element at (x,y)

def gridElementOff(grid, element):
    x,y = element
    # turn off (change background back to black) element at (x,y)

def setupGridBindings(grid, callback):
    # the way it works: when the user moves the mouse over a grid cell, it lights up in some pale dark blue or some other nice color. When the user clicks on a grid cell it turns white but nothing happens until they release the mouse button, and it only counts as a click if they release the button while still hovering over the same cell as they were when they clicked the button. Then the callback is called. If the user moves the mouse away from the grid cell before releasing it the cell turns black again.
    for x in range(grid.x_size):
        for y in range(grid.y_size):
            # the pseudocode syntax for handlers: event description, callback function, arguments
            bindMouseOver(gridElement(grid, x, y), gridMouseOver, grid, x, y)
            bindMouseButtonDown(gridElement(grid, x, y), gridMouseDown, grid, x, y)
            bindMouseButtonUp(gridElement(grid, x, y), gridMouseUp, grid, x, y, callback)

def setupGridSequenceBindings(grid, callback):
    # This sets up the grid for reading a sequence rather than a single click. It actually uses the regular single-element grid reader with an additional "done" button and a different callback.
    grid.sequence = []
    addGridDoneButton(grid, gridDoneCallback, callback)
    setupGridBindings(grid, gridSequenceCallback)

def setupGridBinaryDesignBindings(grid, callback):
    # This sets up the grid for reading a binary design, in which each element can be on or off. Also adds a "done" button. When the user clicks done, the callback function is called with the entire binary matrix.
    grid.design = zeros(grid.x_size, grid.y_size)
    addGridDoneButton(grid, gridBinaryDesignDoneCallback, callback)
    for x in range(grid.x_size):
        for y in range(grid.y_size):
            # the pseudocode syntax for handlers: event description, callback function, arguments
            bindMouseOver(gridElement(grid, x, y), gridBinaryDesignMouseOver, grid, x, y)
            bindMouseButtonDown(gridElement(grid, x, y), gridBinaryDesignMouseDown, grid, x, y)
            bindMouseButtonUp(gridElement(grid, x, y), gridBinaryDesignMouseUp, grid, x, y)

def gridSequenceCallback(grid, element):
    grid.sequence.append(element)
    setupGridBindings(grid, gridSequenceCallback)

def gridDoneCallback(grid, callback):
    removeGridDoneButton(grid)
    apply(callback, grid.sequence)

def gridBinaryDesignDoneCallback(grid, callback):
    removeGridDoneButton(grid)
    apply(callback, grid.design)

def gridMouseOver(grid, x, y):
    if grid.click_state == NO_CLICK:
        if grid.mouse_x is not None:
            gridElementOff(grid, grid.mouse_x, grid.mouse_y)
        grid.mouse_x = x
        grid.mouse_y = y
        gridSetCellBackground(grid, x, y, 'pale dark blue')
    elif grid.click_state == DURING_CLICK:
        gridElementOff(grid, grid.mouse_x, grid.mouse_y)

def gridMouseDown(grid, x, y):
    grid.mouse_x = x
    grid.mouse_y = y
    grid.click_state = DURING_CLICK
    gridElementOn(grid, (x, y))

def gridMouseUp(grid, x, y, callback):
    grid.click_state = NO_CLICK
    if (grid.mouse_x == x) and (grid.mouse_y == y):
        gridElementOff(grid, (x, y))
        removeGridMouseBindings()
        apply(callback, (x, y))

def gridBinaryDesignMouseOver(grid, x, y):
    if grid.click_state == NO_CLICK:
        if grid.mouse_x is not None:
            if grid.design[grid.mouse_x, grid.mouse_y]:
                gridElementOn(grid, (grid.mouse_x, grid.mouse_y))
            else:
                gridElementOff(grid, (grid.mouse_x, grid.mouse_y))
        grid.mouse_x = x
        grid.mouse_y = y
        if grid.design[x, y]:
            gridSetCellBackground(grid, x, y, 'some offwhite color')
        else:
            gridSetCellBackground(grid, x, y, 'pale dark blue')
    elif grid.click_state == DURING_CLICK:
        if grid.design[grid.mouse_x, grid.mouse_y]:
            gridElementOn(grid, (grid.mouse_x, grid.mouse_y))
        else:
            gridElementOff(grid, (grid.mouse_x, grid.mouse_y))

def gridBinaryDesignMouseDown(grid, x, y):
    grid.mouse_x = x
    grid.mouse_y = y
    grid.click_state = DURING_CLICK
    if grid.design[x, y]:
        gridElementOff(grid, (x, y))
    else:
        gridElementOn(grid, (x, y))

def gridBinaryDesignMouseUp(grid, x, y):
    grid.click_state = NO_CLICK
    if (grid.mouse_x == x) and (grid.mouse_y == y):
        if grid.design[x,y]:
            grid.design[x,y] = False
            gridElementOff(grid, (x, y))
        else:
            grid.design[x,y] = True
            gridElementOn(grid, (x, y))
