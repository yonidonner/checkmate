#### Level Estimation

def LevelEstimation(initialLevel, targetSuccessProbability = 0.8):
    TSR()
    currentLevel = initialLevel

def LevelEstimationStart():
    TSRStart()

def LevelEstimationStartStimulusDisplay():
    TSRStartStimulusDisplay()

def LevelEstimationAfterStimulusDisplay():
    TSRAfterStimulusDisplay()

def updateLevel():
    LevelEstimationUpdateLevel()

def LevelEstimationUpdateLevel():
    lastCurrentLevel = currentLevel # save the last level before changing it because we'll use it for the results and statistics update
    # by default we have a discrete level that goes up and down
    u = random.random() # a random number between 0 and 1
    if (responseCorrect) and (u < (1 - targetSuccessProbability)):
        currentLevel += 1
    elif (not responseCorrect) and (u < targetSuccessProbability):
        currentLevel -= 1

def endTrial():
    LevelEstimationEndTrial()

def LevelEstimationEndTrial():
    updateLevel()
    TSREndTrial()

def buildTrialResult():
    LevelEstimationBuildTrialResult()

def LevelEstimationBuildTrialResult():
    TSRBuildTrialResult()
    trialResult['level'] = lastCurrentLevel

def LevelEstimationGenerateResults():
    TSRGenerateResults()
