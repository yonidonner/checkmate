#### Multitrial

def Multitrial():
    Base()
    addStatistic("Trial", trialStatistic)
    finished = False
    trialResults = []

def MultitrialStart():
    BaseStart()
    nTrials = 0
    startNewTrial()

def startNewTrial():
    MultitrialStartNewTrial()

def MultitrialStartNewTrial():
    nTrials += 1
    updateStatistic(trial, nTrials)
    trialStartTime = currentTime()
    createTrial()

def createTrial():
    MultitrialCreateTrial()

def MultitrialCreateTrial():
    pass

def MultitrialEndTrial():
    trialDuration = currentTime() - trialStartTime
    trialResult = {}
    buildTrialResult(trialResult)
    trialResults.append(trialResult)
    finished = checkIfTestFinished()
    if finished:
        testFinished()
    else:
        startNewTrial()

def buildTrialResult():
    MultitrialBuildTrialResult()

def MultitrialBuildTrialResult():
    trialResult['Trial number'] = nTrials
    trialResult['Trial duration'] = trialDuration

def checkIfTestFinished():
    return False

def generateResults():
    MultitrialGenerateResults()

def MultitrialGenerateResults():
    BaseGenerateResults()
    results['trial results'] = trialResults

