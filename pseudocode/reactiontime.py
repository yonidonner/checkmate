#### ReactionTime

def ReactionTime(nTotalTrials):
    TSR()
    ReactionTimeInitStatistics()

def ReactionTimeInitStatistics():
    trialStatistic.setTotalTrials(nTotalTrials)

def ReactionTimeStart():
    TSRStart()

def checkIfTestFinished():
    return ReactionTimeCheckIfTestFinished()

def ReactionTimeCheckIfTestFinished():
    return (nTrials >= nTotalTrials)

def createTrial():
    ReactionTimeCreateTrial()

def ReactionTimeCreateTrial():
    TSRCreateTrial()
    ReactionTimeSetupDelay()

def ReactionTimeSetupDelay():
    delay = sampleExponential(exponentialParam) + constantParam

def startStimulusDisplay():
    ReactionTimeStartStimulusDisplay()

def ReactionTimeStartStimulusDisplay():
    TSRStartStimulusDisplay()
    stimulusOn()
    afterStimulusDisplay()

def afterStimulusDisplay():
    ReactionTimeAfterStimulusDisplay()

def ReactionTimeAfterStimulusDisplay():
    TSRAfterStimulusDisplay()

def setupUserResponse():
    ReactionTimeSetupUserResponse()

def ReactionTimeSetupUserResponse():
    TSRSetupUserResponse()

def handleResponse():
    ReactionTimeHandleResponse()

def ReactionTimeHandleResponse():
    TSRHandleResponse()

def endTrial():
    ReactionTimeEndTrial()

def ReactionTimeEndTrial():
    stimulusOff()
    TSREndTrial()
