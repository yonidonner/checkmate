This file describes the interfaces defined by the classes in the hierarchy and the statistics variables.

Base:
  getInstructions() - gets called by the showIntroScreen() method, should return the test-specific instructions
  start() - gets called when the user clicks the "start" button
  addKeyBinding(keys, function, arguments) - add the key bindings that a test responds to
  addstatistic(statisticName, statisticVariable) - adds rows to the statistics area of the classroom
  generateResults() - create the results object. Methods should add fields to the object called "results".
  quit() - gets called when the user clicks the "quit" button
  testFinished() - gets called when the test is correctly completed
  exitTest() - gets called when a test terminates either way (either finished or not)
  durationStatistic - total time spent in the current trial / test / all-time.

Multitrial:
  startNewTrial() - gets called to start a new trial
  createTrial() - set up the trial-specific variable (e.g. randomize a stimulus)
  endTrial() - gets called at the end of each trial (some other method should call it when a trial is over, the Multitrial class itself has no idea when a trial is over)
  buildTrialResult() - construct the trial-specific results object. Should add fields to the dicionary called "trialResult".
  checkIfTestFinished() - should return True if the test is completed, False otherwise. (Gets called at the end of each trial)
  trialStatistic - the number of the current trial / total trials in this test / total trials done since all-time.

TSR:
  beforeStimulusDelay - this is a variable that is set by default (in TSR itself) to 0, but if set otherwise in an implementation of createTrial() will cause this delay to be used before presenting the stimulus
  prestimulusDelayDone() - gets called beforeStimulusDelay ms after TSRStartNewTrial(). State is BEFORE_STIMULUS_DISPLAY during that time
  startStimulusDisplay() - start displaying the stimulus, switches state to DURING_STIMULUS_DISPLAY
  afterStimulusDisplay() - gets called when the display of the stimulus is finished. Afterwards state is set to AFTER_STIMULUS_DISPLAY
  setupUserResponse() - gets called after displaying the stimulus (in TSRAfterStimulusDisplay), should prepare for getting the response for the user
  handleResponse() - when the user is done entering input, this should be called, but first the variable responseCorrect should be set to True or False (only if state is AFTER_STIMULUS_DISPLAY, otherwise it is not necessary to set responseCorrect to anything). TSRHandleResponse() calls endTrial() so there is no need to call it directly, but be aware of the order of the calls: endTrial() will be called *before* handleResponse() is done. (Is this bad design? how can I avoid this?)
  setFeedbackMessage() - can be called to set the message that gets displayed to the user about how they did on the last trial
  reactionTimeStatistic - the user's last (and mean, standard deviation for the test) reaction time
  readyStatistic - "ready" is the opposite of "too early" (thanks Nick!), the trial version is a "v" box, the test version is a percentage
  correctStatistic - was the last response correct, the trial version is a "v" box and the test version is a percentage

ReactionTime:
  stimulusOn() - gets called to show the stimulus
  stimulusOff() - gets called to hide the stimulus at the end of the trial

LevelEstimation:
  updateLevel() - gets called at the end of each trial and should update the variable currentLevel based on the previous results. Usually it will only care about the last trial  

Span:
  generateSequence(span) - generate a random sequence of size span
  displayNextSequenceElement() - moves on to the next element in the sequence
  displaySequenceElement(element) - gets called for each one of the elements in the sequence, in turn. The Span implementation calls showElement(element), then waits elementOnTime, then calls unshowElement(element), then waits elementOffTime and moves to the next one.
  showElement(element) - show a single element in a sequence
  unshowElement(element) - hide element
  setupReadSequence() - gets called when the sequence is finished displaying and the user input should be read
  handleUserInputSequence(sequence) - should be called with the sequence read from the user. The default Span implementation compares this input to the correct one either forwards or backwards and sets responseCorrect.

HELPER CLASSES

Grid:
  Grid(x_size, y_size) - initializes the grid
  setupGridBindings(grid, callback) - makes the grid respond to mouse-over events and clicks. When the user clicks and releases the mouse button onan element, the callback is function is called.
  setupGridSequenceBindings(grid, callback) - adds a "done" button and sets up the grid to read an entire sequence until the user clicks done.
  setupGridBinaryDesignBindings(grid, callback) - adds a "done" button and sets up the grid to read a binary pattern where each call can be on or off, and user clicks toggle them on and off. When the user clicks done, the callback is called with the entire matrix.
