#### Choice Reaction Time

def ChoiceReactionTime(nChoices):
    ReactionTime()
    self.nChoices = nChoices
    setupChoiceReactionTimeKeys()

def setupChoiceReactionTimeKeys():
    for i in range(nChoices):
        addKeyBinding(['1'+i], ChoiceReactionTimeHandleKey)

def createTrial():
    choiceReactionTimeCreateTrial()

def ChoiceReactionTimeCreateTrial():
    ReactionTimeCreateTrial()
    trialType = random.randint(nChoices)

def ChoiceReactionTimeHandleKey(key):
    userResponse = key - '1'
    if (state == AFTER_STIMULUS_DISPLAY) and (userResponse == trialType):
        responseCorrect = true
    else:
        responseCorrect = false
    ReactionTimeHandleResponse()

def stimulusOn():
    ChoiceReactionTimeShowStimulus(trialType)

def ChoiceReactionTimeShowStimulus(stimulus):
    # turn on circle, or show a number, corresponding to stimulus

def stimulusOff():
    ChoiceReactionTimeStimulusOff()

def ChoiceReactionTimeStimulusOff():
    # turn off the circle or remove the number

def start():
    ChoiceReactionTimeStart()

def ChoiceReactionTimeStart():
    ReactionTimeStart()

def buildTrialResult():
    ChoiceReactionTimeBuildTrialResult()

def ChoiceReactionTimeBuildTrialResult():
    ReactionTimeBuildTrialResult()
    trialResult['trialType'] = trialType
    trialResult['userResponse'] = userResponse

