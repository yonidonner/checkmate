#### TSR

def TSR():
    Multitrial()
    addStatistic("Reaction Time", reactionTimeStatistic)
    addStatistic("Ready", readyStatistic)
    addStatistic("Correct", responseCorrectStatistic)

def TSRStart():
    MultitrialStart()

def createTrial():
    TSRCreateTrial()

def TSRCreateTrial():
    MultitrialCreateTrial()
    beforeStimulusDelay = 0

def startNewTrial():
    TSRStartNewTrial()

def TSRStartNewTrial():
    MultitrialStartNewTrial()
    state = BEFORE_STIMULUS_DISPLAY
    setTimer(beforeStimulusDelay, "callback to prestimulusDelayDone")

def prestimulusDelayDone():
    startStimulusDisplay()

def startStimulusDisplay():
    TSRStartStimulusDisplay()

def TSRStartStimulusDisplay():
    stimulusDisplayStartTime = currentTime()
    state = DURING_STIMULUS_DISPLAY

def afterStimulusDisplay():
    TSRAfterStimulusDisplay()

def TSRAfterStimulusDisplay():
    state = AFTER_STIMULUS_DISPLAY
    setupUserResponse()

def setupUserResponse():
    TSRSetupUserResponse()

def TSRSetupUserResponse():
    userResponseStartTime = getCurrentTime()

def handleResponse():
    TSRHandleResponse()

def TSRHandleResponse():
    if state != AFTER_STIMULUS_DISPLAY:
        setFeedbackMessage("Clicked too early!")
        tooEarly = true
    else:
        tooEarly = false
        reactionTime = getCurrentTime() - userResponseStartTime
        if responseCorrect:
            setFeedbackMessage("Reaction time: %d ms"%reactionTime)
        else:
            setFeedbackMessage("Wrong!")
    endTrial()

def endTrial():
    TSREndTrial()

def TSREndTrial():
    if tooEarly:
        updateStatistic(readyStatistic, "x")
        updateStatistic(responseCorrectStatistic, "x")
    else:
        updateStatistic(readyStatistic, "v")
        if responseCorrect:
            updateStatistic(responseCorrectStatistic, "v")
        else:
            updateStatistic(responseCorrectStatistic, "x")
    if tooEarly or (not responseCorrect):
        updateStatistic(reactionTimeStatistic, "-")
    else:
        updateStatistic(reactionTimeStatistic, reactionTime)
    showFeedbackMessage()
    MultitrialEndTrial()

def showFeedbackMessage():
    # display feedbackMessage somewhere

def buildTrialResult():
    TSRBuildTrialResult()

def TSRBuildTrialResult():
    MultitrialBuildTrialResult()
    trialResult['tooEarly'] = tooEarly
    trialResult['reactionTime'] = reactionTime
    trialResult['responseCorrect'] = responseCorrect
 
