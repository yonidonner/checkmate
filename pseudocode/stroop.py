#### Stroop

def Stroop(nColors = 3, nBaseTrials = 5):
    TSR()
    StroopInitColors()
    StroopSetupKeyBinding()
    StroopInitStatistics()
    stroopTestParts = [INHIBITION, CONTEXT_SWITCHING, BOTH]

def StroopInitStatistics():
    trialStatistic.setTotalTrials(18 * nBaseTrials)

def StroopInitColors():
    allColors = [(RED, ['r','R']),
                 (GREEN, ['g', 'G']),
                 (BLUE, ['b', 'B'])]

def StroopSetupKeyBinding():
    for i in range(nColors):
        for key in allColors[i][1]:
            addKeyBinding(key, StroopUserResponse, allColors[i][0])

def StroopUserResponse(color):
    if state == AFTER_STIMULUS_DISPLAY:
        userResponse = color
        responseCorrect = (color == trialCorrectResponse)
    StroopHandleUserResponse()

def start():
    StroopStart()

def StroopStart():
    stroopFinished = False
    StroopCreateTestParts()
    currentTestPart = -1
    StroopNextTestPart()
    TSRStart()

def StroopCreateTestParts():
    order = []
    order.append(stroopTestParts[random.randint(3)])
    order.append([x for x in stroopTestParts if x != order[0]][random.randint(2)])
    order.append([x for x in stroopTestParts if x not in order][0])

def StroopNextTestPart():
    currentTestPart += 1
    if currentTestPart == len(order):
        stroopFinished = True
        return
    currentTestPartType = order[currentTestPart]
    nRemainingPartTrials = 6 * nBaseTrials
    if currentTestPartType == INHIBITION:
        inhibitionSubPart = 0
        inhibitionSubParts = [(True, True, nBaseTrials),
                              (False, True, 2 * nBaseTrials),
                              (True, False, nBaseTrials),
                              (False, True, 2 * nBaseTrials)]
        nRemainingSubPartTrials = inhibitionSubParts[inhibitionSubPart][2]

def checkIfTestFinished():
    return stroopFinished

def createTrial():
    StroopCreateTrial()

def StroopChooseWordAndColor():
    currentWord = random.randint(nColors)
    if isConcordant:
        currentColor = currentWord
    else:
        currentColor = random.randint(nColors - 1)
        if currentColor == currentWord:
            currentColor = currentColor + 1

def StroopCreateTrial():
    TSRCreateTrial()
    if currentTestPartType == CONTEXT_SWITCHING:
        isConcordant = False
        StroopChooseWordAndColor()
        isTaskWord = random.randBoolean()
    elif currentTestPartType == BOTH:
        currentWord = random.randint(nColors)
        currentColor = random.randint(nColors)
        isTaskWord = random.randBoolean()
    elif currentTestPartType == INHIBITION:
        isConcordant, isTaskWord = inhibitionSubParts[inhibitionSubPart][:2]
        StroopChooseWordAndColor()

def StroopHandleResponse():
    TSRHandleResponse()

def endTrial():
    StroopEndTrial()

def StroopEndTrial():
    hideStroopWord()
    if currentTestPartType == INHIBITION:
        nRemainingSubpartTrials -= 1
        if nRemainingSubpartTrials == 0:
            inhibitionSubPart += 1
            if inhibitionSubPart == len(inhibitionSubParts):
                StroopNextTestPart()
            else:
                nRemainingSubpartTrials = inhibitionSubParts[inhibitionSubPart][2]
    else:
        nRemainingPartTrials -= 1
        if nRemainingPartTrials == 0:
            StroopNextTestPart()
    TSREndTrial()

def buildTrialReult():
    StroopBuildTrialResult()

def StroopBuildTrialResult():
    TSRBuildTrialResult()
    trialResult['color'] = currentColor
    trialResult['word'] = currentWord
    trialResult['task'] = isTaskWord
    trialResult['concordant'] = (currentColor == currentWord)
    trialResult['userResponse'] = userResponse
    trialResult['correct'] = responseCorrect
    trialResult['test part'] = currentTestPartType

def generateResults():
    StroopGenerateResults()

def StroopGenerateResults():
    TSRGenerateResults()
    results['number of colors'] = nColors
    results['number of base trials'] = nBaseTrials
    results['test part order'] = order

def startStimulusDisplay():
    StroopStartStimulusDisplay()

def StroopStartStimulusDisplay():
    TSRStartStimulusDisplay()
    setStroopWord(currentWord)
    setStroopColor(currentColor)
    showStroopWord()
