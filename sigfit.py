import math

def sigmoid_fit(X, Y):
    a = 0.0
    b = 0.0
    while True:
        # compute Q
        q0 = [math.exp(a * x + b) for x in X]
        q1 = [x / (1.0 + x) for x in q0]
        der_a = sum([x * (q1i - y) for (x,q1i,y) in zip(X,q1,Y)])
        der_b = sum([q1i - y for (q1i,y) in zip(q1,Y)])
        H = [[0,0],[0,0]]
        W = [(q1i * (1.0 - q1i)) for q1i in q1]
        H11 = sum([x**2*w for (x,w) in zip(X,W)])
        H12 = sum([x*w for (x,w) in zip(X,W)])
        H21 = H12
        H22 = sum(W)
        det = (H11 * H22) - (H12 * H12)
        z = 1.0 / det
        iH11 = z * H22
        iH12 = -z * H12
        iH22 = z * H11
        dxnt_a = iH11 * (-der_a) + iH12 * (-der_b)
        dxnt_b = iH12 * (-der_a) + iH22 * (-der_b)
        lambdasq = -(der_a * dxnt_a + der_b * dxnt_b)
        if (lambdasq < 1e-8):
            return (a, b)
        t = 1.0
        f0 = sum([math.log(1.0+q0i) for q0i in q0]) - sum([(a*x+b)*y for (x,y) in zip(X,Y)])
        #print "Score:",f0
        while True:
            q2 = [math.exp((a+dxnt_a*t)*x+b+dxnt_b*t) for x in X]
            f1 = sum([math.log(1.0+q2i) for q2i in q2]) - sum([((a+dxnt_a*t)*x+b+dxnt_b*t)*y for (x,y) in zip(X,Y)])
            if f1 < f0:
                break
            t *= 0.5
            if t < 1e-8:
                break
        if t < 1e-8:
            break
        a += dxnt_a * t
        b += dxnt_b * t
    return a,b

def sigmoid_likelihood(X, Y, a, b, guessing_p):
    prnd = 0.1
    ll = 0.0
    for i in xrange(len(X)):
        x = X[i]
        y = Y[i]
        p0 = safe_sigmoid(a*(x-b))
        p = 0.5*prnd + (1.0-prnd)*p0
        p = p + (1.0 - p) * guessing_p
        ll += (y * math.log(p)) + (1-y)*math.log(1-p)
    return ll

def safe_sigmoid(x):
    if x > 30:
        return 1.0
    elif x < -30:
        return 0.0
    else:
        q0 = math.exp(x)
        return q0 / (1.0 + q0)

def bayesian_sigmoid(X, Y, a_vals, guessing_p, b_prior = None):
    target_p = 0.5 * (1.0 + guessing_p)
    target_p1 = (target_p - guessing_p) / (1.0 - guessing_p)
    target_x = math.log(target_p1 / (1.0 - target_p1))
    if b_prior is None:
        b_min = None
        b_max = None
        prior_mean = None
        prior_variance = None
    else:
        (prior_mean, prior_variance, b_min, b_max) = b_prior
    if b_min is None:
        b_min = min(X)
    if b_max is None:
        b_max = max(X)
    n_bvals = 100
    b_vals = [b_min + (b_max - b_min) * i / (n_bvals - 1.0) for i in xrange(n_bvals)]
    res = []
    for a in a_vals:
        for b in b_vals:
            current_estimate = b + target_x/a
            ll = sigmoid_likelihood(X, Y, a, b, guessing_p)
            if prior_mean is not None:
                ll = ll - 0.5*(math.log(2*math.pi*prior_variance)+(current_estimate-prior_mean)**2/prior_variance)
            res.append((ll, current_estimate))
    max_ll = max([x[0] for x in res])
    s = sum([math.exp(x[0]-max_ll) for x in res])
    nw = [math.exp(x[0]-max_ll)/s for x in res]
    est = sum([w*x[1] for (w,x) in zip(nw,res)])
    varest = sum([w*x[1]**2 for (w,x) in zip(nw,res)]) - est**2
    if (varest > 0.0):
        stderr_est = math.sqrt(varest)
    else:
        stderr_est = None
    return est, stderr_est
