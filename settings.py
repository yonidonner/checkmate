import os

# This is not the most well-thought-out workaround
TEMPLATE_DIRS = (
    os.path.dirname(__file__),
    os.path.join(os.path.dirname(__file__), 'templates'),
    #os.path.join(os.path.dirname(__file__), 'templates/tests'),
)
