#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
import results
import defaults
import logging
import datetime
import entities.mental_variables
import entities.experiment
import pickle
import math
import base_dir
from google.appengine.api import users, mail
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp import template
from django.utils import simplejson as json
try:
  from pytz.gae import pytz
except:
  pass

class Reminders(_base.Handler):
  def get(self):
    # do reminders stuff
    experiments = entities.experiment.Experiment.all().filter('deleted',False).filter('active',True).filter('days_to_remind >',0).run()
    for experiment in experiments:
      if experiment.username is None:
        continue
      user = models2.User.all().filter('username', experiment.username).get()
      if user is None:
        continue
      if not user.receive_emails:
        continue
      # find last session
      last_session = models2.Session2.all().filter('experiment', experiment).filter('is_complete', True).order('-start_time').get()
      if last_session is None:
        # no sessions: send first reminder after one week, then another after another week, then stop
        if len(experiment.reminders_sent) == 0:
          if (experiment.join_date is not None) and ((datetime.datetime.utcnow() - experiment.join_date).days) >= 7:
            send_first_reminder_experiment(experiment)
            experiment.add_reminder()
        elif len(experiment.reminders_sent) == 1:
          if ((datetime.datetime.utcnow() - experiment.reminders_sent[-1]).days) >= 7:
            send_second_reminder_experiment(experiment)
            experiment.add_reminder()
      else:
        timedelta = datetime.datetime.utcnow() - last_session.start_time
        daysdelta = timedelta.days
        if daysdelta >= experiment.days_to_remind:
          if (len(experiment.reminders_sent) == 0) or ((datetime.datetime.utcnow() - experiment.reminders_sent[-1]).days) > 1:
            send_session_reminder_experiment(experiment, user, daysdelta)
            experiment.add_reminder()

def send_first_reminder_experiment(experiment):
  return
  user = models2.User.all().filter('username', experiment.username).get()
  if not user:
    return
  mail_body = '''Hi %s!

We're so glad you're part of Quantified Mind! It's time to do a set of fun tests to see how your mental performance changes over time - learn what makes you smarter!
Here's your link to start today's %s session: http://www.quantified-mind.com/lab/take_tests/%d.

Thanks for being awesome,
The Quantified Mind Team
@QuantifiedMind
-----
You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.
'''%(experiment.username, experiment.name, experiment.key().id())
  mail_html = '''<p>Hi %s!</p>
<br>
<p>We're so glad you're part of Quantified Mind! It's time to do a set of fun tests to see how your mental performance changes over time - learn what makes you smarter! </p>
<p>Click <a href="http://www.quantified-mind.com/lab/take_tests/%d">here to start today's %s session</a>.</p>
<br>
<p>Thanks for being awesome,</p>
<p>@QuantifiedMind</p>
<p>The Quantified Mind Team</p>
<p>-----<br />You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.</p>
'''%(experiment.username, experiment.key().id(), experiment.name)
  mail.send_mail(
    to=user.google_account.email(),
    #to='lab@quantified-mind.com',
    #bcc='lab@quantified-mind.com',
    sender='lab@quantified-mind.com',
    subject='Quantified Mind fun with %s'%experiment.name,
    body=mail_body,
    html=mail_html,
    reply_to='lab@quantified-mind.com')

def send_second_reminder_experiment(experiment):
  return
  user = models2.User.all().filter('username', experiment.username).get()
  if not user:
    return
  mail_body = '''Hi %s!

We love your mind almost as much as you do. Here's a quick invitation to have some fun today and do a %s test.
Ready, set, go! http://www.quantified-mind.com/lab/take_tests/%d.

Thanks for being awesome,
The Quantified Mind Team
@QuantifiedMind
-----
You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.
'''%(experiment.username, experiment.name, experiment.key().id())
  mail_html = '''<h3>Hi %s!</h3>

<p>We love your mind almost as much as you do. Here's a quick invitation to <a href="http://www.quantified-mind.com/lab/take_tests/%d">have some fun today and do a %s test.</a>
Ready, set, GO!</p>

<p>Thanks for being awesome,</p>
<p>The Quantified Mind Team</p>
<p>@QuantifiedMind</p>
<p>-----<br />You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.</p>
'''%(experiment.username, experiment.key().id(), experiment.name)
  mail.send_mail(
    to=user.google_account.email(),
    #to='lab@quantified-mind.com',
    #bcc='lab@quantified-mind.com',
    sender='lab@quantified-mind.com',
    subject='Quantified Mind fun with %s'%experiment.name,
    body=mail_body,
    html=mail_html,
    reply_to='lab@quantified-mind.com')

def send_session_reminder_experiment(experiment, user, daysdelta):
  return
  mail_body = '''Hi %s!

How is your mind doing today? We care about how much you mentally rock. Your last session for the %s experiment was %d days ago. Did you get smarter in the meantime?
It's time for some fun to happen today! Click here to go for it: http://www.quantified-mind.com/lab/take_tests/%d

Thanks for being awesome,
The Quantified Mind Team
@QuantifiedMind
-----
You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.
'''%(experiment.username, experiment.name, daysdelta, experiment.key().id())
  mail_html = '''<h3>Hi %s!</h3>
<p>How is your mind doing today? We care about how much you mentally rock. Your last session for the %s experiment was %d days ago. Did you get smarter in the meantime?</p>
<p>It's time for some fun to happen today! Click <a href="http://www.quantified-mind.com/lab/take_tests/%d">here to go for it</a>.</p>
<p>Thanks for being awesome,</p>
<p>The Quantified Mind Team</p>
<p>@QuantifiedMind</p>
<p>-----<br />You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.</p>
'''%(experiment.username, experiment.name, daysdelta, experiment.key().id())
  mail.send_mail(
    to=user.google_account.email(),
    #to='lab@quantified-mind.com',
    #bcc='lab@quantified-mind.com',
    sender='lab@quantified-mind.com',
    subject='Quantified Mind fun with %s'%experiment.name,
    body=mail_body,
    html=mail_html,
    reply_to='lab@quantified-mind.com')

class DailyUpdates(_base.Handler):
  def get(self):
    # find new users in the last 24 hours
    min_datetime = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    joined_users = []
    joined_experiments = []
    done_sessions = []
    for user in models2.User.all().filter('create_date >= ', min_datetime).order('create_date'):
      joined_users.append({'username': user.username,
                           'email': user.google_account.email(),
                           'age': user.age,
                           'gender': user.gender,
                           'education': user.education,
                           'timezone': user.timezone,
                           'joindate': user.create_date})
    experiment_counts = {}
    for experiment in entities.experiment.Experiment.all().filter('join_date >= ', min_datetime).order('join_date'):
      joined_experiments.append({'username': experiment.username,
                                 'name': experiment.name,
                                 'joindate': experiment.join_date})
      experiment_counts[experiment.name] = experiment_counts.get(experiment.name, 0) + 1
      if len(experiment.groups) > 0:
        joined_experiments[-1]['group'] = experiment.groups[experiment.group_index]
    experiment_counts = [{'name': x,
                          'users': experiment_counts[x]}
                         for x in experiment_counts.keys()]
    experiment_counts.sort(key = lambda x: -x['users'])
    sessions_by_experiment = {}
    for session in models2.Session2.all().filter('start_time >= ', min_datetime).order('start_time'):
      if session.experiment is None:
        continue
      if session.user.username in ['yoni','accarmichael']:
        continue
      expname = session.experiment.name
      sessions_by_experiment[expname] = sessions_by_experiment.get(expname, 0) + 1
      si = session.session_information
      new_session = {'start_time': session.start_time,
                     'username': session.user.username,
                     'experiment': session.experiment.name,
                     'mental_variables': [{'mv': mental_variable,
                                           'ms': mental_state}
                                          for (mental_variable, mental_state) in zip(si.mental_variables, si.mental_states)]}
      finished_tests = models2.CompletedTest.all().filter('test_session', session).fetch(1000)
      finished_tests.sort(key = lambda x: x.end_time)
      new_session['finished_tests'] = finished_tests
      done_sessions.append(new_session)
    sessions_by_experiment = [{'experiment': x,
                               'sessions': sessions_by_experiment[x]}
                              for x in sessions_by_experiment.keys()]
    sessions_by_experiment.sort(key = lambda x: -x['sessions'])
    template_path = os.path.join(base_dir.base_dir(), 'templates', 'daily_updates.html')
    template_values = {'n_new_users': len(joined_users),
                       'new_users': joined_users,
                       'joined_experiments': joined_experiments,
                       'experiment_counts': experiment_counts,
                       'done_sessions': done_sessions,
                       'sessions_by_experiment': sessions_by_experiment}
    mail_html = template.render(template_path, template_values)
    #mail_html = compiled_template.render(template.Context(template_values))
    mail.send_mail(
      to='lab@quantified-mind.com',
      sender='lab@quantified-mind.com',
      subject='Quantified Mind: daily summary',
      body='This email must be viewed with HTML',
      html=mail_html,
      reply_to='lab@quantified-mind.com')
    self.response.out.write(mail_html)
    return
