#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
import results
import defaults
import logging
import datetime
import time
import pickle
import entities.experiment
import entities.mental_variables
import handlers.aggregates
import collections
from google.appengine.api import users
from google.appengine.ext import webapp, db
from django.utils import simplejson as json

### NEW DATAMINDING ###

class Dataminding(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    context = self.get_context(user)
    self.render('dataminding.html', context)

  @_base.require_login
  def post(self):
    user = models2.User.get_current()
    context = self.get_context(user)
    context['arguments'] = self.request.arguments()

    experiment_filters = self.request.get_all('experiments')
    context['experiment_filters'] = json.dumps([str(f) for f in experiment_filters])
    variable_keys = self.request.get_all('variable_keys')
    context['variable_keys'] = json.dumps([str(k) for k in variable_keys])
    variable_filters = {}   # TODO: add in variable filters part

    mental_variables = entities.mental_variables.MentalVariable.get_by_id(
      [int(k) for k in variable_keys]) if variable_keys else []

    asr = handlers.aggregates.AggregateSessionResults.get_or_create(user.username)
        
    context["results"] = asr.to_json()

    self.render('dataminding.html', context)

  def get_context(self, user):
    experiments = self.get_experiments(user)
    context = {'user': user,
               'experiments': experiments,
               'experiments_json': json.dumps(experiments),
               'active': 'dataminding'}
    return context

  def get_experiments(self, user):
    orig_experiments = list(entities.experiment.Experiment.all().filter('username', user.username).run(batch_size = 100))
    experiments = []
    total_sessions = sum([e.sessions_complete for e in orig_experiments])
    for orig_experiment in orig_experiments:
      new_experiment = {
        'name': orig_experiment.name,
        'mental_variables': [],
        'tests': [],
        'complete': orig_experiment.sessions_complete == orig_experiment.total_sessions}

      # Are the first_ and last_session_timestamps not getting set? Not on dev_appserver, anyway.
      ts0 = models2.translate_datetime(orig_experiment.first_session_timezone, orig_experiment.first_session_timestamp)
      if ts0 is not None:
        ts0 = (ts0.year, ts0.month, ts0.day)
        new_experiment['start'] = ts0
      ts1 = models2.translate_datetime(orig_experiment.last_session_timezone, orig_experiment.last_session_timestamp)
      if ts1 is not None:
        ts1 = (ts1.year, ts1.month, ts1.day)
        new_experiment['end'] = ts1

      for (variant_name, variant_description, variant_group) in zip(orig_experiment.variants_done_names, orig_experiment.variants_done_descriptions, orig_experiment.variants_done_groups):
        new_experiment['tests'].append({'variant_name': variant_name,
                                        'variant_description': variant_description,
                                        'variant_group': variant_group})

      date_mv = {"name": "Date", "min_value": "", "max_value": "", "key_id": -1, "type": "date"}
      time_mv = {"name": "Time", "min_value": 0, "max_value": 24, "key_id": -2, "type": "numeric"}
      session_mv = {"name": "Session", "min_value": 1, "max_value": total_sessions + 1, "key_id": -3, "type": "numeric"}
      # This min/max are under/over-estimates, since not every experiment contains the same tests
      new_experiment['mental_variables'].append(date_mv)
      new_experiment['mental_variables'].append(time_mv)
      new_experiment['mental_variables'].append(session_mv)
      
      for mv_key in orig_experiment.mental_variables:
        mv = db.get(mv_key)
        if mv:
          new_experiment['mental_variables'].append(mv.to_dict())
        else:
          logging.debug("No mental variables for %s"%mv_key)
      experiments.append(new_experiment)
    return experiments







### OLD DATAMINDING EXPORTS - STILL NEEDED? ###

def make_xml(full_results, base_indentation = 0):
  res = ""
  for (field_name, field_description, unit, value) in full_results:
    res += "%s<%s>"%(" " * base_indentation, field_name)
    if unit == "parent":
      res += "\n" + make_xml(value, base_indentation + 2)
      res += "%s</%s>"%(" " * base_indentation, field_name)
    else:
      res += "%s</%s>"%(value, field_name)
    res += "\n"
  return res

class ExportXML(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    dq = models2.DatamindingQuery.get_or_create(user)
    user_sessions = dq.query_sessions()
    xmldata = []
    for user_session in user_sessions:
      xmldata.append(("test_session", "Test session", "parent", user_session.full_results()))
    filename = "QM_%s_%s.pcl"%(user.username, datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
    self.response.headers['content-disposition'] = "attachment; filename=" + filename
    self.response.headers['Content-Type'] = 'binary/pcl'
    self.response.out.write(pickle.dumps(xmldata))
    #xml = make_xml(xmldata)
    #self.response.out.write(xml)

class FullExportXML(_base.Handler):
  @_base.require_admin
  def get(self):
    users = models2.User.all()
    xmldata = []
    for user in users:
      sesss = models2.Session2.all().filter('user', user).filter('active', False)
      usrdata = user.full_results()
      settings = models2.UserSettings.get_or_create(user)
      usrdata.append(("settings", "Settings", "parent", settings.full_results()))
      for sess in sesss:
        usrdata.append(("test_session", "Test session", "parent", sess.full_results()))
      xmldata.append(("user", "User", "parent", usrdata))
    xml = make_xml(xmldata)
    filename = "QM_%s.xml"%(datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
    self.response.headers['content-disposition'] = "attachment; filename=" + filename
    self.response.headers['Content-Type'] = 'text/xml'
    self.response.out.write(xml)

class DownloadCSV(_base.Handler):
  @_base.require_login
  def get(self, query_id):
    user = models2.User.get_current()
    try:
      dq = models2.DatamindingQuery.get_by_id(int(query_id))
    except:
      dq = None
    if not ((dq is not None) and (dq.variant_description)):
      self.redirect('/dataminding')
      return
    user_sessions = dq.query_sessions()
    test_results = Dataminding.test_results(user_sessions, dq.variant_description)
    if len(test_results) == 0:
      self.redirect('/dataminding')
      return
    fields = test_results[0].fields_to_graph()
    csvdata = ",".join(["date","time"] + [".".join(x) for x in fields])+"\n"
    for test_result in test_results:
      dt = test_result.start_time
      date_str = dt.strftime("%m/%d/%Y")
      time_str = dt.strftime("%H:%M")
      field_data = [str(x[3]) for x in get_fields(test_result.full_results(), fields)]
      csvdata += ",".join([date_str, time_str] + field_data)+"\n"
    filename = "%s_%s_%s.csv"%(user.username, dq.variant_description.replace(" ","").replace("/","").replace("(","").replace(")",""), datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
    self.response.headers['content-disposition'] = "attachment; filename=" + filename
    self.response.headers['Content-Type'] = 'text/csv'
    self.response.out.write(csvdata)

