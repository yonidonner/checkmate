from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import files
from google.appengine.api import datastore_types
from google.appengine.ext import blobstore
import _base
import logging
import string
import datetime
import models2
import results
import random
import StringIO
import zipfile
#import statistics
import pickle
import base64
import hashlib
import entities.mental_variables
import entities.experiment

class DownloadByLink(_base.Handler):
    def get(self, key_id, chunk_id = None, agg_id = None):
        if key_id == "links" and chunk_id is None:
            users_agg = GeneralAggregator.all().filter('agg_type', 'users').filter('agg_id', 'all').get()
            mv_agg = GeneralAggregator.all().filter('agg_type', 'mental_variables').filter('agg_id', 'all').get()
            experiment_agg = GeneralAggregator.all().filter('agg_type', 'experiments').filter('agg_id', 'all').get()
            session_agg = GeneralAggregator.all().filter('agg_type', 'combined_sessions').filter('agg_id', 'all').get()
            links = users_agg.get_download_links() + \
                mv_agg.get_download_links() + \
                experiment_agg.get_download_links() + \
                session_agg.get_download_links()
            self.response.out.write("\n".join(links))
            return
        elif key_id == "links":
            if agg_id is None:
                aggs = GeneralAggregator.all().filter('agg_type', chunk_id).run()
                links = []
                for agg in aggs:
                    links.extend(agg.get_download_links())
            else:
                agg = GeneralAggregator.all().filter('agg_type', chunk_id).filter('agg_id', agg_id).get()
                if not agg:
                    self.response.out.write("Not found!")
                    return
                links = agg.get_download_links()
            self.response.out.write("\n".join(links))
            return
        try:
            GA = GeneralAggregator.get_by_id(int(key_id))
        except:
            GA = None
        if not GA:
            self.response.out.write("Can't find Aggregator id %s"%key_id)
            return
        if chunk_id == 'indices':
            data = pickle.dumps(GA.indices)
        else:
            try:
                chunk = db.get(GA.chunks[int(chunk_id)])
            except:
                self.response.out.write("Can't find Chunk number %s"%chunk_id)
                return
            data = chunk.data
        filename = "agg_%s_%s_%s.pcl"%(GA.agg_type, GA.agg_id, chunk_id)
        self.response.headers['content-disposition'] = "attachment; filename=" + filename
        self.response.headers['Content-Type'] = 'binary/pcl'
        self.response.out.write(data)
        return

class AggregateData(_base.Handler):
    @_base.require_admin
    def get(self, command = None):
        DA = DatastoreAggregatorStatus.get()
        if command:
            if command == "start":
                if DA.status in ['inactive', 'done']:
                    DA.start()
            elif command == "stop":
                if DA.status not in ['inactive', 'done']:
                    DA.stop()
            elif command == "clean":
                GeneralAggregator.clean_all('users')
                GeneralAggregator.clean_all('mental_variables')
                GeneralAggregator.clean_all('experiments')
                GeneralAggregator.clean_all('sessions')
                GeneralAggregator.clean_all('combined_sessions')
                GeneralAggregator.clean_all('all_but_sessions')
            elif command == "download":
                allres = GeneralAggregator.all().filter('agg_type', 'all_but_sessions').filter('agg_id', 'all').get()
                if allres:
                    pcl = pickle.dumps(allres.combine())
                    filename = "all_%s.pcl"%(datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
                    self.response.headers['content-disposition'] = "attachment; filename=" + filename
                    self.response.headers['Content-Type'] = 'binary/pcl'
                    self.response.out.write(pcl)
                    return
            self.redirect('/admin/aggregate_data')
            return
        self.render('admin_aggregate_data.html',
                    {'status': DA.status,
                     'detailed_status': DA.detailed_status,
                     'entities_done': DA.entities_done,
                     'log': DA.log})

class GeneralAggregator(db.Model):
    agg_type = db.StringProperty()
    agg_id = db.StringProperty()
    chunks = db.ListProperty(db.Key, indexed=False)
    chunk_size = db.IntegerProperty(default = 800000)
    indices = db.ListProperty(int, indexed=False)
    checksums = db.ListProperty(datastore_types.ByteString, default = [], indexed=False)
    checksums_updated = db.BooleanProperty(default = False)
    current_chunk_size = db.IntegerProperty()
    blob_key = blobstore.BlobReferenceProperty(default = None)

    @classmethod
    def create(cls, agg_type, agg_id):
        agg = cls(agg_type = agg_type, agg_id = agg_id)
        agg.current_chunk_size = 0
        first_chunk = DataChunk(data = "")
        first_chunk.put()
        agg.chunks = [first_chunk.key()]
        agg.indices = [0]
        agg.checksums = []
        agg.checksums_updated = True
        agg.put()
        return agg

    def get_download_links(self):
        key_id = self.key().id()
        return ["%d %s %s %d"%(key_id, self.agg_type, self.agg_id, self.current_chunk+1)]

    @classmethod
    def reset_or_create(cls, agg_type, agg_id):
        agg = cls.all().filter('agg_type', agg_type).filter('agg_id', agg_id).get()
        if agg:
            agg.clean()
        return cls.create(agg_type, agg_id)

    @classmethod
    def get_or_create(cls, agg_type, agg_id):
        agg = cls.all().filter('agg_type', agg_type).filter('agg_id', agg_id).get()
        if not agg:
            agg = cls.create(agg_type, agg_id)
        return agg

    def verify_checksums(self):
        if self.checksums_updated:
            next_chunk = 0
            current_data = ""
            for i in xrange(len(self.indices)-1):
                data_length = self.indices[i+1]-self.indices[i]
                data = ""
                while data_length > len(current_data):
                    data = data + current_data
                    data_length -= len(current_data)
                    current_data = db.get(self.chunks[next_chunk]).data
                    next_chunk += 1
                data = data + current_data[:data_length]
                current_data = current_data[data_length:]
                checksum = hashlib.md5(data).digest()
                if datastore_types.ByteString(checksum) != self.checksums[i]:
                    return False
            return True
            #full_data = ''.join([db.get(x).data for x in self.chunks])
            #for i in xrange(len(self.indices)-1):
            #    data = full_data[self.indices[i]:self.indices[i+1]]
            #    checksum = hashlib.md5(data).digest()
            #    if datastore_types.ByteString(checksum) != self.checksums[i]:
            #        return False
            #return True
        try:
            combined = self.combine()
            self.make_all_checksums()
            return True
        except:
            return False

    def make_all_checksums(self):
        full_data = ''.join([db.get(x).data for x in self.chunks])
        datas = [full_data[self.indices[i]:self.indices[i+1]] for i in xrange(len(self.indices)-1)]
        self.checksums = [datastore_types.ByteString(hashlib.md5(data).digest()) for data in datas]
        self.checksums_updated = True
        self.put()

    def add_checksum(self, data):
        if not self.checksums_updated:
            self.make_all_checksums()
        self.checksums.append(datastore_types.ByteString(hashlib.md5(data).digest()))

    def add_multiple_data(self, orig_data):
        datas = [pickle.dumps(x) for x in orig_data]
        for data in datas:
            self.add_checksum(data)
            self.indices.append(self.indices[-1] + len(data))
        self.put()
        return self.add_data_to_chunks(''.join(datas))

    def add_data(self, orig_data):
        data = pickle.dumps(orig_data)
        self.add_checksum(data)
        self.indices.append(self.indices[-1] + len(data))
        self.put()
        return self.add_data_to_chunks(data)

    def add_data_to_chunks(self, data):
        chunk = db.get(self.chunks[-1])
        if chunk is None:
            logging.info("Error: chunk is None, deleting self")
            self.clean()
            return -1
        size_to_transfer = min(self.chunk_size - self.current_chunk_size, len(data))
        logging.info("len(chunk.data) = %d, self.current_chunk_size = %s, size_to_transfer = %d"%(len(chunk.data), self.current_chunk_size, size_to_transfer))
        len_data_before = len(chunk.data)
        current_chunk_size_before = self.current_chunk_size
        chunk.data += data[:size_to_transfer]
        try:
            chunk.put()
        except:
            logging.info("Error in chunk.put, deleting self")
            self.clean()
            return -1
        self.current_chunk_size += size_to_transfer
        len_data_before1 = len(data)
        data = data[size_to_transfer:]
        len_data_after = len(chunk.data)
        len_data_after1 = len(data)
        current_chunk_size_after = self.current_chunk_size
        logging.info("size_to_transfer: %d, len(chunk.data) before=%d, len(chunk.data) after=%d, len(data) before=%d, len(data) after=%d, current_chunk_size before=%d, current_chunk_size after=%d"%(size_to_transfer, len_data_before, len_data_after, len_data_before1, len_data_after1, current_chunk_size_before, current_chunk_size_after))
        while len(data) > 0:
            size_to_save = min(self.chunk_size, len(data))
            new_chunk = DataChunk(data = data[:size_to_save])
            new_chunk.put()
            self.chunks.append(new_chunk.key())
            self.current_chunk_size = size_to_save
            data = data[size_to_save:]
        self.put()

    def combine(self):
        full_data = ''.join([db.get(x).data for x in self.chunks])
        data = [pickle.loads(full_data[self.indices[i]:self.indices[i+1]]) for i in xrange(len(self.indices)-1)]
        return data

    def zipme(self):
        data = ''
        for chunkk in self.chunks:
            chunk = db.get(chunkk)
            data = data + chunk.data
        zipstream = StringIO.StringIO()
        zfile = zipfile.ZipFile(file=zipstream, mode='w')
        indices = pickle.dumps(self.indices)
        zfile.writestr('indices.pcl', indices)
        zfile.writestr('data.pcl', data)
        zfile.close()
        zipstream.seek(0)
        return zipstream.getvalue()

    def blobify(self):
        file_name = files.blobstore.create(mime_type='application/octet-stream')
        f = files.open(file_name, 'a')
        z = self.zipme()
        f.write(z)
        files.finalize(file_name)
        blob_key = files.blobstore.get_blob_key(file_name)
        self.blob_key = blob_key
        self.put()
        return blob_key

    def clean(self):
        if self.blob_key is not None:
            blobstore.delete(self.blob_key)
        db.delete(self.chunks)
        db.delete(self)

    @classmethod
    def clean_all(cls, agg_type = None):
        query = cls.all()
        if agg_type is not None:
            query = query.filter('agg_type', agg_type)
        my_all = query.run(batch_size = 1000)
        for my_one in my_all:
            my_one.clean()
        #db.delete(DataChunk.all().run(batch_size = 1000))

class DataChunk(db.Model):
    data = db.BlobProperty()

class DatastoreAggregatorStatus(db.Model):
    status = db.StringProperty()
    entities_done = db.IntegerProperty(default = 0)
    detailed_status = db.StringProperty()
    log = db.ListProperty(str, indexed = False)
    cursor = db.StringProperty()
    aborted = db.BooleanProperty()

    def reset(self):
        self.status = 'inactive'
        self.detailed_status = ''
        self.cursor = ''
        self.aborted = False
        self.log = []

    def add_log_message(self, message):
        self.log.append("%s: %s"%(datetime.datetime.utcnow().strftime("%Y/%m/%d, %H:%M:%S"), message))
        self.put()

    @classmethod
    def get(cls):
        DA = cls.all().get()
        if not DA:
            DA = cls()
            DA.reset()
            DA.put()
        return DA

    def done(self):
        self.status = 'done'
        self.detailed_status = 'success'
        self.cursor = ''
        self.aborted = False
        self.put()

    def new_status(self, status):
        self.status = status
        self.entities_done = 0
        self.cursor = ''
        self.detailed_status = 'Starting'
        self.add_log_message("Switching status: %s"%status)
        self.put()

    def start(self):
        if self.status in ['inactive', 'done']:
            self.reset()
            self.put()
            self.add_log_message("Starting")
            DatastoreAggregatorTask.launch()

    def stop(self):
        if self.status not in ['inactive', 'done']:
            self.status = 'done'
            self.detailed_status = 'aborted'
            self.aborted = True
            self.add_log_message("Aborting")
            self.put()

    def get_current_query(self):
        if self.status == 'users':
            query = models2.User.all().order('username')
        elif self.status == 'mental_variables':
            query = entities.mental_variables.MentalVariable.all().order('creator_username').order('variable_name')
        elif self.status == 'experiments':
            query = entities.experiment.Experiment.all().order('username').order('name')
        elif self.status == 'sessions':
            query = models2.Session2.all().order('user').order('start_time')
        elif self.status == 'combine_user_sessions':
            query = GeneralAggregator.all().filter('agg_type', 'sessions').order('agg_id')
        if self.cursor:
            query = query.with_cursor(self.cursor)
        return query

class DatastoreAggregatorTask(_base.Handler):
    @classmethod
    def launch(cls):
        taskqueue.add(url='/tasks/aggregate_datastore')

    def post(self):
        DA = DatastoreAggregatorStatus.get()
        if DA.aborted:
            DA.status = 'done'
            DA.detailed_status = 'aborted'
            DA.put()
            return
        if DA.status == 'inactive':
            DA.new_status('clean_all')
            self.launch()
            return
        elif DA.status == 'clean_all':
            GeneralAggregator.clean_all('users')
            GeneralAggregator.clean_all('mental_variables')
            GeneralAggregator.clean_all('experiments')
            GeneralAggregator.clean_all('sessions')
            GeneralAggregator.clean_all('combined_sessions')
            GeneralAggregator.clean_all('all_but_sessions')
            DA.add_log_message("Successfully cleaned all aggregators.")
            DA.new_status('users')
            self.launch()
            return
        elif DA.status == 'test_users':
            users_agg = GeneralAggregator.get_or_create('users', 'all')
            users = users_agg.combine()
            DA.add_log_message("Successfully combined %d users."%len(users))
            DA.new_status('mental_variables')
            self.launch()
            return
        elif DA.status == 'test_mvs':
            mv_agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            mvs = mv_agg.combine()
            DA.add_log_message("Successfully combined %d mental variables."%len(mvs))
            DA.new_status('experiments')
            self.launch()
            return
        elif DA.status == 'test_experiments':
            exp_agg = GeneralAggregator.get_or_create('experiments', 'all')
            exps = exp_agg.combine()
            DA.add_log_message("Successfully combined %d experiments."%len(exps))
            DA.new_status('sessions')
            self.launch()
            return
        elif DA.status == 'users':
            query = DA.get_current_query()
            users = query.fetch(100)
            if len(users) == 0:
                DA.add_log_message("Finished with users. Testing users.")
                DA.new_status('test_users')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('users', 'all')
            tups = [user.to_tuple() for user in users]
            agg.add_multiple_data(tups)
            DA.add_log_message("Added %d users."%len(users))
            #for user in users:
            #    tup = user.to_tuple()
            #    agg.add_data(tup)
            #    DA.add_log_message("Adding user %s."%user.username)
            tups = [x.to_tuple() for x in users]
            DA.cursor = query.cursor()
            DA.entities_done += len(users)
            DA.detailed_status = '%d users done'%DA.entities_done
            DA.put()
            self.launch()
        elif DA.status == 'mental_variables':
            query = DA.get_current_query()
            mvs = query.fetch(1000)
            if len(mvs) == 0:
                DA.add_log_message("Finished with mental variables. Testing mental variables.")
                DA.new_status('test_mvs')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            tups = [mv.to_tuple() for mv in mvs]
            agg.add_multiple_data(tups)
            #for mv in mvs:
            #    tup = mv.to_tuple()
            #    agg.add_data(tup)
            DA.add_log_message("Added %d mental variables."%(len(mvs)))
            DA.cursor = query.cursor()
            DA.entities_done += len(mvs)
            DA.detailed_status = '%d mental variables done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'experiments':
            query = DA.get_current_query()
            experiments = query.fetch(1000)
            if len(experiments) == 0:
                DA.add_log_message("Finished with experiments. Testing experiments.")
                DA.new_status('test_experiments')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('experiments', 'all')
            tups = [experiment.to_tuple() for experiment in experiments]
            agg.add_multiple_data(tups)
            #for experiment in experiments:
            #    tup = experiment.to_tuple()
            #    agg.add_data(tup)
            DA.add_log_message("Added %d experiments."%(len(experiments)))
            DA.cursor = query.cursor()
            DA.entities_done += len(experiments)
            DA.detailed_status = '%d experiments done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'sessions':
            query = DA.get_current_query()
            sessions = query.fetch(100)
            if len(sessions) == 0:
                DA.new_status('combine_user_sessions')
                self.launch()
                return
            sessions_by_username = {}
            for session in sessions:
                tup = session.to_tuple()
                sessions_by_username[session.user.username] = sessions_by_username.get(session.user.username, []) + [tup]
            for username in sessions_by_username.keys():
                tups = sessions_by_username[username]
                agg = GeneralAggregator.get_or_create('sessions', username)
                agg.add_multiple_data(tups)
                DA.add_log_message("Added %d test sessions to user %s"%(len(tups), username))
            DA.cursor = query.cursor()
            DA.entities_done += len(sessions)
            DA.detailed_status = '%d sessions done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'combine_user_sessions':
            query = DA.get_current_query()
            aggs = query.fetch(25)
            if len(aggs) == 0:
                DA.add_log_message("Combined all sessions.")
                DA.new_status("combine_all")
                self.launch()
                return
            combined_agg = GeneralAggregator.get_or_create('combined_sessions', 'all')
            datas = [agg.combine() for agg in aggs]
            combined_agg.add_multiple_data(datas)
            DA.add_log_message("Combined %d users."%len(datas))
            #for agg in aggs:
            #    data = agg.combine()
            #    combined_agg.add_data(data)
            #    DA.add_log_message("Combined %d sessions for user %s"%(len(data), agg.agg_id))
            DA.cursor = query.cursor()
            DA.entities_done += len(aggs)
            DA.detailed_status = '%d users done.'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'combine_all':
            users_agg = GeneralAggregator.get_or_create('users', 'all')
            mv_agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            experiment_agg = GeneralAggregator.get_or_create('experiments', 'all')
            session_agg = GeneralAggregator.get_or_create('combined_sessions', 'all')
            agg = GeneralAggregator.reset_or_create('all_but_sessions', 'all')
            users = users_agg.combine()
            mvs = mv_agg.combine()
            experiments = experiment_agg.combine()
            agg.add_data((users, mvs, experiments))
            DA.add_log_message("Combined all but sessions")
            DA.done()
            self.launch()
            return
        elif DA.status == 'done':
            return

class UploadUsers(_base.Handler):
    @_base.require_password
    def post(self):
        users_pcl = base64.decodestring(self.request.get("data"))
        users = pickle.loads(users_pcl)
        for usertup in users:
            user = models2.User.from_tuple(usertup)
            user.put()
        self.response.out.write("Success.")
        return

class ExpDict(db.Model):
    exp_dict = models2.PickleProperty()

    @classmethod
    def get_or_create(cls):
        res = cls.all().get()
        if not res:
            res = cls()
        return res

class UploadMentalVariablesAndExperiments(_base.Handler):
    @_base.require_password
    def post(self):
        mvs_pcl = base64.decodestring(self.request.get("mental_variable_data"))
        mvs = pickle.loads(mvs_pcl)
        exps_pcl = base64.decodestring(self.request.get("experiment_data"))
        exps = pickle.loads(exps_pcl)
        mvs = pickle.loads(mvs_pcl)
        mv_dict = {}
        for mvtup in mvs:
            mv = entities.mental_variables.MentalVariable.from_tuple(mvtup, mv_dict)
        exp_dict = {}
        for exptup in exps:
            exp = entities.experiment.Experiment.from_tuple(exptup, mv_dict, exp_dict)
            logging.info("Restored experiment %s for user %s."%(exp.description, exp.username))
        ed = ExpDict.get_or_create()
        ed.exp_dict = exp_dict
        ed.put()
        self.response.out.write("Success.")
        return

class UploadSessions(_base.Handler):
    @_base.require_password
    def post(self):
        sessions_pcl = base64.decodestring(self.request.get("data"))
        sessions = pickle.loads(sessions_pcl)
        exp_dict = ExpDict.get_or_create().exp_dict
        for sesstup in sessions:
            sess = models2.Session2.from_tuple(sesstup, exp_dict)
            logging.info("Restored %s session (started %s) for user %s"%(sess.battery_name, sess.start_time.strftime("%Y%m%d_%H%M%S"), sess.user.username))
        self.response.out.write("Success.")
        return

class RestoreDatastore(_base.Handler):
    @_base.require_admin
    def get(self):
        base_path = "."
        users = pickle.load(open("%s/agg_users_all.pcl"%base_path,"rb"))
        mvs = pickle.load(open("%s/agg_mental_variables_all.pcl"%base_path,"rb"))
        exps = pickle.load(open("%s/agg_experiments_all.pcl"%base_path,"rb"))
        for usertup in users:
            user = models2.User.from_tuple(usertup)
            user.put()
        mv_dict = {}
        for mvtup in mvs:
            mv = entities.mental_variables.MentalVariable.from_tuple(mvtup, mv_dict)
        exp_dict = {}
        for exptup in exps:
            exp = entities.experiment.Experiment.from_tuple(exptup, mv_dict, exp_dict)
            logging.info("Restored experiment %s for user %s."%(exp.description, exp.username))
        sessions = pickle.load(open("%s/agg_combined_sessions_all.pcl"%base_path,"rb"))
        for user_sessions in sessions:
            for sesstup in user_sessions:
                sess = models2.Session2.from_tuple(sesstup, exp_dict)
                logging.info("Restored %s session (started %s) for user %s"%(sess.battery_name, sess.start_time.strftime("%Y%m%d_%H%M%S"), sess.user.username))
        self.response.out.write("Success!")
        return

class ExportAllSessionResults(_base.Handler):
    @classmethod
    def launch(cls):
        taskqueue.add(url='/tasks/export_all_session_results')

    @_base.require_admin
    def get(self):
        self.launch()

    def post(self):
        DA = DatastoreAggregatorStatus.get()
        if DA.aborted:
            DA.status = 'done'
            DA.detailed_status = 'aborted'
            DA.put()
            return
        if DA.status == 'inactive':
            DA.new_status('clean_all')
            self.launch()
            return
        elif DA.status == 'clean_all':
            GeneralAggregator.clean_all('users')
            GeneralAggregator.clean_all('mental_variables')
            GeneralAggregator.clean_all('experiments')
            GeneralAggregator.clean_all('sessions')
            GeneralAggregator.clean_all('combined_sessions')
            GeneralAggregator.clean_all('all_but_sessions')
            DA.add_log_message("Successfully cleaned all aggregators.")
            DA.new_status('users')
            self.launch()
            return
        elif DA.status == 'test_users':
            users_agg = GeneralAggregator.get_or_create('users', 'all')
            users = users_agg.combine()
            DA.add_log_message("Successfully combined %d users."%len(users))
            DA.new_status('mental_variables')
            self.launch()
            return
        elif DA.status == 'test_mvs':
            mv_agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            mvs = mv_agg.combine()
            DA.add_log_message("Successfully combined %d mental variables."%len(mvs))
            DA.new_status('experiments')
            self.launch()
            return
        elif DA.status == 'test_experiments':
            exp_agg = GeneralAggregator.get_or_create('experiments', 'all')
            exps = exp_agg.combine()
            DA.add_log_message("Successfully combined %d experiments."%len(exps))
            DA.new_status('sessions')
            self.launch()
            return
        elif DA.status == 'users':
            query = DA.get_current_query()
            users = query.fetch(100)
            if len(users) == 0:
                DA.add_log_message("Finished with users. Testing users.")
                DA.new_status('test_users')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('users', 'all')
            tups = [user.to_tuple() for user in users]
            agg.add_multiple_data(tups)
            DA.add_log_message("Added %d users."%len(users))
            #for user in users:
            #    tup = user.to_tuple()
            #    agg.add_data(tup)
            #    DA.add_log_message("Adding user %s."%user.username)
            tups = [x.to_tuple() for x in users]
            DA.cursor = query.cursor()
            DA.entities_done += len(users)
            DA.detailed_status = '%d users done'%DA.entities_done
            DA.put()
            self.launch()
        elif DA.status == 'mental_variables':
            query = DA.get_current_query()
            mvs = query.fetch(1000)
            if len(mvs) == 0:
                DA.add_log_message("Finished with mental variables. Testing mental variables.")
                DA.new_status('test_mvs')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            tups = [mv.to_tuple() for mv in mvs]
            agg.add_multiple_data(tups)
            #for mv in mvs:
            #    tup = mv.to_tuple()
            #    agg.add_data(tup)
            DA.add_log_message("Added %d mental variables."%(len(mvs)))
            DA.cursor = query.cursor()
            DA.entities_done += len(mvs)
            DA.detailed_status = '%d mental variables done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'experiments':
            query = DA.get_current_query()
            experiments = query.fetch(1000)
            if len(experiments) == 0:
                DA.add_log_message("Finished with experiments. Testing experiments.")
                DA.new_status('test_experiments')
                self.launch()
                return
            agg = GeneralAggregator.get_or_create('experiments', 'all')
            tups = [experiment.to_tuple() for experiment in experiments]
            agg.add_multiple_data(tups)
            #for experiment in experiments:
            #    tup = experiment.to_tuple()
            #    agg.add_data(tup)
            DA.add_log_message("Added %d experiments."%(len(experiments)))
            DA.cursor = query.cursor()
            DA.entities_done += len(experiments)
            DA.detailed_status = '%d experiments done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'sessions':
            query = DA.get_current_query()
            sessions = query.fetch(100)
            if len(sessions) == 0:
                DA.new_status('combine_user_sessions')
                self.launch()
                return
            sessions_by_username = {}
            for session in sessions:
                tup = session.to_tuple()
                sessions_by_username[session.user.username] = sessions_by_username.get(session.user.username, []) + [tup]
            for username in sessions_by_username.keys():
                tups = sessions_by_username[username]
                agg = GeneralAggregator.get_or_create('sessions', username)
                agg.add_multiple_data(tups)
                DA.add_log_message("Added %d test sessions to user %s"%(len(tups), username))
            DA.cursor = query.cursor()
            DA.entities_done += len(sessions)
            DA.detailed_status = '%d sessions done'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'combine_user_sessions':
            query = DA.get_current_query()
            aggs = query.fetch(25)
            if len(aggs) == 0:
                DA.add_log_message("Combined all sessions.")
                DA.new_status("combine_all")
                self.launch()
                return
            combined_agg = GeneralAggregator.get_or_create('combined_sessions', 'all')
            datas = [agg.combine() for x in agg]
            combined_agg.add_multiple_data(datas)
            DA.add_log_message("Combined %d users."%len(datas))
            #for agg in aggs:
            #    data = agg.combine()
            #    combined_agg.add_data(data)
            #    DA.add_log_message("Combined %d sessions for user %s"%(len(data), agg.agg_id))
            DA.cursor = query.cursor()
            DA.entities_done += len(aggs)
            DA.detailed_status = '%d users done.'%(DA.entities_done)
            DA.put()
            self.launch()
            return
        elif DA.status == 'combine_all':
            users_agg = GeneralAggregator.get_or_create('users', 'all')
            mv_agg = GeneralAggregator.get_or_create('mental_variables', 'all')
            experiment_agg = GeneralAggregator.get_or_create('experiments', 'all')
            session_agg = GeneralAggregator.get_or_create('combined_sessions', 'all')
            agg = GeneralAggregator.reset_or_create('all_but_sessions', 'all')
            users = users_agg.combine()
            mvs = mv_agg.combine()
            experiments = experiment_agg.combine()
            agg.add_data((users, mvs, experiments))
            DA.add_log_message("Combined all but sessions")
            DA.done()
            self.launch()
            return
        elif DA.status == 'done':
            return

class ExportSessionResultsStatus(db.Model):
    sessions_done = db.IntegerProperty(default = 0)
    log = db.ListProperty(str, indexed = False)
    cursor = db.StringProperty()
    aborted = db.BooleanProperty()

    def reset(self):
        self.sessions_done = 0
        self.cursor = ''
        self.aborted = False
        self.log = []

    def add_log_message(self, message):
        self.log.append("%s: %s"%(datetime.datetime.utcnow().strftime("%Y/%m/%d, %H:%M:%S"), message))
        self.put()

    @classmethod
    def get(cls):
        DA = cls.all().get()
        if not DA:
            DA = cls()
            DA.reset()
            DA.put()
        return DA

    def done(self):
        self.cursor = ''
        self.aborted = False
        self.put()

    def new_status(self, status):
        self.status = status
        self.entities_done = 0
        self.cursor = ''
        self.detailed_status = 'Starting'
        self.add_log_message("Switching status: %s"%status)
        self.put()

    def start(self):
        if self.status in ['inactive', 'done']:
            self.reset()
            self.put()
            self.add_log_message("Starting")
            DatastoreAggregatorTask.launch()

    def stop(self):
        if self.status not in ['inactive', 'done']:
            self.status = 'done'
            self.detailed_status = 'aborted'
            self.aborted = True
            self.add_log_message("Aborting")
            self.put()

    def get_current_query(self):
        if self.status == 'users':
            query = models2.User.all().order('username')
        elif self.status == 'mental_variables':
            query = entities.mental_variables.MentalVariable.all().order('creator_username').order('variable_name')
        elif self.status == 'experiments':
            query = entities.experiment.Experiment.all().order('username').order('name')
        elif self.status == 'sessions':
            query = models2.Session2.all().order('user').order('start_time')
        elif self.status == 'combine_user_sessions':
            query = GeneralAggregator.all().filter('agg_type', 'sessions').order('agg_id')
        if self.cursor:
            query = query.with_cursor(self.cursor)
        return query
