#!/usr/bin/env python
# encoding: utf-8
"""
This file defines our base handler. It gives us access to handy methods that can be called within any handler that descend from it, and provides some default values to any templates rendered by descendent handlers.
"""
#from google.appengine.dist import use_library
#use_library('django', '1.2')

import webapp2
from google.appengine.api import users, memcache
from google.appengine.ext import db, webapp, deferred
from google.appengine.ext.webapp import template
import os
import models
import logging
import base_dir
import models2
import md5
from helpers.gaesessions import get_current_session
import deferred_methods

MD5_ADMIN_PASSWORD = '\x1e\xb5\ne\xf2\xba\\\x85P\xe5\x0c\xa0i\xcc\xc7\xed'

def require_login(handler_method):
  """
  This decorator requires the user be logged in, redirecting if needed.
  """
  def redirect_if_needed(self, *args, **kwargs):
    google_account = users.get_current_user()
    if google_account:
      user = models2.User.all().filter('google_account', google_account).get()
      if user:
        handler_method(self, *args, **kwargs)
      else:
        context = {
          'google_account': google_account,
          'current_timezone': '-8.0',
          'return_url': self.request.uri,
          }
        self.render('signup.html', context)
    else:
      if self.request.get("callback"):
        self.render('login_first.html', {'login_url': users.create_login_url(self.request.uri)})
        return
      self.redirect(users.create_login_url(self.request.uri))

  def redirect_if_needed_old(self, *args, **kwargs):
    if models2.User.get_current():
      handler_method(self, *args, **kwargs)
    else:
      self.redirect('/')
  return redirect_if_needed

def require_admin(handler_method):
  """
  This decorator requires admin, redirecting if needed.
  """
  def redirect_if_needed(self, *args, **kwargs):
    if models2.User.get_current() and users.is_current_user_admin():
      handler_method(self, *args, **kwargs)
    else:
      admin_password = self.request.get("admin_password")
      if admin_password and md5.md5(admin_password).digest() == MD5_ADMIN_PASSWORD:
        handler_method(self, *args, **kwargs)
      else:
        logging.info("Not admin")
        self.redirect('/')
  return redirect_if_needed
  
def require_password(handler_method):
  """
  This decorator requires admin, redirecting if needed.
  """
  def redirect_if_needed(self, *args, **kwargs):
    password = self.request.get("admin_password")
    if not password:
      self.redirect('/')
      return
    if md5.md5(password).digest() == MD5_ADMIN_PASSWORD:
      handler_method(self, *args, **kwargs)
    else:
      logging.info("Wrong password")
      self.redirect('/')
  return redirect_if_needed
  
def cron(handler_method):
  """
  This decorator requires the the request be executed via cron, and returns failure messege if accessed from anywhere else.
  """
  def redirect_if_needed(self, *args, **kwargs):
    real_verb = self.request.get('_method', None)
    if self.request.get('testing') == 'true' or self.request.get('emulate_cron') == 'true':
      handler_method(self, *args, **kwargs)
    else:
      try:
        self.request.headers['X-AppEngine-Cron']
        handler_method(self, *args, **kwargs)
      except KeyError:
        self.response.out.write('Not authorized. Must be run from cron.')
  return redirect_if_needed
  
def methods_via_query_allowed(handler_method):
    """
    This method is the work of Bill Katz. (Updated by Nevin!)
    
    A decorator to automatically re-route overloaded POSTs
    that specify the real HTTP method in a _method query string.

    To use it, decorate your post method like this:

    from handlers import _base
    ...
    @_base.methods_via_query_allowed
    def post(self):
      pass

    The decorator will check for a _method query string or POST argument,
    and if present, will redirect to delete(), put(), etc.
    """
    def redirect_if_needed(self, *args, **kwargs):
        real_verb = self.request.get('_method', None)
        if not real_verb and 'X-HTTP-Method-Override' in self.request.environ:
            real_verb = self.request.environ['X-HTTP-Method-Override']
        if real_verb:
            # logging.debug("Redirected from POST. Detected method override = %s", real_verb)
            method = real_verb.upper()
            if method == 'HEAD':
                self.head(*args, **kwargs)
            elif method == 'PUT':
                self.put(*args, **kwargs)
            elif method == 'DELETE':
                self.delete(*args, **kwargs)
            elif method == 'TRACE':
                self.trace(*args, **kwargs)
            elif method == 'OPTIONS':
                self.head(*args, **kwargs)
            # POST and GET included for completeness
            elif method == 'POST':
                self.post(*args, **kwargs)
            elif method == 'GET':
                self.get(*args, **kwargs)
            else:
                self.error(405)
        else:
            handler_method(self, *args, **kwargs)
    return redirect_if_needed
    
class Handler(webapp2.RequestHandler):
  """Base request handler extends webapp.Request handler

     It defines the render method, which renders a Django template
     in response to a web request
  """
  
  def handle_exception(self, exception, debug_mode):
    import sys
    import traceback
    from google.appengine.api import xmpp

    exception_name = sys.exc_info()[0].__name__
    exception_details = str(sys.exc_info()[1])
    exception_traceback = ''.join(traceback.format_exception(*sys.exc_info()))
    logging.error(exception_traceback)
    self.error(500)

    # from helpers import nevin
    # nevin.notify('Uncaught Exception '+exception_name, "URL: %s\n\n%s: %s\n\n%s" % (self.request.url, exception_name, exception_details, exception_traceback))
      
  def session(self):
    return get_current_session()
  
  def slugify(self, inStr, spacechar='-'):
    removelist = ["a", "an", "as", "at", "before", "but", "by", "for","from","is", "in", "into", "like", "of", "off", "on", "onto","per","since", "than", "the", "this", "that", "to", "up", "via","with"];
    import re
    for a in removelist:
      aslug = re.sub(r'\b'+a+r'\b','',inStr)
    aslug = re.sub('[^\w\s-]', '', aslug).strip().lower()
    aslug = re.sub('\s+', spacechar, aslug)
    return aslug
  
  def set_props(self, entity, skip=[], set_if_not_present={}): #include [entity_name]_id in skip to save cpu time
    """
    Assign the value of any entity prop that matches a form element in the given request object
    @param entity : the entity to set the properties for
    @param skip : a list of string arg names within the request to ignore
    @param set_if_not_present : a dict of args [the dict keys] that should be set to defaults [the dict values] if they aren't in the request
    """
    import string
    skip.append('submit_button')
    failed = []
    skipped = []
    logging.info(str(set_if_not_present))
    for key in set_if_not_present:
      if key not in self.request.arguments():
        val = set_if_not_present[key]
        prop_name = string.split(key,'|')[0]
        try:
          setattr(entity, prop_name, val)
          logging.info('Successfully set "%s" to "%s" (since it wasn\'t included in the request)' % (prop_name, str(val)))
        except:
          logging.info('Failed to set "%s" to "%s" (even though it wasn\'t included in the request)' % (prop_name, str(val)))
          failed.append(prop_name)
    for arg in self.request.arguments():
      name_and_type = string.split(arg,'|')
      prop_name = name_and_type[0]
      try:
        arg_type = name_and_type[1]
      except IndexError:
        arg_type = 'str'
      val_str = self.request.get(arg)
      logging.info('Processing arg "%s"' % prop_name)
      if prop_name not in skip:
        try:
          if val_str == '':
            if arg_type == 'list':
              setattr(entity, prop_name, [])
            else:
              setattr(entity, prop_name, None)
          else:
            try:
              getattr(entity, prop_name)
            except:
              logging.info('Failed to set "%s" to "%s" -- the entity doesn\'t have this property' % (prop_name, val_str))
              failed.append(prop_name)
            try:
              if arg_type > 1:
                if arg_type == 'list':
                  val = self.request.get(arg, allow_multiple=True)
                elif arg_type == 'list_key':
                  val = [db.Key(key) for key in self.request.get(arg, allow_multiple=True)]
                elif arg_type == 'str' or arg_type == 'url' or arg_type == 'email' or arg_type == 'list':
                  val = val_str
                elif arg_type == 'int':
                  val = int(val_str) if val_str != '' else 0
                elif arg_type == 'textile':
                  from helpers import html_generator
                  val = val_str
                  setattr(entity, prop_name+'_html', html_generator.safe_html(val_str))
                elif arg_type == 'float':
                  val = float(val_str)
                elif arg_type == 'bool':
                  val = True if val_str == 'true' or val_str == '1' else False
                elif arg_type == 'key':
                  val = db.Key(val_str)
                elif arg_type == 'date':
                  date_array = val_str.split('/') # expecting m/d/y
                  import datetime
                  val = datetime.datetime(int(date_array[2]), int(date_array[0]), int(date_array[1]))
                elif arg_type == 'text' or arg_type == 'hidden':
                  val = db.Text(val_str)
              else:
                val = val_str
              try:
                setattr(entity, prop_name, val)
                logging.info('Successfully set "%s" to "%s"' % (prop_name, str(val)))
              except:
                logging.info('Failed to set "%s" to "%s" (but was able to understand val)' % (prop_name, str(val)))
                failed.append(prop_name)
            except:
              logging.info('Failed to set "%s" to "%s" (wasn\'t able to understand val)' % (prop_name, str(val)))
              failed.append(prop_name)
        except:
          pass # for expando...
          # This actually should only except the specific error that is thrown in the expando case, so that other errors will still be raised and passed up the chain.
      else:
        skipped.append(prop_name)
    return {'entity':entity, 'failed':failed, 'skipped':skipped}
    
  def get_cdn_urls(self):
    host_url = self.request.host_url
    url_parts = host_url.split('www')
    if len(url_parts) == 1:
      return {'images':[host_url], 'static': host_url, 'scripts': host_url, 'styles':host_url}
    images = []
    i = 1
    while i <= 5:
      images.append(url_parts[0]+'images'+str(i)+url_parts[1])
      i += 1
    return {'images':images, 'static': url_parts[0]+'static'+url_parts[1], 'scripts': url_parts[0]+'scripts'+url_parts[1], 'styles':url_parts[0]+'styles'+url_parts[1]}

  def render(self, template_name, template_values={}, nocache=False):
    """Renders an HTML template along with values
       passed to that template

       Args:
         template_name: A string that represents the name of the HTML template
         template_values: A dictionary that associates objects with a string
           assigned to that object to call in the HTML template.  The defualt
           is an empty dictionary.
    """
    host_url = self.request.host_url
    self.tend_to_housekeeping()
    cdn_urls = self.get_cdn_urls()
    
    # Some default template values
    default_values = {
      'session':self.session(),
      'current_path':self.request.path, # gives /a/282
      'host_url':host_url, # gives http://localhost:8080
      'image_cdns':cdn_urls['images'],
      'script_cdn':cdn_urls['scripts'],
      'style_cdn':cdn_urls['styles'],
      'static_cdn':cdn_urls['static'],
      'version_id':os.environ['CURRENT_VERSION_ID'], # useful for forcing refresh of js and css after a deploy
      'admin': users.is_current_user_admin(),
      'user': models2.User.get_current()
    }
  
    template_values.update(default_values)
      
    path = os.path.join(base_dir.base_dir(), 'templates', template_name)
    logging.info("path = " + path)
    self.response.write(template.render(path, template_values))

    #template_file = open(path)
    #compiled_template = template.Template(template_file.read())
    #template_file.close()
    #self.response.out.write(compiled_template.render(template.Context(template_values)))
    # no caching for the time being, but I have boilerplate code for that if we want to do it
    
  def tend_to_housekeeping(self):
    """
    These are tasks that need to happen every request, even if page is served from cache.
    """    
    session = self.session()
    if session:
      try:
        user = session['user']
        self.set_cookie('user_name', user.get_name(), 43200)
      except:
        self.set_cookie('user_name', 0, -100)
    else:
      self.set_cookie('user_name', 0, -100)
    
  def render_from_cache(self):
    pass # maybe sometime
    
  def set_cookie(self, key, value, lifetime=None, path='/'):
    # lifetime should be an integer, in number of minutes
    if lifetime:
      import datetime
      exp = datetime.datetime.now() + datetime.timedelta(minutes=lifetime)
      exp_str = 'expires='+exp.strftime('%a, %d-%b-%Y %H:%M:%S GMT')+';'
    else:
      exp_str = ''
    # exp_str = 'expires=Fri, 31-Dec-2020 23:59:59 GMT;'
    self.response.headers.add_header('Set-Cookie', '%s=%s; %s path=%s' % (str(key), str(value), exp_str, path))
    
  def get_cookie(self, key):
    value = self.request.cookies.get(key, '')
    if value == '':
      return None
    return value
    
  def reset_cache(self):
    memcache.flush_all()
    
  def notfound(self):
    self.redirect('/notfound?url='+self.request.url)
