#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
import results
import entities.experiment
import logging
import time
import datetime
from django.utils import simplejson as json
from handlers import aggregates, util
from google.appengine.api import users
from google.appengine.ext import webapp, db

class GetSessionData(_base.Handler):
  def get(self):
    google_account = users.get_current_user()
    user = None
    authenticated = False
    if google_account:
      user = models2.User.all().filter('google_account', google_account).get()
      authenticated = True
    if not user:
      request_username = self.request.get("username")
      if request_username:
        user = models2.User.all().filter('username', request_username).get()
        if user:
          request_token = self.request.get("token")
          if request_token:
            #logging.info("%s auth token: %s"%(request_username, user.get_authentication_token())
            if user.get_authentication_token() == request_token:
              authenticated = True
    if not (user and authenticated):
      self.response.out.write(json.dumps({'error': 'error authenticating'}))
      return
    start_time = self.request.get("start_time")
    session_query = models2.Session2.all().filter('user', user).filter('is_complete', True).order('start_time')
    if start_time:
      try:
        seconds = float(start_time)
        dt = datetime.datetime.utcfromtimestamp(seconds)
        session_query = session_query.filter('start_time >= ', dt)
      except:
        self.response.out.write(json.dumps({'error': 'error parsing start_time'}))
        return
    res = []
    N_SESSION_FETCH = 5  # was 1, but we should be able to do more than that; 1 causes paging problems
    test_sessions = session_query.fetch(N_SESSION_FETCH+1)
    if len(test_sessions) > N_SESSION_FETCH:
      partial = True
      next_date = time.mktime(test_sessions[N_SESSION_FETCH].start_time.timetuple())
    else:
      partial = False
    for test_session in test_sessions[:N_SESSION_FETCH]:
      finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
      finished_tests.sort(key = lambda x: x.start_time)
      session_results = []
      for i in xrange(len(finished_tests)):
        stats = finished_tests[i].get_statistics()
        for stat in stats:
          res.append({'session_timestamp': time.mktime(test_session.start_time.timetuple()),
                      'test_timestamp': time.mktime(finished_tests[i].start_time.timetuple()),
                      'test_name': finished_tests[i].variant_name,
                      'result_name': stat['name'],
                      'result_value': stat['value'],
                      'mental_variables': dict(test_session.mental_variables())})
    if partial:
      status = 'partial'
    else:
      status = 'success'
    return_json = {'session_data': res,
                   'status': status}
    if status == 'partial':
      return_json['next_date'] = next_date
    self.response.out.write(json.dumps(return_json))

def api_access(handler_method):
  """
  This decorator manages access to the API.
  A key can be provided to override the usual login requirements.
  """
  def redirect_if_needed(self, *args, **kwargs):
    google_account = users.get_current_user()
    user = None
    authenticated = False
    if google_account:
      user = models2.User.all().filter('google_account', google_account).get()
      authenticated = True
    if not user:
      request_username = self.request.get("username")
      if request_username:
        user = models2.User.all().filter('username', request_username).get()
        if user:
          request_token = self.request.get("token")
          if request_token:
            #logging.info("%s auth token: %s"%(request_username, user.get_authentication_token())
            if user.get_authentication_token() == request_token:
              authenticated = True
    if not (user and authenticated):
      self.response.out.write(json.dumps({'error': 'error authenticating'}))
      return
    return handler_method(self, user, *args, **kwargs)
  return redirect_if_needed

def admin_api_access(handler_method):
  """
  This decorator manages access to the API.
  A key can be provided to override the usual login requirements.
  """
  def redirect_if_needed(self, *args, **kwargs):
    google_account = users.get_current_user()
    user = None
    authenticated = False
    if google_account:
      user = models2.User.all().filter('google_account', google_account).get()
      authenticated = True
    if not user:
      request_username = self.request.get("username")
      if request_username:
        user = models2.User.all().filter('username', request_username).get()
        if user:
          request_token = self.request.get("token")
          if request_token:
            #logging.info("%s auth token: %s"%(request_username, user.get_authentication_token())
            if user.get_authentication_token() == request_token:
              authenticated = True
    if not (user and authenticated):
      self.response.out.write(json.dumps({'error': 'error authenticating'}))
      return
    if user.username == 'yoni':
      return handler_method(self, *args, **kwargs)
    return self.response.out.write(json.dumps({'error': 'no permission'}))
  return redirect_if_needed

class APIExperimentPermissions(db.Model):
  experiment = db.StringProperty()
  username = db.StringProperty()

  @classmethod
  def has_permission(cls, user, experiment):
    if user.username == 'yoni':
      return True
    per = cls.all().filter('username', user.username).filter('experiment', experiment).get()
    if per:
      return True
    return False

  def get_experiment(self):
    exp = entities.experiment.Experiment.all().filter('deleted', False).filter('username', self.username).filter('name', self.experiment).get()
    return exp

class APIDoc(_base.Handler):
  #@_base.require_login
  @api_access
  def get(self, user):
    #user = models2.User.get_current()
    username = user.username
    if user.authentication_token:
      apikey = user.authentication_token
    else:
      apikey = None
    context = {'username': username,
               'apikey': apikey}
    return self.render('apidoc.html', context)


class APIQueryAnonymous(_base.Handler):
  @api_access
  def get(self, user, exp_id, test_id):
    try:
      exp_id = int(exp_id)
    except:
      return self.response.out.write(json.dumps({'error': 'bad experiment id: %s'%exp_id}))
    exp = entities.experiment.Experiment.get_by_id(exp_id)
    if exp is None:
      return self.response.out.write(json.dumps({'error': 'no experiment with id %d'%exp_id}))
    # verify permission
    if APIExperimentPermissions.has_permission(user, exp.name):
      exps = entities.experiment.Experiment.all().filter('name', exp.name).filter("deleted", False)
    else:
      if exp.username != user.username:
        return self.response.out.write(json.dumps({'error': 'user %s has no permission to access experiment %d'%(user.username, exp_id)}))
    test_sessions = models2.Session2.all().filter('experiment', exp)
    test_sessions_for_test_id = []
    for test_session in test_sessions:
      for mental_variable, mental_state in zip(test_session.session_information.mental_variables, test_session.session_information.mental_states):
        if (mental_variable == "id") and (mental_state == test_id):
          test_sessions_for_test_id.append(test_session)
    finished_tests = [x for x in models2.CompletedTest.all().filter('test_session IN', test_sessions_for_test_id) if not x.is_practice]
    finished_tests.sort(key=lambda x: x.start_time)
    res = {}
    res['tests'] = [(test.key().id(), util.test_to_json(test)) for test in finished_tests]
    return self.response.out.write(json.dumps(res, indent=4))


class APIQuery(_base.Handler):
  @api_access
  def get(self, user, command, exp_id = None, session_id = None, test_id = None):
    if exp_id is None:
      # return a list of the user's experiments
      exp_query = entities.experiment.Experiment.all().filter('username', user.username).filter("deleted", False)
      names_ids = [{'name': x.name, 'id': x.key().id()} for x in exp_query]
      res = {'username': user.username,
             'experiments': names_ids}
      if command == 'query':
        return self.response.out.write(json.dumps(res))
      elif command == 'explore':
        return self.render('explore_toplevel.html', res)
      return
    # repeat the session id to specify unknown experiment
    if (session_id is not None) and (session_id == exp_id):
      try:
        session_id = int(session_id)
      except:
        return self.response.out.write(json.dumps({'error': 'bad session id: %s'%session_id}))
      sess = models2.Session2.get_by_id(session_id)
      if (sess is None) or (sess.experiment is None):
        return self.response.out.write(json.dumps({'error': 'no session with id %d'%session_id}))
      exp_id = sess.experiment.key().id()
    # retrieve experiment
    try:
      exp_id = int(exp_id)
    except:
      return self.response.out.write(json.dumps({'error': 'bad experiment id: %s'%exp_id}))
    exp = entities.experiment.Experiment.get_by_id(exp_id)
    if exp is None:
      return self.response.out.write(json.dumps({'error': 'no experiment with id %d'%exp_id}))
    expname = exp.name
    if session_id is None:
      # verify permission
      if APIExperimentPermissions.has_permission(user, expname):
        exps = entities.experiment.Experiment.all().filter('name', expname).filter("deleted", False)
      else:
        if exp.username != user.username:
          return self.response.out.write(json.dumps({'error': 'user %s has no permission to access experiment %d'%(user.username, exp_id)}))
        exps = [exp]
      res = []
      for other_exp in exps:
        sesss = models2.Session2.all().filter('experiment', other_exp).filter('is_complete',True)
        session_ids = [x.key().id() for x in sesss]
        res.append({'username': other_exp.username,
                    'sessions': session_ids})
      if command == 'query':
        return self.response.out.write(json.dumps(res))
      elif command == 'explore':
        return self.render('explore_sessions.html', {'expname': expname, 'expid': exp_id, 'users': res})
      return
    # retrieve session
    try:
      session_id = int(session_id)
    except:
      return self.response.out.write(json.dumps({'error': 'bad session id: %s'%session_id}))
    sess = models2.Session2.get_by_id(session_id)
    if (sess is None) or (sess.experiment is None):
      return self.response.out.write(json.dumps({'error': 'no session with id %d'%session_id}))
    sexpname = sess.experiment.name
    if expname != sexpname:
      return self.response.out.write(json.dumps({'error': 'session %d not in experiment %s'%(session_id, expname)}))
    if sess.is_anonymous:
      sessuser = 'anonymous'
    else:
      sessuser = sess.user.username
    if (sessuser != user.username) and (not APIExperimentPermissions.has_permission(user, expname)):
      return self.response.out.write(json.dumps({'error': 'user %s has no permission for session %d'%(user.username, session_id)}))
    # all permissions are fine!
    if test_id is None:
      res = {'username': sessuser,
             'experiment_id': exp_id,
             'session_id': session_id,
             'experiment': expname,
             'battery': sess.battery_name,
             'user_agent': sess.user_agent,
             'timezone': sess.timezone}
      if sess.start_time:
        res['start_time'] = sess.start_time.strftime('%Y/%m/%d %H:%M:%S.%f')
      if sess.end_time:
        res['end_time'] = sess.end_time.strftime('%Y/%m/%d %H:%M:%S.%f')
      sessinfo = sess.session_information
      if sessinfo:
        res['mental_variables'] = [{'variable': v,
                                    'state': s} for (v,s) in zip(sessinfo.mental_variables, sessinfo.mental_states)]
      finished_tests = [x for x in models2.CompletedTest.all().filter('test_session', sess).fetch(1000) if not x.is_practice]
      finished_tests.sort(key = lambda x: x.start_time)
      res['tests'] = [test.key().id() for test in finished_tests]
      if command == 'query':
        return self.response.out.write(json.dumps(res))
      elif command == 'explore':
        return self.render('explore_session.html', res)
      return
    try:
      test_id = int(test_id)
    except:
      return self.response.out.write(json.dumps({'error': 'bad test id: %s'%test_id}))
    test = models2.CompletedTest.get_by_id(test_id)
    if test is None:
      return self.response.out.write(json.dumps({'error': 'no test with id %d'%test_id}))
    if test.test_session.key().id() != sess.key().id():
      return self.response.out.write(json.dumps({'error': 'test %d does not match session %d'%(test_id, session_id)}))
    res = util.test_to_json(test)
    if command == 'query':
      return self.response.out.write(json.dumps(res))
    elif command == 'explore':
      return self.render('explore_test.html', res)
    return


class AdminAPIQuery(_base.Handler):
  @classmethod
  def encode_user(cls, user):
    res = {}
    res['username'] = user.username
    res['timezone'] = user.timezone
    res['age'] = user.age
    res['gender'] = user.gender
    res['education'] = user.education
    res['email'] = user.google_account.email()
    res['create_date'] = user.create_date.strftime("%Y/%m/%d %H:%M:%S")
    authentication_token = user.authentication_token
    if authentication_token is not None:
      res['authentication_token'] = authentication_token
    last_activity = user.last_activity
    if last_activity is not None:
      res['last_activity'] = last_activity.strftime("%Y/%m/%d %H:%M:%S")
    res['id'] = user.key().id()
    return res

  @classmethod
  def encode_session(cls, session):
    res = {}
    u = models2.Session2.user.get_value_for_datastore(session)
    if u:
      res['user_id'] = u.id()
    else:
      res['user_id'] = None
    res['active'] = session.active
    res['battery_name'] = session.battery_name
    res['start_time'] = session.start_time.strftime("%Y/%m/%d %H:%M:%S")
    if session.end_time is not None:
      res['end_time'] = session.end_time.strftime("%Y/%m/%d %H:%M:%S")
    res['user_agent'] = session.user_agent
    res['timezone'] = session.timezone
    res['n_completed_tests'] = session.n_completed_tests
    res['is_complete'] = session.is_complete
    res['id'] = session.key().id()
    return res

  @classmethod
  def encode_session2(cls, session):
    res = {}
    u = models2.Session2.user.get_value_for_datastore(session)
    if u:
      res['user_id'] = u.id()
    else:
      res['user_id'] = None
    u = models2.Session2.experiment.get_value_for_datastore(session)
    if u:
      exp = db.get(u)
      if exp:
        res['experiment_name'] = exp.name
    si = models2.Session2.session_information.get_value_for_datastore(session)
    if si:
      si = db.get(si)
      if si:
        res['mental_variables'] = si.mental_variables
        res['mental_states'] = si.mental_states
    res['active'] = session.active
    res['battery_name'] = session.battery_name
    res['start_time'] = session.start_time.strftime("%Y/%m/%d %H:%M:%S")
    if session.end_time is not None:
      res['end_time'] = session.end_time.strftime("%Y/%m/%d %H:%M:%S")
    res['user_agent'] = session.user_agent
    res['timezone'] = session.timezone
    res['n_completed_tests'] = session.n_completed_tests
    res['is_complete'] = session.is_complete
    res['id'] = session.key().id()
    return res

  @admin_api_access
  def get(self, command, start_cursor = None):
    res = {}
    if command == 'users':
      query = models2.User.all().order('create_date')
      if start_cursor is not None:
        query = query.with_cursor(start_cursor)
      N_FETCH = 100
      query_res = query.fetch(N_FETCH)
      if len(query_res) == N_FETCH:
        next_cursor = query.cursor()
      else:
        next_cursor = ''
      res['next'] = next_cursor
      res['data'] = [self.encode_user(user) for user in query_res]
    elif command in ['sessions', 'sessions2']:
      query = models2.Session2.all().order('start_time')
      if start_cursor is not None:
        query = query.with_cursor(start_cursor)
      N_FETCH = 100
      query_res = query.fetch(N_FETCH)
      if len(query_res) == N_FETCH:
        next_cursor = query.cursor()
      else:
        next_cursor = ''
      res['next'] = next_cursor
      res['data'] = [(self.encode_session if command=='sessions' else self.encode_session2)(session) for session in query_res]
    elif command == 'tests':
      session_id = int(start_cursor)
      sess = models2.Session2.get_by_id(session_id)
      if sess is None:
        res['error'] = 'Session %d not found'%(session_id)
      tests = sess.get_finished_tests()
      test_ids = [x.key().id() for x in tests]
      res['data'] = test_ids
    else:
      res['error'] = "command must be 'users' or 'sessions'"
    return self.response.out.write(json.dumps(res))

class AdminAPITestQuery(_base.Handler):
  @classmethod
  def encode_result(cls, result):
    res = {}
    res['test_name'] = result.variant_name
    res['test_variant'] = result.variant_variant
    if result.start_time:
      res['start_time'] = result.start_time.strftime('%Y-%m-%d %H:%M:%S')
    if result.end_time:
      res['end_time'] = result.end_time.strftime('%Y-%m-%d %H:%M:%S')
    result.collect_all = True
    obs = result.collect_observations()
    if obs.has_key('all'):
      res['raw_results'] = obs['all']
    try:
      trial_durations = result.get_trial_durations()
      res['trial_durations'] = trial_durations
    except:
      pass
    res['sess_id'] = models2.CompletedTest.test_session.get_value_for_datastore(result).id()
    res['id'] = result.key().id()
    return res

  @admin_api_access
  def get(self, test_name, start_cursor = None):
    res = {}
    clsmap = {'choice_reaction_time': results.ChoiceReactionTimeTestResult,
              'simple_reaction_time': results.SimpleReactionTimeTestResult,
              'gonogo': results.GoNoGoTestResult,
              'visuospatial_paired_associates': results.VisuospatialPairedAssociatesTestResult,
              'verbal_paired_associates': results.VerbalPairedAssociatesTestResult,
              'verbal_paired_associates2': results.VerbalPairedAssociates2TestResult,
              'verbal_learning': results.VerbalLearningTestResult,
              'verbal_learning2': results.VerbalLearning2TestResult,
              'digit_span': results.DigitSpanTestResult,
              'spatial_span': results.SpatialSpanTestResult,
              'design_copy': results.DesignCopyTestResult,
              'design_recognition': results.DesignRecognitionTestResult,
              'mental_rotation': results.MentalRotationTestResult,
              'mental_rotation2': results.MentalRotation2TestResult,
              'inspection_time': results.InspectionTimeTestResult,
              'decision_making': results.DecisionMakingTestResult,
              'coding': results.CodingTestResult,
              'visual_matching': results.FeatureMatchTestResult,
              'color_word': results.ColorWordTestResult,
              'cued_attention': results.CuedAttentionTestResult,
              'sorting': results.SortingTestResult,
              'attention': results.AttentionTestResult,
              'finger_tapping': results.FingerTappingTestResult,
              'n_back': results.SimpleNBackTestResult,
              }
    if clsmap.has_key(test_name):
      cls = clsmap[test_name]
      query = cls.all()
      if start_cursor is not None:
        query = query.with_cursor(start_cursor)
      N_FETCH = 100
      query_res = query.fetch(N_FETCH)
      if len(query_res) == N_FETCH:
        next_cursor = query.cursor()
      else:
        next_cursor = ''
      res['next'] = next_cursor
      res['data'] = [self.encode_result(result) for result in query_res]
    else:
      res['error'] = 'Could not find test named %s'%(test_name)
    return self.response.out.write(json.dumps(res))

class AdminAPIGetTestIDs(_base.Handler):
  @classmethod
  def encode_user(cls, user):
    res = {}
    res['username'] = user.username
    res['timezone'] = user.timezone
    res['age'] = user.age
    res['gender'] = user.gender
    res['education'] = user.education
    res['email'] = user.google_account.email()
    res['create_date'] = user.create_date.strftime("%Y/%m/%d %H:%M:%S")
    authentication_token = user.authentication_token
    if authentication_token is not None:
      res['authentication_token'] = authentication_token
    last_activity = user.last_activity
    if last_activity is not None:
      res['last_activity'] = last_activity.strftime("%Y/%m/%d %H:%M:%S")
    res['id'] = user.key().id()
    return res

  @classmethod
  def encode_session(cls, session):
    res = {}
    res['user_id'] = models2.Session2.user.get_value_for_datastore(session).id()
    res['active'] = session.active
    res['battery_name'] = session.battery_name
    res['start_time'] = session.start_time.strftime("%Y/%m/%d %H:%M:%S")
    if session.end_time is not None:
      res['end_time'] = session.end_time.strftime("%Y/%m/%d %H:%M:%S")
    res['user_agent'] = session.user_agent
    res['timezone'] = session.timezone
    res['n_completed_tests'] = session.n_completed_tests
    res['is_complete'] = session.is_complete
    res['id'] = session.key().id()
    return res

  @admin_api_access
  def get(self, command, start_cursor = None):
    res = {}
    if command == 'users':
      query = models2.User.all().order('create_date')
      if start_cursor is not None:
        query = query.with_cursor(start_cursor)
      N_FETCH = 100
      query_res = query.fetch(N_FETCH)
      if len(query_res) == N_FETCH:
        next_cursor = query.cursor()
      else:
        next_cursor = ''
      res['next'] = next_cursor
      res['data'] = [self.encode_user(user) for user in query_res]
    elif command == 'sessions':
      query = models2.Session2.all().order('start_time')
      if start_cursor is not None:
        query = query.with_cursor(start_cursor)
      N_FETCH = 100
      query_res = query.fetch(N_FETCH)
      if len(query_res) == N_FETCH:
        next_cursor = query.cursor()
      else:
        next_cursor = ''
      res['next'] = next_cursor
      res['data'] = [self.encode_session(session) for session in query_res]
    else:
      res['error'] = "command must be 'users' or 'sessions'"
    return self.response.out.write(json.dumps(res))

class AdminAPILookupUser(_base.Handler):
  @admin_api_access
  def get(self, username):
    user = models2.User.all().filter('username', username).get()
    if not user:
      return self.response.out.write(json.dumps({'error': 'username %s not found'%username}))
    res = {'username': user.username,
           'email': user.google_account.email()}
    return self.response.out.write(json.dumps(res))

class AdminAPIGetAPIKey(_base.Handler):
  @admin_api_access
  def get(self, username):
    user = models2.User.all().filter('username', username).get()
    if not user:
      return self.response.out.write(json.dumps({'error': 'username %s not found'%username}))
    res = {'username': user.username,
           'email': user.google_account.email(),
           'apikey': user.get_authentication_token()}
    return self.response.out.write(json.dumps(res))

class AdminAPIGetPermissions(_base.Handler):
  @admin_api_access
  def get(self):
    perms = APIExperimentPermissions.all()
    res = []
    for perm in perms:
      exp = perm.get_experiment()
      res.append({'username': perm.username,
                  'experiment': exp.name,
                  'experiment_id': exp.key().id()})
    return self.response.out.write(json.dumps(res))

class AdminAPISetPermissions(_base.Handler):
  @admin_api_access
  def get(self, command, username, experiment_id):
    try:
      experiment_id = int(experiment_id)
    except:
      return self.response.out.write(json.dumps({'error': 'bad experiment id %s'%experiment_id}))
    exp = entities.experiment.Experiment.get_by_id(experiment_id)
    if not exp:
      return self.response.out.write(json.dumps({'error': 'no experiment id %d'%experiment_id}))
    perm = APIExperimentPermissions.all().filter('username', username).filter('experiment', exp.name).get()
    if command == 'set':
      if perm:
        return self.response.out.write(json.dumps({'error': 'user %s already has permission to experiment %s'%(username, exp.name)}))
      new_perm = APIExperimentPermissions(username = username, experiment = exp.name)
      new_perm.put()
      return self.response.out.write(json.dumps({'status': 'granted permission to username %s for experiment %s'%(username, exp.name)}))
    elif command == 'unset':
      if not perm:
        return self.response.out.write(json.dumps({'error': 'user %s already has no permission to experiment %s'%(username, exp.name)}))
      db.delete(perm)
      return self.response.out.write(json.dumps({'status': 'removed permission to username %s for experiment %s'%(username, exp.name)}))
    return

