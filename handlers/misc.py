#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
from google.appengine.api import users
from google.appengine.ext import webapp, db

class GettingStarted(_base.Handler):
#  @_base.require_login
  def get(self, first_time = None):
    user = models2.User.get_current()
    if not user:
      context = {
        'active': 'getting_started'
        }
      self.render('getting_started.html', context)
      return
    if first_time is not None:
      user.first_time = False
      user.put()
      if first_time == "skip":
        self.redirect("/lab")
        return
      self.redirect("/getting_started")
      return
    context = {
      'user': user,
      'active': 'getting_started'
    }
    self.render('getting_started.html', context)
