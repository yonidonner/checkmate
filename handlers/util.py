#from google.appengine.dist import use_library
#use_library('django', '1.2')

import logging
import datetime
import os

import _base
import models2
from django.utils import simplejson as json
from google.appengine.api import users, mail, memcache
from google.appengine.ext import webapp, db, deferred
import deferred_methods

import atom

class Static(_base.Handler):
  def get(self, page_name=None):
    if not page_name:
      page_name = 'home'
    path = '_static_'+page_name+'.html'
    if os.path.exists('templates/' + path):
      template_values = {
        'title':page_name.capitalize(),
        'active':page_name
      }
      self.render(path, template_values)
    else:
      self.redirect('/notfound?url='+self.request.path)

class NotFound(_base.Handler):
  def get(self):
    self.render('404.html', {'url': self.request.get('url')})

class RemoveTrailingCharacter(_base.Handler):
  def get(self):
    url = self.request.host_url+self.request.path
    self.redirect(url[:len(url)-1:], permanent=True)

class Login(webapp.RequestHandler):
  def get(self):
    self.redirect(users.create_login_url('/'))

class Logout(webapp.RequestHandler):
  def get(self):
    self.redirect(users.create_logout_url('/'))

class LoginLanding(webapp.RequestHandler):
  def get(self):
    if users.is_current_user_admin():
      self.redirect('/admin')
    else:
      self.redirect('/')

class ClearCache(_base.Handler):
  @_base.require_admin
  def get(self):
    self.reset_cache()
    destination = self.request.get('url') or self.request.host_url
    self.redirect(destination)

class CleanupSessions(webapp.RequestHandler):
  def get(self):
    from helpers.gaesessions import delete_expired_sessions
    while not delete_expired_sessions():
        pass

# TODO: handle spam from this thing, either with honeypots, CAPTCHAs, or regexp
class Contact(_base.Handler):
  def get(self):
    parameters = {'email_address': self.request.get('email_address', ''),
                  'email_message': self.request.get('email_message', '')}
    user = users.get_current_user()
    if user:
      parameters['email_address'] = user.email()
    parameters['active'] = 'contact'
    return self.render('_static_contact.html', parameters)

  def post(self):
    parameters = {'email_address': self.request.get('email_address', ''),
                  'email_message': self.request.get('email_message', '')}
    no_email = 'Please enter your email address.'
    no_message = 'Please write us a message.'
    thanks_message = "Thank you for your message! We'll respond by and by."
    if parameters.get('email_address') in ('', no_email):
      parameters['email_address'] = no_email
    elif parameters.get('email_message') in ('', no_message, thanks_message):
      parameters['email_message'] = no_message
    else:
      mail.send_mail(
        sender='qm@quantified-mind.com',
        to='yonidonner@gmail.com',
        subject='[QM] Feedback - %s'%parameters['email_address'],
        body=parameters['email_message'],
        reply_to=parameters['email_address'])
      parameters['email_message'] = thanks_message

    return self.render('_static_contact.html', parameters)


def test_to_json(test):
    res = {'test_name': test.variant_name,
           'test_variant': test.variant_variant}
    if test.start_time:
      res['start_time'] = test.start_time.strftime('%Y/%m/%d %H:%M:%S.%f')
    if test.end_time:
      res['end_time'] = test.end_time.strftime('%Y/%m/%d %H:%M:%S.%f')
    stats = test.get_statistics()
    for stat in stats:
      if stat['name'] == 'Score' and stat.has_key('stderr'):
        res['score'] = stat['value']
        res['score_stderr'] = stat['stderr']
      else:
        res[stat['name']] = stat['value']
    test.collect_all = True
    obs = test.collect_observations()
    try:
      trial_durations = test.get_trial_durations()
    except:
      trial_durations = None
    if 'all' in obs:
      obs = [x for x in obs['all']]
    else:
      obs = []
    res['raw_results'] = obs
    res['trial_durations'] = trial_durations
    return res
