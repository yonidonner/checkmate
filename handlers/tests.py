#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
import results
import defaults
import logging
import datetime
import helpers.word_lists
import helpers.rpw8_2
import entities.mental_variables
import entities.experiment
from handlers import aggregates
import pickle
import math
import handlers.user
from google.appengine.api import users, mail
from google.appengine.ext import webapp, db
from django.utils import simplejson as json
try:
  from pytz.gae import pytz
except:
  pass

class Download(_base.Handler):
  @_base.require_admin
  def get(self):
    # prepare data for download
    res_users = []
    user_query = models2.User.all()
    for user in user_query:
      res_users.append(user.to_tuple())
    res_wordlist = []
    wordlist_query = results.VerbalLearningWordList.all()
    for wordlist in wordlist_query:
      res_wordlist.append(wordlist.to_tuple())
    res_sess = []
    sess_query = models2.Session2.all().filter('active', False)
    for test_session in sess_query:
      res_sess.append(test_session.to_tuple())
    res = [res_users, res_wordlist, res_sess]
    #logging.info("dl: %s"%res)
    pcl = pickle.dumps(res)
    self.response.headers['content-disposition'] = "attachment; filename=" + 'sessions.pcl'
    self.response.headers['Content-Type'] = 'binary/pcl'
    self.response.out.write(pcl)

class DownloadUserData(_base.Handler):
  @_base.require_admin
  def get(self, username):
    user = models2.User.all().filter('username', username).get()
    if not user:
      self.redirect('/admin/view_users2/users')
      return
    query = entities.mental_variables.MentalVariable.all().filter('creator_username', username)
    mvs = []
    for mv in query.run(batch_size = 1000):
        mvs.append(mv.to_tuple())
    query = entities.experiment.Experiment.all().filter('username', username)
    experiments = []
    for experiment in query.run(batch_size = 1000):
        experiments.append(experiment.to_tuple())
    sessq = models2.Session2.all().filter('active', False).filter('user', user)
    sesss = []
    i = 0
    for sess in sessq:
        sesss.append(sess.to_tuple())
        i += 1
    user_tup = user.to_tuple()
    pcl = pickle.dumps((user_tup, mvs, experiments, sesss))
    filename = 'QM_%s_%s.pcl'%(username, (datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")))
    self.response.headers['content-disposition'] = "attachment; filename=" + filename
    self.response.headers['Content-Type'] = 'binary/pcl'
    self.response.out.write(pcl)
    return

class DownloadUser(_base.Handler):
  @_base.require_admin
  def get(self, username):
    # prepare data for download
    user = models2.User.all().filter('username', username).get()
    if not user:
      self.redirect('/admin')
      return
    sessq = models2.Session2.all().filter('active', False).filter('user', user)
    sesss = []
    for sess in sessq:
      sesss.append(sess.to_tuple())
    user_tup = user.to_tuple()
    pcl = pickle.dumps((user_tup, sesss))
    self.response.headers['content-disposition'] = "attachment; filename=" + 'sessions_%s_%s.pcl'%(username, (datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")))
    self.response.headers['Content-Type'] = 'binary/pcl'
    self.response.out.write(pcl)

class DownloadResults(_base.Handler):
  @_base.require_admin
  def get(self, which_results):
    if which_results == 'creatine':
      sessq = models2.Session2.all().filter('battery_name', 'creatine').filter('active', False)
    else:
      self.redirect('/admin')
      return
    res = []
    for sess in sessq:
      res.append(sess.to_tuple())
    pcl = pickle.dumps(res)
    self.response.headers['content-disposition'] = "attachment; filename=" + '%s_%s.pcl'%(which_results, (datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")))
    self.response.headers['Content-Type'] = 'binary/pcl'
    self.response.out.write(pcl)

class AdminLog(_base.Handler):
  @_base.require_admin
  def get(self):
    # Load all sessions
    sessq = models2.Session2.all()
    sesss = list(sessq)
    sesss.sort(key = lambda x: x.start_time)
    sesss.reverse()
    self.render('admin_log.html', {'test_sessions': sesss})

class TestStatistics(_base.Handler):
  @_base.require_admin
  def get(self):
    # Load all test results
    ctq = models2.CompletedTest.all()
    results = {}
    for completed_test in ctq:
      if not results.has_key(completed_test.variant_name):
        results[completed_test.variant_name] = {}
      rct = results[completed_test.variant_name]
      if not rct.has_key(completed_test.variant_variant):
        rct[completed_test.variant_variant] = []
      test_time = completed_test.end_time - completed_test.start_time
      rct[completed_test.variant_variant].append(test_time.days * 86400 + test_time.seconds)
    for k1 in results.keys():
      for k2 in results[k1].keys():
        n_tests = len(results[k1][k2])
        total_time = sum(results[k1][k2])
        mean_time = total_time * 1.0 / n_tests
        if n_tests > 1:
          variance_time = sum([(x - mean_time)**2 for x in results[k1][k2]]) / (n_tests - 1.0)
          stdev_time = math.sqrt(variance_time)
        else:
          variance_time = None
          stdev_time = None
        results[k1][k2] = {'variant_name': k2,
                           'n': n_tests,
                           'mean_time': mean_time,
                           'stdev_time': stdev_time}
    results = [{'test_name': x,
                'test_variants': sorted(results[x].values(), key=lambda r: r['variant_name'])} for x in results.keys()]
    results.sort(key = lambda r: r['test_name'])
    self.render('test_durations.html', {'results': results})

class UpdateSessionSummaries(_base.Handler):
  @_base.require_admin
  def get(self):
    sessq = models2.Session2.all().filter('active', False).filter('n_completed_tests', None)
    offset = 0
    updated = 0
    while True:
      sessions = sessq.fetch(100, offset)
      offset += len(sessions)
      if len(sessions) == 0:
        break
      for session in sessions:
        session.update_test_information()
        updated += 1
    self.response.out.write("Updated %d sessions."%updated)

class AdminSwitchEmailAccounts(_base.Handler):
  @_base.require_admin
  def get(self, username1, username2):
    user1 = models2.User.all().filter('username', username1).get()
    if user1 is None:
      return self.response.out.write('Cannot find user with username %s.'%(username1))
    user2 = models2.User.all().filter('username', username2).get()
    if user2 is None:
      return self.response.out.write('Cannot find user with username %s.'%(username2))
    (user1.google_account, user2.google_acount) = (user2.google_account, user1.google_account)
    user1.put()
    user2.put()
    return self.response.out.write("Email accounts switched successfully between %s and %s."%(username1, username2))

class AdminViewUsersOld(_base.Handler):
  @_base.require_admin
  def get(self):
    all_users = []
    all_sessions = []
    all_tests = []
    userq = models2.User.all()
    offset = 0
    while True:
      cur_users = userq.fetch(1000, offset = offset)
      offset += len(cur_users)
      if not cur_users:
        break
      all_users.extend(cur_users)
    #self.response.out.write("<p>%d users loaded.</p>"%(len(all_users)))
    sessq = models2.Session2.all()
    offset = 0
    while True:
      cur_sessions = sessq.fetch(1000, offset = offset)
      offset += len(cur_sessions)
      if not cur_sessions:
        break
      all_sessions.extend(cur_sessions)
    sessions_by_user_id = {}
    for session in all_sessions:
      user_id = session.user.key().id()
      if not sessions_by_user_id.has_key(user_id):
        sessions_by_user_id[user_id] = []
      sessions_by_user_id[user_id].append(session)
    #self.response.out.write("<p>%d sessions loaded.</p>"%(len(all_sessions)))
    # testq = models2.CompletedTest.all()
    # offset = 0
    # while True:
    #   cur_tests = testq.fetch(1000, offset = offset)
    #   offset += len(cur_tests)
    #   if not cur_tests:
    #     break
    #   all_tests.extend(cur_tests)
    # self.response.out.write("<p>%d tests loaded.</p>"%(len(all_tests)))
    users = []
    user_stats = {}
    battery_stats = {}
    users_time = []
    for user in all_users:
      user_stats[user.username] = {'sessions': 0,
                                   'tests': 0,
                                   'test_time': 0,
                                   'batteries': {},
                                   'mvs': {}}
      sessq = []
      #query = models2.Session2.all().filter('user', user)
      #offset = 0
      #while True:
      #  cur_sessq = query.fetch(1000, offset = offset)
      #  offset += len(cur_sessq)
      #  if not cur_sessq:
      #    break
      #  sessq.extend(cur_sessq)
      #sessq = [session for session in all_sessions if session.user.key().id() == user.key().id()]
      sessq = sessions_by_user_id.get(user.key().id(),[])
      n_sessions = 0
      n_tests = 0
      total_time = 0
      last_session_start_time = None
      us = user_stats[user.username]
      for sess in sessq:
        sess.update_test_information()
        n_sessions += 1
        if (last_session_start_time is None) or (sess.start_time > last_session_start_time):
          last_session_start_time = sess.start_time
        if not us['batteries'].has_key(sess.battery_name):
          us['batteries'][sess.battery_name] = 0
        us['batteries'][sess.battery_name] += 1
        if not battery_stats.has_key(sess.battery_name):
          battery_stats[sess.battery_name] = {}
        bats = battery_stats[sess.battery_name]
        session_information = sess.session_information
        for (mental_variable, mental_state) in zip(session_information.mental_variables, session_information.mental_states):
          if not us['mvs'].has_key(mental_variable):
            us['mvs'][mental_variable] = {}
          if not us['mvs'][mental_variable].has_key(mental_state):
            us['mvs'][mental_variable][mental_state] = 0
          us['mvs'][mental_variable][mental_state] += 1
        session_test_time = sess.total_test_time
        n_tests += sess.n_completed_tests
        total_time += session_test_time
        if not bats.has_key(user.username):
          bats[user.username] = {'n': 0, 'test_time': 0}
        bats[user.username]['n'] += 1
        bats[user.username]['test_time'] += session_test_time
      us['sessions'] = n_sessions
      us['tests'] = n_tests
      us['test_time'] = total_time
      users_time.append(total_time)
      us['batteries'] = sorted([{'battery': battery_name,
                                 'count': us['batteries'][battery_name]}
                                for battery_name in us['batteries'].keys()],
                               key = lambda x: -x['count'])
      us['mvs'] = sorted([{'variable': mv,
                           'states': [{'state': state,
                                       'count': us['mvs'][mv][state]}
                                      for state in sorted(us['mvs'][mv].keys())]}
                          for mv in sorted(us['mvs'].keys())])
      settings = models2.UserSettings.get_or_create(user)
      mental_variables = [{'variable': mv,
                           'states': states}
                          for (mv, states) in zip(settings.mental_variables, settings.mental_states)]

      users.append({'username': user.username,
                    'email': user.google_account.email(),
                    'number_of_sessions': n_sessions,
                    'number_of_completed_tests': n_tests,
                    'total_time_in_tests': total_time,
                    'last_session_start_time': last_session_start_time,
                    'settings': settings,
                    'batteries': us['batteries'],
                    'mvs': us['mvs'],
                    'mental_variables': mental_variables})
    users.sort(key = lambda x: x['username'])
    for k in battery_stats.keys():
      battery_stats[k] = {'n': sum([x['n'] for x in battery_stats[k].values()]),
                          'test_time': sum([x['test_time'] for x in battery_stats[k].values()]),
                          'n_users': len(battery_stats[k]),
                          'users': [{'name': username,
                                     'stats': battery_stats[k][username]}
                                    for username in sorted(battery_stats[k].keys())]}
    batteries = [{'name': battery_name,
                  'stats': battery_stats[battery_name]}
                 for battery_name in sorted(battery_stats.keys())]
    users_time.sort(reverse=True)
    users_times = [{'x': i, 'y': users_time[i]} for i in xrange(len(users_time))]
    self.render('admin_view_users.html', {'users': users,
                                          'batteries': batteries,
                                          'n_users': len(users_time),
                                          'total_time': sum(users_time),
                                          'users_times': users_times})

class AdminViewUsers2(_base.Handler):
  @_base.require_admin
  def get(self, thing = 'users'):
    ass = aggregates.AggregateSiteStatistics.get_or_create()
    if thing == 'users':
      n_users = 0
      users_by_time = []
      users0 = ass.get_user_aggregator().combine()
      users1 = [x[0] for x in users0]
      users = sorted(users1, key=lambda x:x['create_date'])
      for user in users:
        n_users += 1
        users_by_time.append({'n_users': n_users,
                              'date': user['create_date']})
      self.render('admin_view_users2.html', {'users': users,
                                             'n_users': len(users),
                                             'users_by_time': users_by_time})
      return
    elif thing == 'sessions':
      n_sessions = 0
      sessions_by_time = []
      sessions_per_user = {}
      sessions_per_user_inrange = {}
      max_days_back = 30
      now = datetime.datetime.utcnow()
      all_start_times = []
      users0 = ass.get_user_aggregator().combine()
      for (userinfo, spu) in users0:
        username = userinfo['username']
        for sess_start_time in spu:
          sessions_per_user[username] = sessions_per_user.get(username, 0) + 1
          if (now - sess_start_time).days <= max_days_back:
            sessions_per_user_inrange[username] = sessions_per_user_inrange.get(username, 0) + 1
          all_start_times.append(sess_start_time)
      all_start_times.sort()
      for sess_start_time in all_start_times:
        n_sessions += 1
        sessions_by_time.append({'n_sessions': n_sessions,
                                 'date': sess_start_time})
      n_sessions_per_user = {}
      n_sessions_per_user_inrange = {}
      sessions_dist = []
      sessions_dist_inrange = []
      for (d, n_sess, dist) in [(sessions_per_user, n_sessions_per_user, sessions_dist),
                          (sessions_per_user_inrange, n_sessions_per_user_inrange, sessions_dist_inrange)]:
        for c_i in d.values():
          n_sess[c_i] = n_sess.get(c_i, 0) + 1
        cs = n_sess.keys()
        cs.sort()
        cs.reverse()
        tot = 0
        for c in cs:
          tot += n_sess[c]
          if c > 0:
            dist.append({'sessions': c,
                         'users': tot})
      self.render('admin_view_users2.html', {'n_sessions': n_sessions,
                                             'sessions_dist': sessions_dist,
                                             'sessions_dist_inrange': sessions_dist_inrange,
                                             'sessions_by_time': sessions_by_time})
    elif thing == "experiments":
      users_by_experiment = {}
      for expagg in ass.get_experiment_aggregators():
        users_by_experiment[expagg.agg_id] = expagg.combine()
      #users_by_experiment = ass.users_by_experiment
      ns_by_experiment = {}
      for k in users_by_experiment.keys():
        c = {}
        for (username, c_i) in users_by_experiment[k]:
          c[c_i] = c.get(c_i, 0) + 1
        cs = c.keys()
        cs.sort(key = lambda x: -x)
        n_s = []
        tot = 0
        for c_i in cs:
          tot += c[c_i]
          if c_i > 0:
            n_s.append({'sessions': c_i,
                        'users': tot})
        n_s.reverse()
        ns_by_experiment[k] = n_s
      users_by_exp = [{'id': k_i,
                       'experiment_name': k,
                       'n_sessions': ns_by_experiment[k],
                       'n_users': len(users_by_experiment[k])}
                      for (k_i,k) in enumerate(users_by_experiment.keys())]
      users_by_exp.sort(key = lambda x: -x['n_users'])
      self.render('admin_view_users2.html', {'users_by_experiment': users_by_exp})

class AdminViewUsers(_base.Handler):
  @_base.require_admin
  def get(self):
    all_users = []
    userq = models2.User.all()
    offset = 0
    while True:
      cur_users = userq.fetch(1000, offset = offset)
      offset += len(cur_users)
      if not cur_users:
        break
      all_users.extend(cur_users)
    users = []
    user_stats = {}
    num_users = 0
    #battery_stats = {}
    for user in all_users:
      user_stats[user.username] = {'mvs': {}}
      us = user_stats[user.username]
      us['mvs'] = sorted([{'variable': mv,
                           'states': [{'state': state,
                                       'count': us['mvs'][mv][state]}
                                      for state in sorted(us['mvs'][mv].keys())]}
                          for mv in sorted(us['mvs'].keys())])
      settings = models2.UserSettings.get_or_create(user)
      mental_variables = [{'variable': mv,
                           'states': states}
                          for (mv, states) in zip(settings.mental_variables, settings.mental_states)]

      users.append({'username': user.username,
                    'email': user.google_account.email(),
                    'create_date': user.create_date,
                    'timezone': user.timezone,
                    'age': user.age,
                    'gender': user.gender,
                    #'participant_codes': user.participant_codes,
                    'education': user.education,
                    'settings': settings,
                    'mental_variables': mental_variables})
      num_users += 1
    self.render('admin_view_users.html', {'users': users,
                                          'email': user.google_account.email(),
                                          'create_date': user.create_date,
                                          'timezone': user.timezone,
                                          'age': user.age,
                                          'gender': user.gender,
                                          #'participant_codes': user.participant_codes,
                                          'education': user.education,
                                          'settings': settings,
                                          'mental_variables': mental_variables,
                                          'n_users': num_users})

class AdminViewExperiment(_base.Handler):
  @_base.require_admin
  def get(self):
    all_users = []
    exps = entities.experiment.Experiment.all().filter('deleted', False).filter('username', None).fetch(1000)
    names = set()
    experiments = []
    for exp in exps:
      if exp.name not in names:
        names.add(exp.name)
        experiments.append(exp)
    self.render('admin_view_experiments.html', {'experiments': experiments})
    return

  @_base.require_admin
  def post(self):
    experiment_id = self.request.get("experiment_name")
    try:
      experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
    except:
      self.response.out.write("Error loading experiment from id %s."%experiment_id)
      return
    name = experiment.name
    experiments = entities.experiment.Experiment.all().filter('deleted', False).filter('username', None).filter('name', name).fetch(1000)
    experiments.sort(key = lambda x: x.participant_code)
    res = []
    for expr in experiments:
      user_experiments = [exp for exp in entities.experiment.Experiment.all().filter('deleted', False).filter('participant_code', expr.participant_code).filter('name', name).fetch(1000) if exp.username is not None]
      #user_experiments = entities.experiment.Experiment.all().filter('deleted', False).filter('participant_code', expr.participant_code).filter('username !=', None).fetch(1000)
      for user_exp in user_experiments:
        sessions = models2.Session2.all().filter('experiment', user_exp).filter('is_complete', True).fetch(1000)
        res.append({'username': user_exp.username,
                    'code': user_exp.participant_code,
                    'finished_sessions': len(sessions)})
    self.render('admin_view_experiments.html', {'experiment_name': name,
                                                'results': res})
    return

class AdminViewUserStatistics(_base.Handler):
  @_base.require_admin
  def get(self, username):
    user = models2.User.all().filter('username', username).get()
    if not user:
      self.redirect('/admin')
      return

    user_sessions = []

    #get the user's sessions
    user_sessions = models2.Session2.all().filter('active', False).filter('user', user)

    user_stats = {}
    battery_stats = {}
    users_time = []

    user_stats[user.username] = {'sessions': 0,
                                          'tests': 0,
                                          'test_time': 0,
                                          'batteries': {},
                                          'mvs': {}}

    n_sessions = 0
    n_tests = 0
    total_time = 0
    last_session_start_time = None
    us = user_stats[user.username]

    for sess in user_sessions:
      sess.update_test_information()
      n_sessions += 1
      if (last_session_start_time is None) or (sess.start_time > last_session_start_time):
        last_session_start_time = sess.start_time
      if not us['batteries'].has_key(sess.battery_name):
        us['batteries'][sess.battery_name] = 0
      us['batteries'][sess.battery_name] += 1
      if not battery_stats.has_key(sess.battery_name):
        battery_stats[sess.battery_name] = {}
      bats = battery_stats[sess.battery_name]
      session_information = sess.session_information
      for (mental_variable, mental_state) in zip(session_information.mental_variables, session_information.mental_states):
        if not us['mvs'].has_key(mental_variable):
          us['mvs'][mental_variable] = {}
        if not us['mvs'][mental_variable].has_key(mental_state):
          us['mvs'][mental_variable][mental_state] = 0
        us['mvs'][mental_variable][mental_state] += 1
      session_test_time = sess.total_test_time
      n_tests += sess.n_completed_tests
      total_time += session_test_time
      if not bats.has_key(user.username):
        bats[user.username] = {'n': 0, 'test_time': 0}
      bats[user.username]['n'] += 1
      bats[user.username]['test_time'] += session_test_time
    us['sessions'] = n_sessions
    us['tests'] = n_tests
    us['test_time'] = total_time
    users_time.append(total_time)
    us['batteries'] = sorted([{'battery': battery_name,
                               'count': us['batteries'][battery_name]}
                              for battery_name in us['batteries'].keys()],
                             key = lambda x: -x['count'])
    us['mvs'] = sorted([{'variable': mv,
                         'states': [{'state': state,
                                     'count': us['mvs'][mv][state]}
                                    for state in sorted(us['mvs'][mv].keys())]}
                        for mv in sorted(us['mvs'].keys())])
    settings = models2.UserSettings.get_or_create(user)
    mental_variables = [{'variable': mv,
                         'states': states}
                        for (mv, states) in zip(settings.mental_variables, settings.mental_states)]

    for k in battery_stats.keys():
      battery_stats[k] = {'n': sum([x['n'] for x in battery_stats[k].values()]),
                          'test_time': sum([x['test_time'] for x in battery_stats[k].values()]),
                          'n_users': len(battery_stats[k]),
                          'users': [{'name': username,
                                     'stats': battery_stats[k][username]}
                                    for username in sorted(battery_stats[k].keys())]}
    batteries = [{'name': battery_name,
                  'stats': battery_stats[battery_name]}
                 for battery_name in sorted(battery_stats.keys())]
    users_time.sort(reverse=True)
    users_times = [{'x': i, 'y': users_time[i]} for i in xrange(len(users_time))]

    self.render('detailed_statistics.html', {'current_user': user,
                                             'email': user.google_account.email(),
                                             'num_sessions': us['sessions'],
                                             'num_tests': n_tests,
                                             'total_time_in_tests': total_time,
                                             'last_session_start_time': last_session_start_time,
                                             'batteries': us['batteries'],
                                             'settings': settings,
                                             'mvs': us['mvs'],
                                             'mental_variables': mental_variables,
                                             'total_time': sum(users_time)})
class AdminMenu(_base.Handler):
  @_base.require_admin
  def get(self):
    self.render('admin.html', {})

class UpdateResults(_base.Handler):
  @_base.require_admin
  def get(self):
    q = models2.CompletedTest.all()
    for test in q:
      try:
        test.update_result()
      except:
        self.response.out.write("Error updating, deleting result.")
        db.delete(test.key())

class Upload(_base.Handler):
  @_base.require_admin
  def get(self):
    self.render("upload.html", {})

  @_base.require_admin
  def post(self):
    pcl = self.request.get('file')
    [users_tup, wordlist_tup, session_tup] = pickle.loads(pcl)
    for usertup in users_tup:
      user = models2.User.from_tuple(usertup)
      if user:
        self.response.out.write("<br>Restored user: %s.\n"%(user.username))
    for wordlisttup in wordlist_tup:
      wordlist = results.VerbalLearningWordList.from_tuple(wordlisttup)
    for sesstup in session_tup:
      sess = models2.Session2.from_tuple(sesstup)
      if sess:
        self.response.out.write("<br>Successfully restored test session for user %s in battery %s.\n"%(sess.user.username, sess.battery_name))
      else:
        self.response.out.write("<br>Error restoring session: user = %s.\n"%str(sesstup[0]))

class UploadUser(_base.Handler):
  @_base.require_admin
  def get(self):
    self.render("/admin", {})

  @_base.require_admin
  def post(self):
    DEV = os.environ['SERVER_SOFTWARE'].startswith('Development')
    if not DEV:
      mail.send_mail(
        sender='lab@quantified-mind.com',
        to='yonidonner@gmail.com',
        subject='[QM] Upload on non-local',
        body='Upload attempt on live server')
      self.redirect('/')
      return
    pcl = self.request.get('file')
    (user_tup, mvs, experiments, sesss) = pickle.loads(pcl)
    user = models2.User.from_tuple(user_tup)
    if user:
      self.response.out.write("<br>Restored user: %s.\n"%(user.username))
    else:
      self.response.out.write("Error restoring user!")
      return
    mv_dict = {}
    for mvtup in mvs:
      mv = entities.mental_variables.MentalVariable.from_tuple(mvtup, mv_dict)
    exp_dict = {}
    for exptup in experiments:
      exp = entities.experiment.Experiment.from_tuple(exptup, mv_dict, exp_dict)
      logging.info("Restored experiment %s for user %s."%(exp.description, exp.username))
    for sess in models2.Session2.all().filter('user', user):
      self.response.out.write("<br>Deleting test session for user %s in battery %s.\n"%(sess.user.username, sess.battery_name))
      db.delete(sess.key())
    variant_infos = set()
    for sesstup in sesss:
      sess = models2.Session2.from_tuple(sesstup, exp_dict)
      for finished_test in models2.CompletedTest.all().filter('test_session', sess).filter('is_practice', False).filter('is_tentative', False).run():
        variant_info = (finished_test.variant_name, finished_test.variant().group)
        variant_infos.add(variant_info)
      if sess:
        self.response.out.write("<br>Successfully restored test session for user %s in battery %s.\n"%(sess.user.username, sess.battery_name))
      else:
        self.response.out.write("<br>Error restoring session: user = %s.\n"%str(sesstup[0]))
    asr = aggregates.AggregateSessionResults.reset_or_create(user.username)
    asr.build_from_scratch(max_sessions = 1000)
    for variant_info in variant_infos:
      aggregates.recompute_priors_variant_group(user.username, variant_info)

class RemoveBrokenResults(_base.Handler):
  @_base.require_admin
  def get(self):
    keys = db.Query(models2.CompletedTest, keys_only = True)
    for k in keys:
      try:
        obj = db.get(k)
        variant = models2.TestVariant.all().filter("name", obj.variant_name).filter("variant", obj.variant_variant).get()
        if not variant:
          self.response.out.write("Deleting %s because variant not found.\n"%str(k))
          #db.delete(k)
      except:
        self.response.out.write("Deleting %s.\n"%str(k))
        #db.delete(k)
    vl2 = results.VerbalLearning2TestResult.all().filter('parent_result', None)
    for vl2i in vl2:
      self.response.out.write("Deleting %s.\n"%str(vl2i.key()))
      db.delete(vl2i.key())
    vpa2 = results.VerbalPairedAssociates2TestResult.all().filter('parent_result', None)
    for vpa2i in vpa2:
      self.response.out.write("Deleting %s.\n"%str(vpa2i.key()))
      db.delete(vpa2i.key())
    return

def create_word_list(list_title, full_list):
  word_list = models2.WordList.all().filter('list_title', list_title).get()
  if not word_list:
    word_list = models2.WordList(list_title = list_title)
  word_list.word_list = full_list
  word_list.put()

class ResetDefaults(_base.Handler):
  @_base.require_admin
  def get(self, reset_which = None):
    if (reset_which) and ((reset_which == 'word_lists') or (reset_which == 'all')):
      create_word_list('concrete_nouns', helpers.word_lists.concrete_words)
      create_word_list('rpw8', helpers.word_lists.rpw8)
      create_word_list('rpw8_2', helpers.rpw8_2.rpw8_2[:15000])
      create_word_list('AFINN-111_pos', helpers.word_lists.afinn_111_pos)
      create_word_list('AFINN-111_neg', helpers.word_lists.afinn_111_neg)
      create_word_list('AFINN-111_neutral', helpers.word_lists.afinn_111_neutral)
      create_word_list('anew-1999-pos', helpers.word_lists.anew_1999_pos)
      create_word_list('anew-1999-neg', helpers.word_lists.anew_1999_neg)
      create_word_list('anew-1999-neutral', helpers.word_lists.anew_1999_neutral)
    if (reset_which) and ((reset_which == 'test_variants') or (reset_which == 'all')):
      for (cls, params) in defaults.variants:
        variant = models2.TestVariant.all().filter('name',params['name']).filter('variant',params['variant']).get()
        if variant:
          for k in params.keys():
            setattr(variant, k, params[k])
        else:
          variant = cls(**params)
        variant.put()
        self.response.out.write("Written test %s, variant %s, description %s.\n"%(variant.name, variant.variant, variant.description))
    if (reset_which) and ((reset_which == 'batteries') or (reset_which == 'all')):
      # deactivate existing batteries
      battq = models2.Battery.all().filter('enabled', True)
      for battery in battq:
        battery.enabled = False
        battery.put()
      # create or update batteries
    for (name, description, detailed_description, estimated_duration, focus, permissions, max_time, flow, fixed_tests) in defaults.batteries:
      battery = models2.Battery.all().filter('name', name).get()
      if battery:
        battery.enabled = True
      else:
        battery = models2.Battery(name = name, enabled = True)
      battery.description = description
      battery.detailed_description = detailed_description
      battery.estimated_duration = estimated_duration
      battery.permissions = permissions
      battery.max_time = max_time
      battery.flow = flow
      battery.focus = focus
      battery.fixed_tests = fixed_tests
      battery.put()
      # deactivate active sessions
      #sessq = models2.Session2.all().filter('active', True)
      #for sess in sessq:
      #  sess.active = False
      #  sess.put()
    if (reset_which) and ((reset_which == 'experiments') or (reset_which == 'all')):
      entities.mental_variables.MentalVariable.create_defaults()
      entities.experiment.Experiment.create_defaults()
    # #create default studies
    # for (participant_code, description, battery_name, mental_variables, mental_states) in defaults.studies:
    #   study = models2.Study.all().filter('participant_code', participant_code).get()
    #   if study:
    #     study.enabled = True
    #   else:
    #     study = models2.Study(participant_code = participant_code, enabled = True)
    #   study.description = description
    #   study.battery_name = battery_name
    #   study.mental_variables = mental_variables
    #   study.mental_states = mental_states
    #   study.put()

    # # create special users
    # u = models2.User.all().filter('username', 'alcohol').get()
    # if u:
    #   db.delete(u.key())
    # if not u:
    #   u = models2.User(username = 'alcohol', google_account = users.User(email='alcohol@quantified-mind.com'), is_restricted = True)
    #   u.put()
    self.redirect('/')

class TestResults(_base.Handler):
  @_base.require_login
  def get(self, test_session_id, test_name, variant_name, test_id):
    try:
      test_id = int(test_id)
      test_session_id = int(test_session_id)
    except:
      self.redirect('/')
      return
    user = models2.User.get_current()
    if not user:
      self.redirect('/')
      return
    test_session = models2.Session2.get_by_id(test_session_id)
    if not test_session:
      self.redirect('/')
      return
    if not ((users.is_current_user_admin()) or (test_session.user.key() == user.key())):
      self.redirect('/')
      return
    test_variant = models2.TestVariant.all().filter('name', test_name).filter('variant', variant_name).get()
    if not test_variant:
      self.redirect('/')
      return
    test_result = test_variant.get_result_class().get_by_id(test_id)
    if not test_result:
      self.redirect('/')
      return
    if not test_result.test_session.key() == test_session.key():
      self.response.out.write("Bad session.")
      return
    self.render('view_results.html', {'result': test_result,
                                      'charts': test_result.get_charts(),
                                      'test_session': test_session,
                                      'active': 'tests' if test_session.active else 'statistics',
                                      'user': user})
                                      #'inner_result_name':'results/%s.html'%test_name})

class IgnoreLastResult(_base.Handler):
  @_base.require_login
  def get(self, result_id = None):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if test_session:
      if test_session.experiment is None:
        last_result = None
        # get results
        results_q = models2.CompletedTest.all().filter('test_session', test_session)
        for result in results_q:
          if (last_result is None) or (result.end_time > last_result.end_time):
            last_result = result
        db.delete(last_result.key())
        self.redirect('/session')
        return
      else:
        if result_id is None:
          while True:
            tentative_result = models2.CompletedTest.all().filter('test_session', test_session).filter('is_tentative', True).get()
            if tentative_result is None:
              break
            else:
              db.delete(tentative_result.key())
        else:
          try:
            last_result = models2.CompletedTest.get_by_id(int(result_id))
          except:
            last_result = None
          if last_result:
            db.delete(last_result.key())
        self.redirect('/lab/study_session')
        return
    self.redirect('/')

class IgnoreAnonLastResult(_base.Handler):
  def get(self, result_id):
    try:
      result = models2.CompletedTest.get_by_id(int(result_id))
    except:
      self.render('error.html', {'error': 'Bad result ID: %s'%(result_id,)})
      return
    if result is None:
      self.render('error.html', {'error': 'Could not find result for ID: %s'%(result_id,)})
      return
    test_session = result.test_session
    session_id = test_session.key().id()
    db.delete(result.key())
    self.redirect('/lab/study_session/%d'%(session_id,))
    return

class SessionCentral(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    message = None
    error_message = None
    if user.success_message:
      message = user.success_message
      user.success_message = None
      user.put()
    if user.error_message:
      error_message = user.error_message
      user.error_message = None
      user.put()
    test_session = models2.Session2.get_current(user)
    if test_session:
      available_tests = test_session.get_available_tests()
      unavailable_tests = test_session.get_unavailable_tests()
      finished_tests = test_session.get_finished_tests()
    else:
      self.redirect('/session/new')
      return
    settings = models2.UserSettings.get_or_create(user)
    mental_variables_states = settings.mental_variables_states()
    mental_dict = dict(zip(test_session.session_information.mental_variables,
                           test_session.session_information.mental_states))
    for mental_variable in mental_variables_states:
      mental_variable['selected'] = mental_dict.get(mental_variable['variable'], 'Unspecified')
    specified_any_variables = any([v['selected'] != 'Unspecified'
                                   for v in mental_variables_states])
    context = {'user': user,
               'test_session': test_session,
               'available_tests': available_tests,
               'unavailable_tests': unavailable_tests,
               'finished_tests': finished_tests,
               'settings': settings,
               'mental_variables': mental_variables_states,
               'specified_any_variables': specified_any_variables,
               'saved_changes': self.request.get('saved_changes'),
               'message': message,
               'error_message': error_message,
               'active': 'tests'}
    self.render('session.html', context)

class Settings(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    current_timezone = user.timezone
    settings = models2.UserSettings.get_or_create(user)
    context = {'user': user,
               'settings': settings,
               'timezone_names': pytz.common_timezones,
               'current_timezone': current_timezone,
               #'mental_variables': [{'variable': mental_variable,
               #                      'states': mental_states} for (mental_variable, mental_states) in zip(settings.mental_variables, settings.mental_states)],
               'active': 'settings'}
    self.render('settings.html', context)

  def clean_string(self, s):
    if not s:
      return s
    valid_chars = set(list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_!? .,/'))
    return ''.join([x for x in s if x in valid_chars])

  @_base.require_login
  def post(self):
    user = models2.User.get_current()
    settings = models2.UserSettings.get_or_create(user)
    timezone = self.request.get("timezone")
    if (timezone and timezone != user.timezone):
      user.timezone = timezone
      user.put()
      user.update_session_timezone()
    context = {'user': user,
               'settings': settings,
               'timezone_names': pytz.common_timezones,
               'current_timezone': timezone,
               'active': 'settings'}
    age = self.request.get("age")
    gender = self.request.get("gender")
    education = self.request.get("education")
    valres = handlers.user.validate_user_info(user, user.username, age, gender, education, timezone)
    if not valres[0]:
      context['error_message'] = valres[1]
      self.render('settings.html', context)
      return
    settings.show_statistics = False
    settings.show_feedback = False
    if 'yes' in self.request.get_all("receive_emails"):
      user.receive_emails = True
      user.put()
    else:
      user.receive_emails = False
      user.put()
    for display_setting in self.request.get_all("test_display"):
      if display_setting == "statistics":
        settings.show_statistics = True
      elif display_setting == "feedback":
        settings.show_feedback = True
    settings.mental_variables = []
    settings.mental_states = []
    for i in xrange(100):
      mental_variable = self.clean_string(self.request.get("mental_variable_%d"%i))
      if mental_variable:
        mental_states = self.clean_string(self.request.get("mental_states_%d"%i))
        if mental_states:
          mental_states = mental_states.split(",")
          if not len(mental_states) > 1:
            mental_states = ["yes", "no"]
        else:
          mental_states = ["yes", "no"]
        settings.mental_variables.append(mental_variable)
        settings.mental_states.append(",".join(mental_states))
    settings.put()
    context['saved_changes'] = True
    context['mental_variables'] = [{'variable': mental_variable,
                                    'states': mental_states} for (mental_variable, mental_states) in zip(settings.mental_variables, settings.mental_states)]
    self.render('settings.html', context)

class Statistics(_base.Handler):
  @_base.require_login
  def get(self, test_session_id = None):
    user = models2.User.get_current()
    if test_session_id is None:
      user_sessions = models2.Session2.all().filter('user', user).filter('active', False).fetch(1000)
      user_sessions.sort(key = lambda sess: sess.start_time, reverse = True)
      context = {'user': user,
                 'user_sessions': user_sessions,
                 'active': 'statistics'}
      self.render('statistics.html', context)
    else:
      test_session = models2.Session2.get_by_id(int(test_session_id))
      if not test_session:
        self.redirect('/statistics')
      if not ((users.is_current_user_admin()) or (test_session.user.key() == user.key())):
        self.redirect('/statistics')
      context = {'user': user,
                 'test_session': test_session,
                 'completed_tests': test_session.get_finished_tests(),
                 'active': 'statistics'}
      self.render('statistics.html', context)

def create_word_lists():
  while True:
    wl = models2.WordList().all().fetch(100)
    if len(wl) == 0:
      break
    db.delete(wl)
  wl = models2.WordList(list_title = 'concrete_nouns', word_list = helpers.word_lists.concrete_words)
  wl.put()
  wl = models2.WordList(list_title = 'rpw8', word_list = helpers.word_lists.rpw8)
  wl.put()

def create_default_test_variants():
  create_word_lists()
  while True:
    variants = models2.TestVariant().all().fetch(100)
    if len(variants) == 0:
      break
    db.delete(variants)
  for (cls, params) in defaults.variants:
    variant = cls(**params)
    variant.put()
  for (name, description, detailed_description, estimated_duration, permissions, max_time, flow) in defaults.batteries:
    battery = models2.Battery(name = name, description = description, detailed_description = detailed_description, estimated_duration = estimated_duration, permissions = permissions, max_time = max_time, flow = flow)
    battery.put()

  #default_battery = models2.Battery.create_default()
  #quick_battery = models2.Battery(name = "quick", description = "Debug battery", estimated_duration = "Short", permissions = 'all', max_time = 3600 * 10 * 1000)
  #quick_battery.put()
  #parts = {}
  # for battery, variant_list in [(default_battery, results.default_variants),
  #                               (quick_battery, results.quick_variants)]:
  #   for (cls, params) in variant_list:
  #     if not cls.all().filter('name', params['name']).filter('variant', params['variant']).get():
  #       variant = cls(**params)
  #       variant.put()
  #       test_in_battery = models2.TestInBattery(variant = variant, battery = battery, dependencies = [])
  #       test_in_battery.put()
  #       if variant.variant[:5] == 'part2':
  #         first_part = parts[(variant.name,variant.variant[5:])]
  #         dependency = models2.TestFinishedDependency(dep_test = test_in_battery, finished_test = first_part, min_time = 1000*10, max_time = 1000*15*60, copy_state = True, force_one_child = True)
  #         dependency.put()
  #         test_in_battery.dependencies = [dependency.key()]
  #         test_in_battery.put()
  #       if variant.variant[:5] == 'part1':
  #         parts[(variant.name,variant.variant[5:])] = test_in_battery
  # sequential_battery = models2.Battery(name = "RT", description = "Reaction Time sequence", estimated_duration = "5 minutes", permissions = 'all', max_time = 1000 * 60 * 10)
  # sequential_battery.put()
  # prv_test = None
  # for test_variant in results.RT_sequence:
  #   variant = test_variant.all().filter('variant','default').get()
  #   test_in_battery = models2.TestInBattery(variant = variant, battery = sequential_battery, dependencies = [])
  #   test_in_battery.put()
  #   dep = models2.MaxTimesDependency(dep_test = test_in_battery, num_times = 1)
  #   dep.put()
  #   deps = [dep.key()]
  #   if prv_test is not None:
  #     dep = models2.TestFinishedDependency(dep_test = test_in_battery, finished_test = prv_test)
  #     dep.put()
  #     deps.append(dep.key())
  #   test_in_battery.dependencies = deps
  #   test_in_battery.put()
  #   prv_test = test_in_battery

class TakeTest(_base.Handler):
  @_base.require_login
  def get(self, test_name, variant_name, practice = None):
    test_variant = models2.TestVariant.all().filter('name', test_name).filter('variant', variant_name).get()
    if not test_variant:
      self.redirect('/session')
      return
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if not test_session:
      self.redirect('/session')
      return
    if test_session.timed_out():
      self.redirect('/lab')
      return
    state_class = test_variant.get_state_class()
    state = state_class.get_or_create(user, test_variant)
    if state:
      state.initialize()
      if (practice is not None) and (practice == 'practice'):
        state.is_practice = True
        state.put()
    test_session.last_test_start_time = datetime.datetime.utcnow()
    test_session.put()
    settings = models2.UserSettings.get_or_create(user)
    context = {
      'user':user,
      'test':test_variant,
      'state':state,
      'settings':settings,
      'active':'tests',
      }
    #self.render(os.path.join('tests', '%s.html'%test_variant.name), context)
    self.render('%s.html'%test_variant.name, context)

class TakeTestAnonymously(_base.Handler):
  def get(self, session_id, test_name, variant_name, practice = None):
    test_variant = models2.TestVariant.all().filter('name', test_name).filter('variant', variant_name).get()
    back_url = '/lab/study_session/%s'%(session_id,)
    if not test_variant:
      self.redirect(back_url)
      return
    try:
      session_id = int(session_id)
    except:
      self.render("error.html", {'error': 'Session ID must be numeric: got %s'%(session_id,)})
      return
    try:
      sess = models2.Session2.get_by_id(session_id)
    except:
      self.render("error.html", {'error': 'Bad session ID: %d'%(session_id,)})
      return
    if sess is None:
      self.render("error.html", {'error': 'Session ID not found: %d'%(session_id,)})
      return
    test_session = sess
    if test_session.timed_out():
      self.redirect(back_url)
      return
    state_class = test_variant.get_state_class()
    state = state_class.get_or_create_anon(session_id, test_variant)
    if state:
      state.initialize()
      if (practice is not None) and (practice == 'practice'):
        state.is_practice = True
        state.put()
    test_session.last_test_start_time = datetime.datetime.utcnow()
    test_session.put()
    context = {
      'session_id':session_id,
      'test':test_variant,
      'state':state,
      'anon_session':True,
      'active':'tests',
      'branding': test_session.experiment.custom_branding,
      }
    self.render('%s.html'%test_variant.name, context)

class UpdateSessionInformation(_base.Handler):
  def get(self):
    self.redirect("/session")

  def post(self):
    user = models2.User.get_current()
    if not user:
      self.redirect("/")
    test_session = models2.Session2.get_current(user)
    if not test_session:
      self.redirect("/session")
    test_session.session_information.free_text = self.request.get("free_text")
    test_session.session_information.mental_variables = []
    test_session.session_information.mental_states = []
    settings = models2.UserSettings.get_or_create(user)
    for i in xrange(len(settings.mental_variables)):
      mental_state = self.request.get("mental_variable_%d"%i)
      if mental_state:
        try:
          mental_state_i = int(mental_state)
          if mental_state_i > 0:
            state_str = settings.mental_states[i].split(",")[mental_state_i-1]
            test_session.session_information.mental_variables.append(settings.mental_variables[i])
            test_session.session_information.mental_states.append(state_str)
        except:
          pass
    test_session.session_information.put()
    self.redirect("/session?saved_changes=true")

class EndAndStart(_base.Handler):
  @_base.require_login
  def get(self, battery = None):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if test_session:
      test_session.finish()
    if battery:
      self.redirect('/session/new/%s'%battery)
    else:
      self.redirect('/session/new')

class NewSessionGenomera(_base.Handler):
  @_base.require_login
  def get(self, experiment_name):
    user = models2.User.get_current()
    if experiment_name not in ['butter', 'debug']:
      self.redirect('/lab')
      return
    if experiment_name == 'butter':
      url = '/experiment/buttermind'
      callback_url = self.request.get("callback")
      if callback_url:
        url += "?callback="+callback_url
      self.redirect(url)
      return
    # delete all active sessions
    while True:
      existing_session = models2.Session2.all().filter('user', user).filter('active', True).get()
      if not existing_session:
        break
      db.delete(existing_session)

    if experiment_name == 'butter':
      participant_code = 'buttermind'
    elif experiment_name == 'debug':
      participant_code = 'debug'
    experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('participant_code', participant_code).filter('deleted', False).get()
    if not experiment:
      template_experiment = entities.experiment.Experiment.all().filter('username', None).filter('participant_code', participant_code).get()
      experiment = template_experiment.clone(user.username)
      experiment.put()

    test_battery = models2.Battery.all().filter('name', experiment.battery_name).get()
    if not test_battery:
      self.redirect('/lab')
      return
    user_agent = self.request.headers.get('User-Agent', None)
    test_session = test_battery.new_session(user)
    test_session.user_agent = user_agent
    test_session.timezone = user.timezone
    test_session.experiment = experiment
    test_session.n_completed_tests = 0
    test_session.active = True
    test_session.is_complete = False
    callback_url = self.request.get("callback")
    if callback_url:
      test_session.callback_url = callback_url
    test_session.put()
    self.redirect('/lab/update_session_variables')

class NewSession(_base.Handler):
  @classmethod
  def battery_foci(self, batteries):
    foci = set()
    for battery in batteries:
      if battery.focus:
        foci.add(battery.focus)
    foci = list(foci)
    foci.sort()
    return [{'id':0, 'description': 'Any'}] + [{'id': i+1, 'description': foci[i]} for i in xrange(len(foci))]

  @_base.require_login
  def get(self, battery = None):
    user = models2.User.get_current()

    # Don't allow new session if already in active session
    test_session = models2.Session2.get_current(user)
    if test_session:
      # if old session timed out, end it automatically
      if test_session.timed_out():
        user.success_message = 'Your old %s test session which timed out was automatically ended.'%(test_session.battery().description)
        user.put()
        test_session.finish()
        test_session = None
      else:
        new_session_name = None
        new_session_description = None
        if battery:
          test_battery = models2.Battery.all().filter('name', battery).get()
          if test_battery:
            new_session_name = test_battery.name
            new_session_description = test_battery.description
        existing_session_name = test_session.battery().description
        context = {'user': user,
                   'new_session_name': new_session_name,
                   'new_session_description': new_session_description,
                   'existing_session_name': existing_session_name}
        self.render('session_in_progress.html', context)
        return
      # if callback url, automatically end old session
      #if self.request.get("callback"):
      #  test_session.finish()
      #else:
      #  self.redirect('/session')
      #  return

    # No active session, continue
    if battery:
      test_battery = models2.Battery.all().filter('name', battery).get()
      user_agent = self.request.headers.get('User-Agent', None)
      if test_battery:
        test_session = test_battery.new_session(user)
      test_session.user_agent = user_agent
      test_session.timezone = user.timezone
      callback_url = self.request.get("callback")
      if callback_url:
        test_session.callback_url = callback_url
      test_session.put()
      self.redirect('/session')
    else:
      batteries = models2.Battery.for_user(user)
      foci = self.battery_foci(batteries)
      focus = 0
      for i in xrange(len(foci)):
        if foci[i]['description'] == 'Any':
          focus = i
      batteries.sort(key=lambda battery: battery.estimated_duration)
      context = {'batteries': batteries, 'foci': foci, 'focus': focus, 'duration': 'any', 'user': user, 'active': 'tests'}
      self.render('new_session.html', context)

  @_base.require_login
  def post(self):
    user = models2.User.get_current()

    # Don't allow new session if already in active session
    test_session = models2.Session2.get_current(user)
    if test_session:
      self.redirect('/session')
      return

    # No active session, continue
    batteries = models2.Battery.for_user(user)
    foci = self.battery_foci(batteries)
    user_focus = self.request.get('focus')
    focus = None
    if user_focus:
      for i in xrange(len(foci)):
        if str(foci[i]['id']) == user_focus:
          focus = i

    duration = self.request.get('duration')
    if duration == '-5':
      min_duration = 0
      max_duration = 5
    elif duration == '5-10':
      min_duration = 5
      max_duration = 10
    elif duration == '10-15':
      min_duration = 10
      max_duration = 15
    elif duration == '15-30':
      min_duration = 15
      max_duration = 30
    elif duration == '30-60':
      min_duration = 30
      max_duration = 60
    elif duration == '60-120':
      min_duration = 60
      max_duration = 120
    elif duration == '120-':
      min_duration = 120
      max_duration = 1000000
    else:
      min_duration = 0
      max_duration = 1000000
    batteries = [battery for battery in batteries if min_duration <= battery.estimated_duration <= max_duration]
    if focus is None:
      for i in xrange(len(foci)):
        if foci[i]['description'] == 'Any':
          focus = i
    elif focus > 0:
      batteries = [battery for battery in batteries if battery.focus == foci[focus]['description']]
    batteries.sort(key=lambda battery: battery.estimated_duration)
    context = {'batteries': batteries, 'duration': duration, 'foci': foci, 'focus': focus, 'user': user, 'active': 'tests'}
    self.render('new_session.html', context)

class EndSession(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if test_session:
      test_session.finish()
    else:
      self.redirect('/session')
      return
    if test_session.experiment is not None:
      self.redirect('/lab')
      return
    results = []
    if test_session.callback_url:
      finished_tests = test_session.get_finished_tests()
      finished_tests.sort(key = lambda x: x.end_time)
      for i in xrange(len(finished_tests)):
        #results.append({'name': 'test%d_name'%(i+1),
        #                'value': finished_tests[i].variant_name})
        stats = finished_tests[i].get_all_statistics()
        for (name, value) in stats:
          results.append({'name': '%s_%s'%(finished_tests[i].variant_name, name.lower().replace(" ","_").replace(",","")),
                          'value': str(value).lower().replace(" ","_")})
      context = {'callback_url': test_session.callback_url,
                 'results': results}
      self.render('callback.html', context)
      return
    self.redirect('/statistics/%d'%test_session.key().id())

# TESTS = [('simple_reaction', 'Simple Reaction Time'),
#          ('choice_reaction', 'Choice Reaction Time'),
#          ('inspection_time', 'Inspection Time'),
#          ('stroop', 'Stroop'),
#          ('wcs', 'Wisconsin Card Sort'),
#          ('digit_symbol', 'Digit Symbol'),
#          ('finger_tapping', 'Finger Tapping Speed'),
#          ('digit_span', 'Digit Span'),
#          ('spatial_span', 'Spatial Span'),
#          ('design_copy', 'Design Copy'),
#          ('design_recognition', 'Design Recognition'),
#          ('mental_rotation', 'Mental Rotation'),
#          ('gonogo', 'gonogo'),
#          ('verbal_learning', 'Verbal Learning'),
#          ('verbal_paired_associates', 'Verbal Paired Associates'),
#          ('simple_n_back', 'Simple N-Back'),
#          ('dual_n_back', 'Dual N-Back'),
#          ('jaeggi_dual_n_back', 'Jaeggi Dual N-Back'),
#          ('feature_match', 'Feature Match'),
#          ('visuospatial_paired_associates', 'Visuospatial Paired Associates')]

# TEST_INDEX = dict(TESTS)

# # depricating this soon:
# class Index(_base.Handler):
#   def get(self):
#     self.render(os.path.join('tests', 'index.html'), {'tests': TESTS, 'active': 'tests'})

# class Battery(_base.Handler):
#   def get(self, battery_name):
#     battery = models.Battery.get_by_key_name(battery_name)
#     if battery_name == 'all' and not battery:
#       battery = models.Battery.create_or_update_main_battery()
#     self.render('battery.html', {'battery': battery, 'active': 'tests'})

# depricating this soon:
class TestOld(_base.Handler):
  def get_test_config(self, user, test):
    try:
      config_dict = user.config[test]
    except TypeError:
      config_dict = {}
    except KeyError:
      config_dict = {}
    return json.dumps(config_dict)

  def get_battery_config(battery, test):
    battery = models.Battery.get_by_key_name(battery)
    try:
      config_dict = json.dumps(battery.tests[test]['config'])
    except TypeError:
      config_dict = {}
    except KeyError:
      config_dict = {}
    return config_dict

  @_base.require_login
  def get(self, test):
    if test in TEST_INDEX:
      test_name = TEST_INDEX[test]
      user = models.User.get_or_create_current()
      context = {
        'user':user,
        'test':test,
        'test_name':test_name,
        'active':'tests',
        'test_config':self.get_test_config(user, test)
      }
      self.render(os.path.join('tests', '%s.html'%test), context)
    else:
      self.notfound()

class Test(_base.Handler):
  def get_test_config(self, user, test):
    try:
      config_dict = user.config[test]
    except TypeError:
      config_dict = {}
    except KeyError:
      config_dict = {}
    return json.dumps(config_dict)

  def get_battery_config(self, battery, test):
    try:
      config_dict = json.dumps(battery.tests[test]['config'])
    except TypeError:
      config_dict = {}
    except KeyError:
      config_dict = {}
    return config_dict

  @_base.require_login
  def get(self, battery, test):
    if test in TEST_INDEX:
      test_title = TEST_INDEX[test]
      user = models.User.get_or_create_current()
      battery = models.Battery.get_by_key_name(battery)
      context = {
        'active':'tests',
        'user':user,
        'test':test,
        'test_title':test_title,
        'battery':battery,
        'test_config_json':self.get_test_config(user, test),
        'battery_config_json':self.get_battery_config(battery, test)
      }
      self.render(os.path.join('tests', '%s.html'%test), context)
    else:
      self.notfound()

class AddAutoaddExperiments(_base.Handler):
  @_base.require_admin
  def get(self):
    userq = models2.User.all()
    offset = 0
    users_updated = 0
    experiments = entities.experiment.Experiment.get_autoadd_experiments()
    while True:
      users = userq.fetch(100, offset = offset)
      offset += len(users)
      if not users:
        break
      for user in users:
        user_experiments = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted',False).fetch(1000)
        user_experiment_names = set([x.name for x in user_experiments])
        for experiment in experiments:
          if experiment.name not in user_experiment_names:
            new_experiment = experiment.clone(user.username)
            new_experiment.put()
            users_updated += 1
    self.response.out.write("Updated %d users."%users_updated)
    return

class CreateExperimentsForOldUsers(_base.Handler):
  @_base.require_admin
  def get(self):
    userq = models2.User.all()
    offset = 0
    users_updated = 0
    batteries = {}
    while True:
      users = userq.fetch(100, offset = offset)
      offset += len(users)
      if not users:
        break
      for user in users:
        if user.migration is None:
          user.migration = 0
        if user.migration > 0:
          continue
        # get last session
        last_session = models2.Session2.all().filter('user', user).order('-start_time').get()
        user_mvs = None
        if last_session:
          self.response.out.write("<p>User %s: last session %s.</p>"%(user.username, last_session.battery_name))
          battery_name = last_session.battery_name
          if not batteries.has_key(battery_name):
            battery = models2.Battery.all().filter('name', battery_name).filter('enabled', True).get()
            batteries[battery_name] = battery
          battery = batteries[battery_name]
          if battery:
            new_experiment = entities.experiment.Experiment(username = user.username, active = True, deleted = False, name = "My First Experiment", participant_code = None, description = "This experiment was created automatically from your Quantified Mind profile", battery_name = battery_name, battery_focus = battery.focus, sessions_complete = 0, total_sessions = 10, auto_add = False)
            if user_mvs is None:
              mental_variables = []
              user_settings = models2.UserSettings.all().filter('user', user).get()
              if user_settings:
                if user_settings.mental_variables:
                  for (mv_name, mv_joined_states) in zip(user_settings.mental_variables, user_settings.mental_states):
                    mv_states = mv_joined_states.split(",")
                    new_mv = entities.mental_variables.DiscreteMentalVariable(creator_username = user.username, variable_name = mv_name, variable_type = entities.mental_variables.MV_DISCRETE, add_by_default = True, states = mv_states, default_state = mv_states[0])
                    new_mv.put()
                    mental_variables.append(new_mv.key())
                  user_mvs = mental_variables
                #user_settings.mental_variables = []
                #user_settings.mental_states = []
                #user_settings.put()
              new_experiment.mental_variables = user_mvs
              new_experiment.put()
            user.migration = 1
            user.put()
            users_updated += 1
    self.response.out.write("Updated %d users."%users_updated)
    return
