#from google.appengine.dist import use_library
#use_library('django', '1.2')

import re
import logging
import _base
import time
import datetime
import models2
import deferred_methods
from handlers import aggregates
from django.utils import simplejson as json
from google.appengine.api import users, mail, urlfetch, memcache
from google.appengine.ext import webapp, db, deferred
from google.appengine.api.datastore import Key
from helpers import xmlparse, email, cdns

class RPCHandler(_base.Handler):
  "A simplified RPC handler."
  def post(self):
    args = json.loads(self.request.body)
    func, args = args[0], args[1:]
    if func == 'test_finished':
      #return self.error(404)
      return self.test_finished(*args)
    else:
      return self.error(404)

  def test_finished(self, test_name = None, variant_name = None, results_data = {}, state_data = {}):
    try:
      variant = models2.TestVariant.all().filter('name', test_name).filter('variant', variant_name).get()
    except:
      variant = None
    if not variant:
      self.render("result_error.html", {'error_text': 'Bad test variant.'})
      return
    if (results_data.has_key('anonSession') and results_data['anonSession']):
      try:
        session_id = int(results_data['sessionId'])
      except:
        self.render("result_error.html", {'error_text': 'Bad session ID: %s.'%(results_data.get('sessionId', 'None'))})
        return
      try:
        test_session = models2.Session2.get_by_id(session_id)
      except:
        self.render("result_error.html", {'error_text': 'Could not retrieve session for ID: %d'%(session_id,)})
        return
      if test_session is None:
        self.render("result_error.html", {'error_text': 'No session with ID: %d'%(session_id,)})
        return
      branding = None
      if test_session.experiment:
        branding = test_session.experiment.custom_branding
      state_class = variant.get_state_class()
      state = state_class.update_state_session(session_id, variant, state_data)
      if state:
        state.put()
      result_class = variant.get_result_class()
      result = result_class.make_result(variant, results_data)
      if result:
        result.put()
        if test_session.experiment is not None:
          in_lab = True
        else:
          in_lab = False
        self.render('test_finished.html', {'result':result,
                                           #'summary': result.get_statistics(),
                                           'anon_session': True,
                                           'branding': branding,
                                           'session_id': session_id,
                                           'in_lab': in_lab})
        return
      else:
        self.render('test_finished.html', {'inner_result_name':'result_error.html', 'error_text': 'Bad result.', 'anon_session': True, 'session_id': session_id, 'branding': branding})
        return
    else:
      state_class = variant.get_state_class()
      state = state_class.update_state(variant, state_data)
      if state:
        state.put()
      result_class = variant.get_result_class()
      result = result_class.make_result(variant, results_data)
      if result:
        result.put()
        test_session = result.test_session
        if test_session.experiment is not None:
          in_lab = True
        else:
          in_lab = False
        branding = None
        if test_session.experiment:
          branding = test_session.experiment.custom_branding
        user = models2.User.get_current()
        last_n = [1,5,20,-1]
        if user is not None:
          last_results = aggregates.AggregateSessionResults.last_n_score_statistics_for_user(user.username, (variant.name, variant.group), last_n)
          n_dict = {-1: 'All previous sessions', 1: 'Last session'}
          last_n_str = [n_dict.get(x, 'Last %d sessions'%x) for x in last_n]
          last_results = [{'title': last_n_str[i], 'value': last_results[i][0], 'stderr': last_results[i][1], 'unit': 'number'} for i in xrange(len(last_n_str)) if last_results is not None and last_results[i] is not None]
        else:
          last_results = None
        self.render('test_finished.html', {'result':result,
                                           'summary': result.get_statistics(),
                                           'last_results': last_results,
                                           'branding': branding,
                                           #'session_complete': session_complete,
                                           'in_lab': in_lab})
                                           #'inner_result_name':'results/%s.html'%test_name})
      else:
        self.render('test_finished.html', {'inner_result_name':'result_error.html', 'error_text': 'Bad result.', 'branding': branding})

class OldRPCHandler(webapp.RequestHandler):
  """ Allows the functions defined in the RPCMethods class to be RPCed."""
  def __init__(self):
    webapp.RequestHandler.__init__(self)
    self.methods = RPCMethods()

  def post(self):
    args = json.loads(self.request.body)
    func, args = args[0], args[1:]

    if func[0] == '_':
      self.error(403) # access denied
      return

    # If method starts with "admin" it will only be available to admins.
    if func[0:5] == 'admin':
     if not users.is_current_user_admin():
       self.response.out.write(json.dumps({'success':False,'message':'Admin authorization required','error':403}))
       return

    func = getattr(self.methods, func, None)
    if not func:
      self.error(404) # file not found
      return

    result = func(self.request, *args)
    self.response.out.write(json.dumps(result))

class RPCMethods:
  """ Defines the methods that can be RPCd.
      Methods should return {'success':False} or {'success':True, 'arg_':arg_} (et cetera)
  """

  def store_result(self, request, battery, test_name, result_dict, config_dict):

    def to_class_name(title):
      """turns test_name into TestNameResult"""
      import string
      words = string.split(title, '_')
      for i, word in enumerate(words):
        words[i] = word.capitalize()
      return ''.join(words)+'Result'

    user = models.User.get_current()
    battery = models.Battery.get_by_key_name(battery)
    try:
      result_class = getattr(models, to_class_name(test_name))
    except:
      return {'success':False, 'message':'No results model exists for "%s"' % test_name}
    result = result_class.create(battery, test_name, user, result_dict)
    result.put()
    if not user.config:
      user.config = {}
    user.config[test_name] = config_dict
    user.put()
    return {'success':True, 'message':'Successfully stored results.'}

  def store_config(self, request, key, value):
    user = models.User.get_current()
    if not user.config:
      user.config = {}
    user.config[key] = value
    user.put()
    return {'success':True, 'message':'Successfully stored config.'}

  def get_word_list(self, request, word_type, list_length, already_seen):
    if already_seen == None:
      already_seen = 0
    result = []
    used_indexes = []
    from helpers import word_lists
    import random
    random.seed(already_seen+(65536*list_length))
    if word_type == 'concrete_nouns':
      while len(result) < list_length:
        index = random.randint(0, len(word_lists.concrete_words)-1)
        if index not in used_indexes:
          result.append(word_lists.concrete_words[index])
          used_indexes.append(index)
    elif word_type == 'random_syllables':
      pass # do some generation process here
    return {'success':True, 'word_list':result, 'already_seen':already_seen+1}



