from google.appengine.api import taskqueue
from google.appengine.ext import deferred
from google.appengine.ext import db
from google.appengine.api import users
import _base
import md5
import logging
import string
import datetime
import models2
import results
import random
import math
import csv
import codecs
import StringIO
#import statistics
import pickle
import download_restore
import factor_inference
import entities.mental_variables
import entities.experiment
import scores
import score_params
from django.utils import simplejson as json

class DownloadUserAggregatedResults(_base.Handler):
    @_base.require_admin
    def get(self, username = None):
        if username is None:
            users = [x.username for x in UserAggregatedResults.all().run(batch_size = 1000)]
            users.sort()
            self.response.out.write('\n'.join(users)+'\n')
            return
        if username == 'AFSR':
            afsr = AggregateFullSessionResults.get_or_create()
            data = afsr.get_aggregator().zipme()
            filename = 'QM_AFSR_%s.zip'%(datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
            self.response.headers['content-disposition'] = "attachment; filename=" + filename
            self.response.headers['Content-Type'] = 'binary/zip'
            self.response.out.write(data)
            return
        uar = UserAggregatedResults.all().filter('username', username).get()
        if not uar:
            self.response.out.write("Cannot find username %s."%username)
            return
        if uar.timestamp is None:
            self.response.out.write("No data yet for username %s."%username)
            return
        userdata = uar.to_tuple()
        pcl = pickle.dumps(userdata)
        filename = 'QM_%s_%s.pcl'%(username, (uar.timestamp.strftime("%Y%m%d_%H%M%S")))
        self.response.headers['content-disposition'] = "attachment; filename=" + filename
        self.response.headers['Content-Type'] = 'binary/pcl'
        self.response.out.write(pcl)
        return

class UserAggregatedResults(db.Model):
    username = db.StringProperty()
    timestamp = db.DateTimeProperty()
    user_tup = models2.PickleProperty()
    #mv_tup = models2.PickleProperty()
    #exp_tup = models2.PickleProperty()

    def to_tuple(self):
        sess_tup = self.get_session_aggregator().combine()
        exp_tup = self.get_experiment_aggregator().combine()
        mv_tup = self.get_mentalvariable_aggregator().combine()
        return (self.user_tup, mv_tup, exp_tup, sess_tup)

    def update_butsessions(self):
        user = models2.User.all().filter('username', self.username).get()
        if not user:
            return
        self.update_mentalvariables()
        self.update_experiments()
        self.update_usertup(user)

    def update_mentalvariables(self):
        query = entities.mental_variables.MentalVariable.all().filter('creator_username', self.username)
        mvs = []
        for mv in query.run(batch_size = 1000):
            mvs.append(mv.to_tuple())
        self.add_mentalvariables(mvs)

    def update_experiments(self):
        query = entities.experiment.Experiment.all().filter('username', self.username)
        experiments = []
        for experiment in query.run(batch_size = 1000):
            experiments.append(experiment.to_tuple())
        self.add_experiments(experiments)

    def update_usertup(self, user):
        self.user_tup = user.to_tuple()
        #self.mv_tup = mvs
        #self.exp_tup = experiments
        self.put()

    def add_session(self, session):
        sess_tup = session.to_tuple()
        agg = self.get_session_aggregator()
        agg.add_data(sess_tup)

    def add_experiments(self, exp_tups):
        agg = self.get_experiment_aggregator()
        agg.add_multiple_data(exp_tups)

    def add_mentalvariables(self, mv_tups):
        agg = self.get_mentalvariable_aggregator()
        agg.add_multiple_data(mv_tups)

    def update_sessions_one(self):
        user = models2.User.all().filter('username', self.username).get()
        if not user:
            return
        sessq = models2.Session2.all().filter('active', False).filter('user', user).order('start_time')
        if self.timestamp is not None:
            sessq = sessq.filter('start_time > ', self.timestamp)
        sess = sessq.get()
        if not sess:
            return False
        self.add_session(sess)
        self.timestamp = sess.start_time
        self.put()
        return True

    def update_all(self):
        deferred_update_uar(self.username, 0)

    @classmethod
    def get_agg_type(self):
        return "user_aggregated_session_results"

    @classmethod
    def get_exp_agg_type(self):
        return "user_aggregated_experiment_results"

    @classmethod
    def get_mv_agg_type(self):
        return "user_aggregated_mentalvariable_results"

    def get_agg_id(self):
        return self.username

    def get_session_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_agg_type(), self.get_agg_id())

    def get_experiment_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_exp_agg_type(), self.get_agg_id())

    def get_mentalvariable_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_mv_agg_type(), self.get_agg_id())

    def clean(self):
        self.timestamp = None
        self.user_tup = None
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_exp_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_mv_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        self.put()

    @classmethod
    def clean_all(cls):
        uars = []
        for uar in cls().all().run(batch_size = 1000):
            uars.append(uar)
        for uar in uars:
            uar.clean()
            db.delete(uar)
        logging.info("Done cleaning user aggregates.")

    @classmethod
    def reset_or_create(cls, username):
        uar = cls.all().filter('username', username).get()
        if not uar:
            uar = cls(username = username)
        uar.clean()
        return uar

    @classmethod
    def get_or_create(cls, username):
        uar = cls.all().filter('username', username).fetch(2)
        if len(uar) == 1:
            return uar[0]
        elif len(uar) > 1:
            for uar in cls.all().filter('username', username).run(batch_size = 1000):
                uar.clean()
                db.delete(uar)
        # create
        uar = cls(username = username)
        uar.clean()
        return uar

def deferred_update_uar(username, stage = 0):
    uar = UserAggregatedResults.get_or_create(username)
    if stage == 0:
        uar.update_butsessions()
        deferred.defer(deferred_update_uar, username, 1)
        return
    elif stage == 1:
        if uar.update_sessions_one():
            deferred.defer(deferred_update_uar, username, 1)
        return

def deferred_aggregate_full_session_results(reset = False):
    if reset:
        afsr = AggregateFullSessionResults.reset_or_create()
    else:
        afsr = AggregateFullSessionResults.get_or_create()
    ns = afsr.add_next_sessions()
    if len(ns) > 0:
        logging.info("AFSR: %d sessions added, %d total, last date: %s"%(len(ns), afsr.total_sessions, afsr.last_date.strftime("%Y-%m-%d %H:%M:%S")))
        deferred.defer(deferred_aggregate_full_session_results)

class AggregateFullSessionResults(db.Model):
    last_date = db.DateTimeProperty()
    total_sessions = db.IntegerProperty(default = 0)

    def get_agg_type(self):
        return 'full_session_results'

    def get_agg_id(self):
        return '1'

    def get_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_agg_type(), self.get_agg_id())

    def clean(self):
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        db.delete(self)

    def reset(self):
        self.last_date = None
        return download_restore.GeneralAggregator.reset_or_create(self.get_agg_type(), self.get_agg_id())

    @classmethod
    def get_or_create(cls):
        atr = cls.all().get()
        if atr:
            return atr
        # create
        atr = cls()
        atr.reset()
        atr.put()
        return atr

    @classmethod
    def reset_or_create(cls):
        atr = cls.all().get()
        if not atr:
            atr = cls()
        atr.reset()
        atr.put()
        return atr

    def add_next_sessions(self):
        query = models2.Session2.all().filter('active', False).order('start_time')
        if self.last_date is not None:
            query = query.filter('start_time >',self.last_date)
        sess = query.get()
        if sess is None:
            return []
        dt = sess.start_time
        query2 = query.filter('start_time', sess.start_time)
        sesss = query2.fetch(100)
        agg = self.get_aggregator()
        for test_session in sesss:
            username = test_session.user.username
            session_tup = get_session_information(test_session)
            observations = test_session.collect_observations(collect_all = True)
            if observations.has_key('all'):
                obsks, newobs = self.compress_obs(observations)
                agg.add_data((username, session_tup, obsks, newobs))
                self.total_sessions += 1
        self.last_date = dt
        self.put()
        return sesss

    def compress_obs(self, obs):
        d = {}
        newobs = []
        obsks = []
        for (obsk, obsv) in obs['all']:
            if not d.has_key(obsk):
                d[obsk] = len(d)
                obsks.append(obsk)
            newobs.append((d[obsk], obsv))
        return (obsks, newobs)

class AggregateSessionResults(db.Model):
    username = db.StringProperty()

    def variant_to_name(self, variant):
        v = models2.TestVariant.all().filter('variant_name', variant[0]).filter('variant_group', variant[1]).get()

    def to_csv(self, experiment, with_stderr = False):
        self.variant_translations = {}
        results = self.get_aggregator().combine()
        results.sort(key = lambda result: result[0][1])
        exp_results = []
        tests = set()
        mvs = set()
        for (sessinfo, ss) in results:
            if sessinfo[6] == experiment:
                exp_mvs = []
                if len(sessinfo[7]) > 0:
                    exp_mvs = zip(sessinfo[7][0][0], sessinfo[7][1][0])
                    for (mv,ms) in exp_mvs:
                        mvs.add(mv)
                scores = self.compute_result_scores((sessinfo, ss))
                scores2 = {}
                for score in scores:
                    tests.add(score['name'])
                    if with_stderr:
                        scores2[score['name']] = ('%.3f'%score['value'], '%.3f'%score['stderr'])
                    else:
                        scores2[score['name']] = ('%.3f'%score['value'],)
                dt = models2.translate_datetime(sessinfo[4], sessinfo[1])
                exp_results.append((dt, dict(exp_mvs), scores2))
        mvs = list(mvs)
        mvs.sort()
        tests = list(tests)
        tests.sort()
        tests2 = []
        for test in tests:
            tests2.append(test)
            if with_stderr:
                tests2.append("%s standard error"%test)
        headers = ["Session", "Date", "Time"] + mvs + tests2
        rows = []
        for (i,(dt, exp_mvs, scores)) in enumerate(exp_results):
            new_row = [i+1, dt.strftime("%Y-%m-%d"), dt.strftime("%H:%M:%S")] + [exp_mvs.get(mv,"") for mv in mvs]
            for test in tests:
                if with_stderr:
                    new_row.extend(list(scores.get(test, ('N/A','N/A'))))
                else:
                    new_row.extend(list(scores.get(test, ('N/A',))))
            rows.append(new_row)
        csv_output = StringIO.StringIO()
        utf_8_lines = [[codecs.utf_8_encode('%s'%x) for x in y] for y in [headers] + rows]
        csv.writer(csv_output).writerows(utf_8_lines)
        filename = '%s_%s_%s.csv'%(self.username, ''.join([ch for ch in experiment if ch.isalnum()]), datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
        return (csv_output.getvalue(), filename)

    def to_json(self):
        self.variant_translations = {}
        results = self.get_aggregator().combine()
        results.sort(key = lambda result: result[0][1])
        results2 = [self.result_pre_json(result) for result in results]
        return json.dumps(results2)

    def result_pre_json(self, result):
        mvs = []
        if len(result[0][7]) > 0:
            mvs = zip(result[0][7][0][0], result[0][7][1][0])
        variables = [{'name': mv[0], 'value': mv[1]} for mv in mvs]
        scores = self.compute_result_scores(result)
        dt = models2.translate_datetime(result[0][4], result[0][1])
        t = dt.hour * 1.0 + dt.minute / 60.0 + dt.second / 3600.0
        j = {'utcdt': result[0][1].strftime("%Y%m%d%H%M%S"),
             'experiment': result[0][6],
             'date': dt.strftime("%Y-%m-%d"),
             'time': t,
             'variables': variables,
             'scores': scores}
        return j

    def compute_result_scores(self, result, translation = True):
        ss = result[1]
        theta = score_params.score_params
        by_variant = {}
        for k in ss.keys():
            variant = k[0]
            if not by_variant.has_key(variant):
                by_variant[variant] = {}
            by_variant[variant][k] = ss[k]
        variants = by_variant.keys()
        variants.sort()
        result_scores = []
        for variant in variants:
            if not theta.has_key(variant):
                continue
            if translation:
                if not self.variant_translations.has_key(variant):
                    tv0 = models2.TestVariant.all().filter('name', variant[0]).filter('group', variant[1]).fetch(100)
                    tv = None
                    for tvi in tv0:
                        if tvi.variant.find("practice") == -1:
                            tv = tvi
                            break
                    if tv is None:
                        name = ''
                        for namepart in variant[0].split("_"):
                            name = name + " " + namepart[0:1].upper() + namepart[1:]
                        if len(name) > 0:
                            name = name[1:]
                    else:
                        name = tv.description
                    self.variant_translations[variant] = name
                name = self.variant_translations[variant]
            else:
                name = variant
            mean_score, stderr_score = scores.QMOM2_argmax_score(theta[variant], by_variant[variant])
            result_scores.append({'name': name, 'value': mean_score * 100 + 500, 'stderr': stderr_score * 100})
        return result_scores

    def get_session_results(self, experiments=[], variables={}):
        results = self.get_aggregator().combine()
        summary_statistics = []
        for result in results:
            result_variables = dict(zip(result[0][7][0][0], result[0][7][1][0]))
            if experiments and result[0][6] not in experiments: continue
            if not any([result_variables.get(v_name) != v_state and not
                        (v_state == 'anything' and result_variables.get(v_name))
                        for (v_name, v_state) in variables.items()]):
                summary_statistics.append({'result': result[1],
                                           'variables': result_variables})
        return summary_statistics

    @classmethod
    def get_agg_type(self):
        return "aggregate_session_results"

    def get_agg_id(self):
        agg_id = "%s"%(self.username)
        return agg_id

    def get_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_agg_type(), self.get_agg_id())

    def clean(self):
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        db.delete(self)

    def reset(self):
        return download_restore.GeneralAggregator.reset_or_create(self.get_agg_type(), self.get_agg_id())

    @classmethod
    def weighted_average_scores(cls, scores):
        w = 0.0
        sw = 0.0
        for (m, s) in scores:
            wi = 1.0 / s**2
            w += wi
            sw += wi*m
        if w > 0:
            return (sw / w, math.sqrt(1.0 / w))
        return None

    @classmethod
    def last_n_score_statistics_for_user(cls, username, variant_info, ns):
        asr = cls.all().filter('username', username).get()
        if asr is None:
            return None
        results = asr.get_ordered_variant_results(variant_info)
        if isinstance(variant_info, list):
            return [asr.last_n_score_statistics_from_results(results_one, ns) for results_one in results]
        else:
            return asr.last_n_score_statistics_from_results(results, ns)

    def last_n_score_statistics_from_results(self, my_results, ns):
        if isinstance(ns, int):
            ns = [ns]
        if -1 in ns:
            include_results = my_results
        else:
            include_results = my_results[:max(ns)]
        scores = [self.compute_result_scores(result, translation = False) for result in include_results]
        scores = [(x[0]['value'], x[0]['stderr']) for x in scores if len(x) == 1]
        res = []
        for n in ns:
            if n==-1:
                sc = scores
            elif len(scores) >= n:
                sc = scores[:n]
            else:
                sc = None
            if sc is None:
                res.append(None)
            else:
                res.append(self.weighted_average_scores(sc))
        return res

    def get_ordered_variant_results(self, variant_infos):
        my_sessions = self.get_aggregator().combine()
        my_sessions.sort(key = lambda sess: sess[0][1], reverse=True)
        if not isinstance(variant_infos, list):
            one_variant = True
            variant_infos = [variant_infos]
        else:
            one_variant = False
        my_results = []
        for variant_info in variant_infos:
            my_results.append([])
            for (sessinfo, ss) in my_sessions:
                sss = {}
                for k in ss.keys():
                    if k[0] == variant_info:
                        sss[k] = ss[k]
                if len(sss) > 0:
                    my_results[-1].append((sessinfo, sss))
        if one_variant:
            return my_results[0]
        return my_results

    @classmethod
    def get_or_create(cls, username):
        atr = cls.all().filter('username', username).get()
        if atr:
            return atr
        # create
        atr = cls(username = username)
        atr.reset()
        atr.put()
        return atr

    @classmethod
    def reset_or_create(cls, username):
        atr = cls.all().filter('username', username).get()
        if not atr:
            atr = cls(username = username)
        atr.reset()
        atr.put()
        return atr

    def build_from_scratch(self, cursor = None, max_sessions = 25):
        query = self.make_session_query(self.username)
        if cursor is None:
            self.reset()
        else:
            query = query.with_cursor(cursor)
        test_sessions = query.fetch(max_sessions)
        if len(test_sessions) < max_sessions:
            next_cursor = None
        else:
            next_cursor = query.cursor()
        for test_session in test_sessions:
            logging.info("Adding %s test session (started at %s) for user %s"%(test_session.battery_name, test_session.start_time.strftime("%Y%m%d_%H%M%S"), self.username))
            self.add_test_session(test_session)
        return next_cursor

    @classmethod
    def update_one_session(cls, test_session):
        username = test_session.user.username
        asr = cls.get_or_create(username)
        asr.add_test_session(test_session)

    def add_test_session(self, test_session):
        session_tup = get_session_information(test_session)
        observations = test_session.collect_observations()
        agg = self.get_aggregator()
        agg.add_data((session_tup, observations))

    @classmethod
    def make_session_query(cls, username):
        user = models2.User.all().filter('username', username).get()
        if not user:
            return None
        return models2.Session2.all().filter('user', user).order('start_time')

class AggregateTestResults(db.Model):
    username = db.StringProperty()
    variant_name = db.StringProperty()
    variant_variant = db.StringProperty()

    @classmethod
    def get_agg_type(self):
        return "aggregate_summary_statistics"

    def get_agg_id(self):
        agg_id = "%s:%s:%s"%(self.username, self.variant_name, self.variant_variant)
        return agg_id

    def get_aggregator(self):
        return download_restore.GeneralAggregator.get_or_create(self.get_agg_type(), self.get_agg_id())

    def clean(self):
        agg = download_restore.GeneralAggregator.all().filter('agg_type', self.get_agg_type()).filter('agg_id', self.get_agg_id()).get()
        if agg:
            agg.clean()
        db.delete(self)

    def reset(self):
        return download_restore.GeneralAggregator.reset_or_create(self.get_agg_type(), self.get_agg_id())

    @classmethod
    def collect_for_group_multiple_users(cls, usernames, variant_name, variant_group, flt = None):
        res = []
        for username in usernames:
            print "Collecting for %s..."%username
            resu = cls.collect_for_group(username, variant_name, variant_group, flt)
            res.extend(resu)
        return res

    @classmethod
    def collect_for_group(cls, username, variant_name, variant_group, flt = None):
        res = []
        for variant in models2.TestVariant.all().filter('name', variant_name).filter('group', variant_group).run():
            atr = cls.all().filter('username', username).filter('variant_name', variant_name).filter('variant_variant', variant.variant).get()
            if atr:
                agg = atr.get_aggregator()
                results = agg.combine()
                for (session_tup, sss) in results:
                    if (flt is None) or flt(session_tup):
                        res.append((session_tup,sss))
        res.sort(key = lambda x: x[0][1])
        res = reduce(lambda x,y:x+y, [x[1] for x in res], [])
        return res

    @classmethod
    def clean_all(cls, username):
        query = cls.all().filter('username', username)
        for atr in query.run():
            atr.clean()

    @classmethod
    def get_or_create(cls, username, variant_name, variant_variant):
        atr = cls.all().filter('username', username).filter('variant_name', variant_name).filter('variant_variant', variant_variant).get()
        if atr:
            return atr
        # create
        atr = cls(username = username,
                  variant_name = variant_name,
                  variant_variant = variant_variant)
        atr.reset()
        atr.put()
        return atr

    def build_from_scratch(self):
        self.reset()
        query = self.make_session_query(self.username)
        for session in query.run(batch_size = 100):
            print "Adding %s test session (started at %s) for user %s"%(session.battery_name, session.start_time.strftime("%Y%m%d_%H%M%S"), self.username)
        self.add_test_session(test_session)

    @classmethod
    def build_from_scratch_for_user(cls, username):
        cls.clean_all(username)
        query = cls.make_session_query(username)
        for session in query.run(batch_size = 100):
            print "Processing %s test session (started at %s) for user %s..."%(session.battery_name, session.start_time.strftime("%Y%m%d_%H%M%S"), username)
            cls.add_test_session(session)

    @classmethod
    def add_test_session(cls, test_session):
        session_tup = get_session_information(test_session)
        username = test_session.user.username
        #print "Querying test results..."
        results = models2.CompletedTest.all().filter('test_session', test_session).run()
        results = [result for result in results if ((result.is_practice is None) or (result.is_practice == False)) and ((result.is_tentative is None) or (result.is_tentative is False))]
        #results = models2.CompletedTest.all().filter('test_session', test_session).filter('is_practice', False).filter('is_tentative', False).run()
        #print "Done querying."
        atrs = {}
        for result in results:
            #print "Processing (%s, %s) result..."%(result.variant_name, result.variant_variant)
            atr = cls.get_or_create(username, result.variant_name, result.variant_variant)
            kid = atr.key().id()
            if not atrs.has_key(kid):
                atrs[kid] = (atr, [])
            atrs[kid][1].append(result.summary_statistics())
        for (atr, sss) in atrs.values():
            agg = atr.get_aggregator()
            agg.add_data((session_tup, sss))
        #print "Added results for: %s"%(", ".join(["(%s,%s)"%(atr.variant_name, atr.variant_variant) for (atr, sss) in atrs.values()]))

    @classmethod
    def make_session_query(cls, username):
        user = models2.User.all().filter('username', username).get()
        if not user:
            return None
        return models2.Session2.all().filter('user', user).order('start_time')

    def update(self):
        if not self.update_in_progress:
            taskqueue.add(url='/tasks/update_aggregate_results', params={'id': self.key().id()})

    def get_session_results(self, experiments=[], variables={}):
        """
        Get all the summary statistics, but only for those sessions
        which match the given experiments (list) and mental variables
        (dict).
        """
        agg = self.get_aggregator()
        results = agg.combine()
        summary_statistics = []
        for result in results:
            result_variables = dict(zip(result[0][7][0][0], result[0][7][1][0]))
            if experiments and result[0][6] not in experiments: continue
            if not any([result_variables.get(v_name) != v_state and not
                        (v_state == 'anything' and result_variables.get(v_name))
                        for (v_name, v_state) in variables.items()]):
                summary_statistics.extend(result[1])
        return summary_statistics



def get_session_information(session):
    si = session.session_information
    if si:
        mvs = zip([si.mental_variables, si.mental_states])
    else:
        mvs = []
    if session.experiment:
        experiment_name = session.experiment.name
    else:
        experiment_name = None
    info = [session.battery_name, session.start_time, session.end_time, session.user_agent, session.timezone, session.is_complete, experiment_name, mvs]
    return info

class UpdateAggregateResults(_base.Handler):
    @_base.require_admin
    def get(self, atr_id):
        try:
            atr = AggregateTestResults.get_by_id(int(atr_id))
        except:
            atr = None
        if not atr:
            self.redirect('/admin/download_aggregates')
            return
        atr.update()
        self.redirect('/admin/download_aggregates')

    def post(self):
        try:
            my_id = int(self.request.get("id"))
            atr = AggregateTestResults.get_by_id(my_id)
        except:
            atr = None
        if atr is None:
            return
        cursor = self.request.get("cursor")
        if not cursor:
            cursor = None
            offset = atr.sessions_included
            atr.update_in_progress = True
            atr.put()
        else:
            offset = 0
        sq = atr.make_session_query(atr.username).with_cursor(cursor)
        sessions = list(sq.run(limit = 10, offset = offset))
        if len(sessions) == 0:
            atr.update_in_progress = False
            atr.put()
            return
        for session in sessions:
            results = models2.CompletedTest.all().filter('test_session', session).filter('variant_name', atr.variant_name).filter('variant_variant', atr.variant_variant).filter('is_practice', False).filter('is_tentative', False).run()
            info = None
            for result in results:
                if info is None:
                    info = get_session_information(session)
                ss = result.summary_statistics()
                atr.session_information.append(info)
                atr.result_indices.append(len(atr.result_data))
                atr.result_data.extend(map(float,ss))
                atr.sessions_included += 1
                atr.put()
        taskqueue.add(url='/tasks/update_aggregate_results', params={'id': atr.key().id(),
                                                                     'cursor': sq.cursor()})

class CreateUserAggregateResults(_base.Handler):
    def post(self):
        username = self.request.get("username")
        user = models2.User.all().filter('username', username).get()
        if not user:
            return
        cursor = self.request.get("cursor")
        if not cursor:
            cursor = None
        sq = models2.Session2.all().filter('user', user).order('start_time').with_cursor(cursor)
        sessions = list(sq.run(limit = 10))
        if len(sessions) == 0:
            return
        for session in sessions:
            results = models2.CompletedTest.all().filter("test_session", session).fetch(1000)
            for result in results:
                if result.is_practice or result.is_tentative:
                    continue
                atr = AggregateTestResults.get_or_create(username, result.variant_name, result.variant_variant)
        taskqueue.add(url='/tasks/create_user_aggregate_results', params={'username': username,
                                                                          'cursor': sq.cursor()})
        self.response.set_status(200)
        return

class CreateAggregateResults(_base.Handler):
    @_base.require_admin
    def get(self, username, variant_name, variant_variant):
        atr = AggregateTestResults.get_or_create(username, variant_name, variant_variant)
        if atr.update_in_progress:
            message = 'Update already in progress for user %s and test %s (%s)!'%(username, variant_name, variant_variant)
        else:
            atr.update()
            message = 'Creating aggregates for user %s and test %s (%s)'%(username, variant_name, variant_variant)
        self.render('admin_create_aggregates.html',
                    {'message': message})

    @_base.require_admin
    def post(self):
        username = self.request.get("username")
        user = models2.User.all().filter('username', username).get()
        if not user:
            self.render('admin_create_aggregates.html',
                        {'error': "Can't find user %s"%username})
            return
        taskqueue.add(url='/tasks/create_user_aggregate_results', params={'username': username})
        self.render('admin_create_aggregates.html',
                    {'message': 'Creating aggregates for user %s.'%username})
        return

class DownloadAggregateResults(_base.Handler):
    @_base.require_admin
    def get(self, atr_id = None):
        if atr_id is None:
            all_users = [x.username for x in models2.User.all().run(batch_size = 1000)]
            all_users.sort()
            atrs = list(AggregateTestResults.all().run(batch_size = 1000))
            atrs.sort(key = lambda atr: (atr.username, atr.variant_name, atr.variant_variant))
            context = {'atrs': atrs,
                       'all_users': all_users}
            self.render('admin_download_aggregates.html', context)
            return
        try:
            atr = AggregateTestResults.get_by_id(int(atr_id))
        except:
            atr = None
        if not atr:
            self.redirect('/admin/download_aggregates')
            return
        tup = atr.to_tuple()
        pcl = pickle.dumps(tup)
        filename = "QM_aggregates_%s_%s_%s_%s.pcl"%(atr.username, atr.variant_name, atr.variant_variant, datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
        self.response.headers['content-disposition'] = "attachment; filename=" + filename
        self.response.headers['Content-Type'] = 'binary/pcl'
        self.response.out.write(pcl)

class CreateAggregateResults2(_base.Handler):
    @_base.require_admin
    def get(self, command = None, username = None):
        if command is None:
            self.redirect('/admin')
            return
        if command == "aggregate":
            if username is None:
                aggregate_results_all_users()
                self.redirect('/admin')
                return
            user = models2.User.all().filter('username', username).get()
            if not user:
                self.redirect("/admin")
                return
            aggregate_results_user(user.key().id())
            self.redirect('/admin')
            return

def aggregate_results_all_users(cursor = None):
    query = models2.User.all().order('username')
    if cursor is not None:
        query.with_cursor(cursor)
    user = query.get()
    cursor = query.cursor()
    if not user:
        return
    logging.info("aggregating for user %s..."%user.username)
    aggregate_results_user(user.key().id(), None, callback = aggregate_results_all_users, callback_args = [cursor])

def aggregate_results_user(user_key_id, cursor = None, callback = None, callback_args = None):
    user = models2.User.get_by_id(user_key_id)
    username = user.username
    query = models2.Session2.all().filter('user', user).order('start_time')
    if cursor is None:
        GA = download_restore.GeneralAggregator.reset_or_create('session_summaries', username)
    else:
        GA = download_restore.GeneralAggregator.get_or_create('session_summaries', username)
        query.with_cursor(cursor)
    sessions = query.fetch(10)
    if len(sessions) == 0:
        if callback is not None:
            callback(*callback_args)
        return
    cursor = query.cursor()
    res = []
    for session in sessions:
        logging.info("adding %s session from %s for user %s"%(session.battery_name, session.corrected_start_time().strftime("%Y%m%d_%H%M%S"), user.username))
        test_results = [result for result in models2.CompletedTest.all().filter('test_session', session).run() if ((result.is_practice is None) or (not result.is_practice)) and ((result.is_tentative is None) or (not result.is_tentative))]
        test_results.sort(key = lambda x: x.start_time)
        sres = []
        for result in test_results:
            pfx = (result.variant_name, result.variant_variant)
            ss = result.summary_statistics()
            sm = result.summary_metrics([ss])
            for (name, description, unit, value, stderr) in sm:
                sres.append(pfx + (name, value, stderr))
        res.append((session.timezone, session.start_time, session.mental_variables(), sres))
    GA.add_multiple_data(res)
    deferred.defer(aggregate_results_user, user_key_id, cursor, callback, callback_args)

def deferred_update_prior(result_class, sss, username, variant_info):
  if result_class.update_prior(sss, username, variant_info):
    deferred.defer(deferred_update_prior, result_class, sss, username, variant_info)

def recompute_priors_variant_group(username, variant_info):
  variant = models2.TestVariant.all().filter('name', variant_info[0]).filter('group', variant_info[1]).get()
  if not variant:
      return None
  result_class = variant.get_result_class()
  W = result_class.get_factor_weights(variant_info[1])
  if W is None:
      return None
  factor_info, weights = W
  user_session_aggregates = AggregateSessionResults.all().filter('username', username).get()
  if not user_session_aggregates:
      return None
  try:
      sss = user_session_aggregates.get_aggregator().combine()
  except:
      return None
  Y = []
  for (sessinfo, observations) in sss:
      sess_Y = {}
      for k in observations.keys():
          if weights.has_key(k[:-1]):
              sess_Y[k] = observations[k]
      if len(sess_Y) > 0:
          Y.append(sess_Y)
  if len(Y) == 0:
      return None
  m = len(factor_info)
  X_prior = factor_inference.make_default_prior(m)
  logging.info("Running inference...")
  X = [factor_inference.factor_inference(y, weights, X_prior) for y in Y]
  global_NIW = factor_inference.default_NIW(m)
  logging.info("Learning prior...")
  X_post, prior = factor_inference.estimate_prior(X, global_NIW)
  user_prior = models2.ResultsPrior.get_or_create(username, variant_info[0], variant_info[1])
  user_prior.prior_data = prior
  #user_prior.X_post = X_post
  user_prior.X_post = X
  user_prior.put()
  logging.info("Prior updated successfully.")
  return True

def recompute_priors(test_session):
  username = test_session.user.username
  for finished_test in models2.CompletedTest.all().filter('test_session', test_session).filter('is_practice', False).filter('is_tentative', False).run():
    variant_info = (finished_test.variant_name, finished_test.variant().group)
    deferred.defer(recompute_priors_variant_group, username, variant_info)

def build_session_aggregates_deferred(cursor = None, user_cursor = None):
    query = models2.User.all().order('username')
    if cursor is not None:
        query = query.with_cursor(cursor)
    users = query.fetch(1)
    next_cursor = query.cursor()
    if len(users) == 0:
        return
    username = users[0].username
    if user_cursor is None:
        logging.info("Building session aggregates for %s..."%username)
        asr = AggregateSessionResults.reset_or_create(username)
    else:
        logging.info("Continuing session aggregates for %s..."%username)
        asr = AggregateSessionResults.get_or_create(username)
    next_user_cursor = asr.build_from_scratch(user_cursor)
    if next_user_cursor is not None:
        deferred.defer(build_session_aggregates_deferred, cursor, next_user_cursor)
        return
    try:
        rebuilt = asr.get_aggregator().combine()
        logging.info("Built successfully for %s."%username)
    except:
        logging.info("Error reconstructing aggregates for user %s. Retrying...")
        deferred.defer(build_session_aggregates_deferred, cursor)
        return
    deferred.defer(build_session_aggregates_deferred, next_cursor)

def user_build_session_aggregates_deferred(username, user_cursor = None):
    if user_cursor is None:
        asr = AggregateSessionResults.reset_or_create(username)
    else:
        asr = AggregateSessionResults.get_or_create(username)
    next_user_cursor = asr.build_from_scratch(user_cursor)
    if next_user_cursor is not None:
        deferred.defer(user_build_session_aggregates_deferred, username, next_user_cursor)
        return
    try:
        rebuilt = asr.get_aggregator().combine()
        logging.info("Built successfully for %s."%username)
    except:
        logging.info("Error reconstructing aggregates for user %s. Retrying...")
        deferred.defer(user_build_session_aggregates_deferred, username)
        return

def build_session_aggregates(usernames = None):
    if usernames is None:
        users = models2.User.all()
        print "Reading users..."
        usernames = []
        cursor = None
        for user in users:
            print user.username
            usernames.append(user.username)
        usernames.sort()
        print "Done reading %d users."%len(usernames)
    if isinstance(usernames, basestring):
        print "Running for %s..."%usernames
        return
    for username in usernames:
        deferred.defer(build_session_aggregates, username)

def aggregate_aggregate_session_results(cursor = None):
    logging.info("In aggregate_aggregate_session_results, cursor: %s"%str(cursor))
    query = AggregateSessionResults.all()
    if cursor is None:
        asrall = AggregateSessionResults.reset_or_create('combined_all_users')
    else:
        query = query.with_cursor(cursor)
        asrall = AggregateSessionResults.get_or_create('combined_all_users')
    logging.info("Trying to fetch 5 users.")
    asrs = query.fetch(5)
    logging.info("Done fetching %d users."%(len(asrs)))
    if len(asrs) == 0:
        return
    datas = []
    for asr in asrs:
        username = asr.username
        if username != 'combined_all_users':
            logging.info("Working on user %s..."%username)
            try:
                datas.append((username, asr.get_aggregator().combine()))
                logging.info("Successfully combined data from %s..."%username)
            except:
                logging.info("Error combining data from %s..."%username)
                user_cursor = None
                while True:
                    next_user_cursor = asr.build_from_scratch(user_cursor)
                    if next_user_cursor is None:
                        break
                    user_cursor = next_user_cursor
                deferred.defer(aggregate_aggregate_session_results, cursor)
                return
    logging.info("Adding data...")
    agg = asrall.get_aggregator()
    if agg.add_multiple_data(datas) is -1:
        verified = False
    else:
        verified = agg.verify_checksums()
    if verified:
        logging.info("Data integrity verified, %d users."%(len(agg.checksums)))
    else:
        logging.info("Error reconstructing all aggregates, aborting...")
        #deferred.defer(aggregate_aggregate_session_results)
        return
    deferred.defer(aggregate_aggregate_session_results, query.cursor())

class TasksAggregateSessions(_base.Handler):
    def get(self):
        aggregate_aggregate_session_results()
        return

class AggregateSiteStatistics(db.Model):
    #users = models2.PickleProperty()
    #sessions_per_user = models2.PickleProperty()
    #users_by_experiment = models2.PickleProperty()

    @classmethod
    def reset_user_aggregator(cls):
        agg = download_restore.GeneralAggregator.reset_or_create('site_statistics_users', 'all')
        return agg

    @classmethod
    def get_user_aggregator(cls):
        agg = download_restore.GeneralAggregator.get_or_create('site_statistics_users', 'all')
        return agg

    @classmethod
    def clean_experiment_aggregators(cls):
        for expagg in download_restore.GeneralAggregator.all().filter('agg_type', 'site_statistics_experiments').run(batch_size = 100):
            expagg.clean()

    @classmethod
    def get_experiment_aggregator(cls, name):
        return download_restore.GeneralAggregator.get_or_create('site_statistics_experiments', name)

    @classmethod
    def get_experiment_aggregators(cls):
        return download_restore.GeneralAggregator.all().filter('agg_type', 'site_statistics_experiments').run(batch_size = 100)

    @classmethod
    def reset_or_create(cls):
        ass = AggregateSiteStatistics.all().get()
        if ass is None:
            ass = AggregateSiteStatistics()
        ass.get_user_aggregator().clean()
        #ass.users = []
        #ass.sessions_per_user = {}
        #ass.users_by_experiment = {}
        ass.clean_experiment_aggregators()
        ass.put()
        return ass

    @classmethod
    def get_or_create(cls):
        ass = AggregateSiteStatistics.all().get()
        if not ass:
            return cls.reset_or_create()
        return ass

    def add_user(self, user):
        user_agg = self.get_user_aggregator()
        userinfo = {'username': user.username,
                    'email': user.google_account.email(),
                    'create_date': user.create_date,
                    'timezone': user.timezone,
                    'age': user.age,
                    'gender': user.gender,
                    'education': user.education,
                    'timezone': user.timezone}
        sessions_per_user = []
        for sess in models2.Session2.all().filter('user', user).run(batch_size = 100):
            sessions_per_user.append(sess.start_time)
        if user_agg.add_data((userinfo, sessions_per_user)) is -1:
            return False
        for experiment in entities.experiment.Experiment.all().filter('username', user.username).filter("deleted", False).run(batch_size = 100):
            expagg = self.get_experiment_aggregator(experiment.name)
            if expagg.add_data((experiment.username, experiment.sessions_complete)) is -1:
                return False
        self.put()
        return True

def deferred_aggregate_site_statistics(cursor = None):
    if cursor is None:
        ass = AggregateSiteStatistics.reset_or_create()
    else:
        ass = AggregateSiteStatistics.get_or_create()
    query = models2.User.all()
    if cursor is not None:
        query = query.with_cursor(cursor)
    users = query.fetch(10)
    if len(users) == 0:
        return
    for user in users:
        if not ass.add_user(user):
            logging.info("Aggregate site statistics failed")
            return
    cursor = query.cursor()
    deferred.defer(deferred_aggregate_site_statistics, cursor)

class SiteBackup(db.Model):
    last_backup_date = db.DateTimeProperty()

    @classmethod
    def get_or_create(cls):
        res = cls().all().get()
        if not res:
            res = cls()
        return res

    @classmethod
    def set_to_now(cls):
        res = cls.get_or_create()
        res.last_backup_date = datetime.datetime.utcnow()
        res.put()

    @classmethod
    def get_last_date(cls):
        return cls.get_or_create().last_backup_date

    @classmethod
    def reset(cls):
        res = cls().all().get()
        if res:
            db.delete(res)

class TasksAggregateSiteStatistics(_base.Handler):
    def get(self):
        deferred_aggregate_site_statistics()
        return

def pickle_user_data(username):
    user = models2.User.all().filter('username', username).get()
    if not user:
        return
    query = entities.mental_variables.MentalVariable.all().filter('creator_username', username)
    mvs = []
    for mv in query.run(batch_size = 1000):
        mvs.append(mv.to_tuple())
    query = entities.experiment.Experiment.all().filter('username', username)
    experiments = []
    for experiment in query.run(batch_size = 1000):
        experiments.append(experiment.to_tuple())
    sessq = models2.Session2.all().filter('active', False).filter('user', user)
    sesss = []
    for sess in sessq.run(batch_size = 1000):
        sesss.append(sess.to_tuple())
    user_tup = user.to_tuple()
    user_data = (user_tup, mvs, experiments, sesss)
    agg = download_restore.GeneralAggregator.reset_or_create('user_tuple_data', username)
    agg.add_data(user_data)
    return

def deferred_pickle_all_userdata(cursor = None):
    last_backup = SiteBackup.get_last_date()
    query = models2.User.all()
    if cursor is not None:
        query = query.with_cursor(cursor)
    users = query.fetch(10)
    if len(users) == 0:
        SiteBackup.set_to_now()
        return
    for user in users:
        if user.username in ['nick','yoni']:
            continue
        if ((last_backup is None) or ((user.last_activity is not None) and (user.last_activity >= last_backup))):
            logging.info("Backing up user %s..."%user.username)
            try:
                pickle_user_data(user.username)
            except Exception, e:
                logging.info("Error backing up user %s: %s"%(user.username, str(e)))
                return
    cursor = query.cursor()
    deferred.defer(deferred_pickle_all_userdata, cursor)

def clean_user_aggregates_one():
    query = UserAggregatedResults.all()
    uar = query.get()
    if uar is None:
        return False
    uar.clean()
    db.delete(uar)
    return True

def deferred_run_site_backup(state = 0, cursor = None):
    if state == 0:
        if clean_user_aggregates_one():
            deferred.defer(deferred_run_site_backup, 0)
            return
        else:
            deferred.defer(deferred_run_site_backup, 1)
            return
    query = models2.User.all()
    if cursor is None:
        UserAggregatedResults.clean_all()
    else:
        query = query.with_cursor(cursor)
    users = query.fetch(10)
    if len(users) == 0:
        return
    for user in users:
        uar = UserAggregatedResults.get_or_create(user.username)
        uar.update_all()
    cursor = query.cursor()
    deferred.defer(deferred_run_site_backup, 1, cursor)

class RunSiteBackup(_base.Handler):
    def get(self):
        deferred.defer(deferred_run_site_backup)
        return

def run_deferred_migrate_all_orphans_all_users():
    deferred.defer(deferred_migrate_all_orphans_all_users)

def deferred_migrate_all_orphans_all_users(stage = 0):
    if stage == 0:
        u = models2.User.all().filter('migration', None).get()
        if u is None:
            u = models2.User.all().filter('migration', 0).get()
            if u is None:
                logging.info("Done with migrate1 all users.")
                deferred.defer(deferred_migrate_all_orphans_all_users, 1)
                return
        logging.info("Doing migrate1 on %s..."%u.username)
        u.migrate1()
        deferred.defer(deferred_migrate_all_orphans_all_users, 0)
        return
    elif stage == 1:
        u = models2.User.all().filter('migration', 1).get()
        if u is None:
            u = models2.User.all().filter('migration', 2).get()
        if u is None:
            logging.info("Done with orphan migration.")
            deferred.defer(deferred_migrate_all_orphans_all_users, 2)
            return
        logging.info("Doing migrate2 on %s..."%u.username)
        migrate_all_orphans(u)
        u.migration = 3
        u.put()
        deferred.defer(deferred_migrate_all_orphans_all_users, 1)
        return
    elif stage == 2:
        deferred.defer(build_session_aggregates_deferred)
        return

def migrate_all_orphans(user):
    sessions = []
    exps = set()
    while True:
        s = entities.experiment.migrate_orphans(user)
        if s is None:
            logging.info("Finished migrating sessions for %s."%user.username)
            break
        logging.info("Migrated session from %s for %s."%(str(s.start_time), user.username))
        sessions.append(s)
        exps.add(s.experiment.key().id())
    if len(exps) == 0:
        return
    #experiments = entities.experiment.Experiment.all().filter('username', user.username).filter('is_daddy', True).fetch(100)
    #for exp_id in [x.key().id() for x in experiments]:
    for exp_id in exps:
        experiment = entities.experiment.Experiment.get_by_id(exp_id)
        experiment.reset_variants_done()

def fix_all_sessions_to_have_experiments(cursor = None):
    q = models2.Session2.all()
    if cursor is not None:
        q = q.with_cursor(cursor)
    sessions = q.fetch(100)
    if len(sessions) == 0:
        logging.info("Done fixing all sessions.")
        return
    for sess in sessions:
        if not sess.experiment:
            user = sess.user
            if user.migration > 1:
                logging.info("Restore migration for %s from %d to 1."%(user.username, user.migration))
                user.migration = 1
                user.put()
            sess.experiment = None
            sess.put()
            logging.info("Fixed session for user %s."%(sess.user.username))
    cursor = q.cursor()
    deferred.defer(fix_all_sessions_to_have_experiments, cursor)

def test_general_aggregator(agg):
    # verify that all chunks exist
    for chunkk in agg.chunks:
        if db.get(chunkk) is None:
            logging.error("Chunks are None for agg_type=%s, agg_id=%s"%(agg.agg_type, agg.agg_id))
            return False
    if not agg.verify_checksums():
        logging.error("Bad checksum for agg_type=%s, agg_id=%s"%(agg.agg_type, agg.agg_id))
        return False
    return True

def deferred_test_all_aggregators(cursor = None):
    query = download_restore.GeneralAggregator.all()
    if cursor is not None:
        query = query.with_cursor(cursor)
    agg = query.fetch(1)
    next_cursor = query.cursor()
    if len(agg) == 0:
        return
    agg = agg[0]
    tr = test_general_aggregator(agg)
    name = "(%s, %s)"%(agg.agg_type, agg.agg_id)
    if tr:
        logging.info("Aggregator %s tests fine"%name)
    else:
        logging.info("Errors found in aggregator %s"%name)
    deferred.defer(deferred_test_all_aggregators, next_cursor)
    return

def deferred_clean_all_aggregators():
    agg = download_restore.GeneralAggregator.all().fetch(10)
    if len(agg) == 0:
        chunks = download_restore.DataChunk.all().fetch(10)
        if len(chunks) == 0:
            return
        db.delete(chunks)
        deferred.defer(deferred_clean_all_aggregators)
        return
    else:
        db.delete(agg)
        deferred.defer(deferred_clean_all_aggregators)
        return
