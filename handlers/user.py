#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
from django.utils import simplejson as json
import _base
import models2
import entities.experiment
from google.appengine.api import users, mail
from google.appengine.ext import webapp, db
try:
  from pytz.gae import pytz
except:
  pass

class Unsubscribe(_base.Handler):
  @_base.require_login
  def get(self, key_id):
    user = models2.User.get_current()
    if str(user.key().id()) != str(key_id):
      return
    user.receive_emails = False
    user.success_message = "Unsubscribed from emails."
    user.put()
    self.redirect('/')

class Stats(_base.Handler):
  @_base.require_login
  def get(self):
    context = {
      'user':models.User.get_or_create_current(),
      'active': 'my_stats'
    }
    self.render('stats.html', context)

class Signup(_base.Handler):
  def get(self):
    google_account = users.get_current_user()
    if google_account:
      user = models2.User.all().filter('google_account', google_account).get()
      if user:
        self.redirect('/')
      else:
        context = {
          'google_account': google_account,
          'timezone_names': pytz.common_timezones,
          'current_timezone': 'UTC',
        }
        self.render('signup.html', context)
    else:
      self.redirect(users.create_login_url(self.request.uri))

  def post(self):
    google_account = users.get_current_user()
    context = {'google_account': google_account}
    if google_account:
      username = self.request.get("username")
      age = self.request.get("age")
      gender = self.request.get("gender")
      education = self.request.get("education")
      timezone = self.request.get("timezone")
      valres = validate_user_info(None, username, age, gender, education, timezone)
      if not valres[0]:
        context['message'] = valres[1]
        context['timezone_names'] = pytz.common_timezones
        context['current_timezone'] = 'UTC'
        self.render('signup.html', context)
        return
      user = valres[1]
      user.google_account = google_account
      user.migration = 3
      user.put()
      send_welcome_email(user)
          # DO NOT add default 
          #experiments = entities.experiment.Experiment.get_autoadd_experiments()
          #for experiment in experiments:
          #  new_experiment = experiment.clone(user.username)
          #  new_experiment.put()
          # if mental_variables:
          #   user_settings = models2.UserSettings(user=user, show_statistics=False, show_feedback=False, mental_variables=mental_variables, mental_states=mental_states)
          #   user_settings.put()
      redirect_url = self.request.get('redirect_url') or '/'
      redirect_url += ('&' if '?' in redirect_url else '?')
      redirect_url += 'welcome_to_quantified_mind=yes'
      if redirect_url:
        redirect_url = str(redirect_url)
        self.redirect(redirect_url) 
      else:
        self.redirect('/')
      return
    else:
      self.redirect(users.create_login_url(self.request.uri))

def validate_user_info(user, username, age, gender, education, timezone):
  if user is None:
    if not models2.User.is_valid_username(username):
      return (False, 'User name must be 3-20 characters long and contain only letters, digits and underscores.')
    u2 = models2.User.all().filter('username', username).get()
    if u2:
      return (False, 'Username %s already taken. Please choose another.'%username)
  if age == "":
    age = None
  else:
    if age.isdigit():
      try:
        age = int(age)
        if (age < 1) or (age > 200):
          return (False, "Please enter a valid age, or leave this field empty.")
      except:
        return (False, "Age must be an integer.")
    else:
      return (False, "Age may only contain digits.")
  if user is None:
    user = models2.User(username = username)
  user.age = age
  user.gender = gender
  user.education = education
  user.timezone = timezone
  return (True, user)

def send_welcome_email(user):
  destination_email = user.google_account.email()
  mail.send_mail(
    sender='lab@quantified-mind.com',
    # When ready, uncomment the next line and comment the one below it
    to=user.google_account.email(),
    #bcc='yonidonner@gmail.com',
    #to='lab@quantified-mind.com',
    subject='Welcome to Quantified Mind',
    body="""Welcome to Quantified Mind, %s!

We're glad you joined us. Quantified Mind is a site for you to measure your cognitive performance and experiment with what makes you smarter. Coffee? Meditation? Provigil? Time of day? You can track your mind over time to see how well it's working, and run experiments if you like.

To get started, log in at http://www.quantified-mind.com, or visit our About page (http://www.quantified-mind.com/about) where you can learn more about the project and look at the studies we're currently running. If you've already joined a study, keep going! The more tests you take, the more you'll learn about yourself - all the while contributing to scientific discoveries.

We hope this helps you to optimize your life and be your best self!

Yoni, Alex, Larissa, Stephen, and the rest of the Quantified Mind team
@QuantifiedMind
-----
You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.
"""%(user.username),
    html="""<html><head></head><body>
<p>Welcome to Quantified Mind, %s!</p>

<p>We're glad you joined us. <a href="http://www.quantified-mind.com">Quantified Mind</a> is a site for you to measure your cognitive performance and experiment with what makes you smarter. Coffee? Meditation? Provigil? Time of day? You can track your mind over time to see how well it's working, and run experiments if you like.</p>

<p>To get started, <a href="http://www.quantified-mind.com">log in to Quantified Mind</a>, or visit our <a href="http://www.quantified-mind.com/about">About</a> page where you can learn more about the project and look at the studies we're currently running. If you've already joined a study, keep going! The more tests you take, the more you'll learn about yourself - all the while contributing to scientific discoveries.</p>
<p>We hope this helps you to optimize your life and be your best self!</p>

<p>Yoni, Alex, Larissa, Stephen, and the rest of the Quantified Mind team</p>
<p>@QuantifiedMind</p>
<p>-----<br />You can update your email preferences <a href="http://www.quantified-mind.com/settings">here</a>.</p>
</body></html>
"""%(user.username),
    reply_to='lab@quantified-mind.com')
  return

class GetAuthenticationToken(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    self.response.out.write(json.dumps({'username': user.username,
                                        'token': user.get_authentication_token()}))
    return


class ForwardAuthenticationToken(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    context = {}
    context["service_name"] = self.request.get("service_name")
    context["redirect_url"] = self.request.get("redirect_url")
    context["username"] = user.username
    context["token"] = user.get_authentication_token()
    self.render('forward_token.html', context)

class SetUsermap(_base.Handler):
  @_base.require_admin
  def get(self, username = None):
    if username is None:
      db.delete(models2.UserMap.all())
      self.redirect('/')
      return
    user = models2.User.get_current()
    um = models2.UserMap.all().filter('from_username', user.username).get()
    if not um:
      um = models2.UserMap(from_username = user.username)
    um.to_username = username
    um.put()
    self.redirect('/')
    return
