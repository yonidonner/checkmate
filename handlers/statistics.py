from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import files
import _base
import logging
import string
import datetime
import models2
import results
import random
#import statistics
import pickle
import base64
import entities.mental_variables
import entities.experiment
from handlers import aggregates

class WhatsMyUsername(_base.Handler):
    @_base.require_login
    def get(self):
        user = models2.User.get_current()
        return self.render('whats_my_username.html', {'user': user, 'username': user.username})

class ViewResults(_base.Handler):
    def show_by_query(self, query, cursors = None):
        user = models2.User.get_current()
        query_limit = 100
        results = query.fetch(query_limit)
        cursor = query.cursor()
        if cursors is None:
            prv_cursors = []
        else:
            prv_cursors = pickle.loads(base64.decodestring(cursors))
        if len(prv_cursors) > 0:
            prv_cursor = base64.encodestring(pickle.dumps(prv_cursors[:-1]))
        else:
            prv_cursor = None
        if query.get():
            next_cursor = base64.encodestring(pickle.dumps(prv_cursors + [cursor]))
        else:
            next_cursor = None
            prv_cursors.append(cursor)
        if len(results) > query_limit:
            results = results[:query_limit]
        context = {'user': user,
                   'sessions': results,
                   'next_cursor': next_cursor,
                   'prv_cursor': prv_cursor,
                   'active': 'results'}
        self.render('statistics2.html', context)

    @_base.require_login
    def get(self, command = None, key_id = None):
        user = models2.User.get_current()
        query = models2.Session2.all().filter('user', user).filter('active', False).order('-start_time')
        if command is None:
            return self.get("by_experiment", "any")
        if command == "by_experiment":
            if key_id != 'any':
                try:
                    experiment = entities.experiment.Experiment.get_by_id(int(key_id))
                except:
                    experiment = None
                if experiment is not None:
                    query = query.filter('experiment', experiment)
            return self.show_by_query(query)

class ViewSession(_base.Handler):
    @_base.require_login
    def get(self, coming_from, session_id):
        user = models2.User.get_current()
        try:
            sess = models2.Session2.get_by_id(int(session_id))
            if sess.user.key().id() != user.key().id():
                sess = None
        except:
            sess = None
        if not sess:
            self.redirect('/lab')
            return
        finished_tests = models2.CompletedTest.all().filter('test_session', sess).fetch(1000)
        finished_tests.sort(key = lambda x: x.end_time)
        last_n = [1,5,20,-1]
        last_results = aggregates.AggregateSessionResults.last_n_score_statistics_for_user(user.username, [(ft.variant_name, ft.variant_group) for ft in finished_tests], last_n)
        n_dict = {-1: 'All previous sessions', 1: 'Last session'}
        last_n_str = [n_dict.get(x, 'Last %d sessions'%x) for x in last_n]
        if last_results is not None:
            last_results = [{'finished_test': finished_test, 'score': finished_test.get_score(), 'last_n': last_results[i]} for (i,finished_test) in enumerate(finished_tests) if finished_test is not None]
        active = None
        if coming_from == 'lab':
            active = 'tests'
        else:
            active = 'results'
        context = {'user': user,
                   'test_session': sess,
                   'coming_from': coming_from,
                   'active': active,
                   'last_results': last_results,
                   'last_n_str': last_n_str,
                   'completed_tests': finished_tests}
        self.render("view_session.html", context)
