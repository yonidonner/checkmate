#from google.appengine.dist import use_library
#use_library('django', '1.2')

import os
import _base
import models2
import results
import entities.experiment
import logging
import datetime
import json
import time
import random
import md5
import urllib
from handlers import aggregates, util
from google.appengine.api import urlfetch
from google.appengine.api import users
from google.appengine.ext import webapp, db
from google.appengine.ext import deferred

class ExplainResult(_base.Handler):
  @_base.require_login
  def get(self, result_name):
    self.response.out.write(results.get_result_explanation(result_name))
    return

class DownloadExperimentCSV(_base.Handler):
  @_base.require_login
  def get(self, experiment_id):
    user = models2.User.get_current()
    try:
      exp_id = int(experiment_id)
    except:
      self.redirect('/lab')
      return
    try:
      experiment = entities.experiment.Experiment.get_by_id(exp_id)
    except:
      experiment = None
    if experiment is None:
      user.error_message = "Could not find experiment"
      user.put()
      self.redirect('/lab')
      return
    asr = aggregates.AggregateSessionResults.get_or_create(user.username)
    csv_data, filename = asr.to_csv(experiment.name)
    self.response.headers['content-disposition'] = "attachment; filename=" + str(filename)
    self.response.headers['Content-Type'] = 'text/csv'
    self.response.out.write(csv_data)
    return

class ExperimentLanding(_base.Handler):
  def get(self, experiment_title):
    user = models2.User.get_current()
    experiment = None
    if user is not None:
      # try loading the experiment for the user
      experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('landing_page', experiment_title).filter('deleted',False).get()
    if experiment is None:
      experiment = entities.experiment.Experiment.all().filter('username', None).filter('landing_page', experiment_title).get()
    context = {'user': user,
               'experiment': experiment,
               'landing_page': experiment_title,
               'landing_page_html': 'experiments/%s.html'%experiment_title,
               'active': 'home'}
    if (experiment is not None) and (experiment.choose_start_date):
      context['experiment_start_date'] = datetime.datetime.utcnow().strftime("%m/%d/%Y")
    self.render('experiment_landing.html', context)

class QMIQRavensStudy(_base.Handler):
  def get(self, my_id = None):
    if my_id is not None:
      return self.redirect('/lab/anon_tests/experiment_8029/%s'%(my_id))
    random_id = ''.join([random.choice('123456789')] + [random.choice('0123456789') for x in xrange(19)])
    random_id = random_id + md5.md5(random_id + 'QbwOwHfuKT').hexdigest()
    return self.render('qmiq_ravens.html', {'random_id': random_id})

class DoubleBlindedStudy(_base.Handler):
  def get(self, my_id = None):
    if my_id is not None:
      return self.redirect('/lab/anon_tests/doubleblinded_l_theanine_pilot/%s'%(my_id))
    return self.redirect('/')

class RedirectAnonStudy(_base.Handler):
  def get(self, study_name, my_id):
    return self.redirect('/lab/anon_tests/%s/%s'%(study_name, my_id,))

class Lab(_base.Handler):
  def get(self):
    user = models2.User.get_current()
    if user is None:
      joinable_experiments = entities.experiment.Experiment.all().filter('username', None).filter('participant_code', None).filter('featured', True).fetch(1000)
      joinable_experiments.sort(key = lambda x: x.name)
      self.render('lab.html',
                  {'title':'Home',
                   'joinable_experiments': joinable_experiments,
                   'active':'Home'})
      return
    if (user.migration is None) or (user.migration == 0):
      user.migrate1()
    message = None
    error_message = None
    if user.success_message:
      message = user.success_message
      user.success_message = None
      user.put()
    if user.error_message:
      error_message = user.error_message
      user.error_message = None
      user.put()
    test_session = models2.Session2.get_current(user)
    if test_session:
      if test_session.timed_out():
        test_session.finish()
        test_session = None
        if not error_message:
          error_message = 'Automatically ended old session that timed out.'
      self.redirect('/lab/study_session')
      return
    experiments = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted', False).fetch(1000)
    ongoing = []
    finished = []
    for experiment in experiments:
      if experiment.is_ongoing():
        ongoing.append(experiment)
      else:
        finished.append(experiment)
    recent_sessions = models2.Session2.all().filter('user', user).filter('is_complete', True).order('-start_time').fetch(10)

    joinable_experiments = entities.experiment.Experiment.all().filter('username', None).filter('participant_code', None).filter('featured', True).fetch(1000)
    user_experiment_names = set([x.name for x in experiments])
    joinable_experiments = [x for x in joinable_experiments if x.name not in user_experiment_names]
    joinable_experiments.sort(key = lambda x: x.name)

    context = {'user': user,
               'ongoing_experiments': ongoing,
               'finished_experiments': finished,
               'joinable_experiments': joinable_experiments,
               'recent_sessions': recent_sessions,
               'active': 'home',
               'message': message,
               'error_message': error_message}
    self.render('lab.html', context)

class DeleteExperiment(_base.Handler):
  @_base.require_login
  def get(self, experiment_id):
    user = models2.User.get_current()
    try:
      experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
    except:
      self.redirect('/lab')
      return
    if experiment:
      # finish all active sessions that use this experiment
      sess = models2.Session2.all().filter("experiment", experiment).filter("active", True).get()
      if sess:
        sess.finish()
      experiment.deleted = True
      experiment.put()
      #db.delete(experiment)
    self.redirect('/lab')

class NewExperiment(_base.Handler):
  @_base.require_login
  def get(self, from_template = None, command = None, start_date = None):
    user = models2.User.get_current()
    if from_template is None:
      experiment_template = None
    else:
      try:
        experiment_template = entities.experiment.Experiment.get_by_id(int(from_template))
      except:
        self.redirect('/lab')
        return
      #experiment_template = entities.experiment.Experiment.all().filter('username', None).filter('name', from_template).get()
    if experiment_template is None:
      experiments = entities.experiment.Experiment.all().filter('username', None).filter('participant_code', None).filter('featured', True).fetch(1000)
      user_experiments = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted',False).fetch(1000)
      user_experiment_names = set([x.name for x in user_experiments])
      experiments = [x for x in experiments if x.name not in user_experiment_names]
      experiments.sort(key = lambda x: x.name)
      context = {'user': user,
                 'active': 'tests',
                 'experiments': experiments}
      self.render('new_experiment.html', context)
      return
    # create from template
    # check if user already has this experiment
    existing_experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted', False).filter('name', experiment_template.name).get()
    if existing_experiment:
      self.redirect("/lab/take_tests/%d"%(existing_experiment.key().id()))
      return
    experiment = experiment_template.clone(user.username)
    if start_date is not None:
      experiment.start_date = start_date
    experiment.put()
    if (command is None) or (command == "lp"):
      if experiment.upon_join == 1:
        if experiment.landing_page:
          redirect_url = "/experiment/%s"%experiment.landing_page
          redirect_url += '?welcome_to_quantified_mind=yes'
          self.redirect(redirect_url)
        else:
          self.redirect("/lab")
        return
      elif experiment.upon_join == 2:
        redirect_url = "/lab/take_tests/%d"%(experiment.key().id())
        redirect_url += '?welcome_to_quantified_mind=yes'
        self.redirect(redirect_url)
        return
      elif experiment.upon_join == 3:
        if command is None:
          if experiment.landing_page:
            redirect_url = "/experiment/%s"%experiment.landing_page
            redirect_url += '?welcome_to_quantified_mind=yes'
            self.redirect(redirect_url)
          else:
            self.redirect("/lab")
        elif command == "lp":
          redirect_url = "/lab/take_tests/%d"%(experiment.key().id())
          redirect_url += '?welcome_to_quantified_mind=yes'
          self.redirect(redirect_url)
        return
    elif command == "customize":
      self.redirect("/lab/customize_experiment/%s/first"%experiment.key().id())
      return

  @_base.require_login
  def post(self, from_template = None, command = None):
    if from_template is not None:
      if command == "lp":
        start_date = self.request.get("start_date")
        if start_date:
          try:
            parsed_start_date = datetime.datetime.strptime(start_date, "%m/%d/%Y")
            return self.get(from_template, command, parsed_start_date)
          except ValueError:
            pass
    user = models2.User.get_current()
    participant_code = self.request.get("code")
    if not participant_code:
      self.redirect("/lab")
      return
    experiment = entities.experiment.Experiment.all().filter('username', None).filter('participant_code', participant_code).get()
    if not experiment:
      user.error_message = 'Participant code not found.'
      user.put()
      self.redirect('/lab')
      return
    user_experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted', False).filter('participant_code', participant_code).get()
    if user_experiment:
      user.error_message = "You are already participating using code <strong>%s</strong>."%participant_code
      user.put()
      self.redirect('/lab')
      return
    new_experiment = experiment.clone(user.username)
    new_experiment.put()
    if experiment.landing_page:
      self.redirect("/experiment/%s"%experiment.landing_page)
      return
    user.success_message = "Successfully joined experiment <strong>%s</strong> using participant code <strong>%s</strong>."%(experiment.name, participant_code)
    user.put()
    self.redirect('/lab')
    return

class SetExperimentSubjectID(_base.Handler):
  @_base.require_login
  def post(self, experiment_id):
    user = models2.User.get_current()
    experiment = entities.experiment.Experiment.from_id(experiment_id, user)
    if not experiment or experiment.subject_id:
      self.redirect('/lab')
      return
    experiment.subject_id = self.request.get('subject_id')
    experiment.put()
    self.redirect('/experiment/{}?{}'.format(
      experiment.landing_page, 'welcome_to_quantified_mind=yes'))
    return

class CreateExperiment(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    prefix = "New Experiment"
    suffix = None
    # find an available experiment name
    while True:
      full_name = '%s%s'%(prefix, '' if suffix is None else ' %d'%suffix)
      experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('deleted', False).filter('name', full_name).get()
      if experiment is None:
        break
      if suffix is None:
        suffix = 1
      else:
        suffix += 1
    entities.mental_variables.MentalVariable.clean_user(user.username)
    default_mental_variables = entities.mental_variables.MentalVariable.all().filter('creator_username', user.username).filter("add_by_default", True).order("-create_date").fetch(1000)
    mental_variables = []
    mental_variable_names = set()
    for dmv in default_mental_variables:
      if dmv.variable_name not in mental_variable_names:
        #mental_variable_names.add(dmv.variable_name)
        new_mental_variable = dmv.clone(dmv, user.username)
        new_mental_variable.put()
        mental_variables.append(new_mental_variable.key())
    mental_variables.reverse()
    experiment = entities.experiment.Experiment(username = user.username,
                                                name = full_name,
                                                active = True,
                                                description = '',
                                                participant_code = None,
                                                battery_focus = 'Special',
                                                battery_name = 'coffee',
                                                total_sessions = 20,
                                                session_duration = 15,
                                                mental_variables = mental_variables)
    experiment.put()
    self.redirect('/lab/customize_experiment/%s/first'%experiment.key().id())
    return

class CustomizeExperiment(_base.Handler):
  @_base.require_login
  def get(self, experiment_id, first = None):
    user = models2.User.get_current()
    try:
      experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
    except:
      self.redirect('/lab')
      return
    #experiment = entities.experiment.Experiment.all().filter('username', user.username).filter('name', experiment_name).get()
    if experiment is None:
      self.redirect("/lab")
      return
    batteries = list(models2.Battery.all().filter('enabled', True))
    batteries.sort(key = lambda x: x.estimated_duration)
    batteries2 = [{'id': i,
                   'battery': batteries[i]}
                  for i in xrange(len(batteries))]
    foci = sorted(list(set([battery.focus for battery in batteries])))
    mental_variables = entities.mental_variables.MentalVariable.get(experiment.mental_variables)
    context = {'user': user,
               'experiment': experiment,
               'active': 'tests',
               'foci': foci,
               'first': first,
               'mental_variables': mental_variables,
               'batteries': batteries2}
    self.render('create_experiment.html', context)
    return

  @_base.require_login
  def post(self, experiment_id):
    user = models2.User.get_current()
    try:
      experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
    except:
      self.redirect('/lab')
      return
    experiment_name = self.request.get("name")
    #experiment = entities.experiment.Experiment().all().filter('username', user.username).filter('name', original_experiment_name).get()
    original_experiment_name = experiment.name
    errors = {}
    if (experiment_name != original_experiment_name):
      new_experiment = entities.experiment.Experiment().all().filter('username', user.username).filter('deleted', False).filter('name', experiment_name).get()
      if new_experiment:
        errors['experiment_name'] = 'You already have an experiment named %s'%experiment_name
      else:
        if entities.experiment.Experiment.is_valid_name(experiment_name):
          experiment.name = experiment_name
        else:
          errors['experiment_name'] = 'Name contains invalid characters.'
    # check if experiment already exists
    experiment_description = self.request.get("description")
    if entities.experiment.Experiment.is_valid_description(experiment_description):
      experiment.description = experiment_description
    else:
      errors['experiment_description'] = 'Description contains invalid characters.'
    total_sessions = self.request.get("total_sessions")
    try:
      total_sessions = int(total_sessions)
      if total_sessions < 1:
        errors['total_sessions'] = 'Total sessions must be a positive integer'
        total_sessions = None
      else:
        experiment.total_sessions = total_sessions
    except:
      total_sessions = None
      errors['total_sessions'] = 'Total sessions must be a number'
    focus = self.request.get("focus")
    battery_name = self.request.get("battery")
    experiment.battery_focus = focus
    experiment.battery_name = battery_name
    battery = models2.Battery.all().filter('name', experiment.battery_name).get()
    if battery:
      experiment.session_duration = battery.estimated_duration
    mental_variables = []
    for mental_variable_i in xrange(100):
      prefix = "mental_variable_%d"%mental_variable_i
      mental_variable = self.request.get(prefix)
      if mental_variable:
        add_default = self.request.get("%s_save_default"%prefix)
        if add_default and add_default == "save_default":
          add_by_default = True
        else:
          add_by_default = False
        mental_variable_type = self.request.get("%s_type"%prefix)
        if mental_variable_type == "discrete":
          mental_variable_states = self.request.get("%s_states"%prefix)
          mental_variable_default = self.request.get("%s_default"%prefix)
          states = [x.strip() for x in mental_variable_states.split(",")]
          valid_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_- !?'
          if len(states) < 2:
            errors['discrete_mental_variable_states'] = 'Discrete mental variables should have at least two states.' #works
            #doesn't work if you add a new mental variable, and then set it as the default state in a later edit/the same edit
            #you'd have to add it in first - update the states within the error checking
            #check the states first, and then the default
          if mental_variable_default not in states:
            errors['discrete_mental_variable_default_state'] = 'The default state of a discrete mental variable must be one of its states.'
#checking to see if each character is valid. not sure the methods are correct
          for mental_variable_state in states:
            for x in mental_variable_state:
              if x not in valid_chars:
                errors['discrete_mental_variable_states'] = 'Mental variable states an only contain A-Z, a-z, 0-9, _, -, !, and ?'
          new_mental_variable = entities.mental_variables.DiscreteMentalVariable(creator_username = user.username, variable_name = mental_variable, variable_type = entities.mental_variables.MV_DISCRETE, states = states, default_state = mental_variable_default, add_by_default = add_by_default)
          new_mental_variable.put()
          mental_variables.append(new_mental_variable.key())
        elif mental_variable_type == "likert":
          mental_variable_values = self.request.get("%s_values"%prefix)
          try:
            mental_variable_default = int(self.request.get("%s_default"%prefix))
          except:
            mental_variable_default = None
            errors['likert_mental_variable_default_states'] = 'The default state of a likert mental variable must be an integer.'
          try:
            likert_values = int(mental_variable_values)
            if likert_values < 3:
              errors['likert_mental_variable_states'] = 'Likert range must be greater than 2.'
            if mental_variable_default < 1:
              errors['likert_mental_variable_default_state'] = 'The default state of a likert mental variable must be one of its states.'
            if mental_variable_default > likert_values:
              errors['likert_mental_variable_default_state'] = 'The default state of a likert mental variable must be one of its states.'
          except:
            likert_values = None
            errors['likert_mental_variable_states'] = 'The likert value must be an integer.'
          new_mental_variable = entities.mental_variables.LikertMentalVariable(creator_username = user.username, variable_name = mental_variable, variable_type = entities.mental_variables.MV_LIKERT, likert_values = likert_values, default = mental_variable_default, add_by_default = add_by_default)
          new_mental_variable.put()
          mental_variables.append(new_mental_variable.key())
        elif mental_variable_type == "numeric":
          try:
            mental_variable_minimum = float(self.request.get("%s_minimum"%prefix))
          except:
            mental_variable_minimum = None
            errors['numeric_mental_variable_states'] = 'The minimum numeric mental state must be a float.'
          try:
            mental_variable_maximum = float(self.request.get("%s_maximum"%prefix))
          except:
            mental_variable_maximum = None
            errors['numeric_mental_variable_states'] = 'The maximum numeric mental state must be a float.'
          try:
            mental_variable_default = float(self.request.get("%s_default"%prefix))
          except:
            mental_variable_minimum = None
            errors['numeric_mental_variable_default_state'] = 'The default numeric mental state must be a float.'
          if mental_variable_default > mental_variable_maximum:
            errors['numeric_mental_variable_default_state'] = 'The default mental variable must be between the minimum and maximum.'
          if mental_variable_default < mental_variable_minimum:
            errors['numeric_mental_variable_default_state'] = 'The default mental variable must be between the minimum and maximum.'
          new_mental_variable = entities.mental_variables.NumericMentalVariable(creator_username = user.username, variable_name = mental_variable, variable_type = entities.mental_variables.MV_NUMERIC, min_value = mental_variable_minimum, max_value = mental_variable_maximum, default = mental_variable_default, add_by_default = add_by_default)
          new_mental_variable.put()
          mental_variables.append(new_mental_variable.key())
        elif mental_variable_type == "text":
          new_mental_variable = entities.mental_variables.TextMentalVariable(creator_username = user.username, variable_name = mental_variable, variable_type = entities.mental_variables.MV_TEXT, add_by_default = add_by_default)
          new_mental_variable.put()
          mental_variables.append(new_mental_variable.key())
    experiment.mental_variables = mental_variables
    if len(errors) == 0:
      experiment.put()
      user.success_message = "Experiment <strong>%s</strong> updated successfully."%experiment_name
      user.put()
      self.redirect('/lab')
      return
    else:
      experiment = db.get(experiment.key())
    batteries = list(models2.Battery.all().filter('enabled', True))
    batteries.sort(key = lambda x: x.estimated_duration)
    batteries2 = [{'id': i,
                   'battery': batteries[i]}
                  for i in xrange(len(batteries))]
    foci = sorted(list(set([battery.focus for battery in batteries])))
    template_variables = entities.mental_variables.MentalVariable.get(experiment.mental_variables)
    entities.mental_variables.MentalVariable.clean_user(user.username)
    context = {'user': user,
               'experiment': experiment,
               'active': 'tests',
               'errors': errors,
               'foci': foci,
               'mental_variables': template_variables,
               'batteries': batteries2}
    self.render('create_experiment.html', context)

class TakeTests(_base.Handler):
  @_base.require_login
  def get(self, experiment_id):
    user = models2.User.get_current()
    # delete all active sessions
    while True:
      existing_session = models2.Session2.all().filter('user', user).filter('active', True).get()
      if not existing_session:
        break
      db.delete(existing_session)
    try:
      experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
    except:
      self.redirect('/lab')
      return
    if experiment is None:
      self.redirect('/lab')
      return
    test_battery = models2.Battery.all().filter('name', experiment.battery_name).get()
    if not test_battery:
      self.redirect('/lab')
      return
    user_agent = self.request.headers.get('User-Agent', None)
    test_session = test_battery.new_session(user)
    test_session.user_agent = user_agent
    test_session.timezone = user.timezone
    test_session.experiment = experiment
    test_session.n_completed_tests = 0
    test_session.active = True
    test_session.is_complete = False
    test_session.put()
    #logging.info("PUT THIS: %s"%test_session.__dict__)
    self.redirect('/lab/update_session_variables')

class CancelSession(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if test_session:
      db.delete(test_session)
    self.redirect('/lab')

class CancelAnonSession(_base.Handler):
  def get(self, session_id):
    try:
      test_session = models2.Session2.get_by_id(int(session_id))
    except:
      self.render("error.html", {'error': 'Bad session ID: %s'%(session_id,)})
      return
    if test_session:
      callback_url = str(test_session.abort_url)
      db.delete(test_session)
      if not callback_url:
        callback_url = '/'
      self.redirect(callback_url)
      return
    else:
      self.redirect('/lab')

class EndSession(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if test_session:
      test_session.finish()
      test_session.update_test_information()
      if test_session.n_completed_tests > 0:
        test_session.is_complete = True
        test_session.put()
        update_at_end_of_session(test_session)
    self.redirect('/lab')

class ConfirmResult(_base.Handler):
  @_base.require_login
  def get(self, result_id):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    try:
      result = models2.CompletedTest.get_by_id(int(result_id))
    except:
      result = None
    if result:
      if result.is_tentative:
        result.is_tentative = False
        result.put()
        if not result.is_practice:
          test_session.n_completed_tests += 1
          if result.start_time is None:
            result.start_time = test_session.start_time
            result.put()
          test_session.cumulative_duration += (result.end_time - result.start_time).seconds
          test_session.put()
    self.redirect('/lab/study_session')

class ConfirmAnonResult(_base.Handler):
  def get(self, result_id):
    try:
      result = models2.CompletedTest.get_by_id(int(result_id))
    except:
      self.render('error.html', {'error': 'Bad result ID: %s'%(result_id,)})
      return
    if result is None:
      self.render('error.html', {'error': 'Could not find result for ID: %s'%(result_id,)})
      return
    test_session = result.test_session
    if result.is_tentative:
      result.is_tentative = False
      result.put()
      if not result.is_practice:
        test_session.n_completed_tests += 1
        if result.start_time is None:
          result.start_time = test_session.start_time
          result.put()
        test_session.cumulative_duration += (result.end_time - result.start_time).seconds
        test_session.put()
    self.redirect('/lab/study_session/%d'%(test_session.key().id(),))

class StudySession(_base.Handler):
  @_base.require_login
  def get(self):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if not test_session:
      self.redirect('/lab')
      return
    experiment = test_session.experiment
    session_information = test_session.session_information
    mental_states = [{'variable': x,
                      'state': y}
                     for (x,y) in zip(session_information.mental_variables, session_information.mental_states)]
    last_results = None
    last_n_str = None
    completed_tests = None
    if test_session:
      time_ago = (datetime.datetime.utcnow() - test_session.start_time).seconds // 60
      timeout_left = (test_session.timeout_time - datetime.datetime.utcnow()).seconds // 60
      n_finished = test_session.n_completed_tests
      serial_tests = test_session.get_serial_tests()
      if serial_tests:
        is_serial = True
        finished_tests = serial_tests[:n_finished]
        remaining_tests = serial_tests[n_finished:]
        if len(remaining_tests):
          next_test = remaining_tests[0]
        else:
          if test_session.active:
            test_session.is_complete = True
            test_session.active = False
            test_session.end_time = datetime.datetime.utcnow()
            test_session.update_test_information()
            test_session.put()
            next_test = None
            finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
            finished_tests.sort(key = lambda x: x.end_time)
            last_n = [1,5,20,-1]
            last_results = aggregates.AggregateSessionResults.last_n_score_statistics_for_user(user.username, [(ft.variant_name, ft.variant_group) for ft in finished_tests], last_n)
            n_dict = {-1: 'All previous sessions', 1: 'Last session'}
            last_n_str = [n_dict.get(x, 'Last %d sessions'%x) for x in last_n]
            if last_results is not None:
              last_results = [{'finished_test': finished_test, 'score': finished_test.get_score(), 'last_n': last_results[i]} for (i,finished_test) in enumerate(finished_tests) if finished_test is not None]
            update_at_end_of_session(test_session)
            completed_tests = finished_tests
            finished_tests = [x.variant() for x in finished_tests]
            if test_session.callback_url:
              finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
              finished_tests.sort(key = lambda x: x.end_time)
              session_results = []
              for i in xrange(len(finished_tests)):
                stats = finished_tests[i].get_statistics()
                for stat in stats:
                  session_results.append({'name': '%s_%s'%(finished_tests[i].variant_name, stat['name'].lower().replace(" ","_").replace(",","")),
                                  'value': stat['value']})
              context = {'callback_url': test_session.callback_url,
                         'results': session_results}
              self.render('callback.html', context)
              return
      else:
        is_serial = False
        finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
        finished_tests.sort(key = lambda x: x.end_time)
        stats = [x.get_statistics() for x in finished_tests]
        remaining_tests = test_session.get_available_tests()
        completed_tests = finished_tests
        finished_tests = [x.variant() for x in finished_tests]
        next_test = None
      context = {'user': user,
                 'test_session': test_session,
                 'is_serial': is_serial,
                 'active': 'tests',
                 'finished_tests': finished_tests,
                 'completed_tests': completed_tests,
                 'last_results': last_results,
                 'last_n_str': last_n_str,
                 'remaining_tests': remaining_tests,
                 'next_test': next_test,
                 'started_ago': time_ago,
                 'timeout_left': timeout_left,
                 'experiment': experiment,
                 'mental_states': mental_states}
      if experiment and experiment.landing_page:
        context['next_step'] = 'experiments/%s.html'%experiment.landing_page
      self.render('study_session.html', context)
      return

class StudySessionUpdateMVs(_base.Handler):
  @_base.require_login
  def get(self, box = None):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if not test_session:
      self.redirect('/lab')
      return
    experiment = test_session.experiment
    if not experiment:
      self.redirect('/lab')
      db.delete(test_session)
      return
    #if experiment.participant_code:
    if not experiment.allow_unspecified_mvs:
      allow_ignore = False
    else:
      allow_ignore = True
    mental_variables = [db.get(x) for x in experiment.mental_variables]
    if test_session:
      if len(mental_variables) == 0:
        session_information = models2.SessionInformation()
        session_information.put()
        test_session.session_information = session_information
        test_session.put()
        self.redirect('/lab/study_session')
        return
      mental_states = [None] * len(mental_variables)
      mv_enabled = [False] * len(mental_variables)
      if (test_session.session_information is not None) and len(test_session.session_information.mental_states) == len(mental_variables):
        previous_states = test_session.session_information.mental_states
      else:
        previous_states = None
      for i in xrange(len(mental_variables)):
        if mental_variables[i].type_str() == 'discrete':
          if (previous_states is None) or (previous_states[i] == ''):
            mental_states[i] = 'none_selected'
            #mental_states[i] = mental_variables[i].default_state
          else:
            mental_states[i] = previous_states[i]
            mv_enabled[i] = True
        elif mental_variables[i].type_str() == 'likert':
          if (previous_states is None) or (previous_states[i] == ''):
            mental_states[i] = mental_variables[i].default
          else:
            mental_states[i] = int(previous_states[i])
            mv_enabled[i] = True
        elif mental_variables[i].type_str() == 'numeric':
          if (previous_states is None) or (previous_states[i] == ''):
            mental_states[i] = mental_variables[i].default
          else:
            mental_states[i] = float(previous_states[i])
            mv_enabled[i] = True
        elif mental_variables[i].type_str() == 'text':
          if (previous_states is None) or (previous_states[i] == ''):
            mental_states[i] = ''
          else:
            mental_states[i] = previous_states[i]
            mv_enabled[i] = True
      context = {'user': user,
                 'test_session': test_session,
                 'active': 'tests',
                 'allow_ignore': allow_ignore,
                 'experiment': experiment,
                 'mental_variables': [{'id': i,
                                       'mv': mental_variables[i],
                                       'state': mental_states[i],
                                       'enabled': mv_enabled[i]}
                                      for i in xrange(len(mental_variables))],
                 }
      if (box is not None) and (box == "box"):
        context['box'] = True
        self.render("mental_variable_box.html", context)
        return
      if experiment.landing_page:
        context['protocol_url'] = 'experiments/%s.html'%experiment.landing_page
      self.render('study_session_mv.html', context)
      return
    else:
      self.redirect('/lab')
      return

  @_base.require_login
  def post(self, box = None):
    user = models2.User.get_current()
    test_session = models2.Session2.get_current(user)
    if not test_session:
      self.redirect('/lab')
      return
    experiment = test_session.experiment
    if not experiment.allow_unspecified_mvs:
      allow_ignore = False
    else:
      allow_ignore = True
    mental_variables = [db.get(x) for x in experiment.mental_variables]
    mental_states = [None] * len(mental_variables)
    if test_session.session_information is None:
      test_session.session_information = models2.SessionInformation()
      test_session.put()
    test_session.session_information.mental_variables = [x.variable_name for x in mental_variables]
    for mv_i in xrange(len(mental_variables)):
      if allow_ignore:
        mv_enabled = self.request.get("mental_variable_%d_ignore"%mv_i)
      else:
        mv_enabled = ''
      if mv_enabled and mv_enabled == 'ignore':
        mental_state = ''
      elif mental_variables[mv_i].type_str() == 'discrete':
        state_i = self.request.get("mental_variable_%d"%mv_i)
        if state_i == 'none_selected':
          self.redirect('/lab/update_session_variables')
          return
        mental_state = mental_variables[mv_i].states[int(state_i)]
      else:
        mental_state = self.request.get("mental_variable_%d"%mv_i)
      mental_states[mv_i] = mental_state
    test_session.session_information.mental_states = mental_states
    test_session.session_information.put()
    if box and (box == "box"):
      context = {'mental_states': [{'variable': mental_variables[i].variable_name,
                                       'state': mental_states[i]}
                                      for i in xrange(len(mental_variables))],
                 }
      self.render("study_session_mvs.html", context)
      return
    self.redirect('/lab/study_session')
    return

class ExtendExperiment(_base.Handler):
  @_base.require_login
  def post(self, experiment_id):
    user = models2.User.get_current()
    experiment = entities.experiment.Experiment.from_id(experiment_id, user)
    if not experiment:
      self.redirect('/lab')
      return
    extend_by = self.request.get("extend_by")
    try:
      extend_by = int(extend_by)
    except:
      extend_by = None
    if not (extend_by > 0):
      extend_by = None
    if extend_by is None:
      self.redirect('/lab')
      return
    experiment.total_sessions += extend_by
    experiment.put()
    user.success_message = 'Experiment <strong>%s</strong> extended by %d sessions.'%(experiment.name, extend_by)
    user.put()
    self.redirect('/lab')
    return

def update_at_end_of_session(test_session):
  experiment = test_session.experiment
  if experiment is not None:
    experiment.sessions_complete += 1
    experiment.reminders_sent = []
    experiment.put()
  try:
    aggregates.AggregateTestResults.add_test_session(test_session)
  except:
    logging.info("Error updating aggregates with test session!")
    #raise
  try:
    experiment.update_one_session(test_session)
    experiment.put()
  except:
    logging.info("Error updating test session!")
    #raise
  try:
    aggregates.AggregateSessionResults.update_one_session(test_session)
    logging.info("Session aggregates updated successfully")
  except:
    logging.info("Error updating observations with test session")
  aggregates.recompute_priors(test_session)

class TakeTestsAnonymously(_base.Handler):
  def get(self, experiment_id, external_session_id):
    experiment_translate_name = {
      'experiment_8029': 'Experiment 8029',
      'minerva_assessment': 'Minerva Assessment',
      'doubleblinded_l_theanine_pilot': 'DoubleBlinded L-Theanine Pilot',
      'airmov': 'airmov'}
    if experiment_translate_name.has_key(experiment_id):
      experiment = entities.experiment.Experiment.all().filter('username', 'yoni').filter('name', experiment_translate_name[experiment_id]).filter('deleted', False).get()
    else:
      try:
        experiment = entities.experiment.Experiment.get_by_id(int(experiment_id))
      except:
        self.render("error.html", {'error': 'Bad experiment ID: %s'%(experiment_id,)})
        return
    if experiment is None:
      self.render("error.html", {'error': 'Experiment ID not found: %s'%(experiment_id,)})
      return
    test_battery = models2.Battery.all().filter('name', experiment.battery_name).get()
    if not test_battery:
      self.render("error.html", {'error': 'Could not retrieve test battery %s.'%(experiment.battery_name,)})
      return
    user_agent = self.request.headers.get('User-Agent', None)
    test_session = test_battery.new_session(None)
    test_session.user_agent = user_agent
    test_session.timezone = 'UTC'
    test_session.experiment = experiment
    test_session.n_completed_tests = 0
    test_session.active = True
    test_session.is_complete = False
    test_session.is_anonymous = True
    if test_session.session_information is None:
      session_information = models2.SessionInformation()
    else:
      session_information = test_session.session_information
    session_information.mental_variables = ['id']
    session_information.mental_states = [external_session_id]
    session_information.put()
    test_session.session_information = session_information
    if experiment_id in ('experiment_8029', 'minerva_assessment'):
      test_session.callback_url = 'https://minerva.kgi.edu/a1/done/?session_id=%s'%(external_session_id,)
      test_session.abort_url = 'https://minerva.kgi.edu/a1/start/?is_aborted=1&session_id=%s'%(external_session_id,)
    test_session.put()
    self.redirect('/lab/study_session/%d'%(test_session.key().id()))


def send_test_results(external_session_id, test_session_id, experiment_id):
  experiment = entities.experiment.Experiment.get_by_id(experiment_id)
  test_session = models2.Session2.get_by_id(test_session_id)
  finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
  finished_tests.sort(key=lambda x: x.start_time)
  res = {external_session_id: {}}
  res[external_session_id]['tests'] = [(test.key().id(), util.test_to_json(test)) for test in finished_tests]
  payload = json.dumps(res)

  result = urlfetch.fetch(
    url=experiment.results_url,
    payload=payload,
    method=urlfetch.POST,
    headers={'Content-Type': 'application/json'})
  if result.status_code != 200:
    raise Exception('Retrying task, for url %s response code:' % experiment.results_url, result.status_code, '\nContent:', result.content)


class AnonymousSession(_base.Handler):
  def get(self, session_id):
    retri = 0
    while True:
      try:
        sess = models2.Session2.get_by_id(int(session_id))
      except:
        self.render("error.html", {'error': 'Bad session ID: %s'%(session_id,)})
        return
      if sess is None:
        retri += 1
        if retri >= 5:
          self.render("error.html", {'error': 'Session ID not found: %s'%(session_id,)})
          return
        time.sleep(1)
      else:
        break
    test_session = sess
    experiment = test_session.experiment
    session_information = test_session.session_information
    time_ago = (datetime.datetime.utcnow() - test_session.start_time).seconds // 60
    timeout_left = (test_session.timeout_time - datetime.datetime.utcnow()).seconds // 60
    n_finished = test_session.n_completed_tests
    serial_tests = test_session.get_serial_tests()
    if not serial_tests:
      self.render("error.html", {'error': 'Tests for session %s cannot be taken anonymously.'%(session_id,)})
      return
    finished_tests = serial_tests[:n_finished]
    remaining_tests = serial_tests[n_finished:]
    completed_tests = None
    mvid = test_session.get_mental_variable('id')
    if len(remaining_tests):
      next_test = remaining_tests[0]
    else:
      next_test = None
      if test_session.active:
        test_session.is_complete = True
        test_session.active = False
        test_session.end_time = datetime.datetime.utcnow()
        test_session.update_test_information()
        test_session.put()
        next_test = None
        finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
        finished_tests.sort(key = lambda x: x.end_time)
        #update_at_end_of_session(test_session)
        completed_tests = finished_tests
        finished_tests = [x.variant() for x in finished_tests]
        if experiment.results_url:
            # Send results to this URL
            deferred.defer(send_test_results, mvid, test_session.key().id(), experiment.key().id(), _queue='test-results')
        if test_session.callback_url:
          finished_tests = models2.CompletedTest.all().filter('test_session', test_session).fetch(1000)
          finished_tests.sort(key = lambda x: x.end_time)
          context = {'callback_url': test_session.callback_url,
                     'results': []}
          # self.render('callback.html', context)
          self.redirect(str(test_session.callback_url))
          return
    context = {'test_session': test_session,
               'is_serial': True,
               'active': 'tests',
               'finished_tests': finished_tests,
               'completed_tests': completed_tests,
               'remaining_tests': remaining_tests,
               'next_test': next_test,
               'started_ago': time_ago,
               'timeout_left': timeout_left,
               'experiment': experiment,
               'anon_session': True,
               'branding': experiment.custom_branding,
               }
    if mvid is not None:
      context['anon_sess_id'] = mvid
      context['without_back_button'] = True
    if experiment and experiment.landing_page:
      context['next_step'] = '%s.html'%experiment.landing_page
    if experiment and experiment.custom_branding:
        self.render('study_session_%s.html' % experiment.custom_branding, context)
    else:
        self.render('study_session.html', context)
    return
