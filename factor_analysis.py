from numpy import *

# notations:
# M is the observation matrix where each cell is either None or (mean, variance). Shape is (n, m), n are samples. Yes, samples are rows, not columns.
# X is the factor matrix of shape (m, n_factors). Yes, here the factors are columns.
# Z are the loadings of shape (n, n_factors). Rows are loadings for a specific sample.

def pfa_likelihood(M, X, Z):
    ll = -0.5*(log(2*pi)+sum(Z**2))
    Y = dot(Z, transpose(X))
    for (M_i, Y_i) in zip(M, Y):
        for (M_ij, Y_ij) in zip(M_i, Y_i):
            if M_ij is not None:
                ll += -0.5*(log(2*pi*M_ij[1])+(M_ij[0]-Y_ij)**2/M_ij[1])
    return ll

def weighted_linreg(eqs):
    A = array([x[0] for x in eqs])
    W = array([x[2] for x in eqs])
    b = array([x[1] for x in eqs])
    AtWA = dot(transpose(A),W[:,newaxis] * A)
    AtWAinv = linalg.inv(AtWA)
    AtWb = dot(transpose(A), W*b)
    beta = dot(AtWAinv, AtWb)
    return beta

def pfa_m_step(M, Z):
    # relearn X given M and Z
    # this is a weighted linear regression problem
    # sum_{k=1}^{n_factors} [Z_ik*X_jk] ~ N(M_ij[0], M_ij[1])
    # Solve separately for each component: 1 <= j <= m, there are n equations, 1 <= i <= n,
    # sum_{k=1}^{n_factors} [Z_ik*X_jk] = M_ij[0] with weight 1.0 / M_ij[1]
    (n, n_factors) = Z.shape
    m = len(M[0])
    X = zeros([m, n_factors])
    for j in xrange(m):
        # solve for X[j]
        eqs = []
        for i in xrange(n):
            if M[i][j] is not None:
                eqs.append((Z[i], M[i][j][0], 1.0 / M[i][j][1]))
        beta = weighted_linreg(eqs)
        X[j,:] = beta
    return X    

def pfa_inference(Y, X, mu):
    # Y is a list of either None or (mean, variance)
    # E[Z] = 0
    # Cov[Z,Z] = I_{n_factors}
    # E[Y] = Mu[Y]
    # Y | Z = X*Z + epsilon
    # epsilon ~ N(0, Psi)
    # Cov[Y,Y] = XX' + Psi
    # Cov[Z,Y] = X'
    # E[Z | Y] = X'*(XX' + Psi)^{-1}*(Y - Mu[Y])
    # Cov[Z | Y] = I_{n_factors} - X'*(XX' + Psi)^{-1}*X
    # Var[Z_k | Y] = 1 - sum_j X_jk*(M1Inv*X)_jk = 1 - sum_j [X_jk sum_l [M1Inv_jl * X_lk] = 1 - sum_{j,l} [X_jk * X_lk * M1Inv_jl]
    infinity = 1e8
    (m, n_factors) = X.shape
    M1 = dot(X, transpose(X))
    Y_minus_mu = zeros([m])
    for Y_i in xrange(len(Y)):
        if Y[Y_i] is None:
            Y_minus_mu[Y_i] = 0.0
            M1[Y_i, Y_i] += infinity
        else:
            Y_minus_mu[Y_i] = Y[Y_i][0] - mu[Y_i]
            M1[Y_i, Y_i] += Y[Y_i][1]
    M1inv = linalg.inv(M1)
    Mu_Z = dot(transpose(X), dot(M1inv, Y_minus_mu))
    Sigma_Z = eye(n_factors) - dot(transpose(X), dot(M1inv, X))
    return (Mu_Z, diag(Sigma_Z))

def pfa_e_step(M, X):
    # relearn Z given M and X
    # this is a weighted linear regression problem
    # sum_{k=1}^{n_factors} [Z_ik*X_jk] ~ N(M_ij[0], M_ij[1])
    # Solve separately for each data sample: 1 <= i <= n, there are up to m equations, 1 <= j <= m,
    # sum_{k=1}^{n_factors} [X_jk*Z_ik] = M_ij[0] with weight 1.0 / M_ij[1]
    (m, n_factors) = X.shape
    n = len(M)
    Z = zeros([n, n_factors])
    for i in xrange(n):
        # solve for Z[i]
        Z[i,:] = pfa_inference(M[i], X, zeros([m]))[0]
        continue
        eqs = []
        for j in xrange(m):
            if M[i][j] is not None:
                eqs.append((X[j], M[i][j][0], 1.0 / M[i][j][1]))
        for k in xrange(n_factors):
            v = zeros([n_factors])
            v[k] = 1.0
            eqs.append((v, 0.0, 1.0))
        beta = weighted_linreg(eqs)
        Z[i,:] = beta
    return Z    

def pfa_iterate(M, X, Z = None):
    if Z is not None:
        print "Likelihood at beginning of iteration:",pfa_likelihood(M, X, Z)
    Z = pfa_e_step(M, X)
    print "Likelihood after E-Step:",pfa_likelihood(M, X, Z)
    X = pfa_m_step(M, Z)
    print "Likelihood after M-Step:",pfa_likelihood(M, X, Z)
    return X, Z

def pfa_initialize(M, n_factors):
    n = len(M)
    m = len(M[0])
    # initialize to PCA
    covar = zeros([m,m])
    for j1 in xrange(m):
        for j2 in xrange(m):
            v = []
            for i in xrange(n):
                if (M[i][j1] is not None) and (M[i][j2] is not None):
                    v.append((M[i][j1][0], M[i][j2][0]))
            v1 = array([x[0] for x in v])
            v2 = array([x[1] for x in v])
            cc = corrcoef(v1,v2)[0,1]
            covar[j1,j2] = cc
    w, v = linalg.eig(covar)
    X = v[:,:n_factors]
    return X

def pfa_center(M):
    n = len(M)
    m = len(M[0])
    mu = zeros([m])
    for j in xrange(m):
        v = 0.0
        w = 0.0
        for i in xrange(n):
            if M[i][j] is not None:
                curw = 1.0 / sqrt(M[i][j][1])
                v += M[i][j][0] * curw
                w += curw
        mu[j] = v / w
    M_tag = []
    for M_i in M:
        new_row = []
        for (M_ij,mu_j) in zip(M_i,mu):
            if M_ij is None:
                new_row.append(None)
            else:
                new_row.append((M_ij[0] - mu_j, M_ij[1]))
        M_tag.append(new_row)
    return M_tag, mu

def pfa(M, n_factors):
    M_tag, mu = pfa_center(M)
    X = pfa_initialize(M_tag, n_factors)
    Z = None
    for i in xrange(10):
        X, Z = pfa_iterate(M_tag, X, Z)
    return X, Z, mu
