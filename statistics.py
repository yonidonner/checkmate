def make_histogram(value_lists, n_bins):
    value_lists = [x for x in value_lists if len(x) > 0]
    if len(value_lists) == 0:
        return None
    sorted_lists = map(sorted, value_lists)
    min_val = min([x[0] for x in sorted_lists])
    max_val = max([x[-1] for x in sorted_lists])
    bin_size = (max_val - min_val) * 1.0 / n_bins
    bins = [(min_val + bin_size * i, min_val + bin_size * (i + 1)) for i in range(n_bins)]
    h = [[0 for j in range(len(sorted_lists))] for i in range(n_bins)]
    for k in xrange(len(sorted_lists)):
        sorted_list = sorted_lists[k]
        bin_i = 0
        for j in xrange(len(sorted_list)):
            while (bin_i < n_bins) and (sorted_list[j] > bins[bin_i][1]):
                bin_i += 1
            if (bin_i == n_bins):
                bin_i = n_bins - 1
            h[bin_i][k] += 1
    return [["%d-%d"%(bins[i][0],bins[i][1])] + h[i] for i in xrange(n_bins)]
