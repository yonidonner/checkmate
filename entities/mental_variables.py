import entities.experiment
from google.appengine.ext import db
from google.appengine.ext.db import polymodel
import logging

MENTAL_VARIABLE_TYPES = ['discrete', 'likert', 'numeric', 'text']
[MV_DISCRETE, MV_LIKERT, MV_NUMERIC, MV_TEXT] = range(len(MENTAL_VARIABLE_TYPES))

class MentalVariable(polymodel.PolyModel):
    creator_username = db.StringProperty()
    variable_name = db.StringProperty()
    variable_type = db.IntegerProperty()
    add_by_default = db.BooleanProperty(default = False)
    create_date = db.DateTimeProperty(auto_now_add=True)

    def to_dict(self):
        return {'username': self.creator_username,
                'name': self.variable_name,
                'key_id': self.key().id(),
                'type': self.type_str(),
                'add_by_default': self.add_by_default}

    @classmethod
    def get_actual_class(cls, variable_type):
        if (variable_type == MV_DISCRETE):
            return DiscreteMentalVariable
        elif (variable_type == MV_LIKERT):
            return LikertMentalVariable
        elif (variable_type == MV_NUMERIC):
            return NumericMentalVariable
        elif (variable_type == MV_TEXT):
            return TextMentalVariable

    def to_tuple(self):
        return (self.creator_username, self.variable_name, self.variable_type, self.add_by_default, self.create_date, self.key().id())

    @classmethod
    def from_tuple(cls, tup, mv_dict = None):
        # to build this into mv_dict, initialize it to {} and pass it here
        (creator_username, variable_name, variable_type, add_by_default, create_date, key_id) = tup[:6]
        rest = tup[6:]
        cls2 = cls.get_actual_class(variable_type)
        result = cls2(creator_username = creator_username,
                      variable_name = variable_name,
                      variable_type = variable_type,
                      add_by_default = add_by_default,
                      create_date = create_date)
        result.set_from_tuple(rest)
        if mv_dict is not None:
            result.put()
            mv_dict[key_id] = result.key()
        return result

    def type_str(self):
        return MENTAL_VARIABLE_TYPES[self.variable_type]

    @classmethod
    def clone(cls, mv, username):
        new_mental_variable = cls(creator_username = username,
                                  variable_name = mv.variable_name,
                                  variable_type = mv.variable_type,
                                  add_by_default = False)
        return new_mental_variable

    @classmethod
    def clean_user(cls, username):
        # load all variables for that user
        all_mvs = []
        offset = 0
        while True:
            mvs = cls().all().filter('creator_username', username).fetch(100, offset = offset)
            offset += len(mvs)
            all_mvs.extend(mvs)
            if not mvs:
                break
        all_mvs = [[x, False] for x in all_mvs]
        mv_dict = {}
        for mv_i in xrange(len(all_mvs)):
            mv_dict[all_mvs[mv_i][0].key().id()] = mv_i
        # go over all experiments for that user
        all_experiments = []
        offset = 0
        while True:
            experiments = entities.experiment.Experiment().all().filter('username', username).filter('active', True).filter("deleted", False).fetch(100, offset = offset)
            offset += len(experiments)
            all_experiments.extend(experiments)
            if not experiments:
                break
        for experiment in all_experiments:
            new_mvs = []
            for mental_variable in experiment.mental_variables:
                if mv_dict.has_key(mental_variable.id()):
                    new_mvs.append(mental_variable)
                    all_mvs[mv_dict[mental_variable.id()]][1] = True
            # update MVs
            experiment.mental_variables = new_mvs
            experiment.put()
        # now clean unused variables
        for (mv, found) in all_mvs:
            if not found:
                logging.info("deleting mental variable: %s"%str(mv.key()))
                db.delete(mv.key())

    @classmethod
    def create_defaults(cls):
        for (creator_username, variable_name, variable_type_str, params) in default_mental_variables:
            variable_type = MENTAL_VARIABLE_TYPES.index(variable_type_str)
            mental_variable = MentalVariable.all().filter('creator_username', creator_username).filter('variable_name', variable_name).get()
            if not mental_variable:
                mental_variable = MENTAL_VARIABLE_CLASSES[variable_type](creator_username = creator_username,
                                                            variable_name = variable_name)
            mental_variable.variable_type = variable_type
            if (variable_type == MV_DISCRETE):
                (states, default_state) = params
                mental_variable.states = states
                mental_variable.default_state = default_state
            elif (variable_type == MV_LIKERT):
                (likert_values, default) = params
                mental_variable.likert_values = likert_values
                mental_variable.default = default
            elif (variable_type == MV_NUMERIC):
                (min_value, max_value, default) = params
                mental_variable.min_value = min_value
                mental_variable.max_value = max_value
                mental_variable.default = default
            elif (variable_type == MV_TEXT):
                pass
            mental_variable.put()

    def type_as_str(self):
        return MENTAL_VARIABLE_TYPES[self.variable_type]

    def get_states(self):
        return None


class DiscreteMentalVariable(MentalVariable):
    states = db.ListProperty(str)
    default_state = db.StringProperty()

    def to_dict(self):
        d = super(DiscreteMentalVariable, self).to_dict()
        d.update({'states': self.states,
                  'default_state': self.default_state})
        return d

    def to_tuple(self):
        return super(DiscreteMentalVariable, self).to_tuple() + (self.states, self.default_state)

    def set_from_tuple(self, tup):
        (self.states, self.default_state) = tup

    @classmethod
    def clone(cls, mv, username):
        new_mental_variable = super(DiscreteMentalVariable, cls).clone(mv, username)
        new_mental_variable.states = mv.states
        new_mental_variable.default_state = mv.default_state
        return new_mental_variable

    def states_str(self):
        return ",".join(self.states)

    def enum_states(self):
        return [{'id': i,
                 'state': self.states[i]}
                for i in xrange(len(self.states))]

    def get_states(self):
        return self.states[:]


class LikertMentalVariable(MentalVariable):
    likert_values = db.IntegerProperty()
    default = db.IntegerProperty()

    def to_dict(self):
        d = super(LikertMentalVariable, self).to_dict()
        d.update({'likert_values': self.likert_values,
                  'default': self.default})
        return d

    def to_tuple(self):
        return super(LikertMentalVariable, self).to_tuple() + (self.likert_values, self.default)

    def set_from_tuple(self, tup):
        (self.likert_values, self.default) = tup

    @classmethod
    def clone(cls, mv, username):
        new_mental_variable = super(LikertMentalVariable, cls).clone(mv, username)
        new_mental_variable.likert_values = mv.likert_values
        new_mental_variable.default = mv.default
        return new_mental_variable

    def get_states(self):
        if self.likert_values > 4:
            return None  # treat it like a continuous variable
        return range(1, self.likert_values + 1)


class NumericMentalVariable(MentalVariable):
    min_value = db.FloatProperty()
    max_value = db.FloatProperty()
    default = db.FloatProperty()

    def to_dict(self):
        d = super(NumericMentalVariable, self).to_dict()
        d.update({'min_value': self.min_value,
                  'max_value': self.max_value,
                  'default': self.default})
        return d

    def to_tuple(self):
        return super(NumericMentalVariable, self).to_tuple() + (self.min_value, self.max_value, self.default)

    def set_from_tuple(self, tup):
        (self.min_value, self.max_value, self.default) = tup

    @classmethod
    def clone(cls, mv, username):
        new_mental_variable = super(NumericMentalVariable, cls).clone(mv, username)
        new_mental_variable.min_value = mv.min_value
        new_mental_variable.max_value = mv.max_value
        new_mental_variable.default = mv.default
        return new_mental_variable

class TextMentalVariable(MentalVariable):
    def to_tuple(self):
        return super(TextMentalVariable, self).to_tuple()

    def set_from_tuple(self, tup):
        return

MENTAL_VARIABLE_CLASSES = [DiscreteMentalVariable, LikertMentalVariable, NumericMentalVariable, TextMentalVariable]

default_mental_variables = [(None, 'Sleep last night', 'numeric', (0.0, 24.0, 8.0)),
                            (None, 'Sleep during the day', 'numeric', (0.0, 24.0, 0.0)),
                            (None, 'Age', 'numeric', (4.0, 122.0, 27.0)),
                            (None, 'Mood', 'likert', (5, 3)),
                            (None, 'Energy', 'likert', (5, 3)),
                            (None, 'Health in the last month', 'likert', (5, 3)),
                            (None, 'Willpower', 'likert', (5, 3)),
                            (None, 'Focus', 'likert', (5, 3)),
                            (None, 'What did you do just now?', 'discrete', (['Attend SIY class', 'Work', 'Break', 'Snack'], 'Attend SIY class')),
                            (None, 'Meditation', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Nap', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Qualitative sleep', 'likert', (5, 3)),
                            (None, 'Recent energy intake', 'discrete', (['None', 'Some', 'Much', 'Too much'], 'Some')),
                            (None, 'Stimulants', 'discrete', (['None', 'Some', 'Heavy'], 'None')),
                            (None, 'Pulse', 'numeric', (25.0, 150.0, 50.0)),
                            (None, 'Blood glucose', 'numeric', (0.0, 300.0, 100.0)),
                            (None, 'Temperature', 'numeric', (35.0, 42.0, 37.0)),
                            (None, 'Temperature in Fahrenheit', 'numeric', (95.0, 104.0, 98.0)),
                            (None, 'Heart Rate', 'numeric', (30.0, 150.0, 60.0)),
                            (None, 'Weightlifting', 'discrete', (['None', 'Moderate', 'Intense'], 'None')),
                            (None, 'Cardio', 'discrete', (['None', 'Moderate', 'Intense'], 'None')),
                            (None, 'Coffee', 'discrete', (['None', 'Some', 'A lot'], 'None')),
                            (None, 'Chocolate', 'discrete', (['None', 'White', 'Milk', 'Dark'], 'None')),
                            (None, 'Butter (tbsp)', 'numeric', (0.0,8.0,0.0)),
                            (None, 'Sugar in last 2 hours (gr)', 'numeric', (0.0,200.0,0.0)),
                            (None, 'Artificial sweeteners in last 2 hours', 'discrete', (['None', 'Some', 'A lot'], 'None')),
                            (None, 'Gluten', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Dairy', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Blueberries (cups)', 'numeric', (0.0,8.0,0.0)),
                            (None, 'Nuts', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Butter or Olive Oil', 'discrete', (['Butter', 'Olive Oil'], 'Butter')),
                            (None, 'Dark or White', 'discrete', (['Dark', 'White'], 'Dark')),
                            (None, 'Weight Lifting', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'Vitamin D', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'Fish Oil', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'Butter', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Upgraded or Regular Coffee', 'discrete', (['Upgraded', 'Regular', 'Just practicing'], 'Upgraded')),
                            (None, 'Creatine today (gr)', 'numeric', (0.0, 20.0, 0.0)),
                            (None, 'Posture', 'discrete', (['Sitting', 'Standing', 'Walking'], 'Sitting')),
                            (None, 'Other', 'text', None),
                            (None, 'Anything else?', 'text', None),
                            (None, 'Participant number', 'text', None),
                            (None, 'Which stage of the experiment (practice, before, after) are you on?', 'discrete', (['Practice', 'Before', 'After'], 'Practice')),
                            (None, 'What kind of coffee do you think you got?', 'discrete', (['I do not know', 'Regular', 'Decaf'], 'I do not know')),
                            (None, 'How did you order your coffee?', 'text', None),
                            (None, 'Did you have breakfast?', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Did you have coffee?', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Are you hungry right now?', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Did you do Crossfit today?', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Any other comments?', 'text', None),
                            (None, 'Comments', 'text', None),
                            (None, 'Did you have sex or masturbate?', 'discrete', (['Neither', 'Sex', 'Masturbation'], 'Neither')),
                            (None, 'Are you before or after meditating/activity right now?','discrete', (['Before', 'After'], 'Before')),
                            (None, 'Regular or Decaf?', 'discrete', (['No coffee', 'Regular', 'Decaf'], 'No coffee')),
                            (None, 'Are you testing before or after coffee?', 'discrete', (['Before', 'After'], 'Before')),
                            (None, 'How many minutes did you meditate or do the activity for?', 'numeric', (0.0, 60.0, 0.0)),
                            (None, 'If you had sex or masturbated, how many hours ago was it?', 'numeric', (0.0, 24.0, 0.0)),
                            (None, 'Did you meditate or do a different activity?', 'discrete', (['Meditate', 'Activity'], 'Meditate')),
                            (None, 'What activity did you do if not meditation?', 'text', None),
                            (None, 'How many hours ago did you wake up?', 'numeric', (0.0, 24.0, 0.0)),
                            (None, 'Which coffee bar did you get your coffee at?', 'discrete', (['1220','1225','CL3','Hangout','Mobile baristas'], '1225')),
                            (None, 'How hungry are you right now?', 'likert', (5, 3)),
                            (None, 'How would you describe your current mental focus status?', 'likert', (5, 3)),
                            (None, 'Mood (1 - very unhappy, 7 - very happy)', 'likert', (7, 4)),
                            (None, 'Energy (1 - very sleepy, 7 - wide awake)', 'likert', (7, 4)),
                            (None, 'Which Cognition In Motion condition are you on?', 'discrete', (['Practicing', 'Sitting', 'Standing', 'Walking', 'Biking'], 'Sitting')),
                            (None, 'Is this a practice session?', 'discrete', (['Yes', 'No'], 'Yes')),
                            (None, 'Do you think your drink was regular, half-caf or decaf?', 'discrete', (['I have no idea', 'Regular', 'Half-caf', 'Decaf'], 'I have no idea')),
                            (None, 'Did you have meditate or relax?', 'discrete', (['Meditate', 'Relax'], 'Meditate')),
                            (None, 'Is today a tango class day?', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Are you before or after tango class time right now?', 'discrete', (['Before', 'After'], 'Before')),
                            (None, 'Are you pre-workout or in the middle of your workout?', 'discrete', (['Neither, still practicing', 'Pre-workout', 'During workout'], 'Neither, still practicing')),
                            (None, 'Is this practice week, Soylent week or no-Soylent week?', 'discrete', (['Practice', 'Soylent', 'No Soylent'], 'Practice')),
                            #(None, 'Sum of last 5 in Canabalt', 'numeric', (0.0, 10000.0, 0.0)),
                            (None, 'Sum of last 5 in Canabalt', 'text', None),
                            (None, 'Stimulation on', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'What pill number (if any) did you take?', 'discrete', (['None', 'Blue', 'Red', 'Yellow', 'Green'], 'None')),
                            (None, 'How many hours ago did you last eat something?', 'numeric', (0.0, 24.0, 0.0)),
                            (None, 'Which part of the experiment are you currently in?', 'discrete', (['Practice - Week 1 (three times)', 'Week 2 (three times)', 'Week 3 (three times)', 'Week 4 (three times)'], 'Practice')),
                            (None, 'Arctic Wolf participant ID', 'discrete', (['No ID', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'], 'No ID')),
                            (None, 'Cold', 'likert', (5, 3)),
                            (None, 'Arctic Wolf condition', 'discrete', (['0','1','2','3','4','5','6'], '0')),
                            (None, 'Arctic Wolf battery', 'discrete', (['First', 'Middle 1', 'Middle 2', 'Last', 'Any'], 'Any')),
                            (None, 'Hoe voelt u zich OP DIT MOMENT? Kruis het item aan dat op dit moment het meest van toepassing is.', 'discrete', (["1 - Klaarwakker; zeer alert; optimale concentratie mogelijk", "2 - Alert; goed in staat te concentreren", "3 - Ontspannen; wakker; redelijkalert", "4 - Minder alert; een beetje duf", "5 - Duf; wakker blijven begint moeilijk te worden; aan het inzakken", "6 - Slaperig; vechtend tegen de slaap; het liefst gaan slapen; versuft", "7 - In slaap aan het vallen; gevecht tegen de slaap aan het verliezen"], "1 - Klaarwakker; zeer alert; optimale concentratie mogelijk")),
                            (None, 'Have you eaten in the last hour?', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'Have you smoked in the last hour?', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'How many hours did you sleep last night?', 'text', None),
                            (None, 'How well did you sleep last night? Please select one.', 'discrete', (['Very well', 'Fairly well', 'Fairly badly', 'Very badly'], 'Very well')),
                            (None, 'Have you had any dental work done in the previous week up to now?', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'On which computer are you performing the test?', 'discrete', (['Computer 1', 'Computer 2', 'Computer 3'], 'Computer 1')),
                            (None, 'What time did your shift start?', 'text', None),
                            (None, 'When will your shift end?', 'text', None),
                            (None, 'Did you take a nap during your work shift?', 'discrete', (['Yes', 'No'], 'No')),
                            (None, 'Please indicate the status in your residency program (Ex. 1st year, 2nd year, 3rd year)', 'text', None),
                            (None, 'Compared with your usual day, would you say that today was', 'discrete', (['Typical', 'Fairly typical', 'Not very typical'], 'Typical')),
                            (None, 'Are you still practicing or already taking Olive Leaf Extract?', 'discrete', (['Practicing', 'Taking Olive Leaf Extract'], 'Practicing')),
                            (None, 'Amplify session ID', 'text', None),
                            (None, 'Ritalin', 'discrete', (['No', 'Yes'], 'No')),
                            (None, 'Briefly describe your physical/mental wellbeing over the past 24 hours by noting any differences from how you normally feel, e.g. mood, focus, procrastination, confidence, internal drama, anxiety, energy, physical coordination, reflexes, brain fog, emotional resilience. If there are no notable differences, simply write "normal".', 'text', None),
                            (None, 'Rate your sleep last night, considering both quality of sleep and number of hours. A 5 means you slept about as well as possible, you slept soundly for the full number of hours you feel you need. A 1 means the opposite, either your sleep quality was really bad (e.g. tossing and turning, frequent waking up), you did not get nearly enough hours, or some combination of both.', 'likert', (5, 3)),
                            (None, 'How many Phase 1 Qualia pills did you take today? (enter 0 for none)', 'text', None),
                            (None, 'How many Phase 2 Qualia pills did you take today? (enter 0 for none)', 'text', None),
]
