import util
import math
import logging

def newton_minimize(fgh, X0, f = None, lambdasq_threshold = 1e-16, t_threshold = 1e-8, improvement_threshold = 1e-6, beta = 0.5):
    if f is None:
        f = lambda x: fgh(x)[0]
    X = list(X0)
    while True:
        (fX, gX, hX) = fgh(X)
        ihX = util.matrix_inverse_with_pivoting(hX)
        dX = [0.0] * len(gX)
        for i in xrange(len(dX)):
            for k in xrange(len(dX)):
                dX[i] += ihX[i][k] * gX[k]
        #dX = linalg.solve(hX, gX)
        lambdasq = sum([gX_i * dX_i for (gX_i, dX_i) in zip(gX, dX)])
        #print "lambdasq:",lambdasq
        if lambdasq < lambdasq_threshold:
            break
        t = 1.0
        while True:
            X_tag = [x_i - t * dX_i for (x_i, dX_i) in zip(X, dX)]
            fX_tag = f(X_tag)
            if (fX - fX_tag) > improvement_threshold:
                X = X_tag
                break
            t *= beta
            if t < t_threshold:
                t = None
                break
        if t is None:
            break
    return X

def eye(m):
    res = [[0.0] * m for i in xrange(m)]
    for i in xrange(m):
        res[i][i] = 1.0
    return res

def prior_lgh(X, X_prior, lg = None, lh = None):
    m = len(X)
    if X_prior is None:
        X_prior = ([1.0] * m, eye(m), 0.0, eye(m))
    (mu, Sigma, logdetSigma, iSigma) = X_prior
    X_minus_mu = [x - mu_i for (x, mu_i) in zip(X, mu)]
    iSigma_X = [0.0] * m
    for i in xrange(m):
        for k in xrange(m):
            iSigma_X[i] += iSigma[i][k] * X_minus_mu[k]
    ll = -0.5*logdetSigma
    for i in xrange(m):
        ll -= 0.5*X_minus_mu[i]*iSigma_X[i]
    if lg is not None:
        for i in xrange(m):
            lg[i] -= iSigma_X[i]
    if lh is not None:
        for i in xrange(m):
            for j in xrange(m):
                lh[i][j] -= iSigma[i][j]
    return ll

def X_lgh(X, Y, W, X_prior = None, compute_gh = True):
    lg = None
    lh = None
    m = len(X)
    if compute_gh:
        lg = [0.0] * m
        lh = [[0.0] * m for i in xrange(m)]
    ll = prior_lgh(X, X_prior, lg, lh)
    for k in Y.keys():
        (group_name, observation_id, observation_type, context_key, linear_predictors) = k
        ss = Y[k]
        if not W.has_key(k[:-1]):
            continue
        (weights, params) = W[k[:-1]]
        ll += one_context_lgh(X, observation_type, ss, linear_predictors, weights, params, lg, lh)
    if compute_gh:
        return (ll, lg, lh)
    else:
        return ll

def logistic_lgh(ss, WtX, W, params = None, lg = None, lh = None):
    if WtX > 20.0:
        WtX = 20.0
    if WtX < -20.0:
        WtX = -20.0
    q0 = math.exp(WtX)
    p = q0 / (1.0 + q0)
    logp = math.log(p)
    logq = math.log(1.0-p)
    (n,k) = ss
    n_pos = k
    n_neg = n - k
    ll = n_pos * logp + n_neg * logq
    if lg is not None:
        for i in xrange(len(W)):
            lg[i] += W[i] * (n_pos - p * n)
    if lh is not None:
        for i in xrange(len(W)):
            for j in xrange(len(W)):
                lh[i][j] -= n * p * (1.0 - p) * W[i] * W[j]
    return ll

def gaussian_lgh(ss, WtX, W, params, lg = None, lh = None):
    (n,sumy,sumy2) = ss
    sigmasqr = params
    ll = -0.5*(n*math.log(2*math.pi*sigmasqr)+(sumy2-2*WtX*sumy+n*WtX**2)/sigmasqr)
    if lg is not None:
        for i in xrange(len(W)):
            lg[i] += W[i] * (sumy - n*WtX)/sigmasqr
    if lh is not None:
        for i in xrange(len(W)):
            for j in xrange(len(W)):
                lh[i][j] -= n / sigmasqr * W[i] * W[j]
    return ll

def one_context_lgh(X, observation_type, ss, linear_predictors, weights, params, lg = None, lh = None):
    # Computes the likelihood and possibly gradient and hessian associated with a single context and its observations
    m = len(X)
    extended_X = list(X) + list(linear_predictors) + [1.0]
    extended_W = list(weights)
    W = extended_W[:m]
    WtX = sum([x*w for (x,w) in zip(extended_X, extended_W)])
    if observation_type == 0:
        ll = logistic_lgh(ss, WtX, W, params, lg, lh)
    elif observation_type == 1:
        ll = gaussian_lgh(ss, WtX, W, params, lg, lh)
    return ll

def negate_lgh(lgh):
    ll, lg, lh = lgh
    return (-ll,
             [-x for x in lg],
             [[-x for x in y] for y in lh])

def make_X_fgh(Y, W, X_prior = None):
    return ((lambda x: negate_lgh(X_lgh(x, Y, W, X_prior, compute_gh = True))),
            (lambda x: -X_lgh(x, Y, W, X_prior, compute_gh = False)))

def factor_inference(Y, W, X_prior = None):
    fgh,f = make_X_fgh(Y, W, X_prior)
    if X_prior is not None:
        X0 = X_prior[0]
    else:
        k = Y.keys()[0]
        m = len(W[k[:-1]][0]) - (1+len(k[-1]))
        X0 = [0.0] * m
    X_hat = newton_minimize(fgh, X0, f)
    H = negate_matrix(fgh(X_hat)[2])
    mu_hat = X_hat
    Sigma_hat = negate_matrix(util.matrix_inverse_with_pivoting(H))
    return (mu_hat, Sigma_hat)

def negate_matrix(M):
    return [[-y for y in x] for x in M]

def make_default_prior(m):
    return ([0.0] * m, eye(m), 0.0, eye(m))

def default_NIW(m):
    return ([0.0] * m, m, eye(m), m)
    
def estimate_prior_M_step(posterior_X, global_NIW):
    n = len(posterior_X)
    data_X = [x[0] for x in posterior_X]
    m = len(data_X[0])
    x_bar = [sum([x[i] for x in data_X]) * 1.0 / n for i in xrange(m)]
    data_X_0 = [[x-y for (x,y) in zip(one_X, x_bar)] for one_X in data_X]
    C = [[0.0] * m for i in xrange(m)]
    for i in xrange(m):
        for j in xrange(i+1):
            for k in xrange(n):
                v = data_X_0[k][i] * data_X_0[k][j]
                C[i][j] += v
    for i in xrange(m):
        for j in xrange(i+1, m):
            C[i][j] = C[j][i]
    (mu_0, kappa_0, Psi_0, nu_0) = global_NIW
    kappa = kappa_0 + n
    nu = nu_0 + n
    mu = [(kappa_0 * mu_0[i] + x_bar[i] * n) / kappa for i in xrange(m)]
    Psi = [[0.0] * m for i in xrange(m)]
    x_bar_minus_mu0 = [x_bar[i] - mu_0[i] for i in xrange(m)]
    for i1 in xrange(m):
        for i2 in xrange(m):
            Psi[i1][i2] = Psi_0[i1][i2] + C[i1][i2] + (kappa_0 * n) / kappa * x_bar_minus_mu0[i1] * x_bar_minus_mu0[i2]
    z = 1.0 / (nu + m + 1)
    Sigma = [[Psi[i1][i2] * z for i2 in xrange(m)] for i1 in xrange(m)]
    (iSigma, logdetSigma) = util.matrix_inverse_with_pivoting(Sigma, return_logdet = True)
    return (mu, Sigma, logdetSigma, iSigma)

def vector_add(v1, v2):
    return [x+y for (x,y) in zip(v1,v2)]

def vector_subtract(v1, v2):
    return [x-y for (x,y) in zip(v1,v2)]

def matrix_add(M1, M2):
    return [[(x+y) for (x,y) in zip(r1, r2)] for (r1,r2) in zip(M1, M2)]

def matrix_by_vector(M, v):
    return [sum([M_i[k] * v[k] for k in xrange(len(v))]) for M_i in M]

def estimate_prior_E_step(X, prior):
    (mu, Sigma, logdetSigma, iSigma) = prior
    X_post = []
    for (X_mu, X_Sigma) in X:
        iX_Sigma = util.matrix_inverse_with_pivoting(X_Sigma)
        post_Sigma = util.matrix_inverse_with_pivoting(matrix_add(iSigma, iX_Sigma))
        post_mu = matrix_by_vector(post_Sigma, vector_add(matrix_by_vector(iSigma, mu), matrix_by_vector(iX_Sigma, X_mu)))
        X_post.append((post_mu, post_Sigma))
    return X_post

def vector_dotproduct(v1, v2):
    return sum([x1*x2 for (x1,x2) in zip(v1,v2)])

def matrix_dotproduct(M1, M2):
    return sum([sum([M1_ij*M2_ij for (M1_ij,M2_ij) in zip(M1_i,M2_i)]) for (M1_i,M2_i) in zip(M1,M2)])

def estimate_prior_total_likelihood(X, prior, global_NIW):
    # compute likelihood of prior given global_NIW
    ll = 0.0
    (mu, kappa, Psi, nu) = global_NIW
    m = len(mu)
    (prior_mu, prior_Sigma, prior_logdetSigma, prior_iSigma) = prior
    ll1 = -0.5*((nu+m+1)*prior_logdetSigma+matrix_dotproduct(Psi, prior_iSigma))
    ll2 = -0.5*(math.log(kappa) * m + prior_logdetSigma + (1.0 / kappa) * vector_dotproduct(vector_subtract(prior_mu, mu), matrix_by_vector(prior_iSigma, vector_subtract(prior_mu, mu))))
    # compute likelihood of data given prior
    n = len(X)
    ll3 = -0.5*prior_logdetSigma*n
    for (X_mu, X_Sigma) in X:
        ll3 += -0.5*vector_dotproduct(vector_subtract(X_mu, prior_mu), matrix_by_vector(prior_iSigma, vector_subtract(X_mu, prior_mu)))
    ll = ll1 + ll2 + ll3
    return ll   

def identity_matrix(m):
    M = [[0.0] * m for i in xrange(m)]
    for i in xrange(m):
        M[i][i] = 1.0
    return M

def estimate_prior(X, global_NIW):
    m = len(X[0][0])
    prior = ([0.0] * m, identity_matrix(m), 0.0, identity_matrix(m))
    prv_ll = None
    i = 0
    while True:
        i += 1
        X_post = estimate_prior_E_step(X, prior)
        prior = estimate_prior_M_step(X_post, global_NIW)
        ll = estimate_prior_total_likelihood(X_post, prior, global_NIW)
        #print "Total likelihood after %d iterations: %.4f"%(i, ll)
        if (prv_ll is None) or (ll-prv_ll > 0.01):
            prv_ll = ll
        else:
            break
    return (X_post, prior)
