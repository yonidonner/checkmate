#!/usr/bin/env python

import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from handlers import _base

allowed = ['http://www.thedomainyouwantgoogletoindex.com']

class RobotsHandler(webapp.RequestHandler):
  def get(self):
    if(self.request.host_url in allowed):
      self.response.out.write("""User-agent: *
Allow: /""")
    else:
      self.response.out.write("""User-agent: *
Disallow: /""")
