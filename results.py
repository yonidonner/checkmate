from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.db import polymodel
import logging
import string
import datetime
import models2
import random
import statistics
import pickle
import util
import priors
import factor_inference
import scores
import score_params
import math
from helpers.templatefilters import time_ms, time_flexible

class BaseTestVariant(models2.TestVariant):
  start_button_delay = db.IntegerProperty(default=1000)

  @classmethod
  def get_state_class(cls):
    return UserBaseTestState

class MultitrialTestVariant(BaseTestVariant):
  @classmethod
  def get_state_class(cls):
    return UserMultitrialTestState

class GoNoGoTestVariant(MultitrialTestVariant):
  stimulus_on_time = db.IntegerProperty()
  interstimulus_delay = db.IntegerProperty()
  n_total_trials = db.IntegerProperty()
  first_half_variant = db.StringProperty(default = "random")
  second_half_variant = db.StringProperty(default = "random")

  @classmethod
  def get_result_class(cls):
    return GoNoGoTestResult

class TSRTestVariant(MultitrialTestVariant):
  fixed_duration = db.IntegerProperty(default = 0)
  prestimulus_delay = db.IntegerProperty(default = 0)

  @classmethod
  def get_state_class(cls):
    return UserTSRTestState

class ReactionTimeTestVariant(TSRTestVariant):
  n_trials = db.IntegerProperty()
  delay_alpha = db.IntegerProperty()
  delay_lambda = db.IntegerProperty()

  @classmethod
  def get_state_class(cls):
    return UserReactionTimeTestState

  @classmethod
  def get_result_class(cls):
    return ReactionTimeTestResult

class LevelEstimationTestVariant(TSRTestVariant):
  default_initial_level = db.FloatProperty(default = 1.0)
  #target_success_probability = db.FloatProperty(default = 0.5)
  min_pos_neg = db.FloatProperty(default = 10.0)
  test_time_limit = db.IntegerProperty()

  @classmethod
  def get_state_class(cls):
    return UserLevelEstimationTestState

  @classmethod
  def get_result_class(cls):
    return LevelEstimationTestResult

class SpanTestVariant(LevelEstimationTestVariant):
  is_backward = db.BooleanProperty()
  element_on_time = db.IntegerProperty()
  element_off_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return SpanTestResult

class PairedAssociatesTestVariant(LevelEstimationTestVariant):
  on_time = db.IntegerProperty()
  off_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return PairedAssociatesTestResult

class VisuospatialPairedAssociatesTestVariant(PairedAssociatesTestVariant):
  grid_size = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return VisuospatialPairedAssociatesTestResult

class VerbalPairedAssociatesTestVariant(BaseTestVariant):
  default_initial_level = db.IntegerProperty()
  on_time = db.IntegerProperty()
  off_time = db.IntegerProperty()
  word_list = db.StringProperty()
  target_success_probability = db.FloatProperty(default = 0.5)

  @classmethod
  def get_result_class(cls):
    return VerbalPairedAssociatesTestResult

  @classmethod
  def get_state_class(cls):
    return UserVerbalPairedAssociatesTestState

class VerbalPairedAssociates2TestVariant(BaseTestVariant):
  @classmethod
  def get_result_class(cls):
    return VerbalPairedAssociates2TestResult

  @classmethod
  def get_state_class(cls):
    return UserVerbalPairedAssociates2TestState

class VerbalLearningTestVariant(BaseTestVariant):
  default_initial_level = db.IntegerProperty()
  max_learning_trials = db.IntegerProperty()
  word_on_time = db.IntegerProperty()
  word_off_time = db.IntegerProperty()
  word_list = db.StringProperty()
  fixed_list_size = db.IntegerProperty(default = 0)
  target_success_probability = db.FloatProperty(default = 0.5)

  @classmethod
  def get_result_class(cls):
    return VerbalLearningTestResult

  @classmethod
  def get_state_class(cls):
    return UserVerbalLearningTestState

class VerbalLearning2TestVariant(BaseTestVariant):
  @classmethod
  def get_result_class(cls):
    return VerbalLearning2TestResult

  @classmethod
  def get_state_class(cls):
    return UserVerbalLearning2TestState

class DigitSpanTestVariant(SpanTestVariant):
  is_visual = db.BooleanProperty()
  is_auditory = db.BooleanProperty()
  digits = db.StringProperty()

  @classmethod
  def get_result_class(cls):
    return DigitSpanTestResult

class SpatialSpanTestVariant(SpanTestVariant):
  grid_size = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return SpatialSpanTestResult

class DesignCopyTestVariant(LevelEstimationTestVariant):
  grid_size = db.IntegerProperty()
  stimulus_display_time = db.IntegerProperty()
  mask_repeats = db.IntegerProperty()
  mask_repeat_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return DesignCopyTestResult

class DesignRecognitionTestVariant(LevelEstimationTestVariant):
  num_choices = db.IntegerProperty()
  grid_size = db.IntegerProperty()
  stimulus_display_time = db.IntegerProperty()
  mask_repeats = db.IntegerProperty()
  mask_repeat_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return DesignRecognitionTestResult

class MentalRotationTestVariant(DesignRecognitionTestVariant):
  time_limit = db.IntegerProperty()
  is_connected = db.BooleanProperty(default = False)

  def __init__(self, *args, **kwargs):
    super(MentalRotationTestVariant, self).__init__(*args, **kwargs)

  @classmethod
  def get_result_class(cls):
    return MentalRotationTestResult

class InspectionTimeTestVariant(LevelEstimationTestVariant):
  dec_lambda = db.FloatProperty()
  inc_delta = db.FloatProperty()

  @classmethod
  def get_result_class(cls):
    return InspectionTimeTestResult

class DecisionMakingTestVariant(LevelEstimationTestVariant):
  dec_lambda = db.FloatProperty()
  inc_delta = db.FloatProperty()
  subtype = db.StringProperty()

  @classmethod
  def get_state_class(cls):
  # Override LevelEstimationTestVariant to keep state information
  # from being applied across sessions.
    return UserBaseTestState

  @classmethod
  def get_result_class(cls):
    return DecisionMakingTestResult

class CodingTestVariant(TSRTestVariant):
  image_set = db.StringProperty()
  allow_sequences = db.BooleanProperty(default = False)

  def __init__(self, *args, **kwargs):
    super(CodingTestVariant, self).__init__(*args, **kwargs)

  @classmethod
  def get_result_class(cls):
    return CodingTestResult

class KNBackTestVariant(TSRTestVariant):
  k = db.IntegerProperty(default=1)
  n = db.IntegerProperty(default=1)
  pace = db.IntegerProperty(default=0)
  buffer_on_time = db.IntegerProperty(default=1000)
  buffer_off_time = db.IntegerProperty(default=500)
  n_trials = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return KNBackTestResult

class SimpleNBackTestVariant(KNBackTestVariant):
  n_stimuli = db.IntegerProperty()
  p_identical = db.FloatProperty()

  @classmethod
  def get_result_class(cls):
    return SimpleNBackTestResult

class DualNBackTestVariant(KNBackTestVariant):
  n_stimuli = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return DualNBackTestResult

class JaeggiDualNBackTestVariant(KNBackTestVariant):
  grid_size = db.IntegerProperty()
  letters = db.StringProperty()
  auditory_targets = db.IntegerProperty()
  visual_targets = db.IntegerProperty()
  audiovisual_targets = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return JaeggiDualNBackTestResult

class FeatureMatchTestVariant(TSRTestVariant):
  grid_size = db.IntegerProperty()
  min_trials_per_level = db.IntegerProperty()
  min_level = db.IntegerProperty()
  max_level = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return FeatureMatchTestResult

class MentalRotation2TestVariant(TSRTestVariant):
  grid_size = db.IntegerProperty()
  n_rotations = db.IntegerProperty()
  n_trials_per_rotation = db.IntegerProperty()
  min_complexity = db.IntegerProperty()
  complexity_jump = db.IntegerProperty()
  n_complexities = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return MentalRotation2TestResult

class StroopTestVariant(TSRTestVariant):
  pass

class ColorWordTestVariant(TSRTestVariant):
  n_colors = db.IntegerProperty()
  n_base_trials = db.IntegerProperty()
  p_task_switch = db.FloatProperty(default = 0.2)
  color_word_emphasis = db.StringProperty(default = "all")

  @classmethod
  def get_result_class(cls):
    return ColorWordTestResult

class CuedAttentionTestVariant(ReactionTimeTestVariant):
  axis = db.StringProperty() # "vertical" or "horizontal"
  p_congruent_cue = db.FloatProperty() # probability that the cue is congruent
  cue_types = db.StringProperty() # "spatial" for simply correct/incorrect, "ANT" for center/double/spatial1/spatial2
  cue_style = db.StringProperty() # "bold" or "arrow" for Posner-style, "asterisk" for ANT-style
  stimulus_style = db.StringProperty() # "farrow" for ANT-style arrows with flankers, "arrow" for Posner with left/right arrow, "dot" for dot, "square" for square
  cue_duration = db.IntegerProperty()
  cue_stimulus_delay = db.IntegerProperty()
  cue_stimulus_delay_lambda = db.IntegerProperty(default = 0)
  max_response_time = db.IntegerProperty() # max ms to respond

  @classmethod
  def get_result_class(cls):
    return CuedAttentionTestResult

class SortingTestVariant(TSRTestVariant):
  rule_change_probability = db.FloatProperty()
  num_trials = db.IntegerProperty()
  rule_language = db.StringProperty(default = "English")

  @classmethod
  def get_result_class(cls):
    return SortingTestResult

class FingerTappingTestVariant(MultitrialTestVariant):
  trial_duration = db.IntegerProperty()
  pause_duration = db.IntegerProperty()
  num_trials = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return FingerTappingTestResult

class SimpleReactionTimeTestVariant(ReactionTimeTestVariant):
  @classmethod
  def get_result_class(cls):
    return SimpleReactionTimeTestResult

class ChoiceReactionTimeTestVariant(ReactionTimeTestVariant):
  n_choices = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return ChoiceReactionTimeTestResult

class AttentionTestVariant(ReactionTimeTestVariant):
  max_response_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return AttentionTestResult

class MemtraxTestVariant(ReactionTimeTestVariant):
  min_response_time = db.IntegerProperty()
  max_response_time = db.IntegerProperty()

  @classmethod
  def get_result_class(cls):
    return MemtraxTestResult

class UserBaseTestState(models2.UserTestState):
  pass

class UserMultitrialTestState(UserBaseTestState):
  pass

class UserTSRTestState(UserMultitrialTestState):
  pass

class UserReactionTimeTestState(UserTSRTestState):
  pass

class UserLevelEstimationTestState(UserTSRTestState):
  level = db.FloatProperty()

  @classmethod
  def update_state(cls, variant, state_data):
    state = super(UserLevelEstimationTestState, cls).update_state(variant, state_data)
    if state:
      if state_data.has_key('level'):
        state.level = float(state_data['level'])
      return state

  @classmethod
  def update_state_session(cls, session_id, variant, state_data):
    state = super(UserLevelEstimationTestState, cls).update_state_session(session_id, variant, state_data)
    if state:
      if state_data.has_key('level'):
        state.level = float(state_data['level'])
      return state

class VerbalLearningWordList(db.Model):
  hash_value = db.IntegerProperty()
  words = db.ListProperty(str)

  def full_results(self):
    res = [("word", "Word", "", word) for word in self.words]
    return res

  def to_tuple(self):
    return (self.hash_value, self.words)

  @classmethod
  def from_tuple(cls, tup):
    hash_value, words = tup
    result = cls(hash_value = hash_value, words = words)
    result.put()
    return result

  @classmethod
  def hash_list(cls, words):
    return hash(tuple(words))

  @classmethod
  def get_or_create(cls, words):
    h = cls.hash_list(words)
    l = cls.all().filter('hash_value', h).get()
    if not l:
      l = cls(words = words, hash_value = h)
      l.put()
    return l

class UserVerbalLearningTestState(UserBaseTestState):
  word_list = db.ReferenceProperty(VerbalLearningWordList)
  level = db.IntegerProperty()
  fixed_list_size = db.IntegerProperty(default = 0)
  pair_id = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    super(UserVerbalLearningTestState, self).__init__(*args, **kwargs)

  @classmethod
  def get(cls, user, variant):
    state = super(UserVerbalLearningTestState, cls).get(user, variant)
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant, level = variant.default_initial_level)
    return state

  @classmethod
  def get_or_create_anon(cls, session_id, variant):
    state = super(UserVerbalLearningTestState, cls).get_or_create_anon(session_id, variant)
    state.level = variant.default_initial_level
    return state

  def initialize(self):
    super(UserVerbalLearningTestState, self).initialize()
    if not self.level:
      self.level = self.variant().default_initial_level
    self.fixed_list_size = self.variant().fixed_list_size
    # populate word list
    word_list = models2.WordList.all().filter('list_title', self.variant().word_list).get()
    if self.fixed_list_size > 0:
      new_list = word_list.choose_words(self.fixed_list_size * self.variant().max_learning_trials)
    else:
      new_list = word_list.choose_words(self.level)
    self.word_list = VerbalLearningWordList.get_or_create(new_list)
    self.put()

  def word_list_as_javascript(self, with_pair_id = True):
    if (with_pair_id) and (self.pair_id is not None):
      result = "window.pair_id = %d;\n"%(self.pair_id)
    else:
      result = ""
    result += "".join(["window.wordList[%d] = \"%s\";\n"%(i, self.word_list.words[i]) for i in xrange(len(self.word_list.words))])
    return result

class UserVerbalPairedAssociatesTestState(UserBaseTestState):
  level = db.IntegerProperty()
  word_list = db.ReferenceProperty(VerbalLearningWordList)
  pair_id = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    super(UserVerbalPairedAssociatesTestState, self).__init__(*args, **kwargs)

  @classmethod
  def get(cls, user, variant):
    state = super(UserVerbalPairedAssociatesTestState, cls).get(user, variant)
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant, level = variant.default_initial_level)
    return state

  @classmethod
  def get_or_create_anon(cls, session_id, variant):
    state = super(UserVerbalPairedAssociatesTestState, cls).get_or_create_anon(session_id, variant)
    state.level = variant.default_initial_level
    return state

  def initialize(self):
    super(UserVerbalPairedAssociatesTestState, self).initialize()
    if not self.level:
      self.level = self.variant().default_initial_level
    # populate word list
    word_list = models2.WordList.all().filter('list_title', self.variant().word_list).get()
    new_list = word_list.choose_words(2 * int(self.level))
    self.word_list = VerbalLearningWordList.get_or_create(new_list)
    self.put()

  def word_pairs_as_javascript(self, with_pair_id = True):
    if (with_pair_id) and (self.pair_id is not None):
      result = "window.pair_id = %d;\n"%(self.pair_id)
    else:
      result = ""
    result += "".join(["window.wordPairs[%d] = [\"%s\",\"%s\"];\n"%(i, self.word_list.words[i*2], self.word_list.words[i*2+1]) for i in xrange(len(self.word_list.words)//2)])
    return result

class BaseTestResult(models2.CompletedTest):
  duration = db.IntegerProperty()

  @classmethod
  def test_name(cls):
    return "Base"

  @classmethod
  def get_result_variant_name(cls):
    return "base"

  @classmethod
  def adjusted_rt(cls, rt_ss, correct_ss):
    if rt_ss is None:
      return None
    if correct_ss is None:
      return None
    if correct_ss[1] > 0:
      p_correct = correct_ss[1] * 1.0 / correct_ss[0]
      mean_rt = rt_ss[1] * 1.0 / rt_ss[0]
      var_rt = rt_ss[2] * 1.0 / rt_ss[0] - mean_rt**2
      stderr_rt = math.sqrt(var_rt * 1.0 / rt_ss[0])
      adjusted_mean_rt = mean_rt / p_correct
      adjusted_stderr_rt = stderr_rt / p_correct
      return (adjusted_mean_rt, adjusted_stderr_rt)
    return None

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(BaseTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('startTime') and result_data.has_key('endTime'):
        result.duration = result_data['endTime'] - result_data['startTime']
      return result

  @classmethod
  def from_tuple(cls, tup):
    (duration,) = tup
    result = cls()
    result.duration = duration
    return result

  def summary_statistics(self):
    return []

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    return []

  @classmethod
  def statistics_from_ss(cls, ss):
    return []

  @classmethod
  def statistics_from_observations(cls, ss, username, variant_info):
    Y = ss[0]
    for other_ss in ss[1:]:
      cls.update_observations(Y, other_ss)
    if score_params.score_params.has_key(variant_info):
      score = scores.QMOM2_argmax_score(score_params.score_params[variant_info], Y)
      if score is None:
        return []
      return [{'name': 'Score',
               'description': 'Score',
               'explanation': 'A single score that summarizes performance across multiple aspects of the test',
               'unit': 'number',
               'value': score[0] * 100 + 500,
               'stderr': score[1] * 100}]
    W = cls.get_factor_weights(variant_info[1])
    if W is None:
      return []
    factor_info, weights = W
    default_prior = factor_inference.make_default_prior(len(factor_info))
    try:
      X_prior, X_post = cls.get_prior(username, variant_info)
    except Exception, e:
      logging.error("Got exception %s"%e)
      X_prior = None
      X_post = []
    if X_prior is None:
      X_prior = default_prior
    if X_post is None:
      X_post = []
    factor_values, X_Sigma = factor_inference.factor_inference(Y, weights, default_prior)
    if len(X_post) == 0:
      X_post.append((factor_values, X_Sigma))
    mu = [sum([x_post[0][j] for x_post in X_post]) * 1.0 / len(X_post) for j in xrange(len(factor_values))]
    factor_values = [x_i - mu_i for (x_i, mu_i) in zip(factor_values, mu)]
    factor_values = [100 + x_i*15 for x_i in factor_values]
    factor_stderrs = [math.sqrt(X_Sigma[i][i])*15 for i in xrange(len(factor_values))]
    return [{'name': factor_name,
             'description': factor_description,
             'explanation': get_result_explanation(factor_name),
             'unit': 'number',
             'value': factor_value,
             'stderr': factor_stderr}
            for ((factor_name, factor_description), factor_value, factor_stderr) in zip(factor_info, factor_values, factor_stderrs)]

  def to_tuple(self):
    return super(BaseTestResult, self).to_tuple() + (self.duration,)

  def get_all_statistics(self):
    return [("Total duration", time_flexible(self.duration))]

  def get_statistics_old(self):
    return [x for x in self.get_all_statistics() if x[0] not in self.hide_fields()]

  @classmethod
  def get_factor_weights(cls, group = None):
    if group is not None:
      variant_name = cls.get_result_variant_name()
      sub_W = {}
      for k in full_W.keys():
        if k[0] == (variant_name, group):
          sub_W[k] = full_W[k]
      if len(sub_W) > 0:
        return ([('%s_%s'%(variant_name,group), 'Score')],
                sub_W)
    return None

  def get_score(self):
    ss = self.collect_observations()
    variant_info = (self.variant_name, models2.TestVariant.variant_to_group(self.variant_name, self.variant_variant))
    if score_params.score_params.has_key(variant_info):
      score = scores.QMOM2_argmax_score(score_params.score_params[variant_info], ss)
      #score = scores.QMOM2_score(score_params.score_params[variant_info], ss, Z_resolution = 2000, window = [10.0, 10.0])
      if score is not None:
        score = (score[0] * 100 + 500, score[1] * 100)
      return score
    return None

  def get_statistics(self):
    ss = self.collect_observations()
    variant_info = (self.variant_name, models2.TestVariant.variant_to_group(self.variant_name, self.variant_variant))
    if score_params.score_params.has_key(variant_info):
      score = scores.QMOM2_score(score_params.score_params[variant_info], ss, Z_resolution = 2000, window = [-10.0, 10.0])
      if score is None:
        return []
      return [{'name': 'Score',
               'description': 'Score',
               'explanation': 'A single score that summarizes performance across multiple aspects of the test',
               'unit': 'number',
               'value': score[0] * 100 + 500,
               'stderr': score[1] * 100}]
    #stats = self.statistics_from_ss(ss)
    stats = []
    if len(stats) > 0:
      return [{'name': name,
               'description': description,
               'explanation': explanation,
               'unit': 'number',
               'value': value,
               'stderr': stderr}
              for (name, description, explanation, value, stderr) in stats]
    try:
      W = self.get_factor_weights(self.variant().group)
      logging.info("W: %s"%str(W))
      #W = None
      if W is None:
        username = self.get_username()
        variant_info = (self.variant_name, models2.TestVariant.variant_to_group(self.variant_name, self.variant_variant))
        ss = self.summary_statistics()
        sm = self.summary_metrics([ss], username, variant_info)
        return [{'name': name,
               'description': description,
               'explanation': get_result_explanation(name),
               'unit': unit,
               'value': value,
               'stderr': stderr}
              for (name, description, unit, value, stderr) in sm]
      Y = self.collect_observations()
      factor_info, weights = W
      default_prior = factor_inference.make_default_prior(len(factor_info))
      try:
        X_prior, X_post = self.get_my_prior()
      except:
        X_prior = None
        X_post = []
      if X_prior is None:
        X_prior = default_prior
      if X_post is None:
        X_post = []
      #logging.info("%s HJERE1"%str(self.variant().name))
      #logging.info("weights: %s"%str(weights))
      #logging.info("Y: %s"%str(Y))
      factor_values, X_Sigma = factor_inference.factor_inference(Y, weights, default_prior)
      #logging.info("FACTOR_VALUES: %s"%str(factor_values))
      #logging.info("%s HJERE2"%str(self.variant().name))
      if len(X_post) == 0:
        X_post.append((factor_values, X_Sigma))
      #logging.info("X_POST: %s"%str(X_post))
      mu = [sum([x_post[0][j] for x_post in X_post]) * 1.0 / len(X_post) for j in xrange(len(factor_values))]
      #logging.info("MU: %s"%str(mu))
      #if len(X_post) > 1:
      #  v = [sum([(x_post[0][j]-mu[j])**2 for x_post in X_post]) * 1.0 / (len(X_post) - 1.0) for j in xrange(len(factor_values))]
      #else:
      #  v = None
      factor_values = [x_i - mu_i for (x_i, mu_i) in zip(factor_values, mu)]
      #if v is None:
      #  factor_values = [0.0] * len(factor_values)
      #  v = [1.0] * len(factor_values)
      #factor_values = [x_i / math.sqrt(v_i) for (x_i, v_i) in zip(factor_values, v)]
      factor_values = [100 + x_i*15 for x_i in factor_values]
      factor_stderrs = [math.sqrt(X_Sigma[i][i])*15 for i in xrange(len(factor_values))]
      #for factor_stderrs_i in xrange(len(factor_stderrs)):
      #  if factor_stderrs[factor_stderrs_i] is not None:
      #    factor_stderrs[factor_stderrs_i] = factor_stderrs[factor_stderrs_i] / math.sqrt(v[factor_stderrs_i]) * 15
      #factor_stderrs = [None] * len(factor_values)
      return [{'name': factor_name,
               'description': factor_description,
               'explanation': get_result_explanation(factor_name),
               'unit': 'number',
               'value': factor_value,
               'stderr': factor_stderr}
              for ((factor_name, factor_description), factor_value, factor_stderr) in zip(factor_info, factor_values, factor_stderrs)]
    except:
      return []

  def full_results(self):
    res = super(BaseTestResult, self).full_results()
    res.append(('duration', 'Total duration', 'seconds', self.duration//1000))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(BaseTestResult, cls).cumulative_results(results)
    durations = [x.duration//1000 for x in results]
    duration_ss = models2.FloatSummaryStatistic.create(durations)
    duration_cs = models2.CumulativeStatistic.from_float_sss([duration_ss], ["Duration"])
    res.append(duration_cs)
    return res

  @classmethod
  def cumulative_titles(cls):
    return super(BaseTestResult, cls).cumulative_titles() + ["Session duration"]

  @classmethod
  def fields_to_graph(cls):
    return [('duration',)]

  def extra_sections(self):
    return []

  def get_charts(self):
    return []

class MultitrialTrialResult(polymodel.PolyModel):
  trial_i = db.IntegerProperty()
  duration = db.IntegerProperty()

  def full_results(self):
    res = []
    res.append(('trial_number', 'Number', 'Number', self.trial_i))
    res.append(('trial_duration', 'Duration', 'ms', self.duration))
    return res

  @classmethod
  def create(cls, trial, test_result):
    result = cls(trial_i = trial['n'],
                 duration = trial['endTime'] - trial['startTime'])
    return result

  @classmethod
  def get_all_header_fields(cls):
    return ['Number','Duration']

  def get_all_fields(self):
    return [self.trial_i+1, time_flexible(self.duration)]

  @classmethod
  def hide_fields(cls):
    return set([])

  @classmethod
  def get_header_fields(cls):
    return [x for x in cls.get_all_header_fields() if x not in cls.hide_fields()]

  def get_fields(self):
    all_fields = self.get_all_fields()
    all_header_fields = self.get_all_header_fields()
    hidden = self.hide_fields()
    fields = [all_fields[i] for i in xrange(len(all_fields)) if all_header_fields[i] not in hidden]
    return fields

  def to_tuple(self):
    return (self.trial_i, self.duration)

  @classmethod
  def from_tuple(cls, tup):
    trial_i, duration = tup
    result = cls()
    result.trial_i = trial_i
    result.duration = duration
    return result

  def to_string(self):
    return pickle.dump(self.to_tuple())

class MultitrialTestResult(BaseTestResult):
  num_trials = db.IntegerProperty()
  trial_duration_ss = db.ReferenceProperty(models2.FloatSummaryStatistic)
  trial_results = models2.PickleProperty()
  #trial_results = db.ListProperty(db.Key)

  @classmethod
  def test_name(cls):
    return "Multitrial"

  @classmethod
  def get_result_variant_name(cls):
    return "multitrial"

  def collect_trial_observations(self, trial_results, previous_state = None):
    return (None, {})

  def get_trial_durations(self):
    return [x.duration for x in self.get_trial_results()]

  def collect_observations(self, initial_state = None):
    observations = {}
    try:
      trials = self.get_trial_results()
    except:
      trials = None
    if not trials:
      return observations
    state = initial_state
    for trial in trials:
      (state, trial_observations) = self.collect_trial_observations(trial, state)
      self.update_observations(observations, trial_observations)
    return observations

  def full_results(self):
    res = super(MultitrialTestResult, self).full_results()
    res.append(('num_trials', 'Number of trials', 'Trials', self.num_trials))
    res.append(('trial_duration', 'Trial duration', 'parent', self.trial_duration_ss.full_results('Duration', 'ms')))
    trials = [self.get_trial_class().from_tuple(x) for x in self.trial_results]
    for trial in trials:
      res.append(('trial', 'Trial', 'parent', trial.full_results()))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(MultitrialTestResult, cls).fields_to_graph() + [('num_trials',),('trial_duration','mean')]

  @classmethod
  def get_trial_class(cls):
    return MultitrialTrialResult

  def get_trial_results(self):
    cls = self.get_trial_class()
    return [cls.from_tuple(x) for x in self.trial_results]

  def to_tuple(self):
    return super(MultitrialTestResult, self).to_tuple() + (self.num_trials, self.trial_duration_ss.to_tuple(), [x.to_tuple() for x in self.get_trial_results()])

  @classmethod
  def from_tuple(cls, tup):
    num_trials, trial_duration_ss_tup, trial_results_tup = tup[-3:]
    result = super(MultitrialTestResult, cls).from_tuple(tup[:-3])
    result.num_trials = num_trials
    trial_duration_ss = models2.FloatSummaryStatistic.from_tuple(trial_duration_ss_tup)
    trial_duration_ss.put()
    result.trial_duration_ss = trial_duration_ss
    result_trial_class = cls.get_trial_class()
    result.trial_results = trial_results_tup #[result_trial_class.from_tuple(x) for x in trial_results_tup]
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(MultitrialTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('trials'):
        trial_class = cls.get_trial_class()
        trial_results = [trial_class.create(trial, result) for trial in result_data['trials']]
        result.trial_results = [x.to_tuple() for x in trial_results]
        #for trial_result in trial_results:
        #  trial_result.put()
        #result.trial_results = [x.key() for x in trial_results]
        durations = [x.duration for x in trial_results]
        #durations = [x['endTime'] - x['startTime'] for x in result_data['trials']]
        result.trial_duration_ss = models2.FloatSummaryStatistic.create(durations)
        result.num_trials = result.trial_duration_ss.n
      return result

#  def get_results(self):
#    return [db.get(x) for x in self.trial_results]

  def summary_statistics(self):
    return []

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    return []

  def get_all_statistics(self):
    retval = super(MultitrialTestResult, self).get_all_statistics() + [("Number of trials", self.num_trials),
                                                                       ("Trial duration", self.trial_duration_ss.to_html(ms=True))]
    return retval

class GoNoGoTrialResult(MultitrialTrialResult):
  is_target = db.BooleanProperty()
  user_response = db.BooleanProperty()
  reaction_time = db.IntegerProperty()

  def full_results(self):
    res = super(GoNoGoTrialResult, self).full_results()
    res.append(('trial_type', 'Trial type', '', 'Go' if self.is_target else 'No-go'))
    res.append(('user_action', 'User action', '', 'Go' if self.user_response else 'No-go'))
    if self.user_response:
      res.append(('reaction_time', 'Reaction time', 'ms', self.reaction_time))
    return res

  def to_tuple(self):
    return super(GoNoGoTrialResult, self).to_tuple() + (self.is_target, self.user_response, self.reaction_time)

  @classmethod
  def from_tuple(cls, tup):
    is_target, user_response, reaction_time = tup[-3:]
    result = super(GoNoGoTrialResult, cls).from_tuple(tup[:-3])
    result.is_target = is_target
    result.user_response = user_response
    result.reaction_time = reaction_time
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(GoNoGoTrialResult, cls).create(trial, test_result)
    result.is_target = trial['isTarget']
    result.user_response = trial['userResponse']
    if trial.has_key('reactionTime'):
      result.reaction_time = trial['reactionTime']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(GoNoGoTrialResult, cls).get_all_header_fields() + ['Target?', 'Clicked?', 'Reaction time']

  def get_all_fields(self):
    return super(GoNoGoTrialResult, self).get_all_fields() + [self.is_target, self.user_response, time_flexible(self.reaction_time) if self.reaction_time else "n/a"]

  @classmethod
  def hide_fields(cls):
    return super(GoNoGoTrialResult, cls).hide_fields().union(set(['Duration']))

class GoNoGoTestResult(MultitrialTestResult):
  reaction_time_ss = db.ReferenceProperty(models2.FloatSummaryStatistic)
  omissions_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='omissions_set')
  commisions_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='commisions_set')

  @classmethod
  def test_name(cls):
    return "Go/No-Go"

  @classmethod
  def get_result_variant_name(cls):
    return "gonogo"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = {'default':
             ([('gonogo_default', 'Score')],
              {((u'gonogo', u'default'), 'commission', 0, None): ([0.615, -2.949], None),
               ((u'gonogo', u'default'), 'omission', 0, None): ([-0.246, -6.275], None),
               ((u'gonogo', u'default'), 'reaction time', 1, None): ([-47.785, 360.685], 5077.715)}),
           'Inhibition':
             ([('gonogo_Inhibition', 'Score')],
              {((u'gonogo', u'Inhibition'), 'commission', 0, None): ([-1.228, -2.007], None),
               ((u'gonogo', u'Inhibition'), 'omission', 0, None): ([-2.698, -6.933], None),
               ((u'gonogo', u'Inhibition'), 'reaction time', 1, None): ([-23.065, 364.704], 8861.251)}),
#             ([('gonogo_Inhibition', 'Score')],
#              {((u'gonogo', u'Inhibition'), 'commission', 0, None): ([0.437, -1.737], None),
#               ((u'gonogo', u'Inhibition'), 'omission', 0, None): ([-0.045, -5.194], None),
#               ((u'gonogo', u'Inhibition'), 'reaction time', 1, None): ([-56.342, 362.083], 7050.341)}),
           'Attention':
             ([('gonogo_Attention', 'Score')],
              {((u'gonogo', u'Attention'), 'commission', 0, None): ([0.643, -4.369], None),
               ((u'gonogo', u'Attention'), 'omission', 0, None): ([0.632, -5.066], None),
               ((u'gonogo', u'Attention'), 'reaction time', 1, None): ([-28.952, 469.857], 4196.650)})}
    res['default'] = ([('gonogo_default', 'Score')],
                      {((u'gonogo', u'default'), 'commission', 0, None): ([-1.228, -2.007], None),
                       ((u'gonogo', u'default'), 'omission', 0, None): ([-2.698, -6.933], None),
                       ((u'gonogo', u'default'), 'reaction time', 1, None): ([-23.065, 364.704], 8861.251)})
    res['Attention'] = ([('gonogo_Attention', 'Score')],
                        {((u'gonogo', u'Attention'), 'commission', 0, None): ([-1.228, -2.007], None),
                         ((u'gonogo', u'Attention'), 'omission', 0, None): ([-2.698, -6.933], None),
                         ((u'gonogo', u'Attention'), 'reaction time', 1, None): ([-23.065, 364.704], 8861.251)})
    if res.has_key(group_name):
      return res[group_name]
    else:
      return None

  def summary_statistics(self):
    return self.reaction_time_ss.to_summary_statistics() + self.omissions_ss.to_summary_statistics() + self.commisions_ss.to_summary_statistics()

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (om_p, om_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (com_p, com_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if om_p is not None:
      results.append(("omissions", "Omissions", "fraction", om_p, om_p_se))
    if com_p is not None:
      results.append(("commissions", "Commissions", "fraction", com_p, com_p_se))
    return results

  def full_results(self):
    res = super(GoNoGoTestResult, self).full_results()
    res.append(("reaction_time", "Reaction time", "parent", self.reaction_time_ss.full_results("trials", "ms")))
    res.append(("omissions", "Omissions", "parent", self.omissions_ss.full_results()))
    res.append(("commisions", "Commisions", "parent", self.commisions_ss.full_results()))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(GoNoGoTestResult, cls).cumulative_results(results)
    titles = super(GoNoGoTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    correct_rt = []
    commissions = []
    omissions = []
    for result in results:
      trials = result.get_trial_results()
      # identify halves
      trials1 = [x.is_target for x in trials[:len(trials)//2]]
      trials2 = [x.is_target for x in trials[len(trials)//2:]]
      first_half_exciting = trials1.count(True) > trials1.count(False)
      second_half_exciting = trials2.count(True) > trials2.count(False)
      is_exciting = [first_half_exciting] * len(trials1) + [second_half_exciting] * len(trials2)
      prv_commission = False
      for (trial, trial_exciting) in zip(trials, is_exciting):
        if trial.is_target and trial.user_response:
          correct_rt.append((trial_exciting, prv_commission, trial.reaction_time))
        if trial.is_target:
          omissions.append((trial_exciting, prv_commission, not trial.user_response))
        else:
          commissions.append((trial_exciting, prv_commission, trial.user_response))
        prv_commission = (not trial.is_target) and (trial.user_response)
    preds = [[(lambda x: True, "Overall"),
              (lambda x: x[0], "Exciting"),
              (lambda x: not x[0], "Boring"),
              (lambda x: x[1], "Post-commission")]]
    for pred_set in preds:
      pred_set2 = [(pred,title) for (pred,title) in pred_set if len([x for x in correct_rt if pred(x)]) > 1]
      flsss = [models2.FloatSummaryStatistic.create([x[2] for x in correct_rt if pred(x)]) for (pred,title) in pred_set2]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set2]))
      pred_set3 = [(pred,title) for (pred,title) in pred_set if len([x for x in commissions if pred(x)]) > 1]
      pred_set4 = [(pred,title) for (pred,title) in pred_set if len([x for x in omissions if pred(x)]) > 1]
      commisss = [models2.BooleanSummaryStatistic.create([x[2] for x in commissions if pred(x)]) for (pred,title) in pred_set3]
      omissss = [models2.BooleanSummaryStatistic.create([x[2] for x in omissions if pred(x)]) for (pred,title) in pred_set4]
      res.append(models2.CumulativeStatistic.from_boolean_sss(commisss, [x[1] for x in pred_set3]))
      res.append(models2.CumulativeStatistic.from_boolean_sss(omissss, [x[1] for x in pred_set4]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return []

  @classmethod
  def cumulative_titles(cls):
    titles = super(GoNoGoTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["%s"%(measure) for category in ["Overall"] for measure in ["Reaction time", "Commissions", "Omissions"]])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(GoNoGoTestResult, cls).fields_to_graph() + [('reaction_time','mean'),('omissions', 'fraction_positive'),('commisions','fraction_positive')]

  def to_tuple(self):
    return super(GoNoGoTestResult, self).to_tuple() + (self.reaction_time_ss.to_tuple(), self.omissions_ss.to_tuple(), self.commisions_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(GoNoGoTestResult, cls).from_tuple(tup[:-3])
    reaction_time_ss = models2.FloatSummaryStatistic.from_tuple(tup[-3])
    reaction_time_ss.put()
    omissions_ss = models2.BooleanSummaryStatistic.from_tuple(tup[-2])
    omissions_ss.put()
    commisions_ss = models2.BooleanSummaryStatistic.from_tuple(tup[-1])
    commisions_ss.put()
    result.reaction_time_ss = reaction_time_ss
    result.omissions_ss = omissions_ss
    result.commisions_ss = commisions_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(GoNoGoTestResult, cls).make_result(variant, result_data)
    if result:
      trials = result.get_trial_results()
      omissions = [(not x.user_response) for x in trials if x.is_target]
      commisions = [x.user_response for x in trials if not x.is_target]
      result.omissions_ss = models2.BooleanSummaryStatistic.create(omissions)
      result.commisions_ss = models2.BooleanSummaryStatistic.create(commisions)
      reaction_times = [x.reaction_time for x in trials if x.reaction_time is not None and x.is_target and x.user_response]
      result.reaction_time_ss = models2.FloatSummaryStatistic.create(reaction_times)
      return result

  def collect_trial_observations(self, trial, previous_state = None):
    # key:
    # 0 - commission
    # 1 - omission
    # 2 - reaction time
    observations = {}
    if trial.is_target:
      # omission?
      self.add_observation(observations, 'boolean', not trial.user_response, 'omission', None)
      if trial.user_response:
        # add reaction time
        rt = trial.reaction_time
        self.add_observation(observations, 'float', rt, 'reaction time', None)
    else:
      # commission?
      self.add_observation(observations, 'boolean', trial.user_response, 'commission', None)
    return (None, observations)

  @classmethod
  def get_trial_class(cls):
    return GoNoGoTrialResult

  @classmethod
  def hide_fields(cls):
    return super(GoNoGoTestResult, cls).hide_fields().union(set(['Trial duration']))

  def get_all_statistics(self):
    return super(GoNoGoTestResult, self).get_all_statistics() + \
        [("Commisions", self.commisions_ss.to_html()),
         ("Omissions", self.omissions_ss.to_html()),
         ("Reaction time", self.reaction_time_ss.to_html(ms=True))]

class TSRTrialResult(MultitrialTrialResult):
  ready = db.BooleanProperty()
  correct = db.BooleanProperty()
  reaction_time = db.IntegerProperty()

  def full_results(self):
    res = super(TSRTrialResult, self).full_results()
    res.append(('ready', 'Ready', 'boolean', self.ready))
    res.append(('correct', 'Correct', 'boolean', self.correct))
    if self.reaction_time:
      res.append(('reaction_time', 'Reaction time', 'ms', self.reaction_time))
    return res

  def to_tuple(self):
    return super(TSRTrialResult, self).to_tuple() + (self.ready, self.correct, self.reaction_time)

  @classmethod
  def from_tuple(cls, tup):
    ready, correct, reaction_time = tup[-3:]
    result = super(TSRTrialResult, cls).from_tuple(tup[:-3])
    result.ready = ready
    result.correct = correct
    result.reaction_time = reaction_time
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(TSRTrialResult, cls).create(trial, test_result)
    if trial.has_key('ready'):
      result.ready = trial['ready']
    if trial.has_key('responseCorrect'):
      result.correct = trial['responseCorrect']
    if trial.has_key('reactionTime'):
      result.reaction_time = trial['reactionTime']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(TSRTrialResult, cls).get_all_header_fields() + ['Ready?', 'Correct?', 'Reaction time']

  def get_all_fields(self):
    return super(TSRTrialResult, self).get_all_fields() + [self.ready, self.correct, time_flexible(self.reaction_time) if self.reaction_time else "n/a"]

class TSRTestResult(MultitrialTestResult):
  reaction_time_ss = db.ReferenceProperty(models2.FloatSummaryStatistic)
  ready_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='TSR_ready_set')
  correct_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='TSR_correct_set')

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    if trial.ready:
      if trial.correct:
        if True or (100 <= trial.reaction_time <= 5000):
          self.add_observation(observations, 'boolean', True, 'ready', None)
          self.add_observation(observations, 'boolean', True, 'correct', None)
          self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', None)
      else:
        self.add_observation(observations, 'boolean', True, 'ready', None)
        self.add_observation(observations, 'boolean', False, 'correct', None)
    else:
      self.add_observation(observations, 'boolean', False, 'ready', None)
    return (None, observations)

  @classmethod
  def test_name(cls):
    return "Timed Stimulus-Response"

  @classmethod
  def get_result_variant_name(cls):
    return "tsr"

  @classmethod
  def unpack_summary_statistics(cls, ss, username, variant_info):
    #prior = cls.get_prior(username, variant_info)
    prior = None
    if prior is not None:
      (ready_prior, correct_prior, rt_prior) = prior
      (n, sumx, sumx2) = rt_prior
      rt_prior = priors.normal_scaled_inverse_gamma_params_from_ss(n, sumx, sumx2)
    else:
      ready_prior = None
      correct_prior = None
      rt_prior = None
    res = []
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss, prior = rt_prior)
    (ready_p, ready_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss, beta_prior = ready_prior)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss, beta_prior = correct_prior)
    if (rt_mean is not None) and (rt_se is not None):
      res.append((rt_mean, rt_se**2))
    else:
      res.append(None)
    if (ready_p is not None) and (ready_p_se is not None):
      res.append((ready_p, ready_p_se**2))
    else:
      res.append(None)
    if (correct_p is not None) and (correct_p_se is not None):
      res.append((correct_p, correct_p_se**2))
    else:
      res.append(None)
    return res

  @classmethod
  def update_prior(cls, sss, username, variant_info):
    prior = models2.ResultsPrior.get_or_create(username, variant_info[0], variant_info[1])
    ss = sss
    rt_prior, ss = models2.sum_summaries(ss, 3)
    ready_prior, ss = models2.sum_summaries(ss, 2)
    ready_prior = [1 + ready_prior[1], ready_prior[0] - ready_prior[1] + 1]
    correct_prior, ss = models2.sum_summaries(ss, 2)
    correct_prior = [1 + correct_prior[1], correct_prior[0] - correct_prior[1] + 1]
    prior.prior_data = (ready_prior, correct_prior, rt_prior)
    prior.put()
    return False

  def summary_statistics(self):
    reaction_time_ss = (0, 0.0, 0.0)
    if self.reaction_time_ss is not None:
      reaction_time_ss = self.reaction_time_ss.to_summary_statistics()
    ready_ss = (0, 0)
    if self.ready_ss is not None:
      ready_ss = self.ready_ss.to_summary_statistics()
    correct_ss = (0, 0)
    if self.correct_ss is not None:
      correct_ss = self.correct_ss.to_summary_statistics()
    return reaction_time_ss + ready_ss + correct_ss

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (ready_p, ready_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if ready_p is not None:
      results.append(("ready", "Ready", "fraction", ready_p, ready_p_se))
    if correct_p is not None:
      results.append(("correct", "Correct", "fraction", correct_p, correct_p_se))
    return results

  def full_results(self):
    res = super(TSRTestResult, self).full_results()
    res.append(('reaction_times', 'Reaction times', 'parent', self.reaction_time_ss.full_results('reaction times', 'ms')))
    res.append(('ready_trials', 'Ready trials', 'parent', self.ready_ss.full_results()))
    res.append(('correct_trials', 'Correct trials', 'parent', self.correct_ss.full_results()))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(TSRTestResult, cls).fields_to_graph() + [('reaction_times','mean'),('ready_trials', 'fraction_positive'),('correct_trials','fraction_positive')]

  @classmethod
  def cumulative_results(cls, results):
    res = super(TSRTestResult, cls).cumulative_results(results)
    reaction_time_ss = models2.FloatSummaryStatistic.cumulative_results([x.reaction_time_ss for x in results])
    res.append(models2.CumulativeStatistic.from_float_sss([reaction_time_ss], ["Reaction time"]))
    ready_ss = models2.BooleanSummaryStatistic.cumulative_results([x.ready_ss for x in results])
    res.append(models2.CumulativeStatistic.from_boolean_sss([ready_ss], ["Ready"]))
    correct_ss = models2.BooleanSummaryStatistic.cumulative_results([x.correct_ss for x in results])
    res.append(models2.CumulativeStatistic.from_boolean_sss([correct_ss], ["Correct"]))
    return res

  @classmethod
  def cumulative_titles(cls):
    return super(TSRTestResult, cls).cumulative_titles() + ["Overall reaction time", "Overall ready", "Overall correct"]

  def to_tuple(self):
    return super(TSRTestResult, self).to_tuple() + (self.reaction_time_ss.to_tuple(), self.ready_ss.to_tuple(), self.correct_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    reaction_time_ss_tup, ready_ss_tup, correct_ss_tup = tup[-3:]
    result = super(TSRTestResult, cls).from_tuple(tup[:-3])
    reaction_time_ss = models2.FloatSummaryStatistic.from_tuple(reaction_time_ss_tup)
    reaction_time_ss.put()
    result.reaction_time_ss = reaction_time_ss
    ready_ss = models2.BooleanSummaryStatistic.from_tuple(ready_ss_tup)
    ready_ss.put()
    result.ready_ss = ready_ss
    correct_ss = models2.BooleanSummaryStatistic.from_tuple(correct_ss_tup)
    correct_ss.put()
    result.correct_ss = correct_ss
    return result

  @classmethod
  def get_trial_class(cls):
    return TSRTrialResult

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(TSRTestResult, cls).make_result(variant, result_data)
    if result:
      trials = result.get_trial_results()
      #trials = [db.get(x) for x in result.trial_results]
      reaction_times = [x.reaction_time for x in trials if x.reaction_time is not None and x.ready]
      result.reaction_time_ss = models2.FloatSummaryStatistic.create(reaction_times)
      readies = [x.ready for x in trials if x.ready is not None]
      result.ready_ss = models2.BooleanSummaryStatistic.create(readies)
      corrects = [x.correct for x in trials if x.correct is not None and x.ready]
      result.correct_ss = models2.BooleanSummaryStatistic.create(corrects)
      return result

  def get_all_statistics(self):
    return super(TSRTestResult, self).get_all_statistics() + [("Ready", self.ready_ss.to_html()),
                                                              ("Correct", self.correct_ss.to_html()),
                                                              ("Reaction time", self.reaction_time_ss.to_html(ms=True))]

  def get_charts(self):
    charts = super(TSRTestResult, self).get_charts()
    h = statistics.make_histogram([self.reaction_time_ss.raw], min(50, self.reaction_time_ss.n))
    rt = {'type': 'column',
          'title': 'Reaction times',
          'div_id': 'reaction_time_chart',
          'column_names': ['Trial', 'Reaction time'],
          'width': 400,
          'height': 240,
          'data_rows': h}
    charts.append(rt)
    return charts

class ReactionTimeTrialResult(TSRTrialResult):
  delay = db.IntegerProperty()

  def full_results(self):
    res = super(ReactionTimeTrialResult, self).full_results()
    res.append(('delay', 'Delay', 'ms', self.delay))
    return res

  def to_tuple(self):
    return super(ReactionTimeTrialResult, self).to_tuple() + (self.delay,)

  @classmethod
  def from_tuple(cls, tup):
    delay = tup[-1]
    result = super(ReactionTimeTrialResult, cls).from_tuple(tup[:-1])
    result.delay = delay
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(ReactionTimeTrialResult, cls).create(trial, test_result)
    if trial.has_key('delay'):
      result.delay = int(trial['delay'])
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(ReactionTimeTrialResult, cls).get_all_header_fields() + ['Delay']

  def get_all_fields(self):
    return super(ReactionTimeTrialResult, self).get_all_fields() + [time_flexible(self.delay)]

class ReactionTimeTestResult(TSRTestResult):
  @classmethod
  def test_name(cls):
    return "Reaction Time"

  @classmethod
  def get_result_variant_name(cls):
    return "reaction_time"

  @classmethod
  def get_trial_class(cls):
    return ReactionTimeTrialResult

class SimpleReactionTimeTrialResult(ReactionTimeTrialResult):
  pass

class SimpleReactionTimeTestResult(ReactionTimeTestResult):
  @classmethod
  def test_name(cls):
    return "Simple Reaction Time"

  @classmethod
  def get_result_variant_name(cls):
    return "simple_reaction_time"

  #@classmethod
  # def get_factor_weights(cls, group_name):
  #   return ([('simple_reaction_time_default', 'Score')],
  #           {((u'simple_reaction_time', u'default'), 'reaction time', 1, None): ([-42.081, 304.81], 3285.771),
  #            ((u'simple_reaction_time', u'default'), 'ready', 0, None): ([0.761, 4.029], None)})

  def summary_statistics(self):
    return self.reaction_time_ss.to_summary_statistics() + self.ready_ss.to_summary_statistics()

  @classmethod
  def statistics_from_ss(cls, ss):
    variant_k = ('simple_reaction_time', 'default')
    rt_k = (variant_k, 'reaction time', 1, None, ())
    ready_k = (variant_k, 'ready', 0, None, ())
    art = cls.adjusted_rt(ss.get(rt_k, None), ss.get(ready_k, None))
    if art is None:
      score = 0.0
      stderr = 0.0
    else:
      score = (1000.0 - art[0]) / 100.0
      stderr = art[1] / 100.0
    return [('simple_reaction_time_score', 'Simple reaction time score', 'A score that combines speed and accuracy', score, stderr)]

  def collect_trial_observations(self, trial, previous_state = None):
    # key:
    # 0 - ready
    # 1 - reaction_time
    observations = {}
    if trial.ready:
      if True or (100 <= trial.reaction_time <= 2000):
        self.add_observation(observations, 'boolean', True, 'ready', None)
        self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', None)
    else:
      self.add_observation(observations, 'boolean', False, 'ready', None)
    return (None, observations)

  @classmethod
  def initialize_prior(cls):
    rt_prior = (250.0, 0.0, 0.0, 0.0)
    return (rt_prior,)

  @classmethod
  def update_prior(cls, sss, username, variant_info):
    prior = models2.ResultsPrior.get_or_create(username, variant_info[0], variant_info[1])
    if prior.prior_data is None:
      rt_prior = None
    else:
      (rt_prior,) = prior.prior_data
    rts = []
    for ss in sss:
      (rt_mean, rt_se) = models2.FloatSummaryStatistic.from_summary_statistics([ss], prior = rt_prior)[0]
      if rt_mean is not None:
        rts.append(rt_mean)
    nu = len(rts)
    mu_0 = sum(rts) * 1.0 / nu
    alpha = 0.5 * nu
    beta = 0.5 * sum([(rt-mu_0)**2 for rt in rts])
    rt_new_prior = (mu_0, nu, alpha, beta)
    logging.info("new prior: %s"%str(rt_new_prior))
    if (rt_prior is None) or ((rt_new_prior[0] - rt_prior[0])**2 > 1):
      prior.prior_data = (rt_new_prior,)
      prior.put()
      logging.info("New prior: %s"%str(prior.prior_data))
      return True
    return False

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    #(rt_prior,) = cls.get_prior(username, variant_info)
    rt_prior = None
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss, prior = rt_prior)
    (ready_p, ready_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if ready_p is not None:
      results.append(("ready", "Ready", "fraction", ready_p, ready_p_se))
    return results

  @classmethod
  def hide_fields(cls):
    return super(SimpleReactionTimeTestResult, cls).hide_fields().union(set(['Correct']))

  @classmethod
  def get_trial_class(cls):
    return SimpleReactionTimeTrialResult

class ChoiceReactionTimeTrialResult(ReactionTimeTrialResult):
  true_choice = db.IntegerProperty()
  user_choice = db.IntegerProperty()

  def full_results(self):
    res = super(ChoiceReactionTimeTrialResult, self).full_results()
    res.append(('stimulus', 'Stimulus', '', self.true_choice))
    res.append(('response', 'Response', '', self.user_choice))
    return res

  def to_tuple(self):
    return super(ChoiceReactionTimeTrialResult, self).to_tuple() + (self.true_choice, self.user_choice)

  @classmethod
  def from_tuple(cls, tup):
    true_choice, user_choice = tup[-2:]
    result = super(ChoiceReactionTimeTrialResult, cls).from_tuple(tup[:-2])
    result.true_choice = true_choice
    result.user_choice = user_choice
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(ChoiceReactionTimeTrialResult, cls).create(trial, test_result)
    if trial.has_key('type'):
      result.true_choice = trial['type']
    if trial.has_key('userResponse'):
      result.user_choice = trial['userResponse']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(ChoiceReactionTimeTrialResult, cls).get_all_header_fields() + ['Stimulus', 'User response']

  def get_all_fields(self):
    return super(ChoiceReactionTimeTrialResult, self).get_all_fields() + [self.true_choice, self.user_choice]

class AttentionTrialResult(ReactionTimeTrialResult):
  true_choice = db.IntegerProperty()
  user_choice = db.IntegerProperty()
  missed = db.BooleanProperty()

  def full_results(self):
    res = super(AttentionTrialResult, self).full_results()
    res.append(('stimulus', 'Stimulus', '', self.true_choice))
    res.append(('response', 'Response', '', self.user_choice))
    res.append(('missed', 'Missed', '', self.missed))
    return res

  def to_tuple(self):
    return super(AttentionTrialResult, self).to_tuple() + (self.true_choice, self.user_choice, self.missed)

  @classmethod
  def from_tuple(cls, tup):
    true_choice, user_choice, missed = tup[-3:]
    result = super(AttentionTrialResult, cls).from_tuple(tup[:-3])
    result.true_choice = true_choice
    result.user_choice = user_choice
    result.missed = missed
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(AttentionTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulus'):
      result.true_choice = trial['stimulus']
    if trial.has_key('userResponse'):
      result.user_choice = trial['userResponse']
    if trial.has_key('missed'):
      result.missed = trial['missed']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(AttentionTrialResult, cls).get_all_header_fields() + ['Stimulus', 'User response', 'Missed']

  def get_all_fields(self):
    return super(AttentionTrialResult, self).get_all_fields() + [self.true_choice, self.user_choice, self.missed]

class MemtraxTrialResult(ReactionTimeTrialResult):
  image_index = db.IntegerProperty()
  prv_exposures = db.IntegerProperty()
  user_response = db.BooleanProperty()

  def full_results(self):
    res = super(MemtraxTrialResult, self).full_results()
    return res

  def to_tuple(self):
    return super(MemtraxTrialResult, self).to_tuple() + (self.image_index, self.prv_exposures, self.user_response)

  @classmethod
  def from_tuple(cls, tup):
    image_index, prv_exposures, user_response = tup[-3:]
    result = super(MemtraxTrialResult, cls).from_tuple(tup[:-3])
    result.image_index = image_index
    result.prv_exposures = prv_exposures
    result.user_response = user_response
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(MemtraxTrialResult, cls).create(trial, test_result)
    if trial.has_key('imageIndex'):
      result.image_index = trial['imageIndex']
    if trial.has_key('prvAppearances'):
      result.prv_exposures = trial['prvAppearances']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    return result

class ChoiceReactionTimeTestResult(ReactionTimeTestResult):
  @classmethod
  def statistics_from_ss(cls, ss):
    variant_k = ('choice_reaction_time', 'default')
    rt_k = (variant_k, 'reaction time', 1, None, ())
    ready_k = (variant_k, 'ready', 0, None, ())
    correct_k = (variant_k, 'correct', 0, None, ())
    correct_ss = None
    if ss.has_key(ready_k) and ss.has_key(correct_k):
      correct_ss = (ss[ready_k][0], ss[correct_k][1])
    art = cls.adjusted_rt(ss.get(rt_k, None), correct_ss)
    if art is None:
      score = 0.0
      stderr = 0.0
    else:
      score = (1000.0 - art[0]) / 100.0
      stderr = art[1] / 100.0
    return [('choice_reaction_time_score', 'Choice reaction time score', 'A score that combines speed and accuracy', score, stderr)]

  @classmethod
  def get_factor_weights(self, group_name):
    return ([('choice_reaction_time_default', 'Score')],
            {((u'choice_reaction_time', u'default'), 'correct', 0, None): ([0.371, 3.498], None), # flipped sign
             ((u'choice_reaction_time', u'default'), 'ready', 0, None): ([0.309, 4.278], None), # flipped sign
             ((u'choice_reaction_time', u'default'), 'reaction time', 1, None): ([-77.767, 415.143], 6669.731)})
    #return None
    factor_info = [('choice_reaction_time_score', 'Score')]
    W = {(('choice_reaction_time', 'default'), 'correct', 0, None):
           ([-0.32362689,  3.48966099], None),
         (('choice_reaction_time', 'default'), 'reaction time', 1, None):
           ([ -89.90048296,  420.00593182], 7298.5784182411635),
         (('choice_reaction_time', 'default'), 'ready', 0, None):
           ([-0.17030513,  4.24731103], None)}
    return (factor_info, W)
    factor_info = [('speed', 'Speed'),
                   ('rush', 'Rush')]
    W = {('Choice Reaction Time', 'reaction time', 1, None):
           ([ -62.19417602,  -62.71752353,  415.62074811], 8328.023127013594),
         ('Choice Reaction Time', 'ready', 0, None):
           ([ 1.37129326, -1.52792763,  4.78742999], None),
         ('Choice Reaction Time', 'correct', 0, None):
           ([ 1.08820617, -1.40393838,  3.83221359], None)}
    return (factor_info, W)

  @classmethod
  def test_name(cls):
    return "Choice Reaction Time"

  @classmethod
  def get_result_variant_name(cls):
    return "choice_reaction_time"

  @classmethod
  def get_trial_class(cls):
    return ChoiceReactionTimeTrialResult

  @classmethod
  def cumulative_results(cls, results):
    res = super(ChoiceReactionTimeTestResult, cls).cumulative_results(results)
    titles = super(ChoiceReactionTimeTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    switch_rt = []
    nonswitch_rt = []
    switch_ready = []
    switch_correct = []
    nonswitch_ready = []
    nonswitch_correct = []
    n_overall = 0
    n_switch = 0
    n_nonswitch = 0
    correct_overall = 0
    correct_switch = 0
    correct_nonswitch = 0
    for result in results:
      trials = result.get_trial_results()
      prv = None
      for trial in trials:
        n_overall += 1
        if (prv is None) or (trial.true_choice != prv):
          switch_ready.append(trial.ready)
          n_switch += 1
          if trial.ready:
            correct_switch += 1
            switch_correct.append(trial.correct)
            if trial.correct:
              switch_rt.append(trial.reaction_time)
          prv = trial.true_choice
        else:
          n_nonswitch += 1
          nonswitch_ready.append(trial.ready)
          if trial.ready:
            correct_nonswitch += 1
            nonswitch_correct.append(trial.correct)
            if trial.correct:
              nonswitch_rt.append(trial.reaction_time)
    correct_overall = correct_switch + correct_nonswitch
    fraction_correct_overall = correct_overall * 1.0 / n_overall
    fraction_correct_switch = correct_switch * 1.0 / n_switch
    fraction_correct_nonswitch = correct_nonswitch * 1.0 / n_nonswitch
    overallrt_ss = models2.FloatSummaryStatistic.create(switch_rt + nonswitch_rt)
    coverallrt_ss = models2.FloatSummaryStatistic.create([x / fraction_correct_overall for x in switch_rt + nonswitch_rt])
    overallready_ss = models2.BooleanSummaryStatistic.create(switch_ready + nonswitch_ready)
    overallcorrect_ss = models2.BooleanSummaryStatistic.create(switch_correct + nonswitch_correct)
    switchrt_ss = models2.FloatSummaryStatistic.create(switch_rt)
    cswitchrt_ss = models2.FloatSummaryStatistic.create([x / fraction_correct_switch for x in switch_rt])
    switchready_ss = models2.BooleanSummaryStatistic.create(switch_ready)
    switchcorrect_ss = models2.BooleanSummaryStatistic.create(switch_correct)
    nonswitchrt_ss = models2.FloatSummaryStatistic.create(nonswitch_rt)
    cnonswitchrt_ss = models2.FloatSummaryStatistic.create([x / fraction_correct_nonswitch for x in nonswitch_rt])
    nonswitchready_ss = models2.BooleanSummaryStatistic.create(nonswitch_ready)
    nonswitchcorrect_ss = models2.BooleanSummaryStatistic.create(nonswitch_correct)
    res.append(models2.CumulativeStatistic.from_float_sss([overallrt_ss, switchrt_ss, nonswitchrt_ss], ["Overall", "Switch", "Nonswitch"]))
    res.append(models2.CumulativeStatistic.from_float_sss([coverallrt_ss, cswitchrt_ss, cnonswitchrt_ss], ["Overall", "Switch", "Nonswitch"]))
    res.append(models2.CumulativeStatistic.from_boolean_sss([overallready_ss, switchready_ss, nonswitchready_ss], ["Overall", "Switch", "Nonswitch"]))
    res.append(models2.CumulativeStatistic.from_boolean_sss([overallcorrect_ss, switchcorrect_ss, nonswitchcorrect_ss], ["Overall", "Switch", "Nonswitch"]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(ChoiceReactionTimeTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Reaction time", "Corrected reaction time", "Ready", "Correct"])
    return titles

class AttentionTestResult(ReactionTimeTestResult):
  missed_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='missed_set')

  @classmethod
  def test_name(cls):
    return "Attentional Focus"

  @classmethod
  def get_result_variant_name(cls):
    return "attention"

  @classmethod
  def get_factor_weights(cls, group_name):
    return ([('attention_default', 'Score')],
            {((u'attention', u'default'), 'correct', 0, None): ([1.228, 1.694], None),
             ((u'attention', u'default'), 'missed', 0, None): ([-1.239, -1.821], None),
             ((u'attention', u'default'), 'ready', 0, None): ([1.237, 1.738], None),
             ((u'attention', u'default'), 'reaction time', 1, None): ([-135.905, 701.822], 96117.572)})

    W = {('Attentional Focus', 'missed', 0, None):
           ([-1.68005939, -1.79881454], None),
         ('Attentional Focus', 'ready', 0, None):
           ([ 1.62832138,  1.66628549], None),
         ('Attentional Focus', 'correct', 0, None):
           ([ 1.65621045,  1.66707256], None),
         ('Attentional Focus', 'reaction time', 1, None):
           ([-183.20313112,  709.78131001], 85937.690778991731)}
    factor_info = [('attention_score', 'Score')]
    return (factor_info, W)

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    self.add_observation(observations, 'boolean', trial.ready, 'ready', None)
    if trial.ready:
      self.add_observation(observations, 'boolean', trial.correct, 'correct', None)
      self.add_observation(observations, 'boolean', trial.missed, 'missed', None)
      if trial.correct:
        self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', None)
    return (None, observations)

  def summary_statistics(self):
    return self.reaction_time_ss.to_summary_statistics() + self.ready_ss.to_summary_statistics() + self.correct_ss.to_summary_statistics() + self.missed_ss.to_summary_statistics()

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (ready_p, ready_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (missed_p, missed_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if ready_p is not None:
      results.append(("ready", "Ready", "fraction", ready_p, ready_p_se))
    if correct_p is not None:
      results.append(("correct", "Correct", "fraction", correct_p, correct_p_se))
    if missed_p is not None:
      results.append(("missed", "Missed", "fraction", missed_p, missed_p_se))
    return results

  def full_results(self):
    res = super(AttentionTestResult, self).full_results()
    res.append(('missed_trials', 'Missed trials', 'parent', self.missed_ss.full_results()))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(AttentionTestResult, cls).fields_to_graph() + [('missed_trials', 'fraction_positive')]

  @classmethod
  def get_trial_class(cls):
    return AttentionTrialResult

  @classmethod
  def cumulative_results(cls, results):
    res = super(AttentionTestResult, cls).cumulative_results(results)
    titles = super(AttentionTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    missed_ss = models2.BooleanSummaryStatistic.cumulative_results([x.missed_ss for x in results])
    res.append(models2.CumulativeStatistic.from_boolean_sss([missed_ss], ["Missed"]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return []

  @classmethod
  def cumulative_titles(cls):
    titles = super(AttentionTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Overall missed"])
    return titles

  def to_tuple(self):
    return super(AttentionTestResult, self).to_tuple() + (self.missed_ss.to_tuple(),)

  @classmethod
  def from_tuple(cls, tup):
    (missed_ss_tup,) = tup[-1:]
    result = super(AttentionTestResult, cls).from_tuple(tup[:-1])
    missed_ss = models2.BooleanSummaryStatistic.from_tuple(missed_ss_tup)
    missed_ss.put()
    result.missed_ss = missed_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(AttentionTestResult, cls).make_result(variant, result_data)
    if result:
      trials = result.get_trial_results()
      misseds = [x.missed for x in trials if x.missed is not None and x.ready]
      result.missed_ss = models2.BooleanSummaryStatistic.create(misseds)
      return result

  def get_all_statistics(self):
    return super(AttentionTestResult, self).get_all_statistics() + [("Missed", self.missed_ss.to_html())]

class MemtraxTestResult(ReactionTimeTestResult):
  omissions_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='memtrax_omissions_set')
  commissions_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='memtrax_commisions_set')

  def summary_statistics(self):
    return self.reaction_time_ss.to_summary_statistics() + self.omissions_ss.to_summary_statistics() + self.commissions_ss.to_summary_statistics()

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (omissions_p, omissions_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (commissions_p, commissions_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    #logging.info("here %s %s %s"%(str(rt_mean), str(commissions_p), str(omissions_p)))
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if omissions_p is not None:
      results.append(("omissions", "Omissions", "fraction", omissions_p, omissions_p_se))
    if commissions_p is not None:
      results.append(("commissions", "Commissions", "fraction", commissions_p, commissions_p_se))
    return results

  @classmethod
  def test_name(cls):
    return "Memtrax"

  @classmethod
  def get_result_variant_name(cls):
    return "memtrax"

  @classmethod
  def get_factor_weights(cls, group_name):
    return None

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    is_omission = (trial.prv_exposures > 0) and (not trial.correct)
    is_commission = (trial.prv_exposures == 0) and (not trial.correct)
    self.add_observation(observations, 'boolean', is_omission, 'omission', None)
    self.add_observation(observations, 'boolean', is_commission, 'commission', None)
    if trial.reaction_time:
      self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', None)
    return (None, observations)

  @classmethod
  def get_trial_class(cls):
    return MemtraxTrialResult

  def to_tuple(self):
    return super(MemtraxTestResult, self).to_tuple() + (self.omissions_ss.to_tuple(), self.commissions_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(MemtraxTestResult, cls).from_tuple(tup[:-2])
    omissions_ss = models2.BooleanSummaryStatistic.from_tuple(tup[-2])
    omissions_ss.put()
    commissions_ss = models2.BooleanSummaryStatistic.from_tuple(tup[-1])
    commissions_ss.put()
    result.omissions_ss = omissions_ss
    result.commissions_ss = commissions_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(MemtraxTestResult, cls).make_result(variant, result_data)
    if result:
      trials = result.get_trial_results()
      reaction_times = [x.reaction_time for x in trials if x.reaction_time is not None]
      result.reaction_time_ss = models2.FloatSummaryStatistic.create(reaction_times)
      corrects = [x.correct for x in trials if x.correct is not None]
      result.correct_ss = models2.BooleanSummaryStatistic.create(corrects)
      omissions = [(x.prv_exposures > 0) and (not x.correct) for x in trials if x.correct is not None]
      result.omissions_ss = models2.BooleanSummaryStatistic.create(omissions)
      commissions = [(x.prv_exposures == 0) and (not x.correct) for x in trials if x.correct is not None]
      result.commissions_ss = models2.BooleanSummaryStatistic.create(commissions)
    return result

  def full_results(self):
    res = super(MemtraxTestResult, self).full_results()
    return res

  def get_all_statistics(self):
    return super(MemtraxTestResult, self).get_all_statistics() + \
        [("Commissions", self.commissions_ss.to_html()),
         ("Omissions", self.omissions_ss.to_html()),
         ("Reaction time", self.reaction_time_ss.to_html(ms=True))]

class LevelEstimationTrialResult(TSRTrialResult):
  level = db.FloatProperty()

  def full_results(self):
    res = super(LevelEstimationTrialResult, self).full_results()
    res.append((self.get_level_name().replace(' ','_').lower(), self.get_level_name(), '', self.level))
    return res

  def to_tuple(self):
    return super(LevelEstimationTrialResult, self).to_tuple() + (self.level,)

  @classmethod
  def from_tuple(cls, tup):
    (level,) = tup[-1:]
    result = super(LevelEstimationTrialResult, cls).from_tuple(tup[:-1])
    result.level = level
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(LevelEstimationTrialResult, cls).create(trial, test_result)
    if trial.has_key('level'):
      result.level = float(trial['level'])
    return result

  @classmethod
  def get_level_name(cls):
    return "level"

  def get_level(self):
    return self.level

  @classmethod
  def get_all_header_fields(cls):
    return super(LevelEstimationTrialResult, cls).get_all_header_fields() + [cls.get_level_name()]

  def get_all_fields(self):
    return super(LevelEstimationTrialResult, self).get_all_fields() + [self.get_level()]

  @classmethod
  def hide_fields(cls):
    return super(LevelEstimationTrialResult, cls).hide_fields().union(set(['Ready?', 'Reaction time']))

class LevelEstimationTestResult(TSRTestResult):
  level_ss = db.ReferenceProperty(models2.LevelSummaryStatistic)

  @classmethod
  def test_name(cls):
    return "Level Estimation"

  @classmethod
  def get_result_variant_name(cls):
    return "level_estimation"

  def summary_statistics(self):
    return self.level_ss.to_summary_statistics()

  @classmethod
  def initialize_prior(cls):
    return None

  @classmethod
  def update_prior(cls, sss, username, variant_info):
    prior = models2.ResultsPrior.get_or_create(username, variant_info[0], variant_info[1])
    level_prior = prior.prior_data
    if level_prior is None:
      level_prior = (None, None, None, None)
    counts = models2.sum_summaries(sss, 0)[0]
    if len(counts) == 0:
      return False
    level_prior = (level_prior[0], level_prior[1], min(counts.keys()), max(counts.keys()))
    level_estimates = []
    for ss in sss:
      (level_estimate, level_estimate_se) = models2.LevelSummaryStatistic.from_summary_statistics([ss], cls.a_vals(), cls.guessing_p(), prior = level_prior)[0]
      if level_estimate is not None:
        level_estimates.append(level_estimate)
    if len(level_estimates) > 2:
      prior_mean = sum(level_estimates) * 1.0 / len(level_estimates)
      prior_variance = sum([(x - prior_mean)**2 for x in level_estimates]) / (len(level_estimates) - 1.0)
      prior.prior_data = (prior_mean, prior_variance, level_prior[2], level_prior[3])
      prior.put()
      if (level_prior[0] is None) or ((level_prior[0] - prior_mean)**2 > 0.0001):
        logging.info("New prior: %s"%str(prior.prior_data))
        return True
    return False

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    prior = cls.get_prior(username, variant_info)
    prior = None
    logging.info("prior: %s"%str(prior))
    (level_mean, level_se), ss = models2.LevelSummaryStatistic.from_summary_statistics(ss, cls.a_vals(), cls.guessing_p(), prior = prior)
    results = []
    if level_mean is not None:
      level_name = cls.level_name()
      results.append(("%s"%level_name.replace(" ","_"), "Estimated %s"%(level_name), "number", level_mean, level_se))
    return results

  def full_results(self):
    res = super(LevelEstimationTestResult, self).full_results()
    res.append(("estimated_level", "Estimated level", "", self.level_ss.level_estimate))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(LevelEstimationTestResult, cls).cumulative_results(results)
    titles = super(LevelEstimationTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    positives = []
    negatives = []
    for result in results:
      trials = result.get_trial_results()
      for trial in trials:
        if trial.correct:
          positives.append(trial.level)
        else:
          negatives.append(trial.level)
    level_ss = models2.LevelSummaryStatistic.create(positives, negatives, cls.a_vals(), cls.guessing_p())
    res.append(models2.CumulativeStatistic.from_level_sss([level_ss], ["Estimated span"]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(LevelEstimationTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Estimated %s"%(cls.level_name())])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(LevelEstimationTestResult, cls).fields_to_graph() + [('estimated_level',)]

  def to_tuple(self):
    return super(LevelEstimationTestResult, self).to_tuple() + (self.level_ss.to_tuple(),)

  @classmethod
  def from_tuple(cls, tup):
    level_ss_tup = tup[-1]
    result = super(LevelEstimationTestResult, cls).from_tuple(tup[:-1])
    level_ss = models2.LevelSummaryStatistic.from_tuple(level_ss_tup)
    level_ss.put()
    result.level_ss = level_ss
    return result

  @classmethod
  def a_vals(cls):
    return [-0.25,-0.5,-1.0,-2.0,-4.0]

  @classmethod
  def guessing_p(cls):
    return 0.0

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    level_name = self.level_name()
    self.add_observation(observations, 'boolean', trial.correct, 'correct', None, (trial.level,))
    return (None, observations)

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(LevelEstimationTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('trials'):
        positives = [x['level'] for x in result_data['trials'] if x.has_key('ready') and x.has_key('responseCorrect') and x['ready'] and x['responseCorrect']]
        negatives = [x['level'] for x in result_data['trials'] if x.has_key('ready') and x.has_key('responseCorrect') and x['ready'] and not x['responseCorrect']]
        result.level_ss = models2.LevelSummaryStatistic.create(positives, negatives, cls.a_vals(), cls.guessing_p())
      return result

  @classmethod
  def level_name(cls):
    return "level"

  def get_all_statistics(self):
    return super(LevelEstimationTestResult, self).get_all_statistics() + [("Estimated %s"%self.level_name(), self.level_ss.level_estimate)]

  @classmethod
  def get_trial_class(cls):
    return LevelEstimationTrialResult

  @classmethod
  def hide_fields(cls):
    return super(LevelEstimationTestResult, cls).hide_fields().union(['Ready', 'Reaction time'])

class SpanTrialResult(LevelEstimationTrialResult):
  @classmethod
  def get_level_name(cls):
    return "Span"

class SpanTestResult(LevelEstimationTestResult):
  # def collect_observations(self):
  #   v = self.variant()
  #   group_name = v.group
  #   observations = {}
  #   trials = self.get_trial_results()
  #   if not trials:
  #     return observations
  #   for trial in trials:
  #     self.add_observation(observations, 'boolean', trial.correct, 'correct', None, (trial.level,))
  #   return observations

  @classmethod
  def test_name(cls):
    return "Span"

  @classmethod
  def get_result_variant_name(cls):
    return "span"

  @classmethod
  def level_name(cls):
    return "span"

def count_commisions_omissions(list1, list2):
  slist1 = set([x for x in list1 if len(x.strip()) > 0])
  slist2 = set([x for x in list2 if len(x.strip()) > 0])
  return len(slist2.difference(slist1)),len(slist1.difference(slist2))

class VerbalLearningExposureResult(db.Model):
  user_words = db.ReferenceProperty(VerbalLearningWordList)
  commisions = db.IntegerProperty()
  omissions = db.IntegerProperty()

  def full_results(self):
    res = [("commisions", "Commisions", "", self.commisions),
           ("omissions", "Omissions", "", self.omissions),
           ("words", "Words", "parent", self.user_words.full_results())]
    return res

  def words_correctly_recalled(self):
    return len([x for x in self.user_words.words if len(x.strip()) > 0]) - self.commisions

  def to_tuple(self):
    return (self.user_words.hash_value, self.user_words.words, self.commisions, self.omissions)
#    return (self.user_words.hash_value, self.commisions, self.omissions)

  @classmethod
  def from_tuple(cls, tup):
    (hash_value, words, commisions, omissions) = tup
#    (hash_value, commisions, omissions) = tup
    word_list = VerbalLearningWordList.all().filter('hash_value', hash_value).get()
    if not word_list:
      word_list = VerbalLearningWordList.get_or_create(words)
      #return None
    result = cls(user_words = word_list, commisions = commisions, omissions = omissions)
    return result

  @classmethod
  def create(cls, word_list, user_word_list):
    words = VerbalLearningWordList.get_or_create(word_list)
    user_words = VerbalLearningWordList.get_or_create(user_word_list)
    commisions, omissions = count_commisions_omissions(word_list, user_word_list)
    exposure = cls(words = words, user_words = user_words, commisions = commisions, omissions = omissions)
    exposure.put()
    return exposure

  def all_correct(self):
    return (self.commisions == 0) and (self.omissions == 0)

class VerbalLearningTestResult(BaseTestResult):
  n_exposures = db.IntegerProperty()
  level = db.IntegerProperty()
  words = db.ReferenceProperty(VerbalLearningWordList)
  exposures = db.ListProperty(db.Key)
  final_correct = db.BooleanProperty()
  total_words_shown = db.IntegerProperty()
  pair_id = db.IntegerProperty()

  @classmethod
  def test_name(cls):
    return "Verbal Learning"

  @classmethod
  def get_result_variant_name(cls):
    return "verbal_learning"

  def __init__(self, *args, **kwargs):
    super(VerbalLearningTestResult, self).__init__(*args, **kwargs)

  def collect_observations(self):
    observations = {}
    true_words = set([x.strip().lower() for x in self.words.words if len(x.strip()) > 0])
    user_all_words = set()
    for exposurek in self.exposures:
      exposure = db.get(exposurek)
      for user_word in exposure.user_words.words:
        user_all_words.add(user_word.lower().strip())
    for user_word in user_all_words:
      self.add_observation(observations, 'boolean', user_word in true_words, 'precision')
    for true_word in true_words:
      self.add_observation(observations, 'boolean', true_word in user_all_words, 'recall')
    return observations

  def full_results(self):
    res = super(VerbalLearningTestResult, self).full_results()
    res.append(("n_exposures", "Number of exposures", "", self.n_exposures))
    res.append(("n_words", "Number of words", "", self.level))
    res.append(("success", "Success", "", self.final_correct))
    res.append(("words", "Words", "parent", self.words.full_results()))
    res.append(("exposures", "Exposures", "parent", [("exposure", "Exposure", "parent", db.get(x).full_results()) for x in self.exposures]))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(VerbalLearningTestResult, cls).fields_to_graph() + [('n_words',),('success',)]

  def to_tuple(self):
    return super(VerbalLearningTestResult, self).to_tuple() + (self.n_exposures, self.level, self.words.to_tuple(), [db.get(x).to_tuple() for x in self.exposures], self.final_correct)

  @classmethod
  def from_tuple(cls, tup):
    result = super(VerbalLearningTestResult, cls).from_tuple(tup[:-5])
    result.n_exposures, result.level, result.final_correct = (tup[-5], tup[-4], tup[-1])
    words = VerbalLearningWordList.from_tuple(tup[-3])
    words.put()
    result.words = words
    for exposure_tup in tup[-2]:
      expo = VerbalLearningExposureResult.from_tuple(exposure_tup)
      if not expo: return None
      expo.put()
      result.exposures.append(expo.key())
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(VerbalLearningTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('pair_id'):
        try:
          pair_id = int(result_data['pair_id'])
          result.pair_id = pair_id
        except:
          return None
      if result_data.has_key('totalWordsShown'):
        result.total_words_shown = result_data['totalWordsShown']
        with_replacement = True
      else:
        with_replacement = False
      if result_data.has_key('wordList'):
        result.words = VerbalLearningWordList.get_or_create(result_data['wordList'])
        result.level = len(result.words.words)
      if result_data.has_key('userWords'):
        if with_replacement:
          result.exposures = [VerbalLearningExposureResult.create(y, x).key() for (y,x) in zip(result_data['shownWords'], result_data['userWords'])]
        else:
          result.exposures = [VerbalLearningExposureResult.create(result.words.words, x).key() for x in result_data['userWords']]
        result.n_exposures = len(result.exposures)
        result.final_correct = db.get(result.exposures[-1]).all_correct()
        state = result.variant().get_state_class().get(result.test_session.user, result.variant())
        if state:
          if result.final_correct:
            # all correct, increase level
            increase_level = True
            #if random.random() < (1-result.variant().target_success_probability):
            #  increase_level = True
            #else:
            #  increase_level = False
            if increase_level:
              state.level += 1
              state.put()
          else:
            if (state.level > 1):
              decrease_level = True
              #if random.random() < result.variant().target_success_probability:
              #  decrease_level = True
              if decrease_level:
                state.level -= 1
                state.put()
      return result

  def get_all_statistics(self):
    exposures = [db.get(x) for x in self.exposures]
    res = super(VerbalLearningTestResult, self).get_all_statistics()
    if self.total_words_shown:
      res = res + [("Total words shown", "%d"%(self.total_words_shown))]
      res = res + [("Correct", ",".join(["%d"%(len(x.user_words.words) - x.commisions) for x in exposures]))]
    else:
      res = res + [("Commisions", ",".join(["%d"%x.commisions for x in exposures])),
                   ("Omissions", ",".join(["%d"%x.omissions for x in exposures]))]
      res = res + [("Correct", self.final_correct)]
    return res

  def summary_statistics(self):
    exposures = [db.get(x) for x in self.exposures]
    if self.total_words_shown:
      correct = sum([x.words_correctly_recalled() for x in exposures])
      return (1, correct, correct**2)
    else:
      return (0, 0.0, 0.0)

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (correct_mean, correct_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    results = []
    if correct_mean is not None:
      results.append(("correct", "Correct words", "number", correct_mean, correct_se))
    return results

  @classmethod
  def hide_fields(cls):
    return super(VerbalLearningTestResult, cls).hide_fields().union([])

  def extra_sections(self):
    if not self.total_words_shown:
      return super(VerbalLearningTestResult, self).extra_sections() + [{'title': 'Word lists', 'content': self.word_list_html()}]

  def word_list_html(self):
    return "<p>Actual list: %s</p>\n"%(",".join(self.words.words)) + "".join(["<p>List %d: %s</p>\n"%(i+1, ",".join(db.get(self.exposures[i]).user_words.words)) for i in xrange(len(self.exposures))])

class UserVerbalLearning2TestState(UserBaseTestState):
  parent_result = db.ReferenceProperty(VerbalLearningTestResult)
  parent_state = db.ReferenceProperty(UserVerbalLearningTestState)
  pair_id = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    super(UserVerbalLearning2TestState, self).__init__(*args, **kwargs)

  @classmethod
  def get(cls, user, variant):
    state = super(UserVerbalLearning2TestState, cls).get(user, variant)
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant)
    return state

  def word_list_as_javascript(self):
    res = "window.parentID = %d;\nwindow.pair_id = %d;\n"%(self.parent_result.key().id(), self.pair_id or 0) + self.parent_state.word_list_as_javascript(with_pair_id = False)
    if self.parent_state.fixed_list_size > 0:
      res = res + "window.wordsShown = %d;\n"%(self.parent_result.total_words_shown)
    return res

class VerbalLearning2TestResult(BaseTestResult):
  parent_result = db.ReferenceProperty(VerbalLearningTestResult)
  exposure = db.ReferenceProperty(VerbalLearningExposureResult)
  pair_id = db.IntegerProperty()

  def collect_observations(self):
    observations = {}
    if (self.parent_result is not None) and (self.parent_result.words is not None):
      true_words = set([x.strip().lower() for x in self.parent_result.words.words if len(x.strip()) > 0])
      user_all_words = set()
      for user_word in self.exposure.user_words.words:
        if len(user_word.strip()) > 0:
          user_all_words.add(user_word.strip().lower())
      for user_word in user_all_words:
        self.add_observation(observations, 'boolean', user_word in true_words, 'delayed_precision')
      for true_word in true_words:
        self.add_observation(observations, 'boolean', true_word in user_all_words, 'delayed_recall')
    return observations

  @classmethod
  def test_name(cls):
    return "Verbal Learning 2"

  @classmethod
  def get_result_variant_name(cls):
    return "verbal_learning_2"

  def __init__(self, *args, **kwargs):
    super(VerbalLearning2TestResult, self).__init__(*args, **kwargs)

  def full_results(self):
    res = super(VerbalLearning2TestResult, self).full_results()
    res.append(("n_words", "Number of words", "", self.parent_result.level))
    res.append(("words", "Words", "parent", self.parent_result.words.full_results()))
    res.append(("response", "Response", "parent", self.exposure.full_results()))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(VerbalLearning2TestResult, cls).fields_to_graph() + [('n_words',),('response','commisions'),('response','omissions')]

  def to_tuple(self):
    return super(VerbalLearning2TestResult, self).to_tuple() + (self.parent_result.words.hash_value, self.exposure.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(VerbalLearning2TestResult, cls).from_tuple(tup[:-2])
    word_list = VerbalLearningWordList.all().filter('hash_value', tup[-2]).get()
    if not word_list:
      return None
    parent_result = VerbalLearningTestResult.all().filter('words', word_list).get()
    if not parent_result:
      return None
    result.parent_result = parent_result
    exposure = VerbalLearningExposureResult.from_tuple(tup[-1])
    exposure.put()
    result.exposure = exposure
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(VerbalLearning2TestResult, cls).make_result(variant, result_data)
    if result_data.has_key('pair_id'):
      try:
        pair_id = int(result_data['pair_id'])
        result.pair_id = pair_id
      except:
        return None
    if result_data.has_key('parentID'):
      try:
        parent_result = VerbalLearningTestResult.get_by_id(result_data['parentID'])
        if ((parent_result.test_session.is_anonymous and result.test_session.is_anonymous)) or (parent_result.test_session.user.key() == result.test_session.user.key()):
          result.parent_result = parent_result
      except:
        return None
      if parent_result.total_words_shown:
        result.exposure = VerbalLearningExposureResult.create(result.parent_result.words.words[:parent_result.total_words_shown], result_data['userWords'])
      else:
        result.exposure = VerbalLearningExposureResult.create(result.parent_result.words.words, result_data['userWords'])
    return result

  def get_all_statistics(self):
    res = super(VerbalLearning2TestResult, self).get_all_statistics()
    if self.parent_result.total_words_shown:
      res = res + [("Correct", "%d / %d"%(len(self.exposure.user_words.words) - self.exposure.commisions, self.parent_result.total_words_shown))]
    else:
      res = res + [("Commisions", self.exposure.commisions),
                   ("Omissions", self.exposure.omissions)]
    return res

  def summary_statistics(self):
    if self.parent_result.total_words_shown:
      correct = self.exposure.words_correctly_recalled()
      return (1, correct, correct**2, self.parent_result.total_words_shown, correct)
    else:
      return (0, 0.0, 0.0, 0, 0)

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (correct_mean, correct_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if correct_mean is not None:
      results.append(("correct", "Recalled words", "number", correct_mean, correct_se))
    if correct_p is not None:
      results.append(("fraction_recalled", "Fraction recalled", "number", correct_p, correct_p_se))
    return results

  @classmethod
  def hide_fields(cls):
    return super(VerbalLearning2TestResult, cls).hide_fields().union([])

  def extra_sections(self):
    res = super(VerbalLearning2TestResult, self).extra_sections()
    if not self.parent_result.total_words_shown:
      res = res + [{'title': 'Words', 'content': self.word_list_html()}]
    return res

  def word_list_html(self):
    return "<p>Original list: %s</p>\n"%(",".join(self.parent_result.words.words)) + "<p>Input list: %s</p>\n"%(",".join(self.exposure.user_words.words))

class PairedAssociatesTrialResult(LevelEstimationTrialResult):
  cues = db.ListProperty(str)
  associates = db.ListProperty(str)
  user_associates = db.ListProperty(str)

  def full_results(self):
    res = super(PairedAssociatesTrialResult, self).full_results()
    for (cue,associate,user_associate) in zip(self.cues, self.associates, self.user_associates):
      inner_res = []
      inner_res.append(("cue", "Cue", "", cue))
      inner_res.append(("associate", "Associate", "", associate))
      inner_res.append(("response", "Response", "", user_associate))
      res.append(("pair", "Pair", "parent", inner_res))
    return res

  def to_tuple(self):
    return super(PairedAssociatesTrialResult, self).to_tuple() + (self.cues, self.associates, self.user_associates)

  @classmethod
  def from_tuple(cls, tup):
    (cues, associates, user_associates) = tup[-3:]
    result = super(PairedAssociatesTrialResult, cls).from_tuple(tup[:-3])
    result.cues = cues
    result.associates = associates
    result.user_associates = user_associates
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(PairedAssociatesTrialResult, cls).create(trial, test_result)
    if trial.has_key('pairs'):
      result.cues = [result.cue_to_str(x[0]) for x in trial['pairs']]
      result.associates = [result.associate_to_str(x[1]) for x in trial['pairs']]
      result.user_associates = [result.associate_to_str(x) for x in trial['userChoice']]
    return result

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_level_name(cls):
    return "Pairs"

  @classmethod
  def get_all_header_fields(cls):
    return super(PairedAssociatesTrialResult, cls).get_all_header_fields() + ["Cues", "Associates", "User response"]

  def get_all_fields(self):
    return super(PairedAssociatesTrialResult, self).get_all_fields() + [",".join(self.cues), ",".join(self.associates), ",".join(self.user_associates)]

  @classmethod
  def hide_fields(cls):
    return super(PairedAssociatesTrialResult, cls).hide_fields().union(set(['Ready?', 'Reaction time']))

class PairedAssociatesTestResult(LevelEstimationTestResult):
  @classmethod
  def test_name(cls):
    return "Paired Associates"

  @classmethod
  def get_result_variant_name(cls):
    return "paired_associates"

  @classmethod
  def level_name(cls):
    return "pairs"

  @classmethod
  def get_trial_class(self):
    return PairedAssociatesTrialResult

class VisuospatialPairedAssociatesTrialResult(PairedAssociatesTrialResult):
  def cue_to_str(self, cue):
    return cue[-5]

  def associate_to_str(self, associate):
    return "%d"%associate

  @classmethod
  def create(cls, trial, test_result):
    result = super(VisuospatialPairedAssociatesTrialResult, cls).create(trial, test_result)
    return result

  @classmethod
  def get_level_name(cls):
    return "Visuospatial pairs"

  @classmethod
  def get_all_header_fields(cls):
    return super(VisuospatialPairedAssociatesTrialResult, cls).get_all_header_fields() + []

  def get_all_fields(self):
    return super(VisuospatialPairedAssociatesTrialResult, self).get_all_fields() + []

  @classmethod
  def hide_fields(cls):
    return super(VisuospatialPairedAssociatesTrialResult, cls).hide_fields().union(set(['Ready?', 'Reaction time']))

class VisuospatialPairedAssociatesTestResult(PairedAssociatesTestResult):
  @classmethod
  def test_name(cls):
    return "Visuospatial Paired Associates"

  @classmethod
  def get_result_variant_name(cls):
    return "visuospatial_paired_associates"

  @classmethod
  def get_factor_weights(self, group_name):
    return ([('visuospatial_paired_associates_default', 'Score')],
            {((u'visuospatial_paired_associates', u'default'), 'correct', 0, None):
               ([1.186, -1.854, 5.446], None)})

  @classmethod
  def level_name(cls):
    return "visuospatial pairs"

  @classmethod
  def get_trial_class(cls):
    return VisuospatialPairedAssociatesTrialResult

class VerbalPairedAssociatesTrialResult(PairedAssociatesTrialResult):
  def cue_to_str(self, cue):
    return cue

  def associate_to_str(self, associate):
    return associate

  @classmethod
  def create(cls, trial, test_result):
    result = super(VerbalPairedAssociatesTrialResult, cls).create(trial, test_result)
    return result

  @classmethod
  def get_level_name(cls):
    return "Word pairs"

  @classmethod
  def get_all_header_fields(cls):
    return super(VerbalPairedAssociatesTrialResult, cls).get_all_header_fields() + []

  def get_all_fields(self):
    return super(VerbalPairedAssociatesTrialResult, self).get_all_fields() + []

  @classmethod
  def hide_fields(cls):
    return super(VerbalPairedAssociatesTrialResult, cls).hide_fields().union(set(['Ready?', 'Reaction time']))

class VerbalPairedAssociatesTestResult(BaseTestResult):
  level = db.IntegerProperty()
  correct_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic)
  cues = db.ListProperty(str)
  associates = db.ListProperty(str)
  cues_associates_hash = db.IntegerProperty()
  user_words = db.ListProperty(str)
  pair_id = db.IntegerProperty()

  @classmethod
  def test_name(cls):
    return "Verbal Paired Associates"

  @classmethod
  def get_result_variant_name(cls):
    return "verbal_paired_associates"

  def __init__(self, *args, **kwargs):
    super(VerbalPairedAssociatesTestResult, self).__init__(*args, **kwargs)

  def collect_observations(self):
    observations = {}
    for (real_word, user_word) in zip(self.associates, self.user_words):
      self.add_observation(observations, 'boolean', user_word == real_word, 'correct')
    return observations

  def full_results(self):
    res = super(VerbalPairedAssociatesTestResult, self).full_results()
    res.append(("n_pairs", "Number of pairs", "", self.level))
    res.append(("correct", "Correct", "parent", self.correct_ss.full_results()))
    for (cue,associate,user_associate) in zip(self.cues, self.associates, self.user_words):
      inner_res = []
      inner_res.append(("cue", "Cue", "", cue))
      inner_res.append(("associate", "Associate", "", associate))
      inner_res.append(("response", "Response", "", user_associate))
      res.append(("pair", "Pair", "parent", inner_res))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(VerbalPairedAssociatesTestResult, cls).fields_to_graph() + [('n_pairs',),('correct','fraction_positive')]

  def to_tuple(self):
    return super(VerbalPairedAssociatesTestResult, self).to_tuple() + (self.level, self.correct_ss.to_tuple(), self.cues, self.associates, self.cues_associates_hash, self.user_words)

  @classmethod
  def from_tuple(cls, tup):
    result = super(VerbalPairedAssociatesTestResult, cls).from_tuple(tup[:-6])
    (result.n_exposures, result.cues, result.associates, result.cues_associates_hash, result.user_words) = (tup[-6], tup[-4], tup[-3], tup[-2], tup[-1])
    correct_ss = models2.BooleanSummaryStatistic.from_tuple(tup[-5])
    correct_ss.put()
    result.correct_ss = correct_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(VerbalPairedAssociatesTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('pair_id'):
        try:
          pair_id = int(result_data['pair_id'])
          result.pair_id = pair_id
        except:
          return None
      if result_data.has_key('wordPairs'):
        result.cues = [x[0] for x in result_data['wordPairs']]
        result.associates = [x[1] for x in result_data['wordPairs']]
        result.cues_associates_hash = hash(tuple(map(tuple,(result.cues, result.associates))))
        result.user_words = result_data['userWords']
        if len(result.cues) == len(result.user_words):
          result.level = len(result.cues)
          corrects = [associate.strip().lower()==user_word.strip().lower() for (associate,user_word) in zip(result.associates, result.user_words)]
          result.correct_ss = models2.BooleanSummaryStatistic.create(corrects)
          state = result.variant().get_state_class().get(result.test_session.user, result.variant())
          if state:
            if corrects.count(False) == 0:
              # all correct, increase level
              increase_level = True
              #if random.random() < (1-result.variant().target_success_probability):
              #  increase_level = True
              if increase_level:
                state.level += 1
                state.put()
            else:
              if (state.level > 1):
                decrease_level = True
                #if random.random() < result.variant().target_success_probability:
                #  decrease_level = True
                if decrease_level:
                  state.level -= 1
                  state.put()
      return result

  def get_all_statistics(self):
    return super(VerbalPairedAssociatesTestResult, self).get_all_statistics() + [("Correct", self.correct_ss.to_html())]

  @classmethod
  def hide_fields(cls):
    return super(VerbalPairedAssociatesTestResult, cls).hide_fields().union([])

  def extra_sections(self):
    return super(VerbalPairedAssociatesTestResult, self).extra_sections() + [{'title': 'Word pairs', 'content': self.word_pairs_html()}]

  def word_pairs_html(self):
    s = "<table><thead><tr><td>Cue</td><td>Associate</td><td>Input</td></tr></thead><tbody>"
    for i in xrange(self.level):
      s += "<tr><td>%s</td><td>%s</td><td>%s</td></tr>"%(self.cues[i], self.associates[i], self.user_words[i])
    s += "</tbody></table>"
    return s

class UserVerbalPairedAssociates2TestState(UserBaseTestState):
  parent_result = db.ReferenceProperty(VerbalPairedAssociatesTestResult)
  parent_state = db.ReferenceProperty(UserVerbalPairedAssociatesTestState)
  pair_id = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    super(UserVerbalPairedAssociates2TestState, self).__init__(*args, **kwargs)

  @classmethod
  def get(cls, user, variant):
    state = super(UserVerbalPairedAssociates2TestState, cls).get(user, variant)
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant)
    return state

  def word_pairs_as_javascript(self):
    return "window.pair_id = %d;\nwindow.parentID = %d;\n"%(self.pair_id or 0, self.parent_result.key().id()) + self.parent_state.word_pairs_as_javascript(with_pair_id = False)

class VerbalPairedAssociates2TestResult(BaseTestResult):
  parent_result = db.ReferenceProperty(VerbalPairedAssociatesTestResult)
  user_words = db.ListProperty(str)
  correct_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic)
  pair_id = db.IntegerProperty()

  @classmethod
  def test_name(cls):
    return "Verbal Paired Associates 2"

  @classmethod
  def get_result_variant_name(cls):
    return "verbal_paired_associates_2"

  def __init__(self, *args, **kwargs):
    super(VerbalPairedAssociates2TestResult, self).__init__(*args, **kwargs)

  def collect_observations(self):
    observations = {}
    if self.parent_result is None:
      return observations
    for (real_word, user_word) in zip(self.parent_result.associates, self.user_words):
      self.add_observation(observations, 'boolean', user_word == real_word, 'delayed_correct')
    return observations

  def full_results(self):
    res = super(VerbalPairedAssociates2TestResult, self).full_results()
    res.append(("n_pairs", "Number of pairs", "", self.parent_result.level))
    res.append(("correct", "Correct", "parent", self.correct_ss.full_results()))
    for (cue,associate,user_associate) in zip(self.parent_result.cues, self.parent_result.associates, self.user_words):
      inner_res = []
      inner_res.append(("cue", "Cue", "", cue))
      inner_res.append(("associate", "Associate", "", associate))
      inner_res.append(("response", "Response", "", user_associate))
      res.append(("pair", "Pair", "parent", inner_res))
    return res

  @classmethod
  def fields_to_graph(cls):
    return super(VerbalPairedAssociates2TestResult, cls).fields_to_graph() + [('n_pairs',),('correct','fraction_positive')]

  def to_tuple(self):
    if self.correct_ss is not None:
      correct_ss_tup = self.correct_ss.to_tuple()
    else:
      correct_ss_tup = None
    return super(VerbalPairedAssociates2TestResult, self).to_tuple() + (self.parent_result.cues_associates_hash, self.user_words, correct_ss_tup)

  @classmethod
  def from_tuple(cls, tup):
    result = super(VerbalPairedAssociates2TestResult, cls).from_tuple(tup[:-3])
    cues_associates_hash, result.user_words, correct_ss_tup = tup[-3:]
    parent = VerbalPairedAssociatesTestResult.all().filter('cues_associates_hash', cues_associates_hash).get()
    if not parent:
      return None
    result.parent_result = parent
    if correct_ss_tup is not None:
      correct_ss = models2.BooleanSummaryStatistic.from_tuple(correct_ss_tup)
      correct_ss.put()
      result.correct_ss = correct_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(VerbalPairedAssociates2TestResult, cls).make_result(variant, result_data)
    if result_data.has_key('pair_id'):
      try:
        pair_id = int(result_data['pair_id'])
        result.pair_id = pair_id
      except:
        return None
    if result_data.has_key('parentID'):
      try:
        parent_result = VerbalPairedAssociatesTestResult.get_by_id(result_data['parentID'])
        if parent_result.test_session.user.key() == result.test_session.user.key():
          result.parent_result = parent_result
      except:
        return None
      result.user_words = result_data['userWords']
      if len(result.user_words) == len(result.parent_result.cues):
        corrects = [associate.strip().lower()==user_word.strip().lower() for (associate,user_word) in zip(result.parent_result.associates, result.user_words)]
        result.correct_ss = models2.BooleanSummaryStatistic.create(corrects)
    return result

  def get_all_statistics(self):
    return super(VerbalPairedAssociates2TestResult, self).get_all_statistics() + [("Correct", self.correct_ss.to_html())]

  @classmethod
  def hide_fields(cls):
    return super(VerbalPairedAssociates2TestResult, cls).hide_fields().union([])

  def extra_sections(self):
    return super(VerbalPairedAssociates2TestResult, self).extra_sections() + [{'title': 'Word pairs', 'content': self.word_pairs_html()}]

  def word_pairs_html(self):
    s = "<table><thead><tr><td>Cue</td><td>Associate</td><td>Original input</td><td>Input</td></tr></thead><tbody>"
    for i in xrange(self.parent_result.level):
      s += "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>"%(self.parent_result.cues[i], self.parent_result.associates[i], self.parent_result.user_words[i] if len(self.parent_result.user_words) > i else "(none)", self.user_words[i] if len(self.user_words) > i else "(none)")
    s += "</tbody></table>"
    return s

class DigitSpanTrialResult(SpanTrialResult):
  actual_sequence = db.ListProperty(int)
  user_sequence = db.ListProperty(int)

  def full_results(self):
    res = super(DigitSpanTrialResult, self).full_results()
    res.append(("stimulus", "Stimulus", "parent", [("digit", "Digit", "", digit) for digit in self.actual_sequence]))
    res.append(("response", "Response", "parent", [("digit", "Digit", "", digit) for digit in self.user_sequence]))
    return res

  def to_tuple(self):
    return super(DigitSpanTrialResult, self).to_tuple() + (self.actual_sequence, self.user_sequence)

  @classmethod
  def from_tuple(cls, tup):
    (actual_sequence, user_sequence) = tup[-2:]
    result = super(DigitSpanTrialResult, cls).from_tuple(tup[:-2])
    result.actual_sequence = actual_sequence
    result.user_sequence = user_sequence
    return result

  @classmethod
  def create(cls, trial, test_result):

    def int_safe(char):
      try:
        return int(char)
      except ValueError:
        # non-number values converted to 0
        return 0

    result = super(DigitSpanTrialResult, cls).create(trial, test_result)
    if trial.has_key('sequence'):
      result.actual_sequence = [int(x) for x in trial['sequence']]
      result.user_sequence = [int_safe(x) for x in trial['userSequence']]
    return result

  @classmethod
  def get_level_name(cls):
    return "Digit span"

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_all_header_fields(cls):
    return super(DigitSpanTrialResult, cls).get_all_header_fields() + ['Stimulus', 'Response']

  def get_all_fields(self):
    return super(DigitSpanTrialResult, self).get_all_fields() + [''.join(map(str, self.actual_sequence)), ''.join(map(str, self.user_sequence))]

class DigitSpanTestResult(SpanTestResult):
  @classmethod
  def test_name(cls):
    return "Digit Span"

  @classmethod
  def get_result_variant_name(cls):
    return "digit_span"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = {'Auditory Backward':
             ([('digit_span_auditory_backward', 'Score')],
              {((u'digit_span', u'Auditory Backward'), 'correct', 0, None): ([2.251, -1.115, 8.312], None)}),
           'Visual Backward':
             ([('digit_span_visual_backward', 'Score')],
              {((u'digit_span', u'Visual Backward'), 'correct', 0, None): ([2.171, -1.067, 8.863], None)}),
           'Audiovisual Backward':
             ([('digit_span_audiovisual_backward', 'Score')],
              {((u'digit_span', u'Audiovisual Backward'), 'correct', 0, None): ([1.564, -1.073, 8.527], None)}),
           'Auditory Forward':
             ([('digit_span_auditory_forward', 'Score')],
              {((u'digit_span', u'Auditory Forward'), 'correct', 0, None): ([1.379, -0.961, 8.065], None)}),
           'Visual Forward':
             ([('digit_span_visual_forward', 'Score')],
              {((u'digit_span', u'Visual Forward'), 'correct', 0, None): ([1.294, -0.945, 7.325], None)}),
           'Audiovisual Forward':
             ([('digit_span_audiovisual_forward', 'Score')],
              {((u'digit_span', u'Audiovisual Forward'), 'correct', 0, None): ([1.575, -0.992, 7.772], None)}),
           'Backward Auditory':
             ([('digit_span_auditory_backward', 'Score')],
              {((u'digit_span', u'Backward Auditory'), 'correct', 0, None): ([2.251, -1.115, 8.312], None)})}
    if res.has_key(group_name):
      return res[group_name]
    else:
      return None

  @classmethod
  def level_name(cls):
    return "digit span"

  @classmethod
  def get_trial_class(cls):
    return DigitSpanTrialResult

class SpatialSpanTrialResult(SpanTrialResult):
  actual_sequence = db.ListProperty(int)
  user_sequence = db.ListProperty(int)
  grid_size = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    if not kwargs.has_key('grid_size'):
      kwargs['grid_size'] = 5
    super(SpatialSpanTrialResult, self).__init__(*args, **kwargs)

  def full_results(self):
    res = super(SpatialSpanTrialResult, self).full_results()
    res.append(("grid_size", "Grid size", "", self.grid_size))
    res.append(("stimulus", "Stimulus", "parent", [("location", "Location", "parent",
                                                    [("x", "X", "", location % self.grid_size),
                                                     ("y", "Y", "", location // self.grid_size)])
                                                   for location in self.actual_sequence]))
    res.append(("response", "Response", "parent", [("location", "Location", "parent",
                                                    [("x", "X", "", location % self.grid_size),
                                                     ("y", "Y", "", location // self.grid_size)])
                                                   for location in self.user_sequence]))
    return res

  def to_tuple(self):
    return super(SpatialSpanTrialResult, self).to_tuple() + (self.actual_sequence, self.user_sequence, self.grid_size)

  @classmethod
  def from_tuple(cls, tup):
    (actual_sequence, user_sequence, grid_size) = tup[-3:]
    result = super(SpatialSpanTrialResult, cls).from_tuple(tup[:-3])
    result.actual_sequence = actual_sequence
    result.user_sequence = user_sequence
    result.grid_size = grid_size
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(SpatialSpanTrialResult, cls).create(trial, test_result)
    if trial.has_key('sequence'):
      grid_size = test_result.variant().grid_size
      result.grid_size = grid_size
      result.actual_sequence = [int(x[0]*grid_size+x[1]) for x in trial['sequence']]
      result.user_sequence = [int(x[0]*grid_size+x[1]) for x in trial['userSequence']]
    return result

  @classmethod
  def get_level_name(cls):
    return "Spatial span"

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_all_header_fields(cls):
    return super(SpatialSpanTrialResult, cls).get_all_header_fields() + ['Stimulus', 'Response']

  def get_all_fields(self):
    return super(SpatialSpanTrialResult, self).get_all_fields() + [','.join(map(str, self.actual_sequence)), ','.join(map(str, self.user_sequence))]

class SpatialSpanTestResult(SpanTestResult):
  @classmethod
  def test_name(cls):
    return "Spatial Span"

  @classmethod
  def get_result_variant_name(cls):
    return "spatial_span"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = {'Backward':
             ([('spatial_span_backward', 'Score')],
              {((u'spatial_span', u'Backward'), 'correct', 0, None): ([1.375, -1.383, 7.886], None)}),
           'Forward':
             ([('spatial_span_forward', 'Score')],
              {((u'spatial_span', u'Forward'), 'correct', 0, None): ([1.839, -1.419, 8.904], None)})}
    if res.has_key(group_name):
      return res[group_name]
    else:
      return None

  @classmethod
  def get_trial_class(cls):
    return SpatialSpanTrialResult

class DesignCopyTrialResult(LevelEstimationTrialResult):
  actual_design = db.ListProperty(int)
  user_design = db.ListProperty(int)
  grid_size = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    if not kwargs.has_key('grid_size'):
      kwargs['grid_size'] = 8
    super(DesignCopyTrialResult, self).__init__(*args, **kwargs)

  def full_results(self):
    res = super(DesignCopyTrialResult, self).full_results()
    res.append(("grid_size", "Grid size", "", self.grid_size))
    res.append(("stimulus", "Stimulus", "parent", [("location", "Location", "parent",
                                                    [("x", "X", "", location % self.grid_size),
                                                     ("y", "Y", "", location // self.grid_size)])
                                                   for location in self.actual_design]))
    res.append(("response", "Response", "parent", [("location", "Location", "parent",
                                                    [("x", "X", "", location % self.grid_size),
                                                     ("y", "Y", "", location // self.grid_size)])
                                                   for location in self.user_design]))
    return res

  def to_tuple(self):
    return super(DesignCopyTrialResult, self).to_tuple() + (self.actual_design, self.user_design, self.grid_size)

  @classmethod
  def from_tuple(cls, tup):
    (actual_design, user_design, grid_size) = tup[-3:]
    result = super(DesignCopyTrialResult, cls).from_tuple(tup[:-3])
    result.actual_design = actual_design
    result.user_design = user_design
    result.grid_size = grid_size
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(DesignCopyTrialResult, cls).create(trial, test_result)
    if trial.has_key('design'):
      grid_size = test_result.variant().grid_size
      result.actual_design = [int(i*grid_size+j) for i in xrange(grid_size) for j in xrange(grid_size) if trial['design'][i][j]]
      result.user_design = [int(i*grid_size+j) for i in xrange(grid_size) for j in xrange(grid_size) if trial['userDesign'][i][j]]
    return result

  @classmethod
  def get_level_name(cls):
    return "Design complexity"

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_all_header_fields(cls):
    return super(DesignCopyTrialResult, cls).get_all_header_fields() + ['Stimulus design', 'Response design']

  def get_all_fields(self):
    return super(DesignCopyTrialResult, self).get_all_fields() + [','.join(map(str, self.actual_design)), ','.join(map(str, self.user_design))]

class DesignCopyTestResult(LevelEstimationTestResult):
  @classmethod
  def test_name(cls):
    return "Design Copy"

  @classmethod
  def get_result_variant_name(cls):
    return "design_copy"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = ([('design_copy_default', 'Score')],
           {((u'design_copy', u'default'), 'correct', 0, None): ([2.458, -0.974, 7.790], None)})
    return res

  @classmethod
  def level_name(cls):
    return "complexity"

  @classmethod
  def get_trial_class(cls):
    return DesignCopyTrialResult

class DesignRecognitionTrialResult(LevelEstimationTrialResult):
  @classmethod
  def create(cls, trial, test_result):
    result = super(DesignRecognitionTrialResult, cls).create(trial, test_result)
    return result

  @classmethod
  def get_level_name(cls):
    return "Design complexity"

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_all_header_fields(cls):
    return super(DesignRecognitionTrialResult, cls).get_all_header_fields() + []

  def get_all_fields(self):
    return super(DesignRecognitionTrialResult, self).get_all_fields() + []

class DesignRecognitionTestResult(LevelEstimationTestResult):
  @classmethod
  def test_name(cls):
    return "Design Recognition"

  @classmethod
  def get_result_variant_name(cls):
    return "design_recognition"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = ([('design_recognition_default', 'Score')],
           {((u'design_recognition', u'default'), 'correct', 0, None): ([2.223, -0.174, 3.378], None)})
    return res

  @classmethod
  def level_name(cls):
    return "complexity"

  @classmethod
  def get_trial_class(cls):
    return DesignRecognitionTrialResult

class MentalRotationTrialResult(LevelEstimationTrialResult):
  @classmethod
  def create(cls, trial, test_result):
    result = super(MentalRotationTrialResult, cls).create(trial, test_result)
    return result

  @classmethod
  def get_level_name(cls):
    return "Design complexity"

  def get_level(self):
    return int(self.level)

  @classmethod
  def get_all_header_fields(cls):
    return super(MentalRotationTrialResult, cls).get_all_header_fields() + []

  def get_all_fields(self):
    return super(MentalRotationTrialResult, self).get_all_fields() + []

class MentalRotationTestResult(DesignRecognitionTestResult):
  @classmethod
  def test_name(cls):
    return "Mental Rotation"

  @classmethod
  def get_result_variant_name(cls):
    return "mental_rotation"

  @classmethod
  def get_factor_weights(cls, group_name):
    return ([('mental_rotation_default', 'Score')],
            {((u'mental_rotation', u'default'), 'correct', 0, None): ([6.299, -0.262, 8.922], None)})

  @classmethod
  def get_trial_class(cls):
    return MentalRotationTrialResult

class InspectionTimeTrialResult(LevelEstimationTrialResult):
  is_stimulus_right = db.BooleanProperty()

  def full_results(self):
    res = super(InspectionTimeTrialResult, self).full_results()
    res.append(("stimulus", "Stimulus", "", "Right" if self.is_stimulus_right else "Left"))
    return res

  def to_tuple(self):
    return super(InspectionTimeTrialResult, self).to_tuple() + (self.is_stimulus_right,)

  @classmethod
  def from_tuple(cls, tup):
    (is_stimulus_right,) = tup[-1:]
    result = super(InspectionTimeTrialResult, cls).from_tuple(tup[:-1])
    result.is_stimulus_right = is_stimulus_right
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(InspectionTimeTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulus'):
      result.is_stimulus_right = (trial['stimulus'] == 1)
    return result

  @classmethod
  def get_level_name(cls):
    return "Time"

  def get_level(self):
    return "%.2f"%self.level

  @classmethod
  def get_all_header_fields(cls):
    return super(InspectionTimeTrialResult, cls).get_all_header_fields() + ["Stimulus"]

  def get_all_fields(self):
    return super(InspectionTimeTrialResult, self).get_all_fields() + ["Right" if self.is_stimulus_right else "Left"]

class DecisionMakingTrialResult(LevelEstimationTrialResult):
  smallAmount = db.IntegerProperty()
  bigAmount = db.IntegerProperty()
  catchType = db.StringProperty()
  bigOnLeft = db.BooleanProperty()
  tookBig = db.BooleanProperty()

  def full_results(self):
    res = super(DecisionMakingTrialResult, self).full_results()
    return res

  def to_tuple(self):
    return super(DecisionMakingTrialResult, self).to_tuple() + (
       (('smallAmount', self.smallAmount),
        ('bigAmount', self.bigAmount),
        ('catchType', self.catchType),
        ('bigOnLeft', self.bigOnLeft),
        ('tookBig', self.tookBig)),)

  @classmethod
  def from_tuple(cls, tup):
    result = super(DecisionMakingTrialResult, cls).from_tuple(tup[:-1])
    for k, v in tup[-1]:
        setattr(result, k, v)
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(DecisionMakingTrialResult, cls).create(trial, test_result)
    logging.info("saving trial: %s"%str(trial))
    if trial.has_key('smallAmount'):
      try:
        result.smallAmount = int(trial['smallAmount'])
      except:
        return None
    if trial.has_key('bigAmount'):
      try:
        result.bigAmount = int(trial['bigAmount'])
      except:
        return None
    if trial.has_key('catchType'):
      result.catchType = trial['catchType']
    if trial.has_key('bigOnLeft'):
      result.bigOnLeft = bool(trial['bigOnLeft'])
    if trial.has_key('tookBig'):
      result.tookBig = bool(trial['tookBig'])
    return result

  @classmethod
  def get_level_name(cls):
    return "variable"

  def get_level(self):
    return "%.2f"%self.level

  @classmethod
  def get_all_header_fields(cls):
    return super(DecisionMakingTrialResult, cls).get_all_header_fields() + ["smallAmount", "bigAmount"]

  def get_all_fields(self):
    return super(DecisionMakingTrialResult, self).get_all_fields() + [self.smallAmount, self.bigAmount]

class InspectionTimeTestResult(LevelEstimationTestResult):
  @classmethod
  def test_name(cls):
    return "Inspection Time"

  @classmethod
  def get_result_variant_name(cls):
    return "inspection_time"

  @classmethod
  def a_vals(cls):
    return [0.25,0.5,1.0,2.0,4.0,8.0,16.0,32.0,64.0,128.0]

  @classmethod
  def guessing_p(cls):
    return 0.5

  @classmethod
  def level_name(cls):
    return "inspection time"

  @classmethod
  def get_trial_class(cls):
    return InspectionTimeTrialResult

class DecisionMakingTestResult(LevelEstimationTestResult):
  @classmethod
  def test_name(cls):
    return "Decision Making"

  @classmethod
  def get_result_variant_name(cls):
    return "decision_making"

  @classmethod
  def a_vals(cls):
    return [0.25,0.5,1.0,2.0,4.0,8.0,16.0,32.0,64.0,128.0]

  @classmethod
  def guessing_p(cls):
    return 0.5

  @classmethod
  def level_name(cls):
    return "variable"

  @classmethod
  def get_trial_class(cls):
    return DecisionMakingTrialResult

class CodingTrialResult(TSRTrialResult):
  correct_response = db.IntegerProperty()
  user_response = db.IntegerProperty()

  def full_results(self):
    res = super(CodingTrialResult, self).full_results()
    res.append(("stimulus", "Stimulus", "", self.correct_response))
    res.append(("response", "Response", "", self.user_response))
    return res

  def to_tuple(self):
    return super(CodingTrialResult, self).to_tuple() + (self.correct_response, self.user_response)

  @classmethod
  def from_tuple(cls, tup):
    (correct_response, user_response) = tup[-2:]
    result = super(CodingTrialResult, cls).from_tuple(tup[:-2])
    result.correct_response = correct_response
    result.user_response = user_response
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(CodingTrialResult, cls).create(trial, test_result)
    if trial.has_key('correctResponse'):
      result.correct_response = trial['correctResponse']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(CodingTrialResult, cls).get_all_header_fields() + ["Stimulus", "Response"]

  def get_all_fields(self):
    return super(CodingTrialResult, self).get_all_fields() + [self.correct_response, self.user_response]

  @classmethod
  def hide_fields(cls):
    return super(CodingTrialResult, cls).hide_fields().union(set(['Ready?', 'Duration']))

class CodingTestResult(TSRTestResult):
  trend_ss = db.ReferenceProperty(models2.CorrelationSummaryStatistic)

  @classmethod
  def test_name(cls):
    return "Coding"

  @classmethod
  def get_result_variant_name(cls):
    return "coding"

  @classmethod
  def get_factor_weights(cls, group_name):
    return ([('coding_default', 'Score')],
            {((u'coding', u'default'), 'correct', 0, None): ([0.826, 2.311], None),
             ((u'coding', u'default'), 'ready', 0, None): ([0.0, 24.203], None),
             ((u'coding', u'default'), 'reaction time', 1, None): ([-556.859, 1834.009], 301203.466)})
    W = {('Coding', 'correct', 0, None):
           ([ 0.42813346,  2.68329791], None),
         ('Coding', 'reaction time', 1, None):
           ([-1252.32830279,  2189.38047447], 579957.69040395401)}
    factor_info = [('coding_score', 'Score')]
    return (factor_info, W)

  def __init__(self, *args, **kwargs):
    super(CodingTestResult, self).__init__(*args, **kwargs)
    if self.trend_ss is None and self.trial_results is not None:
      self.compute_trend()

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(CodingTestResult, cls).make_result(variant, result_data)
    if result:
      result.compute_trend()
    return result

  def compute_trend(self):
    trials = self.get_trial_results()
    if trials:
      total_seconds = self.duration // 1000
      cps = [0] * total_seconds
      t = 0
      for trial in trials:
        if trial.correct:
          sec = t // 1000
          while sec >= len(cps):
            cps.append(0)
          cps[sec] += 1
        t += trial.duration
      self.trend_ss = models2.CorrelationSummaryStatistic.create(range(total_seconds), cps)
      self.put()

  def summary_statistics(self):
    correct_ss = (0, 0)
    if self.correct_ss is not None:
      correct_ss = self.correct_ss.to_summary_statistics()
    reaction_time_ss = (0, 0.0, 0.0)
    if self.reaction_time_ss is not None:
      reaction_time_ss = self.reaction_time_ss.to_summary_statistics()
    trend_ss = (0, 0.0, 0.0, 0.0, 0.0, 0.0)
    if self.trend_ss is not None:
      trend_ss = self.trend_ss.to_summary_statistics()
    return (self.duration,) + correct_ss + reaction_time_ss + trend_ss

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (duration,),ss = models2.sum_summaries(ss, 1)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (pearson_r, trend, trend_se), ss = models2.CorrelationSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if correct_p is not None:
      results.append(("correct", "Correct", "fraction", correct_p, correct_p_se))
    if trend is not None:
      results.append(("trend", "Trend", "z-score", trend, trend_se))
    return results

  @classmethod
  def hide_fields(cls):
    return super(CodingTestResult, cls).hide_fields().union(['Ready'])

  @classmethod
  def get_trial_class(cls):
    return CodingTrialResult

  @classmethod
  def cumulative_results(cls, results):
    res = super(CodingTestResult, cls).cumulative_results(results)
    titles = super(CodingTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    cpm = []
    rt_by_code = [[] for i in xrange(10)]
    correct_by_code = [[] for i in xrange(10)]
    for result in results:
      trials = result.get_trial_results()
      corrects = len([trial for trial in trials if trial.correct])
      for trial in trials:
        correct_by_code[0].append(trial.correct)
        correct_by_code[trial.correct_response+1].append(trial.correct)
        if trial.correct:
          rt_by_code[0].append(trial.reaction_time)
          rt_by_code[trial.correct_response+1].append(trial.reaction_time)
      duration = result.duration
      cpm.append(corrects * 1000.0 / duration)
    correct_ss = [models2.BooleanSummaryStatistic.create(x) for x in correct_by_code]
    rt_ss = [models2.FloatSummaryStatistic.create([x / correct_ss[i].fraction_positive for x in rt_by_code[i]]) for i in xrange(10)]
    res.append(models2.CumulativeStatistic.from_float_sss(rt_ss, ["Overall"] + ["Code %d"%i for i in xrange(1,10)]))
    res.append(models2.CumulativeStatistic.from_boolean_sss(correct_ss, ["Overall"] + ["Code %d"%i for i in xrange(1,10)]))
    cpm_ss = models2.FloatSummaryStatistic.create(cpm)
    res.append(models2.CumulativeStatistic.from_float_sss([cpm_ss], ["Codes/minute"]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall ready"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(CodingTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Corrected reaction time", "Correct", "Correct codes per minute"])
    return titles

class KNBackTrialResult(TSRTrialResult):
  @classmethod
  def create(cls, trial, test_result):
    result = super(KNBackTrialResult, cls).create(trial, test_result)
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(KNBackTrialResult, cls).get_all_header_fields() + []

  def get_all_fields(self):
    return super(KNBackTrialResult, self).get_all_fields() + []

class KNBackTestResult(TSRTestResult):
  @classmethod
  def test_name(cls):
    return "K-N-Back"

  @classmethod
  def get_result_variant_name(cls):
    return "k_n_back"

  @classmethod
  def hide_fields(cls):
    return super(KNBackTestResult, cls).hide_fields().union(['ready'])

  @classmethod
  def get_trial_class(cls):
    return KNBackTrialResult

class SimpleNBackTrialResult(KNBackTrialResult):
  previous_stimuli = db.ListProperty(int)
  stimulus = db.IntegerProperty()
  correct_response = db.BooleanProperty()
  user_response = db.BooleanProperty()

  def full_results(self):
    res = super(SimpleNBackTrialResult, self).full_results()
    res.append(("previous_stimuli", "Previous stimuli", "parent", [("stimulus", "Stimulus", "", stim) for stim in self.previous_stimuli]))
    res.append(("stimulus", "Stimulus", "", self.stimulus))
    res.append(("same_n_back", "Same as N back", "boolean", self.correct_response))
    res.append(("response", "Response", "", self.user_response))
    return res

  def to_tuple(self):
    return super(SimpleNBackTrialResult, self).to_tuple() + (self.previous_stimuli, self.stimulus, self.correct_response, self.user_response)

  @classmethod
  def from_tuple(cls, tup):
    (previous_stimuli, stimulus, correct_response, user_response) = tup[-4:]
    result = super(SimpleNBackTrialResult, cls).from_tuple(tup[:-4])
    result.previous_stimuli = previous_stimuli
    result.stimulus = stimulus
    result.correct_response = correct_response
    result.user_response = user_response
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(SimpleNBackTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulus'):
      result.stimulus = trial['stimulus'][0]
    if trial.has_key('stimulus_history'):
      result.previous_stimuli = [x[0] for x in trial['stimulus_history']]
    if trial.has_key('correctAnswer'):
      result.correct_response = trial['correctAnswer'][0]
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse'][0]
    return result

  @classmethod
  def hide_fields(cls):
    return super(SimpleNBackTrialResult, cls).hide_fields().union(set(['Ready?', 'Duration']))

  @classmethod
  def get_all_header_fields(cls):
    return super(SimpleNBackTrialResult, cls).get_all_header_fields() + ["Current stimulus", "Previous stimuli", "Correct response", "User response"]

  def get_all_fields(self):
    return super(SimpleNBackTrialResult, self).get_all_fields() + [self.stimulus, ",".join(["%d"%x for x in self.previous_stimuli]), self.correct_response, self.user_response]

class SimpleNBackTestResult(KNBackTestResult):
  @classmethod
  def test_name(cls):
    return "N-back"

  @classmethod
  def get_result_variant_name(cls):
    return "simple_n_back"

  # @classmethod
  # def get_factor_weights(cls, group_name):
  #   res = {'2-back':
  #            ([('simple_n_back_2-back', 'Score')],
  #             {((u'simple_n_back', u'2-back'), 'correct', 0, None): ([0.375, 2.017], None),
  #              ((u'simple_n_back', u'2-back'), 'ready', 0, None): ([-0.000, 23.203], None),
  #              ((u'simple_n_back', u'2-back'), 'reaction time', 1, None): ([-475.634, 1075.996], 275221.149)}),
  #          '3-back':
  #            ([('simple_n_back_3-back', 'Score')],
  #             {((u'simple_n_back', u'3-back'), 'correct', 0, None): ([-0.175, 0.888], None),
  #              ((u'simple_n_back', u'3-back'), 'ready', 0, None): ([-0.000, 24.203], None),
  #              ((u'simple_n_back', u'3-back'), 'reaction time', 1, None): ([-657.569, 1262.423], 460899.538)}),
  #          '1-back':
  #            ([('simple_n_back_1-back', 'Score')],
  #             {((u'simple_n_back', u'1-back'), 'correct', 0, None): ([0.187, 2.844], None),
  #              ((u'simple_n_back', u'1-back'), 'ready', 0, None): ([-0.000, 23.203], None),
  #              ((u'simple_n_back', u'1-back'), 'reaction time', 1, None): ([-162.023, 470.915], 32172.169)})}
  #   return res.get(group_name, None)

  def summary_statistics(self):
    return self.reaction_time_ss.to_summary_statistics() + self.correct_ss.to_summary_statistics()

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if rt_mean is not None:
      results.append(("reaction_time", "Reaction time", "ms", rt_mean, rt_se))
    if correct_p is not None:
      results.append(("correct", "Correct", "fraction", correct_p, correct_p_se))
    return results

  @classmethod
  def get_trial_class(cls):
    return SimpleNBackTrialResult

  @classmethod
  def hide_fields(cls):
    return super(SimpleNBackTestResult, cls).hide_fields().union(['Trial duration','Ready'])

  def get_all_statistics(self):
    return super(SimpleNBackTestResult, self).get_all_statistics() + []

  @classmethod
  def cumulative_results(cls, results):
    res = super(SimpleNBackTestResult, cls).cumulative_results(results)
    titles = super(SimpleNBackTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    rts = []
    corrects = []
    for result in results:
      trials = result.get_trial_results()
      prv_correct_response = None
      for trial in trials:
        uniform = len(set(trial.previous_stimuli)) == 1
        if (prv_correct_response is None) or (trial.correct_response != prv_correct_response):
          switch = True
        else:
          switch = False
        prv_correct_response = trial.correct_response
        vals = (uniform,switch,prv_correct_response)
        corrects.append(vals+(trial.correct,))
        if trial.correct:
          rts.append(vals+(trial.reaction_time,))
    preds = [[(lambda x: True, "Overall"),
              (lambda x: x[0], "Uniform buffer"),
              (lambda x: not x[0], "Variable buffer")],
             [(lambda x: True, "Overall"),
              (lambda x: x[1], "Switch"),
              (lambda x: not x[1], "Non-witch")],
             [(lambda x: True, "Overall"),
              (lambda x: x[2], "Same same"),
              (lambda x: not x[2], "Different")]]
    for pred_set in preds:
      pred_set2 = [(pred,title) for (pred,title) in pred_set if len([x for x in rts if pred(x)]) > 1]
      flsss = [models2.FloatSummaryStatistic.create([x[3] for x in rts if pred(x)]) for (pred,title) in pred_set2]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set2]))
      boolsss = [models2.BooleanSummaryStatistic.create([x[3] for x in corrects if pred(x)]) for (pred,title) in pred_set2]
      cflsss = [models2.FloatSummaryStatistic.create([x[3] / bss.fraction_positive for x in rts if pred(x)]) for ((pred,title),bss) in zip(pred_set2,boolsss)]
      res.append(models2.CumulativeStatistic.from_float_sss(cflsss, [x[1] for x in pred_set2]))
      res.append(models2.CumulativeStatistic.from_boolean_sss(boolsss, [x[1] for x in pred_set2]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(SimpleNBackTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["%s by %s"%(measure, category) for category in ["uniform buffer", "Answer switch", "identity to n-back"] for measure in ["Reaction time", "Corrected reaction time", "Correct"]])
    return titles

class DualNBackTrialResult(KNBackTrialResult):
  stimulus1 = db.IntegerProperty()
  stimulus2 = db.IntegerProperty()
  previous_stimuli1 = db.ListProperty(int)
  previous_stimuli2 = db.ListProperty(int)
  correct_response1 = db.BooleanProperty()
  correct_response2 = db.BooleanProperty()
  user_response1 = db.BooleanProperty()
  user_response2 = db.BooleanProperty()

  def full_results(self):
    res = super(DualNBackTrialResult, self).full_results()
    res.append(("previous_stimuli_1", "Previous stimuli 1", "parent", [("stimulus", "Stimulus", "", stim) for stim in self.previous_stimuli1]))
    res.append(("previous_stimuli_2", "Previous stimuli 2", "parent", [("stimulus", "Stimulus", "", stim) for stim in self.previous_stimuli2]))
    res.append(("stimulus_1", "Stimulus 1", "", self.stimulus1))
    res.append(("stimulus_2", "Stimulus 2", "", self.stimulus2))
    res.append(("same_n_back_1", "Same as N back 1", "boolean", self.correct_response1))
    res.append(("same_n_back_2", "Same as N back 2", "boolean", self.correct_response2))
    res.append(("response_1", "Response 1", "", self.user_response1))
    res.append(("response_2", "Response 2", "", self.user_response2))
    return res

  def to_tuple(self):
    return super(DualNBackTrialResult, self).to_tuple() + (self.previous_stimuli1, self.stimulus1, self.correct_response1, self.user_response1, self.previous_stimuli2, self.stimulus2, self.correct_response2, self.user_response2)

  @classmethod
  def from_tuple(cls, tup):
    (previous_stimuli1, stimulus1, correct_response1, user_response1, previous_stimuli2, stimulus2, correct_response2, user_response2) = tup[-8:]
    result = super(DualNBackTrialResult, cls).from_tuple(tup[:-8])
    result.previous_stimuli1 = previous_stimuli1
    result.stimulus1 = stimulus1
    result.correct_response1 = correct_response1
    result.user_response1 = user_response1
    result.previous_stimuli2 = previous_stimuli2
    result.stimulus2 = stimulus2
    result.correct_response2 = correct_response2
    result.user_response2 = user_response2
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(DualNBackTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulus'):
      result.stimulus1 = trial['stimulus'][0]
      result.stimulus2 = trial['stimulus'][1]
    if trial.has_key('stimulus_history'):
      result.previous_stimuli1 = [x[0] for x in trial['stimulus_history']]
      result.previous_stimuli2 = [x[1] for x in trial['stimulus_history']]
    if trial.has_key('correctAnswer'):
      result.correct_response1 = trial['correctAnswer'][0]
      result.correct_response2 = trial['correctAnswer'][1]
    if trial.has_key('userResponse'):
      result.user_response1 = trial['userResponse'][0]
      result.user_response2 = trial['userResponse'][1]
    return result

  @classmethod
  def hide_fields(cls):
    return super(DualNBackTrialResult, cls).hide_fields().union(set(['Ready?', 'Reaction time']))

  @classmethod
  def get_all_header_fields(cls):
    return super(DualNBackTrialResult, cls).get_all_header_fields() + ["Current stimulus", "Previous stimuli", "Correct response", "User response"]

  def bool_to_str(self, b):
    return "T" if b else "F"

  def get_all_fields(self):
    return super(DualNBackTrialResult, self).get_all_fields() + ["%d-%d"%(self.stimulus1, self.stimulus2), ",".join(["%d-%d"%(x,y) for (x,y) in zip(self.previous_stimuli1,self.previous_stimuli2)]), "%s-%s"%(self.bool_to_str(self.correct_response1), self.bool_to_str(self.correct_response2)), "%s-%s"%(self.bool_to_str(self.user_response1), self.bool_to_str(self.user_response2))]

class DualNBackTestResult(KNBackTestResult):
  @classmethod
  def test_name(cls):
    return "Dual N-back"

  @classmethod
  def get_result_variant_name(cls):
    return "dual_n_back"

  @classmethod
  def get_trial_class(cls):
    return DualNBackTrialResult

  @classmethod
  def hide_fields(cls):
    return super(DualNBackTestResult, cls).hide_fields().union(['Trial duration','Ready', 'Reaction time'])

  def get_all_statistics(self):
    return super(DualNBackTestResult, self).get_all_statistics() + []

class JaeggiDualNBackTrialResult(DualNBackTrialResult):
  pass

class JaeggiDualNBackTestResult(DualNBackTestResult):
  @classmethod
  def test_name(cls):
    return "Dual N-back"

  @classmethod
  def get_result_variant_name(cls):
    return "dual_n_back"

class MentalRotation2TrialResult(TSRTrialResult):
  correct_response = db.BooleanProperty()
  user_response = db.BooleanProperty()
  complexity = db.IntegerProperty()
  rotation0 = db.IntegerProperty()
  rotation = db.IntegerProperty()
  seed = db.IntegerProperty()

  def full_results(self):
    res = super(MentalRotation2TrialResult, self).full_results()
    res.append(("complexity", "Complexity", "", self.complexity))
    res.append(("rotation", "Rotation", "", self.rotation))
    res.append(("flipped", "Flipped", "boolean", not self.correct_response))
    res.append(("response", "Response", "", self.user_response))
    return res

  def to_tuple(self):
    return super(MentalRotation2TrialResult, self).to_tuple() + (self.correct_response, self.user_response, self.complexity, self.rotation0, self.rotation, self.seed)

  @classmethod
  def from_tuple(cls, tup):
    result = super(MentalRotation2TrialResult, cls).from_tuple(tup[:-6])
    (result.correct_response, result.user_response, result.complexity, result.rotation0, result.rotation, result.seed) = tup[-6:]
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(MentalRotation2TrialResult, cls).create(trial, test_result)
    if trial.has_key('correctResponse'):
      result.correct_response = trial['correctResponse']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    if trial.has_key('complexity'):
      result.complexity = trial['complexity']
    if trial.has_key('rotation0'):
      result.rotation0 = trial['rotation0']
    if trial.has_key('rotation'):
      result.rotation = trial['rotation']
    if trial.has_key('seed'):
      result.seed = trial['seed']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(MentalRotation2TrialResult, cls).get_all_header_fields() + ["Complexity", "Rotation", "Match?", "Response"]

  def get_all_fields(self):
    return super(MentalRotation2TrialResult, self).get_all_fields() + [self.complexity, self.rotation, self.correct_response, self.user_response]

class FeatureMatchTrialResult(TSRTrialResult):
  correct_response = db.BooleanProperty()
  user_response = db.BooleanProperty()
  level = db.IntegerProperty()

  def full_results(self):
    res = super(FeatureMatchTrialResult, self).full_results()
    res.append(("complexity", "Complexity", "", self.level))
    res.append(("match", "Match", "boolean", self.correct_response))
    res.append(("response", "Response", "", self.user_response))
    return res

  def to_tuple(self):
    return super(FeatureMatchTrialResult, self).to_tuple() + (self.correct_response, self.user_response, self.level)

  @classmethod
  def from_tuple(cls, tup):
    (correct_response, user_response, level) = tup[-3:]
    result = super(FeatureMatchTrialResult, cls).from_tuple(tup[:-3])
    result.correct_response = correct_response
    result.user_response = user_response
    result.level = level
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(FeatureMatchTrialResult, cls).create(trial, test_result)
    if trial.has_key('correctResponse'):
      result.correct_response = trial['correctResponse']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    if trial.has_key('level'):
      result.level = trial['level']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(FeatureMatchTrialResult, cls).get_all_header_fields() + ["Match?", "Response"]

  def get_all_fields(self):
    return super(FeatureMatchTrialResult, self).get_all_fields() + [self.correct_response, self.user_response]

class MentalRotation2TestResult(TSRTestResult):
  match_ss = db.ReferenceProperty(models2.LinearRegressionSummaryStatistic, collection_name='MentalRotation2_match_set')
  nonmatch_ss = db.ReferenceProperty(models2.LinearRegressionSummaryStatistic, collection_name='MentalRotation2_nonmatch_set')

  @classmethod
  def test_name(cls):
    return "Mental Rotation 2"

  @classmethod
  def get_result_variant_name(cls):
    return "mental_rotation_2"

  @classmethod
  def get_factor_weights(cls, group_name):
    return None

  def summary_statistics(self):
    match_ss = (0, 0.0, 0.0, 0.0, 0.0, 0.0)
    nonmatch_ss = (0, 0.0, 0.0, 0.0, 0.0, 0.0)
    if self.match_ss is not None:
      match_ss = self.match_ss.to_summary_statistics()
    if self.nonmatch_ss is not None:
      nonmatch_ss = self.nonmatch_ss.to_summary_statistics()
    return match_ss + nonmatch_ss

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (match_slope, match_slope_se, match_intercept, match_intercept_se), ss = models2.LinearRegressionSummaryStatistic.from_summary_statistics(ss)
    (nonmatch_slope, nonmatch_slope_se, nonmatch_intercept, nonmatch_intercept_se), ss = models2.LinearRegressionSummaryStatistic.from_summary_statistics(ss)
    results = []
    if match_slope is not None:
      results.append(("mr2_match_slope", "Match slope", "number", match_slope, match_slope_se))
    if match_intercept is not None:
      results.append(("mr2_match_intercept", "Match intercept", "number", match_intercept, match_intercept_se))
    if nonmatch_slope is not None:
      results.append(("mr2_nonmatch_slope", "Nonmatch slope", "number", nonmatch_slope, nonmatch_slope_se))
    if nonmatch_intercept is not None:
      results.append(("mr2_nonmatch_intercept", "Nonmatch intercept", "number", nonmatch_intercept, nonmatch_intercept_se))
    return results

  def full_results(self):
    res = super(MentalRotation2TestResult, self).full_results()
    res.append(("match_linear_regression", "Match linear regression", "parent", self.match_ss.full_results()))
    res.append(("nonmatch_linear_regression", "Nonmatch linear regression", "parent", self.nonmatch_ss.full_results()))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(MentalRotation2TestResult, cls).cumulative_results(results)
    titles = super(MentalRotation2TestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    match_rts = []
    match_corrects = []
    nonmatch_rts = []
    nonmatch_corrects = []
    for result in results:
      trials = result.get_trial_results()
      for trial in trials:
        cval = (trial.level, trial.correct)
        if trial.correct_response:
          match_corrects.append(cval)
        else:
          nonmatch_corrects.append(cval)
        if trial.correct:
          rtval = (trial.rotation, trial.reaction_time)
          if trial.correct_response:
            match_rts.append(rtval)
          else:
            nonmatch_rts.append(rtval)
    match_fraction_correct = len([x for x in match_corrects if x[1]]) * 1.0 / len(match_corrects)
    nonmatch_fraction_correct = len([x for x in nonmatch_corrects if x[1]]) * 1.0 / len(nonmatch_corrects)
    scatter_match = models2.ScatterData()
    cscatter_match = models2.ScatterData()
    scatter_nonmatch = models2.ScatterData()
    cscatter_nonmatch = models2.ScatterData()
    for (scatter, X, Y) in [(scatter_match, [x[0] for x in match_rts], [x[1] for x in match_rts]),
                            (cscatter_match, [x[0] for x in match_rts], [x[1] / match_fraction_correct for x in match_rts]),
                            (scatter_nonmatch, [x[0] for x in nonmatch_rts], [x[1] for x in nonmatch_rts]),
                            (cscatter_nonmatch, [x[0] for x in nonmatch_rts], [x[1] / nonmatch_fraction_correct for x in nonmatch_rts])]:
      scatter.X = X
      scatter.Y = Y
      scatter.x_title = "Complexity"
      scatter.y_title = "Reaction time"
      scatter.min_x = 1
      scatter.max_x = 15
      scatter.compute_trendline()
    res.append(scatter_match)
    res.append(cscatter_match)
    res.append(scatter_nonmatch)
    res.append(cscatter_nonmatch)
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(MentalRotation2TestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Reaction time (match)", "Corrected reaction time (match)",
                   "Reaction time (non-match)", "Corrected reaction time (non-match)"])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(MentalRotation2TestResult, cls).fields_to_graph() + [('match_linear_regression','slope'),('match_linear_regression','intercept'),('nonmatch_linear_regression','slope'),('nonmatch_linear_regression','intercept')]

  def to_tuple(self):
    match_ss = None
    if self.match_ss is not None:
      match_ss = self.match_ss.to_tuple()
    nonmatch_ss = None
    if self.nonmatch_ss is not None:
      nonmatch_ss = self.nonmatch_ss.to_tuple()
    return super(MentalRotation2TestResult, self).to_tuple() + (match_ss, nonmatch_ss)

  @classmethod
  def from_tuple(cls, tup):
    result = super(MentalRotation2TestResult, cls).from_tuple(tup[:-2])
    if tup[-2] is not None:
      match_ss = models2.LinearRegressionSummaryStatistic.from_tuple(tup[-2])
      match_ss.put()
      result.match_ss = match_ss
    if tup[-1] is not None:
      nonmatch_ss = models2.LinearRegressionSummaryStatistic.from_tuple(tup[-1])
      nonmatch_ss.put()
      result.nonmatch_ss = nonmatch_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(MentalRotation2TestResult, cls).make_result(variant, result_data)
    match_x_values = []
    match_y_values = []
    nonmatch_x_values = []
    nonmatch_y_values = []
    for trial in result.get_trial_results():
    #for trial in [db.get(x) for x in result.trial_results]:
      if trial.correct:
        x_val = abs(trial.rotation)
        y_val = trial.reaction_time
        if trial.correct_response:
          match_x_values.append(x_val)
          match_y_values.append(y_val)
        else:
          nonmatch_x_values.append(x_val)
          nonmatch_y_values.append(y_val)
      result.match_ss = models2.LinearRegressionSummaryStatistic.create(match_x_values, match_y_values)
      result.nonmatch_ss = models2.LinearRegressionSummaryStatistic.create(nonmatch_x_values, nonmatch_y_values)
    return result

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    self.add_observation(observations, 'boolean', trial.correct, 'correct', (trial.complexity,trial.correct_response,), (abs(trial.rotation),))
    if trial.correct:
      self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', (trial.complexity,trial.correct_response,), (abs(trial.rotation),))
    return (None, observations)

  @classmethod
  def hide_fields(cls):
    return super(MentalRotation2TestResult, cls).hide_fields().union(['Ready'])

  @classmethod
  def get_trial_class(cls):
    return MentalRotation2TrialResult

  def get_all_statistics(self):
    match_intercept = None
    match_slope = None
    nonmatch_intercept = None
    nonmatch_slope = None
    if self.match_ss:
      match_intercept = self.match_ss.linear_b
      match_slope = self.match_ss.slope_to_html()
    if self.nonmatch_ss:
      nonmatch_intercept = self.nonmatch_ss.linear_b
      nonmatch_slope = self.nonmatch_ss.slope_to_html()
    return super(MentalRotation2TestResult, self).get_all_statistics() + [("Match intercept", match_intercept),
                                                                       ("Match slope", match_slope),
                                                                       ("Non-match intercept", nonmatch_intercept),
                                                                       ("Non-match slope", nonmatch_slope)]

class FeatureMatchTestResult(TSRTestResult):
  match_ss = db.ReferenceProperty(models2.LinearRegressionSummaryStatistic, collection_name='FeatureMatch_match_set')
  nonmatch_ss = db.ReferenceProperty(models2.LinearRegressionSummaryStatistic, collection_name='FeatureMatch_nonmatch_set')

  @classmethod
  def test_name(cls):
    return "Visual Matching"

  @classmethod
  def get_result_variant_name(cls):
    return "feature_match"

  @classmethod
  def get_factor_weights(cls, group_name):
    return ([('feature_match_default', 'Score')],
            {((u'feature_match', u'default'), 'correct', 0, (False,)):
               ([-0.075, -0.058, 2.551], None), # flipped
             ((u'feature_match', u'default'), 'correct', 0, (True,)):
               ([-0.254, -0.000, 2.939], None), # flipped
             ((u'feature_match', u'default'), 'reaction time', 1, (False,)):
               ([-429.549, 33.565, 1006.864], 320570.232),
             ((u'feature_match', u'default'), 'reaction time', 1, (True,)):
               ([-585.670, 50.812, 1011.275], 274018.117)})

  def summary_statistics(self):
    match_ss = (0, 0.0, 0.0, 0.0, 0.0, 0.0)
    nonmatch_ss = (0, 0.0, 0.0, 0.0, 0.0, 0.0)
    if self.match_ss is not None:
      match_ss = self.match_ss.to_summary_statistics()
    if self.nonmatch_ss is not None:
      nonmatch_ss = self.nonmatch_ss.to_summary_statistics()
    return match_ss + nonmatch_ss

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (match_slope, match_slope_se, match_intercept, match_intercept_se), ss = models2.LinearRegressionSummaryStatistic.from_summary_statistics(ss)
    (nonmatch_slope, nonmatch_slope_se, nonmatch_intercept, nonmatch_intercept_se), ss = models2.LinearRegressionSummaryStatistic.from_summary_statistics(ss)
    results = []
    if match_slope is not None:
      results.append(("match_slope", "Match slope", "number", match_slope, match_slope_se))
    if match_intercept is not None:
      results.append(("match_intercept", "Match intercept", "number", match_intercept, match_intercept_se))
    if nonmatch_slope is not None:
      results.append(("nonmatch_slope", "Nonmatch slope", "number", nonmatch_slope, nonmatch_slope_se))
    if nonmatch_intercept is not None:
      results.append(("nonmatch_intercept", "Nonmatch intercept", "number", nonmatch_intercept, nonmatch_intercept_se))
    return results

  def full_results(self):
    res = super(FeatureMatchTestResult, self).full_results()
    res.append(("match_linear_regression", "Match linear regression", "parent", self.match_ss.full_results()))
    res.append(("nonmatch_linear_regression", "Nonmatch linear regression", "parent", self.nonmatch_ss.full_results()))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(FeatureMatchTestResult, cls).cumulative_results(results)
    titles = super(FeatureMatchTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    match_rts = []
    match_corrects = []
    nonmatch_rts = []
    nonmatch_corrects = []
    for result in results:
      trials = result.get_trial_results()
      for trial in trials:
        cval = (trial.level, trial.correct)
        if trial.correct_response:
          match_corrects.append(cval)
        else:
          nonmatch_corrects.append(cval)
        if trial.correct:
          rtval = (trial.level, trial.reaction_time)
          if trial.correct_response:
            match_rts.append(rtval)
          else:
            nonmatch_rts.append(rtval)
    match_fraction_correct = len([x for x in match_corrects if x[1]]) * 1.0 / len(match_corrects)
    nonmatch_fraction_correct = len([x for x in nonmatch_corrects if x[1]]) * 1.0 / len(nonmatch_corrects)
    scatter_match = models2.ScatterData()
    cscatter_match = models2.ScatterData()
    scatter_nonmatch = models2.ScatterData()
    cscatter_nonmatch = models2.ScatterData()
    for (scatter, X, Y) in [(scatter_match, [x[0] for x in match_rts], [x[1] for x in match_rts]),
                            (cscatter_match, [x[0] for x in match_rts], [x[1] / match_fraction_correct for x in match_rts]),
                            (scatter_nonmatch, [x[0] for x in nonmatch_rts], [x[1] for x in nonmatch_rts]),
                            (cscatter_nonmatch, [x[0] for x in nonmatch_rts], [x[1] / nonmatch_fraction_correct for x in nonmatch_rts])]:
      scatter.X = X
      scatter.Y = Y
      scatter.x_title = "Complexity"
      scatter.y_title = "Reaction time"
      scatter.min_x = 1
      scatter.max_x = 15
      scatter.compute_trendline()
    res.append(scatter_match)
    res.append(cscatter_match)
    res.append(scatter_nonmatch)
    res.append(cscatter_nonmatch)
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(FeatureMatchTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Reaction time (match)", "Corrected reaction time (match)",
                   "Reaction time (non-match)", "Corrected reaction time (non-match)"])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(FeatureMatchTestResult, cls).fields_to_graph() + [('match_linear_regression','slope'),('match_linear_regression','intercept'),('nonmatch_linear_regression','slope'),('nonmatch_linear_regression','intercept')]

  def to_tuple(self):
    match_ss = None
    if self.match_ss is not None:
      match_ss = self.match_ss.to_tuple()
    nonmatch_ss = None
    if self.nonmatch_ss is not None:
      nonmatch_ss = self.nonmatch_ss.to_tuple()
    return super(FeatureMatchTestResult, self).to_tuple() + (match_ss, nonmatch_ss)

  @classmethod
  def from_tuple(cls, tup):
    result = super(FeatureMatchTestResult, cls).from_tuple(tup[:-2])
    if tup[-2] is not None:
      match_ss = models2.LinearRegressionSummaryStatistic.from_tuple(tup[-2])
      match_ss.put()
      result.match_ss = match_ss
    if tup[-1] is not None:
      nonmatch_ss = models2.LinearRegressionSummaryStatistic.from_tuple(tup[-1])
      nonmatch_ss.put()
      result.nonmatch_ss = nonmatch_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(FeatureMatchTestResult, cls).make_result(variant, result_data)
    match_x_values = []
    match_y_values = []
    nonmatch_x_values = []
    nonmatch_y_values = []
    for trial in result.get_trial_results():
    #for trial in [db.get(x) for x in result.trial_results]:
      if trial.correct:
        x_val = trial.level
        y_val = trial.reaction_time
        if trial.correct_response:
          match_x_values.append(x_val)
          match_y_values.append(y_val)
        else:
          nonmatch_x_values.append(x_val)
          nonmatch_y_values.append(y_val)
      result.match_ss = models2.LinearRegressionSummaryStatistic.create(match_x_values, match_y_values)
      result.nonmatch_ss = models2.LinearRegressionSummaryStatistic.create(nonmatch_x_values, nonmatch_y_values)
    return result

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    self.add_observation(observations, 'boolean', trial.correct, 'correct', (trial.correct_response,), (trial.level,))
    if trial.correct:
      self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', (trial.correct_response,), (trial.level,))
    return (None, observations)

  @classmethod
  def hide_fields(cls):
    return super(FeatureMatchTestResult, cls).hide_fields().union(['Ready'])

  @classmethod
  def get_trial_class(cls):
    return FeatureMatchTrialResult

  def get_all_statistics(self):
    match_intercept = None
    match_slope = None
    nonmatch_intercept = None
    nonmatch_slope = None
    if self.match_ss:
      match_intercept = self.match_ss.linear_b
      match_slope = self.match_ss.slope_to_html()
    if self.nonmatch_ss:
      nonmatch_intercept = self.nonmatch_ss.linear_b
      nonmatch_slope = self.nonmatch_ss.slope_to_html()
    return super(FeatureMatchTestResult, self).get_all_statistics() + [("Match intercept", match_intercept),
                                                                       ("Match slope", match_slope),
                                                                       ("Non-match intercept", nonmatch_intercept),
                                                                       ("Non-match slope", nonmatch_slope)]

class SortingTrialResult(TSRTrialResult):
  stimulus_color = db.IntegerProperty()
  stimulus_shape = db.IntegerProperty()
  stimulus_number = db.IntegerProperty()
  rule = db.IntegerProperty()
  user_response = db.IntegerProperty()

  def full_results(self):
    res = super(SortingTrialResult, self).full_results()
    res.append(("species", "Species", "", self.get_shape_name(self.stimulus_shape)))
    res.append(("color", "Color", "", self.get_color_name(self.stimulus_color)))
    res.append(("number", "Number", "", self.stimulus_number))
    res.append(("rule", "Rule", "", self.get_rule_name(self.rule)))
    res.append(("response", "Response", "", self.user_response))
    return res

  def to_tuple(self):
    return super(SortingTrialResult, self).to_tuple() + (self.stimulus_color, self.stimulus_shape, self.stimulus_number, self.rule, self.user_response)

  @classmethod
  def from_tuple(cls, tup):
    (stimulus_color, stimulus_shape, stimulus_number, rule, user_response) = tup[-5:]
    result = super(SortingTrialResult, cls).from_tuple(tup[:-5])
    (result.stimulus_color, result.stimulus_shape, result.stimulus_number, result.rule, result.user_response) = (stimulus_color, stimulus_shape, stimulus_number, rule, user_response)
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(SortingTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulusColor'):
      result.stimulus_color = trial['stimulusColor']
    if trial.has_key('stimulusShape'):
      result.stimulus_shape = trial['stimulusShape']
    if trial.has_key('stimulusNumber'):
      result.stimulus_number = trial['stimulusNumber']
    if trial.has_key('rule'):
      result.rule = trial['rule']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(SortingTrialResult, cls).get_all_header_fields() + ["Color","Shape","Number","Rule","Response"]

  def get_color_name(self, color):
    return ["Red","Green","Blue","Yellow"][color]

  def get_shape_name(self, shape):
    return ["Species 1", "Species 2", "Species 3", "Species 4"][shape]

  def get_rule_name(self, rule):
    return ["Shape", "Color", "Number"][rule]

  def get_all_fields(self):
    return super(SortingTrialResult, self).get_all_fields() + ["%d (%s)"%(self.stimulus_color+1,self.get_color_name(self.stimulus_color)),
                                                           "%d (%s)"%(self.stimulus_shape+1,self.get_shape_name(self.stimulus_shape)),
                                                           self.stimulus_number+1,
                                                           self.get_rule_name(self.rule),
                                                           self.user_response+1]

  @classmethod
  def hide_fields(cls):
    return super(SortingTrialResult, cls).hide_fields().union(set(['Ready?','Duration']))

class SortingTestResult(TSRTestResult):
  switch_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name = 'Sorting_switch_set')
  nonswitch_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name = 'Sorting_nonswitch_set')

  @classmethod
  def test_name(cls):
    return "Sorting"

  @classmethod
  def get_result_variant_name(cls):
    return "sorting"

  @classmethod
  def get_factor_weights(cls, group_name):
    res = ([('sorting_default', 'Score')],
           {((u'sorting', u'default'), 'correct', 0, (0, 0, False)): ([0.158, 4.799], None),
            ((u'sorting', u'default'), 'correct', 0, (0, 0, True)): ([0.000, 19.203], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (0, 1, False)): ([0.050, 3.449], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (0, 1, True)): ([0.019, 2.916], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (0, 2, False)): ([0.049, 3.019], None),
            ((u'sorting', u'default'), 'correct', 0, (0, 2, True)): ([0.091, 2.055], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (1, 0, False)): ([0.229, 4.952], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (1, 0, True)): ([0.000, 19.203], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (1, 1, False)): ([0.062, 3.886], None),
            ((u'sorting', u'default'), 'correct', 0, (1, 1, True)): ([0.244, 3.136], None),
            ((u'sorting', u'default'), 'correct', 0, (1, 2, False)): ([0.129, 3.036], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (1, 2, True)): ([0.111, 2.244], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 0, False)): ([0.112, 5.243], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 0, True)): ([0.000, 19.203], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 1, False)): ([0.060, 3.371], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 1, True)): ([0.231, 3.007], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 2, False)): ([0.299, 2.297], None), # flipped
            ((u'sorting', u'default'), 'correct', 0, (2, 2, True)): ([0.214, 1.692], None), # flipped
            ((u'sorting', u'default'), 'reaction time', 1, (0, 0, False)): ([-251.564, 862.425], 79509.453),
            ((u'sorting', u'default'), 'reaction time', 1, (0, 0, True)): ([-324.755, 1037.735], 174110.214),
            ((u'sorting', u'default'), 'reaction time', 1, (0, 1, False)): ([-263.646, 902.871], 87309.359),
            ((u'sorting', u'default'), 'reaction time', 1, (0, 1, True)): ([-442.447, 1184.117], 385367.554),
            ((u'sorting', u'default'), 'reaction time', 1, (0, 2, False)): ([-294.293, 947.005], 114653.190),
            ((u'sorting', u'default'), 'reaction time', 1, (0, 2, True)): ([-435.939, 1262.653], 349894.006),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 0, False)): ([-255.097, 852.844], 78550.141),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 0, True)): ([-297.135, 1027.100], 95199.559),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 1, False)): ([-260.803, 912.641], 76851.392),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 1, True)): ([-411.897, 1157.721], 331161.644),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 2, False)): ([-247.375, 973.684], 95487.522),
            ((u'sorting', u'default'), 'reaction time', 1, (1, 2, True)): ([-361.776, 1234.455], 244322.228),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 0, False)): ([-258.060, 914.412], 60055.344),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 0, True)): ([-700.583, 1145.970], 2311528.707),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 1, False)): ([-295.442, 1039.403], 126674.011),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 1, True)): ([-406.261, 1273.812], 272837.595),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 2, False)): ([-346.884, 1174.727], 180632.779),
            ((u'sorting', u'default'), 'reaction time', 1, (2, 2, True)): ([-428.938, 1471.019], 362135.077)})
    return res
    W = {('Sorting', 'correct', 0, (0, 0, True)):
           ([ -8.20201673e-12,   1.92028948e+01], None),
         ('Sorting', 'correct', 0, (0, 1, True)):
           ([-0.1220539 ,  2.96176401], None),
         ('Sorting', 'correct', 0, (0, 2, True)):
           ([-0.17272795,  2.08996442], None),
         ('Sorting', 'reaction time', 1, (0, 2, False)):
           ([-283.56021913,  943.99458924], 81994.587497216358),
         ('Sorting', 'reaction time', 1, (2, 2, False)):
           ([ -339.57950671,  1179.87107829], 131637.00949783801),
         ('Sorting', 'correct', 0, (2, 2, False)):
           ([-0.35664247,  2.30827401], None),
         ('Sorting', 'correct', 0, (1, 2, True)):
           ([-0.08121081,  2.23178982], None),
         ('Sorting', 'reaction time', 1, (0, 1, False)):
           ([-259.94015388,  900.00465047], 66950.974230990978),
         ('Sorting', 'correct', 0, (0, 1, False)):
           ([-0.0923681 ,  3.47587873], None),
         ('Sorting', 'reaction time', 1, (0, 0, False)):
           ([-253.59989986,  857.94964921], 38100.310438195316),
         ('Sorting', 'reaction time', 1, (0, 1, True)):
           ([ -453.54645412,  1189.90296394], 105988.35573137531),
         ('Sorting', 'reaction time', 1, (2, 0, False)):
           ([-272.02070352,  921.66436132], 40661.428637533339),
         ('Sorting', 'reaction time', 1, (2, 1, True)):
           ([ -390.60225451,  1272.72834788], 121020.00455904081),
         ('Sorting', 'correct', 0, (2, 0, False)):
           ([ 0.2182141 ,  5.39499204], None),
         ('Sorting', 'correct', 0, (0, 2, False)):
           ([ 0.00687223,  3.0252043 ], None),
         ('Sorting', 'reaction time', 1, (1, 1, True)):
           ([ -373.90008839,  1153.09355454], 93636.424947515567),
         ('Sorting', 'reaction time', 1, (1, 0, False)):
           ([-220.16948655,  850.92066811], 30663.710315018452),
         ('Sorting', 'correct', 0, (2, 0, True)):
           ([ -6.73964683e-11,   1.92028948e+01], None),
         ('Sorting', 'correct', 0, (2, 1, False)):
           ([-0.06802794,  3.38675195], None),
         ('Sorting', 'correct', 0, (1, 1, True)):
           ([ 0.17261213,  3.10577968], None),
         ('Sorting', 'correct', 0, (1, 0, False)):
           ([ 0.02766025,  5.01672543], None),
         ('Sorting', 'reaction time', 1, (1, 1, False)):
           ([-220.73274036,  909.01105734], 62084.567316198474),
         ('Sorting', 'correct', 0, (0, 0, False)):
           ([ 0.20691433,  4.87588066], None),
         ('Sorting', 'reaction time', 1, (0, 0, True)):
           ([ -362.20959377,  1045.19608979], 24489.73557078337),
         ('Sorting', 'reaction time', 1, (1, 2, False)):
           ([-216.75806126,  973.40809929], 83163.639993698656),
         ('Sorting', 'reaction time', 1, (2, 2, True)):
           ([ -421.51122366,  1478.83338328], 226967.41250323129),
         ('Sorting', 'correct', 0, (1, 2, False)):
           ([-0.18461025,  3.03275144], None),
         ('Sorting', 'correct', 0, (2, 2, True)):
           ([-0.20398908,  1.71152509], None),
         ('Sorting', 'reaction time', 1, (1, 2, True)):
           ([ -305.4751146 ,  1235.36540186], 112685.35164438625),
         ('Sorting', 'correct', 0, (2, 1, True)):
           ([-0.36602681,  3.01111296], None),
         ('Sorting', 'reaction time', 1, (1, 0, True)):
           ([ -295.07124882,  1022.14263444], 52310.121263973793),
         ('Sorting', 'correct', 0, (1, 0, True)):
           ([ -1.05041478e-10,   1.92028948e+01], None),
         ('Sorting', 'correct', 0, (1, 1, False)):
           ([-0.00839307,  3.87386582], None),
         ('Sorting', 'reaction time', 1, (0, 2, True)):
           ([ -447.81717639,  1266.28605715], 121851.37987861691),
         ('Sorting', 'reaction time', 1, (2, 0, True)):
           ([-2207.30869608,  1153.83318011], 38135.074914903424),
         ('Sorting', 'reaction time', 1, (2, 1, False)):
           ([ -295.49932615,  1043.73699768], 97619.213444333232)}
    factor_info = [('sorting_score', 'Score')]
    return (factor_info, W)
    factor_info = [('speed', 'Speed'),
                   ('rush', 'Rush'),
                   ('context_switching', 'Context switching'),
                   ('inhibition', 'Inhibition')]
    W = {('Sorting', 'correct', 0, (0, 0, False)):
           ([0.442, -0.313, -0.065, 0.192, 4.831], 1.9250124068129382),
         ('Sorting', 'correct', 0, (1, 0, False)):
           ([0.189, -0.268, 0.059, 0.003, 4.971], 1.9094821575227516),
         ('Sorting', 'correct', 0, (2, 0, False)):
           ([0.399, -0.332, -0.095, 0.063, 5.327], 1.9225071587496327),
         ('Sorting', 'correct', 0, (0, 1, False)):
           ([0.182, -0.703, 0.044, 0.831, 3.417], 1.4255699902927508),
         ('Sorting', 'correct', 0, (1, 1, False)):
           ([0.760, -0.532, -0.823, 0.419, 4.032], 1.479719199885576),
         ('Sorting', 'correct', 0, (2, 1, False)):
           ([1.027, -1.267, 0.181, 0.350, 3.421], 1.4294924886792895),
         ('Sorting', 'correct', 0, (0, 2, False)):
           ([0.090, -0.501, -0.012, 0.841, 2.845], 1.1383229353165034),
         ('Sorting', 'correct', 0, (1, 2, False)):
           ([1.526, -1.194, -1.047, 0.193, 3.318], 1.1262998609913022),
         ('Sorting', 'correct', 0, (2, 2, False)):
           ([1.291, -1.428, -0.380, 0.218, 2.324], 0.8963501061403393),
         ('Sorting', 'correct', 0, (0, 0, True)):
           ([0.220, -0.220, -0.000, 0.000, 19.169], 1.9309655453630639),
         ('Sorting', 'correct', 0, (1, 0, True)):
           ([0.220, -0.220, 0.000, -0.000, 19.162], 1.9292670187517573),
         ('Sorting', 'correct', 0, (2, 0, True)):
           ([0.220, -0.220, -0.000, 0.000, 19.206], 1.9292670187517553),
         ('Sorting', 'correct', 0, (0, 1, True)):
           ([0.178, -0.995, 0.410, 0.861, 2.986], 1.874956195946918),
         ('Sorting', 'correct', 0, (1, 1, True)):
           ([0.125, -0.511, 0.211, 0.930, 3.131], 1.8937142397442768),
         ('Sorting', 'correct', 0, (2, 1, True)):
           ([0.783, -0.970, -0.092, 0.256, 2.942], 1.9095476272909477),
         ('Sorting', 'correct', 0, (0, 2, True)):
           ([0.249, -0.911, 0.210, 0.896, 2.045], 1.7406449733701448),
         ('Sorting', 'correct', 0, (1, 2, True)):
           ([1.098, -1.185, -0.098, 0.254, 2.195], 1.7929457268434517),
         ('Sorting', 'correct', 0, (2, 2, True)):
           ([1.256, -1.933, 0.737, 0.353, 1.680], 1.6872651151131912),
         ('Sorting', 'reaction time', 1, (0, 0, False)):
           ([-143.340, -150.517, 10.389, -21.827, 824.195], 41106.92636797347),
         ('Sorting', 'reaction time', 1, (1, 0, False)):
           ([-132.241, -149.308, 18.587, -50.525, 818.992], 45798.389008943945),
         ('Sorting', 'reaction time', 1, (2, 0, False)):
           ([-151.544, -159.744, 10.137, -22.984, 875.975], 44925.200147487725),
         ('Sorting', 'reaction time', 1, (0, 1, False)):
           ([-143.217, -147.726, 15.512, -58.010, 869.543], 88381.314013789),
         ('Sorting', 'reaction time', 1, (1, 1, False)):
           ([-126.920, -151.447, 29.689, -82.698, 883.389], 71491.46262398739),
         ('Sorting', 'reaction time', 1, (2, 1, False)):
           ([-155.819, -158.412, 5.844, -71.644, 1007.508], 111645.63187801687),
         ('Sorting', 'reaction time', 1, (0, 2, False)):
           ([-147.252, -150.857, 12.894, -96.173, 917.021], 93236.36569224783),
         ('Sorting', 'reaction time', 1, (1, 2, False)):
           ([-124.370, -151.394, 34.513, -85.053, 947.060], 88260.42643303733),
         ('Sorting', 'reaction time', 1, (2, 2, False)):
           ([-172.131, -162.691, -1.719, -109.968, 1148.155], 169117.40789186786),
         ('Sorting', 'reaction time', 1, (0, 0, True)):
           ([-170.468, -162.776, -50.439, 16.857, 1006.823], 28760.49596930323),
         ('Sorting', 'reaction time', 1, (1, 0, True)):
           ([-159.888, -160.216, -50.204, 0.764, 994.491], 70080.64174826501),
         ('Sorting', 'reaction time', 1, (2, 0, True)):
           ([-227.010, -197.094, -85.184, 105.756, 1103.199], 52734.49137586942),
         ('Sorting', 'reaction time', 1, (0, 1, True)):
           ([-153.149, -158.084, -105.386, -59.139, 1151.789], 147909.0407055014),
         ('Sorting', 'reaction time', 1, (1, 1, True)):
           ([-146.686, -160.262, -103.075, -62.956, 1123.787], 108714.4273590407),
         ('Sorting', 'reaction time', 1, (2, 1, True)):
           ([-145.673, -158.107, -98.209, -59.889, 1239.809], 131936.71885819559),
         ('Sorting', 'reaction time', 1, (0, 2, True)):
           ([-155.523, -161.200, -112.234, -85.594, 1238.642], 154565.93565439983),
         ('Sorting', 'reaction time', 1, (1, 2, True)):
           ([-137.536, -152.119, -91.923, -99.982, 1209.663], 130260.38493910524),
         ('Sorting', 'reaction time', 1, (2, 2, True)):
           ([-157.603, -160.425, -100.326, -71.644, 1451.709], 272552.40041129)}
    return (factor_info, W)

  @classmethod
  def get_conditions(cls):
    conditions = [(rule, discordance, context_switch) for rule in range(3) for discordance in range(3) for context_switch in [True, False]]
    condict = dict(zip(conditions, range(len(conditions))))
    return conditions, condict

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    prv_rule = previous_state
    rule = trial.rule
    if (prv_rule is None) or (prv_rule != rule):
      prv_rule = rule
      context_switch = True
    else:
      context_switch = False
    correct_answer = (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number)[trial.rule]
    discordance = len([x for x in (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number) if x != correct_answer])
    context_key = (2 - trial.rule, discordance, context_switch)
    correct = trial.correct
    self.add_observation(observations, 'boolean', correct, 'correct', context_key)
    if trial.correct:
      rt = trial.reaction_time
      self.add_observation(observations, 'float', rt, 'reaction time', context_key)
    return (prv_rule, observations)

  def summary_statistics(self):
    conditions, condict = self.get_conditions()
    correct_sss = [[0,0] for condition in conditions]
    rt_sss = [[0,0.0,0.0] for condition in conditions]
    trials = self.get_trial_results()
    if trials:
      prv_rule = None
      for trial in trials:
        rule = trial.rule
        if (prv_rule is None) or (prv_rule != rule):
          prv_rule = rule
          context_switch = True
        else:
          context_switch = False
        correct_answer = (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number)[trial.rule]
        discordance = len([x for x in (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number) if x != correct_answer])
        condition = (2 - trial.rule, discordance, context_switch)
        cond_i = condict[condition]
        correct = trial.correct
        correct_sss[cond_i][0] += 1
        if trial.correct:
          correct_sss[cond_i][1] += 1
          rt = trial.reaction_time
          rt_sss[cond_i][0] += 1
          rt_sss[cond_i][1] += rt
          rt_sss[cond_i][2] += rt*rt
    res = []
    for (correct_ss,rt_ss) in zip(correct_sss, rt_sss):
      res.extend(correct_ss)
      res.extend(rt_ss)
    return res

  @classmethod
  def unpack_summary_statistics(cls, ss, username, variant_info):
    prior = cls.get_prior(username, variant_info)
    if prior is not None:
      (beta_priors, rt_priors) = prior
    conditions, condict = cls.get_conditions()
    res = []
    for condition_i in xrange(len(conditions)):
      beta_prior = None
      rt_prior = None
      if prior is not None:
        beta_prior = beta_priors[condition_i]
        (n, sumx, sumx2) = rt_priors[condition_i]
        rt_prior = priors.normal_scaled_inverse_gamma_params_from_ss(n, sumx, sumx2)
      (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss, beta_prior = beta_prior)
      if (correct_p is not None) and (correct_p_se is not None):
        res.append((correct_p, correct_p_se**2))
      else:
        res.append(None)
      (rt_mean, rt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss, prior = rt_prior)
      if rt_mean and rt_se:
        res.append((rt_mean, rt_se**2))
      else:
        res.append(None)
    return res

  @classmethod
  def initialize_prior(cls):
    return None

  @classmethod
  def update_prior(cls, sss, username, variant_info):
    prior = models2.ResultsPrior.get_or_create(username, variant_info[0], variant_info[1])
    conditions, condict = cls.get_conditions()
    ss = sss
    beta_priors = []
    rt_priors = []
    for condition_i in xrange(len(conditions)):
      beta_prior, ss = models2.sum_summaries(ss, 2)
      beta_prior = [1 + beta_prior[1], beta_prior[0] - beta_prior[1] + 1]
      rt_prior, ss = models2.sum_summaries(ss, 3)
      beta_priors.append(beta_prior)
      rt_priors.append(rt_prior)
    prior.prior_data = (beta_priors, rt_priors)
    prior.put()
    return False
    prv_prior = prior.prior_data
    #logging.info("prv_prior: %s"%str(prv_prior))
    n_vars = 6
    betas = [[] for i in xrange(n_vars)]
    conditions, condict = cls.get_conditions()
    outputs = []
    #outputs = [[] for i in xrange(len(conditions))]
    for ss in sss:
      eqs = cls.build_equations([ss], prv_prior)
      (n,p,xtx,xty,yty) = util.summary_statistics_from_equations([x[0] for x in eqs], [x[1] for x in eqs], [x[2] for x in eqs], [x[3] for x in eqs])
      (beta, beta_se) = util.multiple_regression(n, xtx, xty, yty)
      if beta is not None and beta_se is not None:
        for beta_i in xrange(n_vars):
          betas[beta_i].append((beta[beta_i], beta_se[beta_i]))
        for (coeff, output, w, (var_type, var_i)) in eqs:
          if var_type == 'output':
            my_output = sum([x*y for (x,y) in zip(beta, coeff)])
            sqerr = (output - my_output)**2
            outputs.append(sqerr)
            #outputs[var_i].append(sqerr)
    output_prior = None
    if len(outputs) > 2:
      output_prior = sum(outputs) * 1.0 / len(outputs)
      #output_prior = [None] * len(outputs)
      #for i in xrange(len(outputs)):
      #  output_prior[i] = sum(outputs[i]) * 1.0 / len(outputs[i])
    beta_prior = None
    if all([len(x) > 2 for x in betas]):
      beta_prior = [None] * len(betas)
      for beta_i in xrange(n_vars):
        m_i = sum([x[0] for x in betas[beta_i]]) * 1.0 / len(betas[beta_i])
        v_i = sum([(x[0] - m_i)**2+1.0*x[1]**2 for x in betas[beta_i]]) * 1.0 / (len(betas[beta_i]) - 1.0)
        beta_prior[beta_i] = (m_i, v_i)
    if output_prior is not None and beta_prior is not None:
      prior.prior_data = (beta_prior, output_prior)
      prior.put()
      if prv_prior is None:
        return True
      total_diff = sum([(x1[0]-x2[0])**2 for (x1,x2) in zip(prv_prior[0], beta_prior)]) + \
          (prv_prior[1] - output_prior)**2
      logging.info("Total diff: %.5f"%total_diff)
      if total_diff > 1.0:
        return True
    return False

  @classmethod
  def build_equations(cls, ss, prior):
    n_vars = 6
    conditions, condict = cls.get_conditions()
    n_outputs = len(conditions)
    if prior is not None:
      (vars_prior, output_prior) = prior
    eqs = []
    for condition_i in xrange(len(conditions)):
      (rule, discordance, context_switch) = conditions[condition_i]
      c = [0.0] * n_vars
      if discordance == 1:
        c[0] = 1.0
      elif discordance == 2:
        c[1] = 1.0
      if context_switch:
        c[2] = 1.0
      if rule == 1:
        c[3] = 1.0
      elif rule == 2:
        c[4] = 1.0
      c[5] = 1.0
      (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
      (rt_mean, rt_se, n), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss, return_n = True)
      if correct_p and rt_mean:
        crt_mean = rt_mean / correct_p
        w = float(n)
        if prior is not None:
          w = w / (1.0 + output_prior)
        eqs.append((c, crt_mean, w, ('output',condition_i)))
    if prior is not None:
      for var_i in xrange(n_vars):
        (m_i, v_i) = vars_prior[var_i]
        if (m_i is not None) and (v_i is not None):
          c = [0.0] * n_vars
          c[var_i] = 1.0
          eqs.append((c, m_i, 1.0 / (1.0 + v_i), ('prior', var_i)))
    return eqs

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    prior = cls.get_prior(username, variant_info)
    prior = None
    logging.info("Using prior: %s"%str(prior))
    eqs = cls.build_equations(ss, prior)
    (n,p,xtx,xty,yty) = util.summary_statistics_from_equations([x[0] for x in eqs], [x[1] for x in eqs], [x[2] for x in eqs], [x[3] for x in eqs])
    logging.info("n: %s"%str(n))
    (beta, beta_se) = util.multiple_regression(n, xtx, xty, yty)
    results = []
    if beta:
      if beta_se is None:
        beta_se = [None] * len(beta)
      results.append(("corrected_base_reaction_time", "Corrected base reaction time", "ms", beta[5], beta_se[5]))
      results.append(("partial_discordance_cost", "Partial discordance cost", "ms", beta[0], beta_se[0]))
      results.append(("full_discordance_cost", "Full discordance cost", "ms", beta[1], beta_se[1]))
      results.append(("context_switch_cost", "Context switch cost", "ms", beta[2], beta_se[2]))
      results.append(("color_rule_cost", "Color rule cost", "ms", beta[3], beta_se[3]))
      results.append(("species_rule_cost", "Species rule cost", "ms", beta[4], beta_se[4]))
    return results

  def full_results(self):
    res = super(SortingTestResult, self).full_results()
    res.append(("switch_reaction_times", "Switch reaction times", "parent", self.switch_ss.full_results('reaction times', 'ms')))
    res.append(("nonswitch_reaction_times", "Non-switch reaction times", "parent", self.nonswitch_ss.full_results('reaction times', 'ms')))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(SortingTestResult, cls).cumulative_results(results)
    titles = super(SortingTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    rts = []
    corrects = []
    for result in results:
      trials = result.get_trial_results()
      prv_rule = None
      for trial in trials:
        if (prv_rule is None) or (trial.rule != prv_rule):
          switch = True
          prv_rule = trial.rule
        else:
          switch = False
        correct_answer = (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number)[trial.rule]
        concordant = len([x for x in (trial.stimulus_shape, trial.stimulus_color, trial.stimulus_number) if x == correct_answer])
        if trial.correct:
          rts.append([trial.rule, switch, concordant, trial.reaction_time])
        corrects.append([trial.rule, switch, concordant, trial.correct])
    preds = [[(lambda x: True, "Overall"),
              (lambda x: x[0] == 0, "Rule: species"),
              (lambda x: x[0] == 1, "Rule: color"),
              (lambda x: x[0] == 2, "Rule: number")],
             [(lambda x: True, "Overall"),
              (lambda x: x[1], "Task switch"),
              (lambda x: not x[1], "Task nonswitch")],
             [(lambda x: True, "Overall"),
              (lambda x: x[2] == 3, "Fully concordant"),
              (lambda x: x[2] == 2, "Partially concordant"),
              (lambda x: x[2] == 1, "Discordant")]]
    for pred_set in preds:
      pred_set2 = [(pred,title) for (pred,title) in pred_set if len([x for x in rts if pred(x)]) > 1]
      flsss = [models2.FloatSummaryStatistic.create([x[3] for x in rts if pred(x)]) for (pred,title) in pred_set2]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set2]))
      boolsss = [models2.BooleanSummaryStatistic.create([x[3] for x in corrects if pred(x)]) for (pred,title) in pred_set2]
      cflsss = [models2.FloatSummaryStatistic.create([x[3] / bss.fraction_positive for x in rts if pred(x)]) for ((pred,title),bss) in zip(pred_set2,boolsss)]
      res.append(models2.CumulativeStatistic.from_float_sss(cflsss, [x[1] for x in pred_set2]))
      res.append(models2.CumulativeStatistic.from_boolean_sss(boolsss, [x[1] for x in pred_set2]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(SortingTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["%s by %s"%(measure, category) for category in ["rule", "rule switching", "level of concordancy"] for measure in ["Reaction time", "Corrected reaction time", "Correct"]])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(SortingTestResult, cls).fields_to_graph() + [('switch_reaction_times','mean'),('nonswitch_reaction_time','mean')]

  def to_tuple(self):
    return super(SortingTestResult, self).to_tuple() + (self.switch_ss.to_tuple(), self.nonswitch_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(SortingTestResult, cls).from_tuple(tup[:-2])
    switch_ss = models2.FloatSummaryStatistic.from_tuple(tup[-2])
    switch_ss.put()
    result.switch_ss = switch_ss
    nonswitch_ss = models2.FloatSummaryStatistic.from_tuple(tup[-1])
    nonswitch_ss.put()
    result.nonswitch_ss = nonswitch_ss
    return result

  @classmethod
  def hide_fields(cls):
    return super(SortingTestResult, cls).hide_fields().union(['Ready', 'Reaction time'])

  @classmethod
  def get_trial_class(cls):
    return SortingTrialResult

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(SortingTestResult, cls).make_result(variant, result_data)
    if result:
      if result_data.has_key('trials'):
        rts = {False: [], True: []}
        prv_rule = None
        for trial_i in xrange(len(result_data['trials'])):
          if result_data['trials'][trial_i].has_key('rule') and result_data['trials'][trial_i].has_key('reactionTime'):
            cur_rule = result_data['trials'][trial_i]['rule']
            is_switch = (prv_rule is None) or (prv_rule != cur_rule)
            prv_rule = cur_rule
            rt = result_data['trials'][trial_i]['reactionTime']
            rts[is_switch].append(rt)
        result.switch_ss = models2.FloatSummaryStatistic.create(rts[True])
        result.nonswitch_ss = models2.FloatSummaryStatistic.create(rts[False])
      return result

  def get_all_statistics(self):
    return super(SortingTestResult, self).get_all_statistics() + [("Overall reaction time", self.reaction_time_ss.to_html(ms=True)),
                                                              ("Context-switch reaction time", self.switch_ss.to_html(ms=True)),
                                                              ("No context-switch reaction time", self.nonswitch_ss.to_html(ms=True))]

class ColorWordTrialResult(TSRTrialResult):
  word = db.IntegerProperty()
  color = db.IntegerProperty()
  is_task_word = db.BooleanProperty()
  user_response = db.IntegerProperty()

  def full_results(self):
    res = super(ColorWordTrialResult, self).full_results()
    res.append(("word", "Word", "", self.get_color_name(self.word)))
    res.append(("color", "Color", "", self.get_color_name(self.color)))
    res.append(("task", "Task", "", "Word" if self.is_task_word else "Color"))
    res.append(("response", "Response", "", self.get_color_name(self.user_response)))
    return res

  def to_tuple(self):
    return super(ColorWordTrialResult, self).to_tuple() + (self.word, self.color, self.is_task_word, self.user_response)

  @classmethod
  def from_tuple(cls, tup):
    (word, color, is_task_word, user_response) = tup[-4:]
    result = super(ColorWordTrialResult, cls).from_tuple(tup[:-4])
    (result.word, result.color, result.is_task_word, result.user_response) = (word, color, is_task_word, user_response)
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(ColorWordTrialResult, cls).create(trial, test_result)
    if trial.has_key('isTaskWord'):
      result.is_task_word = trial['isTaskWord']
    if trial.has_key('word'):
      result.word = trial['word']
    if trial.has_key('color'):
      result.color = trial['color']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(ColorWordTrialResult, cls).get_all_header_fields() + ["Word", "Color", "Task", "Response"]

  def get_color_name(self, color):
    return ["Red","Green","Blue"][color]

  def get_all_fields(self):
    return super(ColorWordTrialResult, self).get_all_fields() + [self.get_color_name(self.word),
                                                              self.get_color_name(self.color),
                                                              "Word" if self.is_task_word else "Color",
                                                              self.get_color_name(self.user_response)]

  @classmethod
  def hide_fields(cls):
    return super(ColorWordTrialResult, cls).hide_fields().union(set(['Ready?','Duration']))

class CuedAttentionTrialResult(ReactionTimeTrialResult):
  true_choice = db.IntegerProperty()
  user_choice = db.IntegerProperty()
  missed = db.BooleanProperty()
  cue_location = db.IntegerProperty()
  stimulus_location = db.IntegerProperty()

  def full_results(self):
    res = super(CuedAttentionTrialResult, self).full_results()
    res.append(('stimulus', 'Stimulus', '', self.true_choice))
    res.append(('response', 'Response', '', self.user_choice))
    res.append(('missed', 'Missed', '', self.missed))
    return res

  def to_tuple(self):
    return super(CuedAttentionTrialResult, self).to_tuple() + (self.true_choice, self.user_choice, self.missed, self.cue_location, self.stimulus_location)

  @classmethod
  def from_tuple(cls, tup):
    true_choice, user_choice, missed, cue_location, stimulus_location = tup[-5:]
    result = super(CuedAttentionTrialResult, cls).from_tuple(tup[:-5])
    result.true_choice = true_choice
    result.user_choice = user_choice
    result.missed = missed
    result.cue_location = cue_location
    result.stimulus_location = stimulus_location
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(CuedAttentionTrialResult, cls).create(trial, test_result)
    if trial.has_key('stimulus'):
      result.true_choice = trial['stimulus']
    if trial.has_key('userResponse'):
      result.user_choice = trial['userResponse']
    if trial.has_key('missed'):
      result.missed = trial['missed']
    if trial.has_key('cueLocation'):
      result.cue_location = trial['cueLocation']
    if trial.has_key('stimulusLocation'):
      result.stimulus_location = trial['stimulusLocation']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(CuedAttentionTrialResult, cls).get_all_header_fields() + ['Stimulus', 'User response', 'Missed']

  def get_all_fields(self):
    return super(CuedAttentionTrialResult, self).get_all_fields() + [self.true_choice, self.user_choice, self.missed]

class CuedAttentionTestResult(ReactionTimeTestResult):
  congruent_missed_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='cued_attention_congruent_missed_set')
  incongruent_missed_ss = db.ReferenceProperty(models2.BooleanSummaryStatistic, collection_name='cued_attention_incongruent_missed_set')
  congruent_reaction_time_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='cued_attention_congruent_reaction_time')
  incongruent_reaction_time_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='cued_attention_incongruent_reaction_time')

  @classmethod
  def test_name(cls):
    return "Cued Attention"

  @classmethod
  def get_result_variant_name(cls):
    return "cued_attention"

  def to_tuple(self):
    return super(CuedAttentionTestResult, self).to_tuple() + \
        (models2.BooleanSummaryStatistic.ss_to_tuple(self.congruent_missed_ss),
         models2.BooleanSummaryStatistic.ss_to_tuple(self.incongruent_missed_ss),
         models2.FloatSummaryStatistic.ss_to_tuple(self.congruent_reaction_time_ss),
         models2.FloatSummaryStatistic.ss_to_tuple(self.incongruent_reaction_time_ss))

  @classmethod
  def from_tuple(cls, tup):
    (congruent_missed_ss_tup,incongruent_missed_ss_tup,congruent_reaction_time_ss_tup,incongruent_reaction_time_ss_tup,) = tup[-4:]
    result = super(CuedAttentionTestResult, cls).from_tuple(tup[:-4])
    congruent_missed_ss = models2.BooleanSummaryStatistic.from_tuple(congruent_missed_ss_tup)
    incongruent_missed_ss = models2.BooleanSummaryStatistic.from_tuple(incongruent_missed_ss_tup)
    congruent_reaction_time_ss = models2.FloatSummaryStatistic.from_tuple(congruent_reaction_time_ss_tup)
    incongruent_reaction_time_ss = models2.FloatSummaryStatistic.from_tuple(incongruent_reaction_time_ss_tup)
    congruent_missed_ss.put()
    incongruent_missed_ss.put()
    congruent_reaction_time_ss.put()
    incongruent_reaction_time_ss.put()
    result.congruent_missed_ss = congruent_missed_ss
    result.incongruent_missed_ss = incongruent_missed_ss
    result.congruent_reaction_time_ss = congruent_reaction_time_ss
    result.incongruent_reaction_time_ss = incongruent_reaction_time_ss
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(CuedAttentionTestResult, cls).make_result(variant, result_data)
    if result:
      trials = result.get_trial_results()
      congruent_trials = [x for x in trials if x.ready and x.cue_location == x.stimulus_location]
      incongruent_trials = [x for x in trials if x.ready and x.cue_location != x.stimulus_location]
      if len(congruent_trials) > 0:
        result.congruent_missed_ss = models2.BooleanSummaryStatistic.create([x.missed for x in congruent_trials])
        result.congruent_reaction_time_ss = models2.FloatSummaryStatistic.create([x.reaction_time for x in congruent_trials if x.correct])
      if len(incongruent_trials) > 0:
        result.incongruent_missed_ss = models2.BooleanSummaryStatistic.create([x.missed for x in incongruent_trials])
        result.incongruent_reaction_time_ss = models2.FloatSummaryStatistic.create([x.reaction_time for x in incongruent_trials if x.correct])
      return result

  def get_all_statistics(self):
    return super(CuedAttentionTestResult, self).get_all_statistics() + [("Missed", self.missed_ss.to_html())]

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    congruent = trial.cue_location == trial.stimulus_location
    context = (congruent,)
    self.add_observation(observations, 'boolean', trial.ready, 'ready', context)
    if trial.ready:
      self.add_observation(observations, 'boolean', trial.correct, 'correct', context)
      self.add_observation(observations, 'boolean', trial.missed, 'missed', context)
      if trial.correct:
        self.add_observation(observations, 'float', trial.reaction_time, 'reaction time', context)
    return (None, observations)

  def summary_statistics(self):
    readyss = (0, 0)
    if self.ready_ss is not None:
      readyss = self.ready_ss.to_summary_statistics()
    crtss = (0, 0.0, 0.0)
    if self.congruent_reaction_time_ss is not None:
      crtss = self.congruent_reaction_time_ss.to_summary_statistics()
    cmss = (0, 0)
    if self.congruent_missed_ss is not None:
      cmss = self.congruent_missed_ss.to_summary_statistics()
    icrtss = (0, 0.0, 0.0)
    if self.incongruent_reaction_time_ss is not None:
      icrtss = self.incongruent_reaction_time_ss.to_summary_statistics()
    icmss = (0, 0)
    if self.incongruent_missed_ss is not None:
      icmss = self.incongruent_missed_ss.to_summary_statistics()
    return readyss+crtss+cmss+icrtss+icmss

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (ready_p, ready_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (crt_mean, crt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (cm_p, cm_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    (icrt_mean, icrt_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (icm_p, icm_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
    results = []
    if ready_p is not None:
      results.append(("ready", "Ready", "fraction", ready_p, ready_p_se))
    if cm_p is not None:
      results.append(("congruent_missed", "Congruent missed", "fraction", cm_p, cm_se))
    if crt_mean is not None:
      results.append(("congruent_reaction_time", "Congruent reaction time", "ms", crt_mean, crt_se))
    if icm_p is not None:
      results.append(("incongruent_missed", "Incongruent missed", "fraction", icm_p, icm_se))
    if icrt_mean is not None:
      results.append(("incongruent_reaction_time", "Incongruent reaction time", "ms", icrt_mean, icrt_se))
    return results

  def full_results(self):
    res = super(CuedAttentionTestResult, self).full_results()
    res.append(('missed_trials', 'Missed trials', 'parent', self.missed_ss.full_results()))
    return res

  @classmethod
  def get_trial_class(cls):
    return CuedAttentionTrialResult

class ColorWordTestResult(TSRTestResult):
  switch_concordant_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='ColorWord_switch_concordant_set')
  switch_discordant_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='ColorWord_switch_discordant_set')
  nonswitch_concordant_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='ColorWord_nonswitch_concordant_set')
  nonswitch_discordant_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='ColorWord_nonswitch_discordant_set')

  @classmethod
  def test_name(cls):
    return "Color-Word"

  @classmethod
  def get_result_variant_name(cls):
    return "color_word"

  @classmethod
  def get_factor_weights(self, group_name):
    return ([('color_word_default', 'Score')],
            {((u'color_word', u'default'), 'correct', 0, (False, False, False)):
               ([0.026, 2.475], None),
             ((u'color_word', u'default'), 'correct', 0, (False, False, True)):
               ([0.106, 2.220], None),
             ((u'color_word', u'default'), 'correct', 0, (False, True, False)):
               ([0.853, 4.314], None), # flipped sign
             ((u'color_word', u'default'), 'correct', 0, (False, True, True)):
               ([1.142, 4.852], None), # flipped sign
             ((u'color_word', u'default'), 'correct', 0, (True, False, False)):
               ([0.190, 3.299], None), # flipped sign
             ((u'color_word', u'default'), 'correct', 0, (True, False, True)):
               ([0.141, 2.520], None),
             ((u'color_word', u'default'), 'correct', 0, (True, True, False)):
               ([0.791, 4.380], None), # flipped sign
             ((u'color_word', u'default'), 'correct', 0, (True, True, True)):
               ([1.338, 5.374], None), # flipped sign
             ((u'color_word', u'default'), 'reaction time', 1, (False, False, False)):
               ([-423.173, 1004.751], 231128.678),
             ((u'color_word', u'default'), 'reaction time', 1, (False, False, True)):
               ([-441.665, 1133.889], 378624.645),
             ((u'color_word', u'default'), 'reaction time', 1, (False, True, False)):
               ([-323.503, 788.791], 84251.869),
             ((u'color_word', u'default'), 'reaction time', 1, (False, True, True)):
               ([-422.911, 1024.568], 220467.110),
             ((u'color_word', u'default'), 'reaction time', 1, (True, False, False)):
               ([-378.431, 844.110], 158631.882),
             ((u'color_word', u'default'), 'reaction time', 1, (True, False, True)):
               ([-512.365, 1123.117], 434296.643),
             ((u'color_word', u'default'), 'reaction time', 1, (True, True, False)):
               ([-376.344, 828.091], 135445.822),
             ((u'color_word', u'default'), 'reaction time', 1, (True, True, True)):
               ([-485.543, 1168.044], 548887.162)})
    #return None
    factor_info = [('color_word_score', 'Score')]
    W = {('Color-Word', 'correct', 0, (False, False, True)):
           ([ 0.11484744,  2.20396405], None),
         ('Color-Word', 'correct', 0, (False, False, False)):
           ([ 0.03531188,  2.47462981], None),
         ('Color-Word', 'correct', 0, (True, False, True)):
           ([ 0.1353661 ,  2.49763615], None),
         ('Color-Word', 'correct', 0, (True, False, False)):
           ([-0.18548703,  3.31138048], None),
         ('Color-Word', 'correct', 0, (False, True, False)):
           ([-0.80003068,  4.27822041], None),
         ('Color-Word', 'correct', 0, (True, True, False)):
           ([-0.69054766,  4.32405972], None),
         ('Color-Word', 'correct', 0, (False, True, True)):
           ([-1.16507361,  4.8612506 ], None),
         ('Color-Word', 'correct', 0, (True, True, True)):
           ([-2.17478938,  6.16396977], None),
         ('Color-Word', 'reaction time', 1, (False, True, False)):
           ([-334.99681763,  795.78828699], 63876.736555842668),
         ('Color-Word', 'reaction time', 1, (True, True, False)):
           ([-380.95015408,  828.67829457], 91892.620263831792),
         ('Color-Word', 'reaction time', 1, (True, False, False)):
           ([-381.57113547,  846.34861625], 125939.26850564954),
         ('Color-Word', 'reaction time', 1, (False, False, False)):
           ([ -424.7347009 ,  1008.00535241], 181347.97908782947),
         ('Color-Word', 'reaction time', 1, (False, True, True)):
           ([ -446.72549789,  1039.29896146], 123479.33299618478),
         ('Color-Word', 'reaction time', 1, (True, False, True)):
           ([ -515.25486454,  1131.77870856], 321377.46754799061),
         ('Color-Word', 'reaction time', 1, (False, False, True)):
           ([ -449.15515389,  1143.96706214], 280761.2049338856),
         ('Color-Word', 'reaction time', 1, (True, True, True)):
           ([ -534.20250512,  1188.83811709], 181185.93671456282)}
    return (factor_info, W)

  @classmethod
  def get_conditions(cls):
    conditions = [(name_the_color, discordance, context_switch) for name_the_color in [True, False] for discordance in [True, False] for context_switch in [True, False]]
    condict = dict(zip(conditions, range(len(conditions))))
    return conditions, condict

  def summary_statistics(self):
    conditions, condict = self.get_conditions()
    correct_sss = [[0,0] for condition in conditions]
    rt_sss = [[0,0.0,0.0] for condition in conditions]
    trials = self.get_trial_results()
    if trials:
      prv_task = None
      for trial in trials:
        if (prv_task is None) or (trial.is_task_word != prv_task):
          switch = True
          prv_task = trial.is_task_word
        else:
          switch = False
        concordant = trial.word == trial.color
        condition = (not trial.is_task_word, not concordant, switch)
        cond_i = condict[condition]
        correct = trial.correct
        correct_sss[cond_i][0] += 1
        if trial.correct:
          correct_sss[cond_i][1] += 1
          rt = trial.reaction_time
          rt_sss[cond_i][0] += 1
          rt_sss[cond_i][1] += rt
          rt_sss[cond_i][2] += rt*rt
    res = []
    for (correct_ss,rt_ss) in zip(correct_sss, rt_sss):
      res.extend(correct_ss)
      res.extend(rt_ss)
    return res

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    prv_rule = previous_state
    rule = trial.is_task_word
    if rule is None:
      rule = False
    if (prv_rule is None) or (prv_rule != rule):
      prv_rule = rule
      context_switch = True
    else:
      context_switch = False
    concordant = trial.word == trial.color
    correct = trial.correct and (100 <= trial.reaction_time <= 5000)
    context_key = (rule, concordant, context_switch)
    self.add_observation(observations, 'boolean', correct, 'correct', context_key)
    if correct:
      rt = trial.reaction_time
      self.add_observation(observations, 'float', rt, 'reaction time', context_key)
    return (prv_rule, observations)

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    conditions, condict = cls.get_conditions()
    eqs = []
    for (name_the_color, discordance, context_switch) in conditions:
      c = [0.0] * 4
      if name_the_color:
        c[0] = 1.0
      if discordance:
        c[1] = 1.0
      if context_switch:
        c[2] = 1.0
      c[3] = 1.0
      (correct_p, correct_p_se), ss = models2.BooleanSummaryStatistic.from_summary_statistics(ss)
      (rt_mean, rt_se, n), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss, return_n = True)
      if correct_p and rt_mean:
        crt_mean = rt_mean / correct_p
        w = float(n)
        eqs.append((c, crt_mean, n))
    (n,p,xtx,xty,yty) = util.summary_statistics_from_equations([x[0] for x in eqs], [x[1] for x in eqs], [x[2] for x in eqs])
    (beta, beta_se) = util.multiple_regression(n, xtx, xty, yty)
    results = []
    if beta:
      if beta_se is None:
        beta_se = [None] * len(beta)
      results.append(("corrected_base_reaction_time", "Corrected base reaction time", "ms", beta[3], beta_se[3]))
      results.append(("color_naming_cost", "Color naming cost", "ms", beta[0], beta_se[0]))
      results.append(("interference_cost", "Interference cost", "ms", beta[1], beta_se[1]))
      results.append(("context_switch_cost", "Context switch cost", "ms", beta[2], beta_se[2]))
    return results

  def full_results(self):
    res = super(ColorWordTestResult, self).full_results()
    res.append(("switch_concordant_reaction_times", "Switch concordant reaction times", "parent", self.switch_concordant_ss.full_results('reaction times', 'ms')))
    res.append(("nonswitch_concordant_reaction_times", "Non-switch concordant reaction times", "parent", self.nonswitch_concordant_ss.full_results('reaction times', 'ms')))
    res.append(("switch_discordant_reaction_times", "Switch discordant reaction times", "parent", self.switch_discordant_ss.full_results('reaction times', 'ms')))
    res.append(("nonswitch_discordant_reaction_times", "Non-switch discordant reaction times", "parent", self.nonswitch_discordant_ss.full_results('reaction times', 'ms')))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(ColorWordTestResult, cls).cumulative_results(results)
    titles = super(ColorWordTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    rts = []
    corrects = []
    for result in results:
      trials = result.get_trial_results()
      prv_task = None
      for trial in trials:
        if (prv_task is None) or (trial.is_task_word != prv_task):
          switch = True
          prv_task = trial.is_task_word
        else:
          switch = False
        concordant = trial.word == trial.color
        if trial.correct:
          rts.append([trial.is_task_word, switch, concordant, trial.reaction_time])
        corrects.append([trial.is_task_word, switch, concordant, trial.correct])
    preds = [[(lambda x: True, "Overall"),
              (lambda x: x[0], "Task: word"),
              (lambda x: not x[0], "Task: color")],
             [(lambda x: True, "Overall"),
              (lambda x: x[1], "Task switch"),
              (lambda x: not x[1], "Task nonswitch")],
             [(lambda x: True, "Overall"),
              (lambda x: x[2], "Concordant"),
              (lambda x: not x[2], "Discordant")]]
    for pred_set in preds:
      flsss = [models2.FloatSummaryStatistic.create([x[3] for x in rts if pred(x)]) for (pred,title) in pred_set]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set]))
      boolsss = [models2.BooleanSummaryStatistic.create([x[3] for x in corrects if pred(x)]) for (pred,title) in pred_set]
      cflsss = [models2.FloatSummaryStatistic.create([x[3] / bss.fraction_positive for x in rts if pred(x)]) for ((pred,title),bss) in zip(pred_set,boolsss)]
      res.append(models2.CumulativeStatistic.from_float_sss(cflsss, [x[1] for x in pred_set]))
      res.append(models2.CumulativeStatistic.from_boolean_sss(boolsss, [x[1] for x in pred_set]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(ColorWordTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["%s by %s"%(measure, category) for category in ["task", "task switch", "concordant / discordant"] for measure in ["Reaction time", "Corrected reaction time", "Correct"]])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(ColorWordTestResult, cls).fields_to_graph() + [('switch_concordant_reaction_times','mean'),('nonswitch_concordant_reaction_times','mean'), ('switch_discordant_reaction_times','mean'),('nonswitch_discordant_reaction_times','mean')]

  def to_tuple(self):
    return super(ColorWordTestResult, self).to_tuple() + (self.switch_concordant_ss.to_tuple(), self.nonswitch_concordant_ss.to_tuple(), self.switch_discordant_ss.to_tuple(), self.nonswitch_discordant_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(ColorWordTestResult, cls).from_tuple(tup[:-4])
    (switch_concordant_ss_tup, nonswitch_concordant_ss_tup, switch_discordant_ss_tup, nonswitch_discordant_ss_tup) = tup[-4:]
    switch_concordant = models2.FloatSummaryStatistic.from_tuple(switch_concordant_ss_tup)
    switch_concordant.put()
    result.switch_concordant_ss = switch_concordant
    switch_discordant = models2.FloatSummaryStatistic.from_tuple(switch_discordant_ss_tup)
    switch_discordant.put()
    result.switch_discordant_ss = switch_discordant
    nonswitch_discordant = models2.FloatSummaryStatistic.from_tuple(nonswitch_discordant_ss_tup)
    nonswitch_discordant.put()
    result.nonswitch_discordant_ss = nonswitch_discordant
    nonswitch_concordant = models2.FloatSummaryStatistic.from_tuple(nonswitch_concordant_ss_tup)
    nonswitch_concordant.put()
    result.nonswitch_concordant_ss = nonswitch_concordant
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(ColorWordTestResult, cls).make_result(variant, result_data)
    rts = {(False,False): [],
           (False, True): [],
           (True, False): [],
           (True,  True): []}
    prv_task = None
    trials = result.get_trial_results()
    #trials = [db.get(x) for x in result.trial_results]
    for trial in trials:
      cur_task = trial.is_task_word
      is_switch = (prv_task is None) or (prv_task != cur_task)
      prv_task = cur_task
      is_concordant = trial.word == trial.color
      rt = trial.reaction_time
      rts[is_switch, is_concordant].append(rt)
    result.switch_concordant_ss = models2.FloatSummaryStatistic.create(rts[True, True])
    result.switch_discordant_ss = models2.FloatSummaryStatistic.create(rts[True, False])
    result.nonswitch_concordant_ss = models2.FloatSummaryStatistic.create(rts[False, True])
    result.nonswitch_discordant_ss = models2.FloatSummaryStatistic.create(rts[False, False])
    return result

  @classmethod
  def hide_fields(cls):
    return super(ColorWordTestResult, cls).hide_fields().union(['Ready', 'Reaction time'])

  def get_all_statistics(self):
    return super(ColorWordTestResult, self).get_all_statistics() + [("Overall reaction time", self.reaction_time_ss.to_html(ms=True)),
                                                                 ("Context-switch, concordant reaction time", self.switch_concordant_ss.to_html(ms=True)),
                                                                 ("Context-switch, discordant reaction time", self.switch_discordant_ss.to_html(ms=True)),
                                                                 ("No context-switch, concordant reaction time", self.nonswitch_concordant_ss.to_html(ms=True)),
                                                                 ("No context-switch, discordant reaction time", self.nonswitch_discordant_ss.to_html(ms=True))]

  @classmethod
  def get_trial_class(cls):
    return ColorWordTrialResult

class FingerTappingTrialResult(MultitrialTrialResult):
  taps = db.ListProperty(int)
  tap_count = db.IntegerProperty()
  is_hand_right = db.BooleanProperty()

  def full_results(self):
    res = super(FingerTappingTrialResult, self).full_results()
    res.append(("hand", "Hand", "", "Right" if self.is_hand_right else "Left"))
    res.append(("tap_count", "Tap count", "", self.tap_count))
    res.append(("taps", "Taps", "parent", [("tap", "Tap", "ms", tap) for tap in self.taps]))
    return res

  def to_tuple(self):
    return super(FingerTappingTrialResult, self).to_tuple() + (self.taps, self.tap_count, self.is_hand_right)

  @classmethod
  def from_tuple(cls, tup):
    (taps, tap_count, is_hand_right) = tup[-3:]
    result = super(FingerTappingTrialResult, cls).from_tuple(tup[:-3])
    (result.taps, result.tap_count, result.is_hand_right) = (taps, tap_count, is_hand_right)
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(FingerTappingTrialResult, cls).create(trial, test_result)
    if trial.has_key('tapCount'):
      result.tap_count = trial['tapCount']
    if trial.has_key('taps'):
      result.taps = trial['taps']
    if trial.has_key('hand'):
      result.is_hand_right = trial['hand'] == 'right'
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(FingerTappingTrialResult, cls).get_all_header_fields() + ["Taps", "Hand"]

  def get_all_fields(self):
    return super(FingerTappingTrialResult, self).get_all_fields() + [self.tap_count,
                                                                     "Right" if self.is_hand_right else "Left"]

  @classmethod
  def hide_fields(cls):
    return super(FingerTappingTrialResult, cls).hide_fields().union(set(['Ready?','Reaction time', 'Correct?', 'Duration']))

class FingerTappingTestResult(MultitrialTestResult):
  left_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='FingerTapping_left_set')
  right_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='FingerTapping_right_set')
  left_tps = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='FingerTapping_left_tps_set')
  right_tps = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='FingerTapping_right_tps_set')

  @classmethod
  def test_name(cls):
    return "Finger Tapping"

  @classmethod
  def get_result_variant_name(cls):
    return "finger_tapping"

  @classmethod
  def get_factor_weights(cls, group_name):
    return ([('finger_tapping_default', 'Score')],
            {((u'finger_tapping', u'default'), 'tap interval', 1, (False,)): ([-23.738, 155.623], 2813.920),
             ((u'finger_tapping', u'default'), 'tap interval', 1, (True,)): ([-18.549, 144.948], 2470.463)})
    W = {('Finger Tapping', 'tap interval', 1, (False,)):
           ([ -23.74283635,  155.21351295], 2601.4206440290941),
         ('Finger Tapping', 'tap interval', 1, (True,)):
           ([ -18.63106177,  144.67329813], 2329.9046016461316)}
    factor_info = [('finger_tapping_score', 'Score')]
    return (factor_info, W)

  def __init__(self, *args, **kwargs):
    super(FingerTappingTestResult, self).__init__(*args, **kwargs)
    if (self.trial_results is not None) and (self.left_tps is None or self.right_tps is None):
      self.compute_tps()
      self.put()

  def summary_statistics(self):
    return self.left_tps.to_summary_statistics() + self.right_tps.to_summary_statistics()

  @classmethod
  def summary_metrics(cls, ss, username = None, variant_info = None):
    (left_tps_mean, left_tps_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    (right_tps_mean, right_tps_se), ss = models2.FloatSummaryStatistic.from_summary_statistics(ss)
    results = []
    if left_tps_mean is not None:
      left_tps_mean = left_tps_mean / 3600
      if left_tps_se is not None:
        left_tps_se = left_tps_se / 3600
      results.append(("left_tps", "Taps per second (left)", "number", left_tps_mean, left_tps_se))
    if right_tps_mean is not None:
      right_tps_mean = right_tps_mean / 3600
      if right_tps_se is not None:
        right_tps_se = right_tps_se / 3600
      results.append(("right_tps", "Taps per second (right)", "number", right_tps_mean, right_tps_se))
    return results

  def full_results(self):
    res = super(FingerTappingTestResult, self).full_results()
    res.append(("left_taps", "Left taps", "parent", self.left_ss.full_results('tap times', 'ms')))
    res.append(("right_taps", "Right taps", "parent", self.right_ss.full_results('tap times', 'ms')))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(FingerTappingTestResult, cls).cumulative_results(results)
    titles = super(FingerTappingTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    tps = []
    for result in results:
      trials = result.get_trial_results()
      for trial in trials:
        val = (trial.is_hand_right, trial.tap_count * 1000.0 / trial.duration)
        tps.append(val)
    preds = [[(lambda x: True, "Overall"),
              (lambda x: not x[0], "Left"),
              (lambda x: x[0], "Right")]]
    for pred_set in preds:
      flsss = [models2.FloatSummaryStatistic.create([x[1] for x in tps if pred(x)]) for (pred,title) in pred_set]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return []

  @classmethod
  def cumulative_titles(cls):
    titles = super(FingerTappingTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["Taps per second"])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(FingerTappingTestResult, cls).fields_to_graph() + [('left_taps','mean'),('right_taps','mean')]

  def to_tuple(self):
    return super(FingerTappingTestResult, self).to_tuple() + (self.left_ss.to_tuple(), self.right_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(FingerTappingTestResult, cls).from_tuple(tup[:-2])
    left_ss = models2.FloatSummaryStatistic.from_tuple(tup[-2])
    left_ss.put()
    result.left_ss = left_ss
    right_ss = models2.FloatSummaryStatistic.from_tuple(tup[-1])
    right_ss.put()
    result.right_ss = right_ss
    return result

  def compute_tps(self):
    left_tps = []
    right_tps = []
    trials = self.get_trial_results()
    for trial in trials:
      if trial.is_hand_right:
        right_tps.append(trial.tap_count * 3600 * 1000.0 / trial.duration)
      else:
        left_tps.append(trial.tap_count * 3600 * 1000.0 / trial.duration)
    self.left_tps = models2.FloatSummaryStatistic.create(left_tps)
    self.right_tps = models2.FloatSummaryStatistic.create(right_tps)

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(FingerTappingTestResult, cls).make_result(variant, result_data)
    trials = result.get_trial_results()
    #trials = [db.get(x) for x in result.trial_results]
    left_values = []
    right_values = []
    for trial in trials:
      cur_time = 0
      vals = []
      for tap_time in trial.taps:
        val = tap_time - cur_time
        cur_time = tap_time
        vals.append(val)
      if trial.is_hand_right:
        right_values.extend(vals)
      else:
        left_values.extend(vals)
    result.left_ss = models2.FloatSummaryStatistic.create(left_values)
    result.right_ss = models2.FloatSummaryStatistic.create(right_values)
    result.compute_tps()
    return result

  def collect_trial_observations(self, trial, previous_state = None):
    observations = {}
    context_key = (trial.is_hand_right,)
    prv_tap = 0
    for tap_time in trial.taps:
      tap_interval = tap_time - prv_tap
      prv_tap = tap_time
      self.add_observation(observations, 'float', tap_interval, 'tap interval', context_key)
    return (None, observations)

  @classmethod
  def get_trial_class(cls):
    return FingerTappingTrialResult

  @classmethod
  def hide_fields(cls):
    return super(FingerTappingTestResult, cls).hide_fields().union(['Trial duration'])

  def get_all_statistics(self):
    return super(FingerTappingTestResult, self).get_all_statistics() + [("Left interval", self.left_ss.to_html(ms=True)),
                                                                        ("Right interval", self.right_ss.to_html(ms=True)),
                                                                        ("Left taps per hour", self.left_tps.to_html()),
                                                                        ("Right taps per hour", self.right_tps.to_html())]

class AttentionalBiasTestVariant(TSRTestVariant):
  n_base_trials = db.IntegerProperty()
  fixation_duration = db.IntegerProperty()
  display_duration = db.IntegerProperty()
  list1_name = db.StringProperty()
  list1_description = db.StringProperty()
  list2_name = db.StringProperty()
  list2_description = db.StringProperty()
  neutral_name = db.StringProperty()
  neutral_description = db.StringProperty()

  @classmethod
  def get_result_class(cls):
    return AttentionalBiasTestResult

  @classmethod
  def get_state_class(cls):
    return UserAttentionalBiasTestState

class UserAttentionalBiasTestState(UserTSRTestState):
  word_list1 = db.ListProperty(str)
  word_list2 = db.ListProperty(str)
  word_neutral_list = db.ListProperty(str)
  n_base_trials = db.IntegerProperty()

  def __init__(self, *args, **kwargs):
    super(UserAttentionalBiasTestState, self).__init__(*args, **kwargs)

  @classmethod
  def get(cls, user, variant):
    state = super(UserAttentionalBiasTestState, cls).get(user, variant)
    if not state:
      state = cls(user = user, variant_name = variant.name, variant_variant = variant.variant)
    state.n_base_trials = variant.n_base_trials
    return state

  def initialize(self):
    super(UserAttentionalBiasTestState, self).initialize()
    # populate word list
    variant = self.variant()
    word_list1 = models2.WordList.all().filter('list_title', variant.list1_name).get()
    word_list2 = models2.WordList.all().filter('list_title', variant.list2_name).get()
    word_list_neutral = models2.WordList.all().filter('list_title', variant.neutral_name).get()
    self.word_list1 = word_list1.choose_words(self.n_base_trials * 8)
    self.word_list2 = word_list2.choose_words(self.n_base_trials * 8)
    self.word_neutral_list = word_list_neutral.choose_words(self.n_base_trials * 16)
    self.put()

  def word_lists_as_javascript(self):
    result = ""
    for (listName, wordList) in [('1', self.word_list1),
                                 ('2', self.word_list2),
                                 ('Neutral', self.word_neutral_list)]:
      result += "window.wordList%s = [];\n"%listName
      result += "".join(["window.wordList%s[%d] = \"%s\";\n"%(listName, i, wordList[i]) for i in xrange(len(wordList))])
    return result

class AttentionalBiasTrialResult(TSRTrialResult):
  list_number = db.IntegerProperty()
  correct_response = db.BooleanProperty()
  bias_position = db.BooleanProperty()
  stimulus_position = db.BooleanProperty()
  user_response = db.BooleanProperty()
  bias_word = db.StringProperty()
  neutral_word = db.StringProperty()

  def full_results(self):
    res = super(AttentionalBiasTrialResult, self).full_results()
    # res.append(("word", "Word", "", self.get_color_name(self.word)))
    # res.append(("color", "Color", "", self.get_color_name(self.color)))
    # res.append(("task", "Task", "", "Word" if self.is_task_word else "Color"))
    # res.append(("response", "Response", "", self.get_color_name(self.user_response)))
    return res

  def to_tuple(self):
    return super(AttentionalBiasTrialResult, self).to_tuple() + (self.list_number, self.correct_response, self.stimulus_position, self.bias_position, self.user_response, self.bias_word, self.neutral_word)

  @classmethod
  def from_tuple(cls, tup):
    (list_number, correct_response, stimulus_position, bias_position, user_response, bias_word, neutral_word) = tup[-7:]
    result = super(AttentionalBiasTrialResult, cls).from_tuple(tup[:-7])
    (result.list_number, result.correct_response, result.stimulus_position, result.bias_position, result.user_response, result.bias_word, result.neutral_word) = (list_number, correct_response, stimulus_position, bias_position, user_response, bias_word, neutral_word)
    return result

  @classmethod
  def create(cls, trial, test_result):
    result = super(AttentionalBiasTrialResult, cls).create(trial, test_result)
    if trial.has_key('correctResponse'):
      result.correct_response = trial['correctResponse']
    if trial.has_key('stimulusPosition'):
      result.stimulus_position = trial['stimulusPosition']
    if trial.has_key('biasPosition'):
      result.bias_position = trial['biasPosition']
    if trial.has_key('userResponse'):
      result.user_response = trial['userResponse']
    if trial.has_key('listNumber'):
      result.list_number = trial['listNumber']
    if trial.has_key('biasWord'):
      result.bias_word = trial['biasWord']
    if trial.has_key('neutralWord'):
      result.neutral_word = trial['neutralWord']
    return result

  @classmethod
  def get_all_header_fields(cls):
    return super(AttentionalBiasTrialResult, cls).get_all_header_fields() + ["Bias type", "Bias word", "Neutral word", "Bias on right?", "Stimulus on right?", "Stimulus", "Response"]

  def get_all_fields(self):
    return super(AttentionalBiasTrialResult, self).get_all_fields() + [self.list_number, self.bias_word, self.neutral_word, self.bias_position, self.stimulus_position, self.correct_response, self.user_response]

  @classmethod
  def hide_fields(cls):
    return super(AttentionalBiasTrialResult, cls).hide_fields().union(set(['Ready?','Duration']))

class AttentionalBiasTestResult(TSRTestResult):
  list1_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='attentional_bias_list1_set')
  list2_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='attentional_bias_list2_set')
  neutral1_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='attentional_bias_neutral1_set')
  neutral2_ss = db.ReferenceProperty(models2.FloatSummaryStatistic, collection_name='attentional_bias_neutral2_set')

  @classmethod
  def test_name(cls):
    return "Attentional Bias"

  @classmethod
  def get_result_variant_name(cls):
    return "attentional_bias"

  def full_results(self):
    res = super(AttentionalBiasTestResult, self).full_results()
    variant = res.variant()
    res.append(("%s_reaction_times"%variant.list1_description, "%s reaction times"%variant.list1_description, "parent", self.list1_ss.full_results('reaction times', 'ms')))
    res.append(("%s_reaction_times"%variant.list2_description, "%s reaction times"%variant.list2_description, "parent", self.list2_ss.full_results('reaction times', 'ms')))
    res.append(("neutral1_reaction_times", "neutral vs %s reaction times"%variant.list1_description, "parent", self.neutral1_ss.full_results('reaction times', 'ms')))
    res.append(("neutral2_reaction_times", "neutral vs %s reaction times"%variant.list2_description, "parent", self.neutral2_ss.full_results('reaction times', 'ms')))
    return res

  @classmethod
  def cumulative_results(cls, results):
    res = super(AttentionalBiasTestResult, cls).cumulative_results(results)
    titles = super(AttentionalBiasTestResult, cls).cumulative_titles()
    res = [res[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    rts = []
    corrects = []
    for result in results:
      trials = result.get_trial_results()
      prv_task = None
      for trial in trials:
        if (prv_task is None) or (trial.is_task_word != prv_task):
          switch = True
          prv_task = trial.is_task_word
        else:
          switch = False
        concordant = trial.word == trial.color
        if trial.correct:
          rts.append([trial.is_task_word, switch, concordant, trial.reaction_time])
        corrects.append([trial.is_task_word, switch, concordant, trial.correct])
    preds = [[(lambda x: True, "Overall"),
              (lambda x: x[0], "Task: word"),
              (lambda x: not x[0], "Task: color")],
             [(lambda x: True, "Overall"),
              (lambda x: x[1], "Task switch"),
              (lambda x: not x[1], "Task nonswitch")],
             [(lambda x: True, "Overall"),
              (lambda x: x[2], "Concordant"),
              (lambda x: not x[2], "Discordant")]]
    for pred_set in preds:
      flsss = [models2.FloatSummaryStatistic.create([x[3] for x in rts if pred(x)]) for (pred,title) in pred_set]
      res.append(models2.CumulativeStatistic.from_float_sss(flsss, [x[1] for x in pred_set]))
      boolsss = [models2.BooleanSummaryStatistic.create([x[3] for x in corrects if pred(x)]) for (pred,title) in pred_set]
      cflsss = [models2.FloatSummaryStatistic.create([x[3] / bss.fraction_positive for x in rts if pred(x)]) for ((pred,title),bss) in zip(pred_set,boolsss)]
      res.append(models2.CumulativeStatistic.from_float_sss(cflsss, [x[1] for x in pred_set]))
      res.append(models2.CumulativeStatistic.from_boolean_sss(boolsss, [x[1] for x in pred_set]))
    return res

  @classmethod
  def cumulative_exclude(cls):
    return ["Overall reaction time", "Overall ready", "Overall correct"]

  @classmethod
  def cumulative_titles(cls):
    titles = super(AttentionalBiasTestResult, cls).cumulative_titles()
    titles = [titles[i] for i in xrange(len(titles)) if titles[i] not in cls.cumulative_exclude()]
    titles.extend(["%s by %s"%(measure, category) for category in ["task", "task switch", "concordant / discordant"] for measure in ["Reaction time", "Corrected reaction time", "Correct"]])
    return titles

  @classmethod
  def fields_to_graph(cls):
    return super(AttentionalBiasTestResult, cls).fields_to_graph() + [('switch_concordant_reaction_times','mean'),('nonswitch_concordant_reaction_times','mean'), ('switch_discordant_reaction_times','mean'),('nonswitch_discordant_reaction_times','mean')]

  def to_tuple(self):
    return super(AttentionalBiasTestResult, self).to_tuple() + (self.list1_ss.to_tuple(), self.list2_ss.to_tuple(), self.neutral1_ss.to_tuple(), self.neutral2_ss.to_tuple())

  @classmethod
  def from_tuple(cls, tup):
    result = super(AttentionalBiasTestResult, cls).from_tuple(tup[:-4])
    (list1_ss_tup, list2_ss_tup, neutral1_ss_tup, neutral2_ss_tup) = tup[-4:]
    list1 = models2.FloatSummaryStatistic.from_tuple(list1_ss_tup)
    list1.put()
    result.list1_ss = list1
    list2 = models2.FloatSummaryStatistic.from_tuple(list2_ss_tup)
    list2.put()
    result.list2_ss = list2
    neutral1 = models2.FloatSummaryStatistic.from_tuple(neutral1_ss_tup)
    neutral1.put()
    result.neutral1_ss = neutral1
    neutral2 = models2.FloatSummaryStatistic.from_tuple(neutral2_ss_tup)
    neutral2.put()
    result.neutral2_ss = neutral2
    return result

  @classmethod
  def make_result(cls, variant, result_data):
    result = super(AttentionalBiasTestResult, cls).make_result(variant, result_data)
    rts = [[] for i in range(4)]
    trials = result.get_trial_results()
    for trial in trials:
      list_number = trial.list_number
      if (list_number == 0) and (bias_position == stimulus_position):
        rtlist = rts[0]
      elif (list_number == 0) and (bias_position != stimulus_position):
        rtlist = rts[2]
      elif (list_number == 1) and (bias_position == stimulus_position):
        rtlist = rts[1]
      elif (list_number == 1) and (bias_position != stimulus_position):
        rtlist = rts[3]

  # correct_response = db.BooleanProperty()
  # bias_position = db.BooleanProperty()
  # stimulus_position = db.BooleanProperty()
  # user_response = db.BooleanProperty()
  # bias_word = db.StringProperty()
  # neutral_word = db.StringProperty()
  #     cur_task = trial.is_task_word
  #     is_switch = (prv_task is None) or (prv_task != cur_task)
  #     prv_task = cur_task
  #     is_concordant = trial.word == trial.color
  #     rt = trial.reaction_time
  #     rts[is_switch, is_concordant].append(rt)
  #   result.switch_concordant_ss = models2.FloatSummaryStatistic.create(rts[True, True])
  #   result.switch_discordant_ss = models2.FloatSummaryStatistic.create(rts[True, False])
  #   result.nonswitch_concordant_ss = models2.FloatSummaryStatistic.create(rts[False, True])
  #   result.nonswitch_discordant_ss = models2.FloatSummaryStatistic.create(rts[False, False])
    return result

  @classmethod
  def hide_fields(cls):
    return super(AttentionalBiasTestResult, cls).hide_fields().union(['Ready', 'Reaction time'])

  def get_all_statistics(self):
    variant = self.variant()
    return super(AttentionalBiasTestResult, self).get_all_statistics() + [("%s reaction time"%variant.list1_description, self.list1_ss.to_html(ms=True)),
                                                                          ("%s reaction time"%variant.list2_description, self.list2_ss.to_html(ms=True)),
                                                                          ("neutral vs %s reaction time"%variant.list1_description, self.neutral1_ss.to_html(ms=True)),
                                                                          ("neutral vs %s reaction time"%variant.list2_description, self.neutral2_ss.to_html(ms=True))]

  @classmethod
  def get_trial_class(cls):
    return AttentionalBiasTrialResult

explanations = {'reaction_time': 'average time to respond to the stimulus.',
                'congruent_reaction_time': 'average time in congruent trials to respond to the stimulus.',
                'incongruent_reaction_time': 'average time in incongruent trials to respond to the stimulus.',
                'omissions': 'trials not responding to a target.',
                'commissions': 'trials responding to a non-target.',
                'ready': 'trials in which the response was not too early.',
                'correct': 'trials in which the correct response was given.',
                'missed': 'trials in which the stimulus was not detected.',
                'congruent_missed': 'congruent trials in which the stimulus was not detected.',
                'incongruent_missed': 'incongruent trials in which the stimulus was not detected.',
                'level': 'an estimate for the subject\'s skill in this test.',
                'span': 'the estimated number of items for which the correct response rate is 50%.',
                'pairs': 'the number of cue-associate pairs for which the correct response rate to all of them is 50%.',
                'visuospatial_pairs': 'the number of cue-associate pairs for which the correct response rate to all of them is 50%.',
                'digit_span': 'the estimated number of digits for which the correct response rate is 50%.',
                'complexity': 'the estimated number of white dots for which the rate of correct responses above chance is 50%.',
                'inspection_time': 'the estimated stimulus display time for which the correct response rate is 75%.',
                'fraction_recalled': 'the fraction of words recalled correctly out of all words displayed.',
                'trend': 'the correlation between test time and number of correct responses per second throughout the test. This is represents as a z-score: positive values correspond to a learning effect (getting faster at responding correctly during the rest), and negative values correspond to decreased performance as the test advances.',
                'match_slope': 'the average increase in corrected reaction time for each additional black cell, when the two designs match.',
                'match_intercept': 'the constant element in the corrected reaction times that does not depend on the number of black cells, when the two designs match.',
                'nonmatch_slope': 'the average increase in corrected reaction time for each additional black cell, when the two designs do not match.',
                'nonmatch_intercept': 'the constant element in the corrected reaction times that does not depend on the number of black cells, when the two designs do not match.',
                'mr2_match_slope': 'the average increase in corrected reaction time for each additional degree of rotation, when the two images are a rotation of each other.',
                'mr2_match_intercept': 'the constant element in the corrected reaction times that does not depend on the rotation, when the two images are a rotation of each other.',
                'mr2_nonmatch_slope': 'the average increase in corrected reaction time for each additional degree of rotation, when the two images are mirror images of a rotation of each other.',
                'mr2_nonmatch_intercept': 'the constant element in the corrected reaction times that does not depend on the rotation, when the two images are mirror images of a rotation of each other.',
                'corrected_base_reaction_time': 'the estimated reaction time to the easiest class of trials, with no additional costs. Costs are properties of trials which make it more difficult to respond.',
                'partial_discordance_cost': 'the average increase in corrected reaction times when the correct answer differs from the answers corresponding to one of the two other rules, but not the other.',
                'full_discordance_cost': 'the average increase in corrected reaction times when the correct answer differs from the answers corresponding to both of the two other rules.',
                'context_switch_cost': 'the average increase in corrected reaction times when the rule used in this trial is different from the rule used in the previous trial.',
                'color_rule_cost': 'the average increase in corrected reaction times when the rule used is <strong>color</strong>.',
                'species_rule_cost': 'the average increase in corrected reaction times when the rule used is <strong>species</strong>.',
                'color_naming_cost': 'the average increase in corrected reaction times when asked to respond to the color rather than the word.',
                'interference_cost': 'the average increase in corrected reaction times when the color the word is written in is different from the word itself. This is also known as the <strong>Stroop effect</strong>.',
                'context_switch_cost': 'the average increase in corrected reaction times immediately after the rule changes between naming the word and naming the color.',
                'left_tps': 'average number of taps per second, using the left hand.',
                'right_tps': 'average number of taps per second, using the right hand.',
                'speed': 'correct results per time unit. This goes up as reaction times go down, but also as accuracy goes up.',
                'rush': 'a measure of the speed increase that comes at the expense of accuracy.',
                'gonogo_default': 'a single score that combines reaction times, commissions (responding to a non-target) and omissions (not responding to a target)',
                'gonogo_Inhibition': 'a single score that combines reaction times, commissions (responding to a non-target) and omissions (not responding to a target)',
                'gonogo_Attention': 'a single score that combines reaction times, commissions (responding to a non-target) and omissions (not responding to a target)',
                'choice_reaction_time_default': 'a single score that combines reaction times, too early responses and correct responses',
                'simple_reaction_time_default': 'a single score that combines reaction times and too early responses',
                'mental_rotation_default': 'mental rotation score',
                'design_copy_default': 'design copy score',
                'design_recognition_default': 'design recognition score',
                'attention_default': 'a single score that combines reaction times, missed stimuli, and degree of accuracy',
                'coding_default': 'a score corresponding to the number of correct responses per unit time',
                'simple_n_back_1-back': 'a single score that combines reaction times and accuracy',
                'simple_n_back_2-back': 'a single score that combines reaction times and accuracy',
                'simple_n_back_3-back': 'a single score that combines reaction times and accuracy',
                'feature_match_default': 'a single score that combines reaction times and accuracy for both matching and non-matching trials',
                'sorting_default': 'a single score that combines processing speed, accuracy, context switching and inhibition skills',
                'color_word_default': 'a single score that combines processing speed, accuracy, context switching and inhibition skills',
                'finger_tapping_default': 'a combined score for finger tapping speed from both hands',
                'design_recognition_default': 'design recognition score',
                'digit_span_auditory_backward': 'auditory backward digit span score',
                'digit_span_visual_backward': 'visual backward digit span score',
                'digit_span_audiovisual_backward': 'audiovisual backward digit span score',
                'digit_span_auditory_forward': 'auditory forward digit span score',
                'digit_span_visual_forward': 'visual forward digit span score',
                'digit_span_audiovisual_forward': 'audiovisual forward digit span score',
                'digit_span_auditory_backward': 'auditory backward digit span score',
                'spatial_span_backward': 'backward spatial span score',
                'spatial_span_forward': 'forward spatial span score',
                'visuospatial_paired_associates_default': 'visuospatial paired associates score',
                'Score': 'A single score that summarizes performance across all aspects of the test. Higher is better!',
                'Oops!': "No description",
                'Oops2': "Oops! Looks like Yoni forgot to type in the explanation for this one. Why don't you <a href=\"/contact\">let us know</a>?"}

results_scores = set(['gonogo_default',
                      'gonogo_Inhibition',
                      'gonogo_Attention',
                      'choice_reaction_time_default',
                      'simple_reaction_time_default',
                      'mental_rotation_default',
                      'design_copy_default',
                      'design_recognition_default',
                      'attention_default',
                      'coding_default',
                      'simple_n_back_1-back',
                      'simple_n_back_2-back',
                      'simple_n_back_3-back',
                      'feature_match_default',
                      'sorting_default',
                      'color_word_default',
                      'finger_tapping_default',
                      'design_recognition_default',
                      'digit_span_auditory_backward',
                      'digit_span_visual_backward',
                      'digit_span_audiovisual_backward',
                      'digit_span_auditory_forward',
                      'digit_span_visual_forward',
                      'digit_span_audiovisual_forward',
                      'digit_span_auditory_backward',
                      'spatial_span_backward',
                      'spatial_span_forward',
                      'visuospatial_paired_associates_default'])

def get_result_explanation(result_name):
  if not explanations.has_key(result_name):
    return explanations["Oops2"]
  expl = explanations[result_name]
  if result_name in results_scores:
    expl = expl + ". Scores are \"Personal IQ\": 100 is your own average performance. See the <a href=\"/faq\" target=\"_blank\">FAQ</a> for more details."
  return expl

full_W = {((u'simple_n_back', u'3-back'), 'correct', 0, None): (list([ 0.99789646,  1.14570965]), None), ((u'simple_reaction_time', u'default'), 'reaction time', 1, None): (list([ -42.0814094 ,  304.80997363]), 3285.7714750030068), ((u'simple_n_back', u'3-back'), 'reaction time', 1, None): (list([ -608.68247136,  1195.84294007]), 363767.91467571293), ((u'simple_n_back', u'1-back'), 'reaction time', 1, None): (list([-224.79185763,  560.72389104]), 66324.11234365686), ((u'simple_n_back', u'2-back'), 'reaction time', 1, None): (list([ -477.94358837,  1171.52354058]), 321626.7401201168), ((u'choice_reaction_time', u'default'), 'ready', 0, None): (list([ 1.19242773,  5.08535108]), None), ((u'simple_n_back', u'2-back'), 'correct', 0, None): (list([ 1.12213863,  2.1800847 ]), None), ((u'simple_n_back', u'1-back'), 'correct', 0, None): (list([ 0.64192425,  3.01247926]), None), ((u'simple_reaction_time', u'default'), 'ready', 0, None): (list([ 0.761165  ,  4.02911051]), None), ((u'choice_reaction_time', u'default'), 'reaction time', 1, None): (list([ -77.74322716,  418.29683584]), 6759.9339638865567), ((u'choice_reaction_time', u'default'), 'correct', 0, None): (list([ 0.88367418,  3.87683994]), None)}
