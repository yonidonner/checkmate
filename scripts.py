import sys

sys.path.append('.')

from google.appengine.ext import db

import main
import entities.experiment
from handlers import aggregates
import models2
import pickle
import math
import time
import datetime
import csv

def reset_experiments_variants_and_timestamps(username = None):
    query = entities.experiment.Experiment.all()
    if username is None:
        query = query.filter('username !=', None)
    else:
        if username[0] == '>':
            query = query.filter('username >=',username[1:])
        else:
            query = query.filter('username', username)
    experiments = query.run(batch_size = 100)
    for experiment in experiments:
        print "Resetting for experiment %s for user %s..."%(experiment.name, experiment.username)
        experiment.reset_variants_done()

def reset_test_session_aggregates(username = None):
    if username is None:
        query = models2.User.all().order('username')
    else:
        if username[0] == '>':
            query = models2.User.all().filter('username >=', username[1:])
        else:
            print "Running for user %s"%username
            aggregates.AggregateTestResults.build_from_scratch_for_user(username)
            return
    users = query.run(batch_size = 1000)
    for user in users:
        for i in xrange(5):
            try:
                reset_test_session_aggregates(user.username)
                break
            except:
                raise
                time.sleep(60)
    return

def get_user_data(username):
    user = models2.User.all().filter('username', username).get()
    if not user:
        return
    print "On mental variables"
    query = entities.mental_variables.MentalVariable.all().filter('creator_username', username)
    mvs = []
    for mv in query.run(batch_size = 1000):
        mvs.append(mv.to_tuple())
    query = entities.experiment.Experiment.all().filter('username', username)
    print "On experiments"
    experiments = []
    for experiment in query.run(batch_size = 1000):
        experiments.append(experiment.to_tuple())
    sessq = models2.Session2.all().filter('active', False).filter('user', user)
    sesss = []
    i = 0
    for sess in sessq:
        print "\rOn session %d"%i,
        sys.stdout.flush()
        sesss.append(sess.to_tuple())
        i += 1
    user_tup = user.to_tuple()
    pcl = pickle.dumps((user_tup, mvs, experiments, sesss))
    return pcl

def compute_reliabilities1(variant_name, variant_group, usernames = None):
    if usernames is None:
        print "Reading usernames..."
        usernames = []
        for user in models2.User.all().run(batch_size = 1000):
            print user.username
            usernames.append(user.username)
    variant_info = (variant_name, variant_group)
    variant = models2.TestVariant.all().filter('name', variant_info[0]).filter('group', variant_info[1]).get()
    if not variant:
        return None
    result_class = variant.get_result_class()
    results = {}
    for username in usernames:
        sss = aggregates.AggregateTestResults.collect_for_group(username, variant_name, variant_group)
        print sss
        print "Computing results for user %s..."%username
        res1 = [result_class.summary_metrics([ss], username, variant_info) for ss in sss]
        for res in res1:
            for (result_name, result_description, unit, value, stderr) in res:
                if not results.has_key(result_name):
                    results[result_name] = {}
                if value is not None:
                    if not results[result_name].has_key(username):
                        results[result_name][username] = []
                    results[result_name][username].append(value)
    return results

def compute_reliabilities2(results):
    my_res = {}
    for result_name in results.keys():
        n = 0
        all_vals = []
        sumx2 = 0.0
        for username in results[result_name].keys():
            vals = results[result_name][username]
            if len(vals) > 4:
                vals = vals[3:]
                n += (len(vals) - 1)
                mn = sum(vals) * 1.0 / len(vals)
                sumx2 += sum([(x-mn)**2 for x in vals])
                all_vals.extend(vals)
        if n > 0:
            v = sumx2 / n
            mn = sum(all_vals) * 1.0 / len(all_vals)
            v2 = sum([(x-mn)**2 for x in all_vals]) * 1.0 / (len(all_vals) - 1.0)
            if v2 > 0.0:
                my_res[result_name] = math.sqrt(max(0,1.0 - v * 1.0 / v2))
    return my_res

def download_all_observations():
    asrs = aggregates.AggregateSessionResults.all().run(batch_size = 10000)
    obs = {}
    errors = []
    for asr in asrs:
        username = asr.username
        print "Working on %s..."%(username)
        try:
            obs[username] = asr.get_aggregator().combine()
        except:
            print "Failed retrieving %s!"%username
            errors.append(username)
    return obs, errors

def get_unpacked_summary_statistics(variant_name, variant_group, usernames = None):
    if usernames is None:
        print "Reading usernames..."
        usernames = []
        for user in models2.User.all().run(batch_size = 1000):
            print user.username
            usernames.append(user.username)
    variant_info = (variant_name, variant_group)
    variant = models2.TestVariant.all().filter('name', variant_info[0]).filter('group', variant_info[1]).get()
    if not variant:
        return None
    result_class = variant.get_result_class()
    results = {}
    for username in usernames:
        print username
        if not results.has_key(username):
            results[username] = []
        sss = aggregates.AggregateTestResults.collect_for_group(username, variant_name, variant_group)
        for ss in sss:
            curres = result_class.unpack_summary_statistics([ss])
            results[username].append(curres)
    return results

def get_unpacked_summary_statistics2(variant_name, variant_group, usernames = None):
    if usernames is None:
        print "Reading usernames..."
        usernames = []
        for user in models2.User.all().run(batch_size = 1000):
            print user.username
            usernames.append(user.username)
        usernames.sort()
    variant_info = (variant_name, variant_group)
    variant = models2.TestVariant.all().filter('name', variant_info[0]).filter('group', variant_info[1]).get()
    if not variant:
        return None
    result_class = variant.get_result_class()
    sss = aggregates.AggregateTestResults.collect_for_group_multiple_users(usernames, variant_info[0], variant_info[1], flt = None)
    print "Updating prior..."
    result_class.update_prior(sss, None, variant_info)
    print "Unpacking results..."
    unpacked = [result_class.unpack_summary_statistics([ss], None, variant_info) for ss in sss]
    return unpacked

def assign_orphaned_sessions_to_experiments(username='nick'):
    """Any session for nick with experiment None and battery full25
    should go into My First Experiment."""
    mfe = (entities.experiment.Experiment.all()
           .filter('username', username)
           .filter('name', 'My First Experiment')).get()
    user = models2.User.all().filter('username', username).get()
    sessq = models2.Session2.all().filter('user', user)
    sesss = []
    for i, sess in enumerate(sessq):
        print "\rOn session %d"%i,
        sys.stdout.flush()
        if sess.battery_name == 'full25' and sess.experiment is None:
            sess.experiment = mfe
        if sess.battery_name == 'full25':
            sess.is_complete = True
        sesss.append(sess)
    print "Got %d sessions"%len(sesss)
    db.put(sesss)

def get_all_session_observations():
    asr = aggregates.AggregateSessionResults.all().filter('username', 'combined_all_users').get()
    agg = asr.get_aggregator()
    all_results = agg.combine()
    return all_results

def get_participant_codes():
    pcs = {}
    prv_user = None
    cursor = None
    query = entities.experiment.Experiment.all().order('username')
    while True:
        if cursor is not None:
            query.with_cursor(cursor)
        exps = query.fetch(50)
        cursor = query.cursor()
        if len(exps) == 0:
            break
        for experiment in exps:
            username = experiment.username
            if experiment.participant_code:
                pc = experiment.participant_code
                if not pcs.has_key(username):
                    pcs[username] = set()
                if not pc in pcs[username]:
                    print "Adding %s to %s"%(pc, username)
                    pcs[username].add(pc)
    return pcs

def update_all_variant_groups():
    query = models2.CompletedTest.all()
    cursor = None
    total_i = 0
    while True:
        if cursor is not None:
            query.with_cursor(cursor)
        cts = query.fetch(10)
        if len(cts) == 0:
            return
        for ct in cts:
            if ct.variant_group is None:
                ct.update_variant_group()
            total_i += 1
        cursor = query.cursor()
        print "%d test results updated."%total_i

def save_session_aggregates():
    agg = aggregates.AggregateSessionResults.all().filter('username', 'combined_all_users').get()
    data = agg.get_aggregator().combine()
    pickle.dump(data, open("../allres_%s.pcl"%(datetime.datetime.utcnow().strftime("%Y%m%d_%H%M")),"wb"))

def find_active_users(last_days = 30):
    min_date = datetime.datetime.utcnow() - datetime.timedelta(days = last_days)
    q = models2.Session2.all().filter('start_time >', min_date)
    ud = {}
    sess_i = 0
    for sess in q:
        last_date = sess.start_time
        username = sess.user.username
        ud[username] = ud.get(username, 0) + 1
        sess_i += 1
        if (sess_i % 100) == 0:
            print "%d sessions processed, last date: %s"%(sess_i, last_date)
    return ud

def get_user_email(username):
    return models2.User.all().filter('username', username).get().google_account.email()

def get_halo_summary():
    haloq = entities.experiment.Experiment.all().filter('name','Halo')
    header1 = ['username', 'email', 'join_date', 'total_sessions']
    header2 = ['username', 'session_date', 'last_5_canabalt', 'stimulation', 'comments']
    rows2 = []
    tests_dict = {}
    test_names = []
    with open('halo_users.csv','wb') as csvfile1:
        writer1 = csv.writer(csvfile1)
        writer1.writerow(header1)
        with open('halo_sessions.csv','wb') as csvfile2:
            writer2 = csv.writer(csvfile2)
            #writer2.writerow(header2)
            for exp in haloq:
                if exp.username is not None and exp.username not in ['yoni']:
                    print "User %s joined on %s"%(exp.username, exp.join_date)
                    sessq = models2.Session2.all().filter('experiment', exp)
                    sess = sessq.fetch(100)
                    email = models2.User.all().filter('username', exp.username).get().google_account.email()
                    row1 = [exp.username, email, exp.join_date.strftime("%Y%m%d_%H%M%S"), '%d'%len(sess)]
                    writer1.writerow(row1)
                    for onesess in sess:
                        finished_tests = models2.CompletedTest.all().filter('test_session', onesess).fetch(1000)
                        finished_tests.sort(key = lambda x: x.start_time)
                        session_results = {}
                        for i in xrange(len(finished_tests)):
                            variant_name = finished_tests[i].variant_name
                            if not tests_dict.has_key(variant_name):
                                test_names.append(variant_name)
                                tests_dict[variant_name] = len(tests_dict)
                            score = finished_tests[i].get_score()
                            session_results[variant_name] = score
                        mental_states = onesess.session_information.mental_states
                        if len(mental_states) < 3:
                            mental_states = ['','','']
                        row2 = [exp.username, onesess.start_time.strftime("%Y%m%d_%H%M%S"), mental_states[0], mental_states[1], mental_states[2], session_results]
                        rows2.append(row2)
            test_names.sort()
            header2b = header2 + test_names
            writer2.writerow(header2b)
            for row2 in rows2:
                res = [None] * len(test_names)
                for i in xrange(len(test_names)):
                    if row2[-1].has_key(test_names[i]):
                        res[i] = '%.2f'%(row2[-1][test_names[i]][0])
                    else:
                        res[i] = 'NA'
                row2b = row2[:-1] + res
                writer2.writerow(row2b)
    return

def save_cognition_in_motion_summary():
    save_experiment_summary('Cognition In Motion', 'cognition_in_motion', ['condition', 'comments'])

def save_experiment_summary(experiment_name, filename_prefix, mvs = []):
    haloq = entities.experiment.Experiment.all().filter('name',experiment_name)
    header1 = ['username', 'email', 'join_date', 'total_sessions']
    header2 = ['username', 'session_date'] + mvs
    rows2 = []
    tests_dict = {}
    test_names = []
    with open('%s_users.csv'%filename_prefix,'wb') as csvfile1:
        writer1 = csv.writer(csvfile1)
        writer1.writerow(header1)
        with open('%s_sessions.csv'%filename_prefix,'wb') as csvfile2:
            writer2 = csv.writer(csvfile2)
            #writer2.writerow(header2)
            for exp in haloq:
                if exp.username is not None and exp.username not in ['yoni']:
                    print "User %s joined on %s"%(exp.username, exp.join_date)
                    sessq = models2.Session2.all().filter('experiment', exp)
                    sess = sessq.fetch(100)
                    email = models2.User.all().filter('username', exp.username).get().google_account.email()
                    row1 = [exp.username, email, exp.join_date.strftime("%Y%m%d_%H%M%S"), '%d'%len(sess)]
                    writer1.writerow(row1)
                    for onesess in sess:
                        finished_tests = models2.CompletedTest.all().filter('test_session', onesess).fetch(1000)
                        finished_tests.sort(key = lambda x: x.start_time)
                        session_results = {}
                        for i in xrange(len(finished_tests)):
                            variant_name = finished_tests[i].variant_name + "_" + finished_tests[i].variant_variant
                            if not tests_dict.has_key(variant_name):
                                test_names.append(variant_name)
                                tests_dict[variant_name] = len(tests_dict)
                            score = finished_tests[i].get_score()
                            session_results[variant_name] = score
                        mental_states = onesess.session_information.mental_states
                        if len(mental_states) < 2:
                            mental_states = ['','']
                        row2 = [exp.username, onesess.start_time.strftime("%Y%m%d_%H%M%S")] + mental_states + [session_results]
                        rows2.append(row2)
            test_names.sort()
            header2b = header2 + test_names
            writer2.writerow(header2b)
            for row2 in rows2:
                res = [None] * len(test_names)
                for i in xrange(len(test_names)):
                    if row2[-1].has_key(test_names[i]):
                        res[i] = '%.2f'%(row2[-1][test_names[i]][0])
                    else:
                        res[i] = 'NA'
                row2b = row2[:-1] + res
                writer2.writerow(row2b)
    return

def dtd(y, m, d):
    return datetime.datetime.combine(
        datetime.date(y, m, d),
        datetime.time(0, 0))
