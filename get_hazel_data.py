# Call as, e.g.,:
#   python remote_wrap.py quantified-mind.appspot.com get_hazel_data.py /home/data/hazel.json
# to update /home/data/hazel.json . Old sessions are not re-retrieved.

import sys, errno, datetime, json, re, mechanize
import main
import models2
from google.appengine.ext.db import Key

json_file = sys.argv[1]

# ----------------------------------------------------------

max_sessions = 1000
max_tests = 20

def test_name(test):
     return '3back' if test.variant_group == '3-back' else test.variant_group

# ----------------------------------------------------------

print 'Getting deployment times'

br = mechanize.Browser()
r = br.open("https://appengine.google.com/adminlogs?app_id=s%7Equantified-mind&type=version_updated&limit=50")

br.select_form(nr = 0)
br['Email'] = gae_username
br['Passwd'] = gae_password
r = br.submit()

deployment_times = sorted(
    [t + ' ' + d for t, d in set(re.findall(
        r'<td>Completed update of a new default version</td>\s*'
            r'<td>version=[^<]+\.(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d:\d\d)Z</td>',
        r.read()))],
    reverse = True)

# ----------------------------------------------------------

try:
    with open(json_file) as f:
        data = json.load(f)
except IOError as e:
    if e.errno != errno.ENOENT: raise
    data = dict(subjects = {})

q = models2.Session2.all()
start_times = [
    sess['start_t']
    for subj in data['subjects'].values()
    for sess in subj['sessions']]
if start_times:
    q = q.filter('start_time >', datetime.datetime.strptime(
        max(start_times),
        '%Y-%m-%d %H:%M:%S.%f'))
sessions = sorted(
    q
        .filter('battery_name', 'lunch_decision_making')
        .filter('is_complete', True)
        .fetch(max_sessions),
    key = lambda x: x.start_time)
if len(sessions) == max_sessions:
    raise Exception('Too many sessions')

for sess in sessions:
    print 'Saving session:', str(sess.start_time)
    user = sess.user
    if user.username not in data['subjects']:
        data['subjects'][user.username] = dict(
            sessions = [],
            profile = dict(
                key = str(user.key()),
                subject_id = sess.experiment.subject_id,
                join_t = str(user.create_date),
                age = user.age,
                education = user.education,
                gender = user.gender,
                google_account = user.google_account.email(),
                timezone = user.timezone))
    si = sess.session_information
    tests = sorted(
        models2.CompletedTest.all()
            .filter('test_session', sess)
            .fetch(max_tests),
        key = lambda x: x.start_time)
    d = dict(
        key = str(sess.key()),
        start_t = str(sess.start_time),
        end_t = str(sess.end_time),
        deployed_t = next(v for v in deployment_times if v < str(sess.start_time)),
        user_agent = sess.user_agent,
        since_ate = si.mental_states[0] if si.mental_states else None,
        hungry = si.mental_states[1] if si.mental_states else None,
        comments = si.mental_states[2] if si.mental_states else None,
        test_order = [test_name(x) for x in tests],
        tests = {})
    for test in tests:
        if test.variant_name == 'decision_making':
            rs = test.trial_results
            g = lambda k: [dict(r[-1])[k] for r in rs]
            d['tests'][test_name(test)] = dict(
                duration = test.duration,
                small = g('smallAmount'),
                big = g('bigAmount'),
                catchtype = g('catchType'),
                big_on_left = g('bigOnLeft'),
                took_big = g('tookBig'),
                rt = [r[1] for r in rs])
        elif test.variant_group == '3-back':
            rs = test.trial_results
            d['tests'][test_name(test)] = dict(
                duration = test.duration,
                stimulus = rs[0][-4] + [r[-3] for r in rs],
                correct_answer = 3 * [None] + [r[-2] for r in rs],
                response = 3 * [None] + [r[-1] for r in rs],
                rt = 3 * [None] + [r[1] for r in rs])
        else:
            raise ValueError("Don't know how to handle variant_name = " + test.variant_name)
    data['subjects'][user.username]['sessions'].append(d)

with open(json_file, 'w') as f:
    json.dump(data, f, sort_keys = True, indent = 2, separators = (',', ': '))
