import math
import time

GUESSING_AB = [ -1.38135096, -13.81350956]

def parse_theta(theta):
    w_O, outlier_params, a, b, c, sigmasqr_S, sigmasqr_T = theta
    res = {'w_O': w_O, 'a': a, 'b': b, 'c': c, 'sigmasqr_S': sigmasqr_S, 'sigmasqr_T': sigmasqr_T}
    if len(outlier_params) == 1:
        res['outlier_type'] = 0
        (res['lamb'],) = outlier_params
    else:
        res['outlier_type'] = 1
        (res['mu_outlier'], res['sigmasqr_outlier']) = outlier_params
    return res

def parse_boolean_theta(theta):
    (a,b,c,P_g) = theta
    res = {'a': a, 'b': b, 'c': c, 'P_g': P_g}
    return res

def QMOM2_normal_fgh(flatss, z, a, b, sigmasqr_T, lamb, G):
    # f = log p(x | z, G=0) = -0.5*(n*log(2*pi*sigma_T^2) + (S-2*a*T*z+n*a^2*z^2)/sigma_T^2)
    # f' = -(n*a^2*z-a*T)/sigma_T^2
    # f'' = -n*a^2/sigma_T^2
    n, T, S, T0 = flatss
    if (G==0) or (T0 > n * b):
        f = -0.5*(n*math.log(2*math.pi*sigmasqr_T) + (S - 2.0*T*a*z + n*a**2*z**2)/sigmasqr_T)
        ftag = -(n*a**2*z-a*T)/sigmasqr_T
        ftagtag = -n*a**2/sigmasqr_T
    else:
        trunc = 1.0 - math.exp(-lamb * b)
        f = n * (math.log(lamb) - math.log(trunc)) - lamb * T0
        ftag = 0.0
        ftagtag = 0.0
    return (f, ftag, ftagtag)

def QMOM2_boolean_fgh(ss, z, a, b, P_g = 0.0):
    # q = exp(a*z+b)/(1+exp(a*z+b))
    # q' = a*q*(1-q)
    # f1 = p(x_ij = 1 | z) = P_g + (1-P_g)*q
    # f0 = (1-P_g)*(1-q)
    # f = log p(x | z, G=0) = k log f1 + (n - k) log f0
    # (log f1)' = (1-P_g)*a*q*(1-q)/f1
    # (log f0)' = -(1-P_g)*a*q*(1-q)/f0
    # f' = (1-P_g)*a*q*(1-q)*(k/f1 - (n-k)/f0) =
    # = (1-P_g)*a*q*(1-q)*(k*f0-(n-k)*f1)/(f0*f1) =
    # = a*q*(k-n*f1)/f1 = a*q*k/f1 - a*q*n = a*q/f1*(k-f1*n)
    # f'' = a*k*q'/f1 - a*q*k*f1'/f1^2 - a*q'*n =
    # = (a*q')*(k/f1-n) - a*q*k*(1-P_g)*q'/f1^2 =
    # = (a^2*q*(1-q))*(k/f1-n-q*k*(1-P_g)/f1^2) =
    # = (a^2*q*(1-q))*(k*(1/f1-(f1-P_g)/f1^2)-n) =
    # = (a^2*q*(1-q))*(k*(1/f1-1/f1+P_g/f1^2)-n) =
    # = (a^2*q*(1-q))*(k*P_g/f1^2-n)
    q0 = a*z+b
    if (q0 < 20):
        q1 = math.exp(q0)
        q2 = q1 / (1.0 + q1)
    else:
        q2 = 1.0
    f1 = max(1e-20,P_g + (1.0 - P_g) * q2)
    f0 = max(1-f1,1e-20)
    f = ss[1] * math.log(f1) + (ss[0]-ss[1]) * math.log(f0)
    ftag = a*q2/f1*(ss[1]-f1*ss[0])
    ftagtag = a**2*q2*(1.0-q2)*(ss[1]*P_g/f1**2-ss[0])
    return (f, ftag, ftagtag)

def QMOM2_data_ll_fgh(parsed_theta, flat_ss, boolean_ss, z, G):
    total_f = 0.0
    total_ftag = 0.0
    total_ftagtag = 0.0
    for k in flat_ss.keys():
        if parsed_theta.has_key(k):
            pk = parsed_theta[k]
            a,b,sigmasqr_T,lamb = (pk['a'], pk['b'], pk['sigmasqr_T'], pk['lamb'])
            f, ftag, ftagtag = QMOM2_normal_fgh(flat_ss[k], z, a, b, sigmasqr_T, lamb, G)
            total_f += f
            total_ftag += ftag
            total_ftagtag += ftagtag
    for k in boolean_ss.keys():
        if parsed_theta.has_key(k):
            pk = parsed_theta[k]
            a,b,c,P_g = (pk['a'], pk['b'], pk['c'], pk['P_g'])
            for (covariates, ss) in boolean_ss[k]:
                if c is not None:
                    btag = b + sum([c[i] * covariates[i] for i in xrange(len(c))])
                else:
                    btag = b
                f, ftag, ftagtag = QMOM2_boolean_fgh(ss, z, a, btag, P_g)
                total_f += f
                total_ftag += ftag
                total_ftagtag += ftagtag
    return (total_f, total_ftag, total_ftagtag)

def QMOM2_ll_fgh(parsed_theta, flat_ss, boolean_ss, z, G):
    f_prior = -0.5*(math.log(2*math.pi)+z**2)
    ftag_prior = -z
    ftagtag_prior = -1
    a_G, b_G = GUESSING_AB
    q0_G = a_G * z + b_G
    if (q0_G < 20):
        q1_G = math.exp(a_G * z + b_G)
        q2_G = q1_G / (1.0 + q1_G)
    else:
        q2_G = 1.0
    p2_G = 1.0 - q2_G
    if G == 1:
        f_G = math.log(max(1e-20, q2_G))
        ftag_G = a_G * p2_G
        ftagtag_G = -a_G**2*q2_G*p2_G
    else:
        f_G = math.log(max(1e-20, p2_G))
        ftag_G = -a_G * q2_G
        ftagtag_G = -a_G**2*q2_G*p2_G
    f_ll, ftag_ll, ftagtag_ll = QMOM2_data_ll_fgh(parsed_theta, flat_ss, boolean_ss, z, G)
    f = f_prior + f_G + f_ll
    ftag = ftag_prior + ftag_G + ftag_ll
    ftagtag = ftagtag_prior + ftagtag_G + ftagtag_ll
    #f = f_prior + f_ll
    #ftag = ftag_prior + ftag_ll
    #ftagtag = ftagtag_prior + ftagtag_ll
    return (f, ftag, ftagtag)

def QMOM2_argmax_score(theta, ss, newton_params = {}):
    # Solve for 2nd-order expansion of:
    # log p(z, G=0 | x) = log p(x | z, G=0) + log p(z) + log p(G=0 | z) - log p(x)
    # log p(z, G=1 | x) = log p(x | z, G=1) + log p(z) + log p(G=1 | z) - log p(x)
    # If we have log p = c_0 + 0.5 * c_2 * (x - x_0)^2 then the integral is (assume c_2 < 0 and let
    # sigma^2 = -1/c_2):
    # int_z p = (exp(c_0)*sqrt(2*pi*sigma^2)) * int_x (1/sqrt(2*pi*sigma^2) * exp(-0.5 * (x - x_0)^2/sigma^2) = exp(c_0)*sqrt(2*pi*sigma^2)
    # int_z p1 / (int_z p1 + int_z p2) = exp(c_1)*sigma_1/((exp(c_1)*sigma_1)+exp(c_2)*sigma_2)
    # Then p(G=1 | x) = exp(c_1)*sigma_1/(exp(c_1)*sigma_1+exp(c_2)*sigma_2)
    # p(z | x) = p(G=0 | x) p(z | x, G=0) + p(G=1 | x) p(z | x, G=1)
    # This is a mixture of normalized Gaussians so the marginal Gaussian approximation can be estimated
    # by moment matching. The mean is:
    # E[z | x] = p(G=0 | x) E[z | x, G=0] + p(G=0 | x) E[z | x, G=0]
    # V[z | x] = p(G=0 | x) (E[(z - mu_0)^2 | x] + (mu_0 - E[Z | x])^2) + P(G=1 | x) (E[(z - mu_1)^2 | x] + (mu_1 - E[Z | x])^2) =
    # = p(G=0 | x) (V[z | x, G=0] + (mu_0 - E[Z | x])^2) + P(G=1 | x) (V[z | x, G=1] + (mu_1 - E[Z | x])^2)
    # These terms are individually easy to compute and take first and second derivatives.
    parsed_theta = {}
    for k in theta.keys():
        if k[2] == 1:
            parsed_theta[k] = parse_theta(theta[k])
        else:
            parsed_theta[k] = parse_boolean_theta(theta[k])
    flat_ss = {}
    boolean_ss = {}
    for k in ss.keys():
        k2 = k[:-1]
        if not parsed_theta.has_key(k2):
            continue
        covariates = k[-1]
        if k[2] == 1:
            b = parsed_theta[k2]['b']
            c = parsed_theta[k2]['c']
            prd = b
            if c is not None:
                prd += sum([covariates[i] * c[i] for i in xrange(len(c))])
            if not flat_ss.has_key(k2):
                flat_ss[k2] = [0, 0.0, 0.0, 0.0]
            flat_ss[k2][0] += ss[k][0]
            flat_ss[k2][1] += ss[k][1] - prd * ss[k][0]
            flat_ss[k2][2] += ss[k][2] + ss[k][0]*prd**2 - 2*prd*ss[k][1]
            flat_ss[k2][3] += ss[k][1]
        else:
            if not boolean_ss.has_key(k2):
                boolean_ss[k2] = []
            boolean_ss[k2].append((covariates, ss[k]))
    G0_x, G0_c, G0_h = newton_solve(lambda z: QMOM2_ll_fgh(parsed_theta, flat_ss, boolean_ss, z, 0), 3.0, **newton_params)
    G1_x, G1_c, G1_h = newton_solve(lambda z: QMOM2_ll_fgh(parsed_theta, flat_ss, boolean_ss, z, 1), 0.0, **newton_params)
    #print "0: ",G0_x, G0_c, G0_h
    #print "1: ",G1_x, G1_c, G1_h
    mx_c = max(G0_c, G1_c)
    sigmasqr0 = -1.0 / G0_h
    sigmasqr1 = -1.0 / G1_h
    PG0 = math.exp(G0_c - mx_c) * math.sqrt(sigmasqr0)
    PG1 = math.exp(G1_c - mx_c) * math.sqrt(sigmasqr1)
    posterior_G = PG1 / (PG1 + PG0)
    Z_mu = (1.0 - posterior_G) * G0_x + posterior_G * G1_x
    Z_var = (1.0 - posterior_G) * (sigmasqr0 + (G0_x - Z_mu)**2) + posterior_G * (sigmasqr1 + (G1_x - Z_mu)**2)
    Z_stderr = math.sqrt(Z_var)
    # Then p(G=1 | x) = exp(c_1)*sigma_1/(exp(c_1)*sigma_1+exp(c_2)*sigma_2)
    return (Z_mu, Z_stderr)

def newton_iteration(fgh, x, lmb2_threshold = 1e-8, t_threshold = 1e-6, f_threshold = 1e-8, backtracking_alpha = 0.5):
    f, ftag, ftagtag = fgh(x)
    d = -ftag/ftagtag
    lmb2 = d*ftag
    #print "lmb2:",lmb2
    if lmb2 < 0:
        d = -d
        lmb2 = -lmb2
    if lmb2 < lmb2_threshold:
        return (x, None, f, ftagtag)
    t = 1.0
    while True:
        x_new = x + t * d
        f_new = fgh(x_new)
        if (f_new[0] - f > f_threshold):
            return (x_new, t, f_new[0], f_new[2])
        else:
            t *= backtracking_alpha
            if (t < t_threshold):
                return (x, None, f, ftagtag)

def robust_newton_solve(fgh, x0s, lmb2_threshold = 1e-8, t_threshold = 1e-6, f_threshold = 1e-8, backtracking_alpha = 0.5):
    best_f = None
    for x0 in x0s:
        (f, ftag, ftagtag) = newton_solve(fgh, x0, lmb2_threshold, t_threshold, f_threshold, backtracking_alpha)
        if (best_f is None) or (f > best_f):
            best_f = f
            best_ftag = ftag
            best_ftagtag = ftagtag
    return best_f, best_ftag, best_ftagtag

def newton_solve(fgh, x0, lmb2_threshold = 1e-8, t_threshold = 1e-6, f_threshold = 1e-8, backtracking_alpha = 0.5, timeout = None):
    x = x0
    t0 = time.time()
    while True:
        x_new, t, f, ftagtag = newton_iteration(fgh, x, lmb2_threshold, t_threshold, f_threshold, backtracking_alpha)
        if (t is None) or ((timeout is not None) and ((time.time() - t0) > timeout)):
            return (x_new, f, ftagtag)
        x = x_new

def QMOM2_score(theta, ss, Z_resolution = 200, window = None):
    parsed_theta = {}
    for k in theta.keys():
        if k[2] == 1:
            parsed_theta[k] = parse_theta(theta[k])
        else:
            parsed_theta[k] = parse_boolean_theta(theta[k])
    if window is None:
        window = [-10.0,10.0]
    Z_vals = [None] * Z_resolution
    for Z_i in xrange(Z_resolution):
        Z_vals[Z_i] = window[0] + (window[1]-window[0])*(0.5+Z_i)/Z_resolution
    guessing_q0 = [Z_vals[Z_i] * GUESSING_AB[0] + GUESSING_AB[1] for Z_i in xrange(Z_resolution)]
    guessing_q1 = [math.exp(x) for x in guessing_q0]
    guessing_p = [x / (1.0 + x) for x in guessing_q1]
    guessing_p = [min(max(x,0.0000001),0.9999999) for x in guessing_p]
    flat_ss = {}
    boolean_ss = {}
    for k in ss.keys():
        k2 = k[:-1]
        if not parsed_theta.has_key(k2):
            continue
        covariates = k[-1]
        if k[2] == 1:
            b = parsed_theta[k2]['b']
            c = parsed_theta[k2]['c']
            prd = b
            if c is not None:
                prd += sum([covariates[i] * c[i] for i in xrange(len(c))])
            if not flat_ss.has_key(k2):
                flat_ss[k2] = [0, 0.0, 0.0, 0.0]
            flat_ss[k2][0] += ss[k][0]
            flat_ss[k2][1] += ss[k][1] - prd * ss[k][0]
            flat_ss[k2][2] += ss[k][2] + ss[k][0]*prd**2 - 2*prd*ss[k][1]
            flat_ss[k2][3] += ss[k][1]
        else:
            if not boolean_ss.has_key(k2):
                boolean_ss[k2] = []
            boolean_ss[k2].append((covariates, ss[k]))
    lP_X_Z_sum = [-0.5*(math.log(2*math.pi)+Z_vals[Z_i]**2) for Z_i in xrange(Z_resolution)]
    for k in flat_ss.keys():
        n, T, S, T0 = flat_ss[k]
        outlier_type = parsed_theta[k]['outlier_type']
        if outlier_type == 0:
            lamb = parsed_theta[k]['lamb']
            if T0 > n * parsed_theta[k]['b']:
                lP_X_O1 = None
            else:
                trunc = 1.0 - math.exp(-lamb * parsed_theta[k]['b'])
                lP_X_O1 = n * (math.log(lamb) - math.log(trunc)) - lamb * T0
        elif outlier_type == 1:
            mu_outlier = parsed_theta[k]['mu_outlier']
            sigmasqr_outlier = parsed_theta[k]['sigmasqr_outlier']
            lP_X_O1 = -0.5 * (n * math.log(2 * math.pi * sigmasqr_outlier) + (S - 2.0 * T * mu_outlier + n * mu_outlier**2)/sigmasqr_outlier)
        a = parsed_theta[k]['a']
        sigmasqr_S = parsed_theta[k]['sigmasqr_S']
        sigmasqr_T = parsed_theta[k]['sigmasqr_T']
        T_hat = [n * a * Z_vals[Z_i] for Z_i in xrange(Z_resolution)]
        S_hat = [n * a**2 * Z_vals[Z_i]**2 for Z_i in xrange(Z_resolution)]
        lP_X_O0 = [-0.5 * (n * math.log(2 * math.pi * sigmasqr_T) + (S - 2.0 * T * T_hat[Z_i] / n + S_hat[Z_i]) / sigmasqr_T) for Z_i in xrange(Z_resolution)]
        w_O = parsed_theta[k]['w_O']
        if lP_X_O1 is not None:
            lP_X_O1p = [lP_X_O1 + math.log(guessing_p[Z_i]) for Z_i in xrange(Z_resolution)]
        lP_X_O0p = [lP_X_O0[Z_i] + math.log(1.0 - guessing_p[Z_i]) for Z_i in xrange(Z_resolution)]
        if lP_X_O1 is None:
            lP_X_Z = lP_X_O0p
        else:
            lP_X_O_mx = [max(lP_X_O0p[Z_i], lP_X_O1p[Z_i]) for Z_i in xrange(Z_resolution)]
            lP_X_Z = [math.log(math.exp(lP_X_O0p[Z_i] - lP_X_O_mx[Z_i]) + math.exp(lP_X_O1p[Z_i] - lP_X_O_mx[Z_i])) + lP_X_O_mx[Z_i] for Z_i in xrange(Z_resolution)]
        for Z_i in xrange(Z_resolution):
            lP_X_Z_sum[Z_i] += lP_X_Z[Z_i]
    for k in boolean_ss.keys():
        a = parsed_theta[k]['a']
        b = parsed_theta[k]['b']
        c = parsed_theta[k]['c']
        P_g = parsed_theta[k]['P_g']
        for (covariates, (n,correct)) in boolean_ss[k]:
            Q0 = [a * Z_vals[Z_i] + b for Z_i in xrange(Z_resolution)]
            if c is not None:
                for Z_i in xrange(Z_resolution):
                    Q0[Z_i] += sum([c[i] * covariates[i] for i in xrange(len(c))])
            Q1 = [math.exp(Q0[Z_i]) for Z_i in xrange(Z_resolution)]
            porig = [Q1[Z_i] / (1.0 + Q1[Z_i]) for Z_i in xrange(Z_resolution)]
            p = [max(1e-20,P_g + porig[Z_i] * (1.0 - P_g)) for Z_i in xrange(Z_resolution)]
            q = [max(1e-20,(1.0 - P_g) * (1.0 - porig[Z_i])) for Z_i in xrange(Z_resolution)]
            logp = [math.log(p[Z_i]) for Z_i in xrange(Z_resolution)]
            logq = [math.log(q[Z_i]) for Z_i in xrange(Z_resolution)]
            lP_X_Z = [logp[Z_i] * correct + logq[Z_i] * (n - correct) for Z_i in xrange(Z_resolution)]
            for Z_i in xrange(Z_resolution):
                lP_X_Z_sum[Z_i] += lP_X_Z[Z_i]
    lP_X_Z_mx = max(lP_X_Z_sum)
    w = [math.exp(lP_X_Z_sum[Z_i] - lP_X_Z_mx) for Z_i in xrange(Z_resolution)]
    sw = sum(w)
    w = [w[Z_i] / sw for Z_i in xrange(Z_resolution)]
    mu_Z = sum([w[Z_i] * Z_vals[Z_i] for Z_i in xrange(Z_resolution)])
    #if mu_Z < -5:
    #    return None
    window_width = window[1] - window[0]
    Zspace = window_width * 1.0 / Z_resolution
    if Zspace > 0.1:
        new_window = [mu_Z - 0.25 * window_width, mu_Z + 0.25 * window_width]
        return QMOM2_score(theta, ss, Z_resolution = Z_resolution, window = new_window)
    if (abs(mu_Z - window[0]) < 1.0) or (abs(mu_Z - window[1]) < 1.0):
        return QMOM2_score(theta, ss, Z_resolution = Z_resolution, window = [mu_Z - 3.0, mu_Z + 3.0])
    #if ((window is None) and (abs(mu_Z) > 1.5)) or ((window is not None) and ((mu_Z - window[0] < 1.0) or (window[1] - mu_Z < 1.0) or ((window[1] - window[0]) > 6.0))):
    #    return QMOM2_score(theta, ss, Z_resolution = 100, window = [mu_Z - 3.0, mu_Z + 3.0])
    sigmasqr_Z = sum([w[Z_i] * Z_vals[Z_i]**2 for Z_i in xrange(Z_resolution)]) - mu_Z**2
    stderr_Z = math.sqrt(sigmasqr_Z)
    return (mu_Z, stderr_Z)
