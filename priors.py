def normal_scaled_inverse_gamma_params_from_ss(n, sumx, sumx2):
    return normal_scaled_inverse_gamma_posterior_from_ss((0.0, 0.0, 0.0, 0.0), n, sumx, sumx2)

def normal_scaled_inverse_gamma_posterior_from_ss(params, n, sumx, sumx2):
    if n == 0:
        return params
    (mu_0, nu, alpha, beta) = params
    meanx = sumx * 1.0 / n
    posterior_nu = nu + n
    posterior_mu = (nu * mu_0 + sumx) * 1.0 / posterior_nu
    posterior_alpha = alpha + 0.5 * n
    posterior_beta = beta + 0.5 * (sumx2 - meanx*sumx) + n * nu * 1.0 / (n + nu) * 0.5 * (meanx - mu_0)**2
    return (posterior_mu, posterior_nu, posterior_alpha, posterior_beta)

def normal_scaled_inverse_gamma_posterior(params, data):
    n = len(data)
    sumx = sum(data)
    sumx2 = sum([x**2 for x in data])
    return normal_scale_inverse_gamma_posterior_from_ss(params, n, sumx, sumx2)

def normal_scaled_inverse_gamma_mle(params):
    (mu, nu, alpha, beta) = params
    mle_mean = mu
    mle_variance = beta / (alpha + 1.0)
    variance_of_mean = mle_variance / nu
    return (mle_mean, mle_variance, variance_of_mean)

