import logging
import math
import sigfit

def float_statistics(numbers):
    statistics = {}
    mean_stdev = mean_and_stdev(numbers)
    statistics['mean'] = mean_stdev[0]
    statistics['stdev'] = mean_stdev[1]
    sorted_numbers = list(numbers)
    sorted_numbers.sort()
    statistics['median'] = sorted_numbers[int(len(sorted_numbers)*0.5)]
    return statistics

def mean_and_stdev(numbers):
    sumX = 0.0
    sumX2 = 0.0
    n = 0
    for number in numbers:
        n += 1
        sumX += number
        sumX2 += number*number
    mean = sumX / float(n)
    variance = (n / (n - 1.0)) * ((sumX2 / float(n)) - mean*mean)
    return mean, math.sqrt(variance)

def estimate_level(positives, negatives, success_probability):
    # e^(a*x+b)/(1+e^(a*x+b))=p
    # e^(a*x+b)=p+p*e^(a*x+b)
    # (1-p) e^(a*x+b) = p
    # a*x+b = log(p/(1-p))
    # x = (log(p/(1-p))-b)/a
    X = positives + negatives
    Y = ([1] * len(positives)) + ([0] * len(negatives))
    a, b = sigfit.sigmoid_fit(X, Y)
    q = success_probability / (1.0 - success_probability)
    level = (math.log(q) - b) / a
    return level, a, b

def bayesian_estimate_level(positives, negatives, a_vals, guessing_p, b_prior = None):
    X = positives + negatives
    Y = ([1] * len(positives)) + ([0] * len(negatives))
    level, se = sigfit.bayesian_sigmoid(X, Y, a_vals, guessing_p, b_prior)
    return level, se

def matrix_inverse(matrix):
    matrix = [map(float, x) for x in matrix]
    diminsion = len(matrix)
    inverse = [[0.0] * diminsion for i in xrange(diminsion)]
    for i in xrange(diminsion):
        inverse[i][i] = 1.0
    for k in xrange(diminsion):
        for i in xrange(k, diminsion):
            val = matrix[i][k]
            for j in xrange(k, diminsion):
                matrix[i][j] /= val
            for j in xrange(diminsion):
                inverse[i][j] /= val
        for i in xrange(k+1, diminsion):
            for j in xrange(k, diminsion):
                matrix[i][j] -= matrix[k][j]
            for j in xrange(diminsion):
                inverse[i][j] -= inverse[k][j]
    for i in xrange(diminsion-2,-1,-1):
        for j in xrange(diminsion-1,i,-1):
            for k in xrange(diminsion):
                inverse[i][k] -= matrix[i][j] * inverse[j][k]
            for k in xrange(diminsion):
                matrix[i][k] -= matrix[i][j] * matrix[j][k]
    return inverse

def matrix_inverse_with_pivoting(matrix, return_logdet = False):
    matrix = [map(float, x) for x in matrix]
    n = len(matrix)
    logdet = 0.0
    sign = 1
    inverse = [[0.0] * n for i in xrange(n)]
    for i in xrange(n):
        inverse[i][i] = 1.0
    for iPass in xrange(n):
        imx = iPass
        for irow in xrange(iPass, n):
            if abs(matrix[irow][iPass]) > abs(matrix[imx][iPass]):
                imx = irow
        if imx != iPass:
            inverse[iPass], inverse[imx] = inverse[imx], inverse[iPass]
            matrix[iPass], matrix[imx] = matrix[imx], matrix[iPass]
        pivot = matrix[iPass][iPass]
        if pivot == 0:
            return None
        z = 1.0 / pivot
        if return_logdet:
            if z < 0:
                sign = -sign
                logdet -= math.log(-z)
            else:
                logdet -= math.log(z)
        matrix[iPass] = [x * z for x in matrix[iPass]]
        inverse[iPass] = [x * z for x in inverse[iPass]]
        for irow in xrange(n):
            if (irow == iPass):
                continue
            factor = matrix[irow][iPass]
            for icol in xrange(n):
                matrix[irow][icol] -= factor * matrix[iPass][icol]
                inverse[irow][icol] -= factor * inverse[iPass][icol]
    if return_logdet:
        return (inverse, logdet)
    return inverse

def multiple_regression(n, xtx, xty, yty):
    p = len(xty)
    if n < p:
        return (None, None)
    sxtx = [[0.0] * p for i in xrange(p)]
    for i in xrange(p):
        for j in xrange(p):
            sxtx[i][j] = xtx[i*p+j]
    ixtx = matrix_inverse_with_pivoting(sxtx)
    if not ixtx:
        return (None, None)
    beta = [0.0] * p
    if (n-p)<1:
        return (beta, None)
    for i in xrange(p):
      for j in xrange(p):
        beta[i] += ixtx[i][j] * xty[j]
    # sum [y_i-x_i*beta]^2 = sum [y_i^2 - 2*y_i*x_i*beta + x_i*beta'*beta*x_i'] =
    # yty - 2*xty*beta + beta'*xtx*beta
    bxb = 0.0
    for i in xrange(p):
      for j in xrange(p):
        bxb += beta[i] * beta[j] * xtx[i*p+j]
    xtyb = 0.0
    for i in xrange(p):
        xtyb += xty[i] * beta[i]
    SSE = yty - 2*xtyb + bxb
    MSE = SSE / (n-p)
    if MSE <= 0.0:
        return (beta, None)
    beta_se = [math.sqrt(max(0,MSE * ixtx[i][i])) for i in range(p)]
    return (beta, beta_se)

def summary_statistics_from_equations(dep, indep, w = None, eqinfo = None):
    if w is None:
        n = len(dep)
        w = [1.0] * n
    else:
        if eqinfo is None:
            n = len(dep)
        else:
            n = len([None for (var_type, var_i) in eqinfo if var_type == 'output'])
        #n = sum(w)
    p = len(dep[0])
    xtx = [0.0] * (p * p)
    for i in xrange(p):
        for j in xrange(p):
            for k in xrange(len(dep)):
                xtx[i * p + j] += dep[k][i] * dep[k][j] * w[k]
    xty = [0.0] * p
    for i in xrange(p):
        for k in xrange(len(dep)):
            xty[i] += dep[k][i] * indep[k] * w[k]
    yty = 0.0
    for k in xrange(len(dep)):
        yty += indep[k] * indep[k] * w[k]
    return (n,p,xtx,xty,yty)
